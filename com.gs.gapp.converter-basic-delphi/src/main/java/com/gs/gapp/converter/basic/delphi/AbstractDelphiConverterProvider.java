/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.basic.delphi;

import org.jenerateit.modelconverter.ModelConverterProviderI;

/**
 * @author hrr
 *
 */
public abstract class AbstractDelphiConverterProvider implements ModelConverterProviderI {
	

	/**
	 * 
	 */
	public AbstractDelphiConverterProvider() {
		super();
	}
}
