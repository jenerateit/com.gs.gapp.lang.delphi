/**
 *
 */
package com.gs.gapp.converter.basic.delphi;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalMessage;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElementCreator;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalNamingRulesEnforcer;

/**
 * This abstract class serves as the parent class for converters that create an instance of a type that inherits from CSharpTypeParameter.
 *
 * @author mmt
 *
 *
 */
public abstract class AbstractModelElementToDelphiConverter<S extends ModelElementI, T extends ObjectPascalModelElement> extends
		AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AbstractModelElementToDelphiConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}



	/**
	 * @param modelConverter
	 * @param previousResultingElementRequired
	 */
	public AbstractModelElementToDelphiConverter(
			AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}
	
	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public AbstractModelElementToDelphiConverter(AbstractConverter modelConverter,
			ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected BasicToDelphiConverter getModelConverter() {
		return (BasicToDelphiConverter) super.getModelConverter();
	}


	/**
	 * @return
	 */
	protected ObjectPascalModelElementCreator getModelElementCreator() {
		return getModelConverter().getModelElementCreator();
	}

	protected String normalizeIdentifier(ModelElement modelElement, String identifier) {
		String normalizedIdentifier = ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(identifier);
		if (normalizedIdentifier.length() == 0) {
			Message errorMessage = ObjectPascalMessage.ERROR_EMPTY_IDENTIFIER.getMessageBuilder()
				.modelElement(modelElement)
				.parameters(identifier)
				.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		return normalizedIdentifier;
	}
}