package com.gs.gapp.converter.basic.delphi;

import java.util.Set;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.objectpascal.Namespace;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 *
 * @param <S>
 * @param <T>
 */
public class BasicModuleToUnitConverter<S extends Module, T extends Unit>
		extends	AbstractModelElementToDelphiConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public BasicModuleToUnitConverter(
			AbstractConverter modelConverter) {
		super(modelConverter);
	}

	
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElementI, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S basicModule, T unit) {
		super.onConvert(basicModule, unit);
		
		// --- convert all incoming types to delphi types that are then becoming part of the unit
		for (ModelElement modelElement : basicModule.getElements()) {
			if (modelElement instanceof com.gs.gapp.metamodel.basic.typesystem.Type) {
				com.gs.gapp.metamodel.basic.typesystem.Type typeInModule = (com.gs.gapp.metamodel.basic.typesystem.Type) modelElement;
				if (isTypeConverted(typeInModule, unit)) {
					@SuppressWarnings("unused")
					Type type = this.convertWithOtherConverter(Type.class, typeInModule, unit);
				}
			}
		}
	}
	
	/**
	 * Override this method when you need more fine-grained control over the placement of
	 * Delphi types, e.g. when you would want to create a separate unit to put all exception
	 * types in it.
	 * 
	 * @param type
	 * @param unit
	 * @return true when the given type should be converted to a Delphi type that is going to become part of the unit
	 */
	protected boolean isTypeConverted(com.gs.gapp.metamodel.basic.typesystem.Type type, T unit) {
		return true;
	}
	
    /**
     * @param module
     * @return
     */
    protected boolean isModuleContainingElementsToJustifyConversion(Module module) {
    	Set<com.gs.gapp.metamodel.basic.typesystem.ComplexType> complexTypesOfModule =
                module.getElements(com.gs.gapp.metamodel.basic.typesystem.ComplexType.class);
        return !complexTypesOfModule.isEmpty();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement,
			ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		boolean result = false;
		if (generalResponsibility) {
			Module module = (Module) originalModelElement;
			
			if (!module.isGenerated()) {
			    // in this case we definitely are not generating something for the given module
			} else {
	            result = isModuleContainingElementsToJustifyConversion(module);
	            
	            if (result == false) {
	                // finally, we _have_ to generate a unit when the module contains primitive types that constrain another primtive type (CHQDG-153)
	                Set<PrimitiveType> primitiveTypesOfModule =
	                        module.getElements(PrimitiveType.class);
	                for (PrimitiveType primitiveType : primitiveTypesOfModule) {
	                    if (primitiveType.getConstraints() != null) {
	                        result = true;
	                        break;
	                    }
	                }
	            }
			}
		}
		
		return result;
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected T onCreateModelElement(S basicModule,
			ModelElementI previousResultingModelElement) {
		
//		Namespace namespace = this.convertWithOtherConverter(Namespace.class, basicModule.getNamespace());
		Namespace namespace = null;
		
    	@SuppressWarnings("unchecked")
		T unit = (T) new Unit(StringTools.firstUpperCase(basicModule.getName()) + "Types", namespace);
		return unit;
	}
}

	