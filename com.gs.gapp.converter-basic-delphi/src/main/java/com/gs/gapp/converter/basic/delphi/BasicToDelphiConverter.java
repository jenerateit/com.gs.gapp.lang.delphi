package com.gs.gapp.converter.basic.delphi;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.objectpascal.DelphiPrimitiveTypeEnum;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalConstant;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElementCache;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElementCreator;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.type.TypeIdentifier;
import com.gs.gapp.metamodel.product.Capability;
import com.gs.vd.metamodel.delphi.predef.system.AnsiString;
import com.gs.vd.metamodel.delphi.predef.system.Byte;
import com.gs.vd.metamodel.delphi.predef.system.Cardinal;
import com.gs.vd.metamodel.delphi.predef.system.Currency;
import com.gs.vd.metamodel.delphi.predef.system.Double;
import com.gs.vd.metamodel.delphi.predef.system.Int64;
import com.gs.vd.metamodel.delphi.predef.system.Integer;
import com.gs.vd.metamodel.delphi.predef.system.ShortInt;
import com.gs.vd.metamodel.delphi.predef.system.ShortString;
import com.gs.vd.metamodel.delphi.predef.system.Single;
import com.gs.vd.metamodel.delphi.predef.system.SmallInt;
import com.gs.vd.metamodel.delphi.predef.system.TDate;
import com.gs.vd.metamodel.delphi.predef.system.TDateTime;
import com.gs.vd.metamodel.delphi.predef.system.TTime;
import com.gs.vd.metamodel.delphi.predef.system.UInt64;
import com.gs.vd.metamodel.delphi.predef.system.WideString;
import com.gs.vd.metamodel.delphi.predef.system.Word;
import com.gs.vd.metamodel.delphi.predef.system.string;



/**
 * @author mmt
 *
 */
public class BasicToDelphiConverter extends AbstractAnalyticsConverter {
	
	private final ObjectPascalModelElementCreator modelElementCreator = new ObjectPascalModelElementCreator();
	
	private BasicToDelphiConverterOptions converterOptions;

	/**
	 *
	 */
	public BasicToDelphiConverter() {
		super(new ObjectPascalModelElementCache());
	}
	
	/**
	 * @param modelElementCache
	 */
	public BasicToDelphiConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		this.converterOptions = new BasicToDelphiConverterOptions(getOptions());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#getModelElementCache()
	 */
	@Override
	public ObjectPascalModelElementCache getModelElementCache() {
		return (ObjectPascalModelElementCache) super.getModelElementCache();
	}

	/**
	 * @return the javaModelElementCreator
	 */
	public ObjectPascalModelElementCreator getDelphiModelElementCreator() {
		return modelElementCreator;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters
		result.add(new BasicModuleToUnitConverter<>(this));
		result.add(new EnumerationToDelphiEnumerationConverter<>(this));
		result.add(new ExceptionTypeToDelphiClassConverter<>(this));
		result.add(new ComplexTypeToDelphiClassConverter<>(this));
		result.add(new NamespaceToDelphiNamespaceConverter<>(this));
		
		// --- slave element converters
		result.add(new FieldToDelphiPropertyConverter<>(this));
		result.add(new PrimitiveTypeToDelphiTypeConverter<>(this));

		return result;
	}

	/**
	 * Identifies the instance of a Delphi type that corresponds to the given primitive type.
	 *
	 * @return
	 */
	private Type getMappedTypeForDelphiSpecificPrimitiveType(PrimitiveType primitiveType) {
		Type result = null;
		
		DelphiPrimitiveTypeEnum delphiPrimitiveTypeEnum = DelphiPrimitiveTypeEnum.get(primitiveType.getName());
		if (delphiPrimitiveTypeEnum != null) {
			switch (delphiPrimitiveTypeEnum) {
			case ANSI_STRING:
				result = AnsiString.TYPE;
				break;
			case SHORT_STRING:
				result = ShortString.TYPE;
				break;
			case UNICODE_STRING:
				result = string.TYPE;
				break;
			case WIDE_STRING:
				result = WideString.TYPE;
				break;
			default:
				throw new RuntimeException("unhandled Delphi-specific primitive type found during conversion from basic model to Delphi model:" + primitiveType.toString());
			}
			
			result.setOriginatingElement(primitiveType);
		}
		
		return result;
	}
	
	/**
	 * Identifies the instance of a Delphi type that corresponds to the given primitive type.
	 *
	 * @return
	 */
	public Type getMappedType(PrimitiveType primitiveType) {

		Type result = null;

		if (primitiveType != null) {
			PrimitiveTypeEnum primitiveTypeEnumEntry = PrimitiveTypeEnum.fromString(primitiveType.getName());
			if (primitiveTypeEnumEntry == null) {
				// now lets check a Delphi-specific primitive type
				result = getMappedTypeForDelphiSpecificPrimitiveType(primitiveType);
			} else {
				switch (primitiveTypeEnumEntry) {
				case UINT1:
					result = com.gs.vd.metamodel.delphi.predef.system.Boolean.TYPE;
					break;
				case DATE:
					result = com.gs.vd.metamodel.delphi.predef.system.TDate.TYPE;
					break;
				case TIME:
					result = com.gs.vd.metamodel.delphi.predef.system.TTime.TYPE;
					break;
				case DATETIME:
					result = com.gs.vd.metamodel.delphi.predef.system.TDateTime.TYPE;
					break;

				case FLOAT16:
				case FLOAT32:
					result = com.gs.vd.metamodel.delphi.predef.system.Single.TYPE;
					break;

				case FLOAT64:
					result = com.gs.vd.metamodel.delphi.predef.system.Double.TYPE;
					break;

				case FLOAT128:
				case UINT128:
				case SINT128:
					result = com.gs.vd.metamodel.delphi.predef.system.Currency.TYPE;
					break;

				case SINT8:
					result = com.gs.vd.metamodel.delphi.predef.system.ShortInt.TYPE;
					break;

				case UINT8:
					result = com.gs.vd.metamodel.delphi.predef.system.Byte.TYPE;
					break;

				case SINT16:
					result = com.gs.vd.metamodel.delphi.predef.system.SmallInt.TYPE;
					break;

				case UINT16:
					result = com.gs.vd.metamodel.delphi.predef.system.Word.TYPE;
					break;

				case SINT32:
					result = com.gs.vd.metamodel.delphi.predef.system.Integer.TYPE;
					break;

				case UINT32:
					result = com.gs.vd.metamodel.delphi.predef.system.Cardinal.TYPE;
					break;

				case SINT64:
					result = com.gs.vd.metamodel.delphi.predef.system.Int64.TYPE;
					break;

				case UINT64:
					result = com.gs.vd.metamodel.delphi.predef.system.UInt64.TYPE;
					break;

				case CHARACTER:
					result = com.gs.vd.metamodel.delphi.predef.system.UCS4Char.TYPE;
					break;

				case STRING:
					result = com.gs.vd.metamodel.delphi.predef.system.string.TYPE;
					break;

			    default:
					throw new RuntimeException("unhandled primitive type found during conversion from basic model to Delphi model:" + primitiveType.toString());
				}
				
				result.setOriginatingElement(primitiveType);
			}
		}
		
		if (result == null) {
		    Set<TypeIdentifier> typeIdentifiers = primitiveType.getResultingModelElements(TypeIdentifier.class);
		    if (!typeIdentifiers.isEmpty()) {
		        result = typeIdentifiers.iterator().next();
		    }
		}

		return result;
	}

	/**
	 * @param namespace
	 * @return
	 */
	public com.gs.gapp.metamodel.objectpascal.Namespace getOrCreateDelphiNamespace(Namespace namespace) {
		com.gs.gapp.metamodel.objectpascal.Namespace resultingModelElement = null;

        String[] packageNameParts = namespace.getName().split("[.]");
        StringBuilder objectPascalNamespace = new StringBuilder();

        for (String packageNamePart : packageNameParts) {
        	com.gs.gapp.metamodel.objectpascal.Namespace objectPascalNamespaceFromCache =
        		(com.gs.gapp.metamodel.objectpascal.Namespace) getModelElementCache().findModelElement(packageNamePart,
			        				                  (objectPascalNamespace.length() == 0 ?
			        				                		  ObjectPascalConstant.DEFAULT_NAMESPACE.getName() : objectPascalNamespace.toString()), com.gs.gapp.metamodel.objectpascal.Namespace.class);
			if (objectPascalNamespaceFromCache == null) {
				if (resultingModelElement == null) {
					resultingModelElement = new com.gs.gapp.metamodel.objectpascal.Namespace(packageNamePart);
				} else {
	        	    resultingModelElement = new com.gs.gapp.metamodel.objectpascal.Namespace(packageNamePart, resultingModelElement);
				}
	        	addModelElement(resultingModelElement, (objectPascalNamespace.length() == 0 ? ObjectPascalConstant.DEFAULT_NAMESPACE.getName() : objectPascalNamespace.toString()) );
	        	resultingModelElement.setOriginatingElement(namespace);
        	} else {
        		resultingModelElement = objectPascalNamespaceFromCache;
        	}
        	if (objectPascalNamespace.length() > 0) {
        		objectPascalNamespace.append(".");
        	}
        	objectPascalNamespace.append(packageNamePart);
        }


		return resultingModelElement;
	}



	/**
	 * @return
	 */
	@Override
	public BasicToDelphiConverterOptions getConverterOptions() {
		return converterOptions;
	}

	public ObjectPascalModelElementCreator getModelElementCreator() {
		return modelElementCreator;
	}
	
	public Capability getCapabilityContext() {
		Set<Capability> capabilities = getModel().getOriginatingElement(Model.class).getElements(Capability.class);
		return capabilities != null && capabilities.size() > 0 ? capabilities.iterator().next() : null;
	}
	
    
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
		return super.onPerformModelConsolidation(normalizedElements);
		
		// ---  TODO add ForwardDeclarations (mmt 10-Jun-2020) 
	}



	/**
	 * @author mmt
	 *
	 */
	public static enum DelphiTypeInfo {
		BOOLEAN   (com.gs.vd.metamodel.delphi.predef.system.Boolean.TYPE, "False", "AsBoolean", "AsBoolean", "BoolToStr({0})", "StrToBoolDef({0}, {1})"),
		TDATE     (TDate.TYPE,     "0",  "AsDateTime", "AsDate",     "DateToStr({0})",     "StrToDateDef({0}, {1})"),
		TTIME     (TTime.TYPE,     "0",  "AsDateTime", "AsTime",     "TimeToStr({0})",     "StrToTimeDef({0}, {1})"),
		TDATETIME (TDateTime.TYPE, "0",  "AsDateTime", "AsDateTime", "DateTimeToStr({0})", "StrToDateTimeDef({0}, {1})"),
		SINGLE    (Single.TYPE,    "0",  "AsSingle", "AsSingle",   "IntToStr({0})",      "StrToIntDef({0}, {1})"),
		DOUBLE    (Double.TYPE,    "-1", "AsFloat", "AsFloat",    "FloatToStr({0})",    "StrToFloatDef({0}, {1})"),  // AsDouble does not exist
		CURRENCY  (Currency.TYPE,  "0",  "AsCurrency", "AsCurrency", "CurrToStr({0})",     "StrToCurrDef({0}, {1})"),
		SHORTINT  (ShortInt.TYPE,  "0",  "AsInteger", "AsShortInt", "IntToStr({0})",      "StrToIntDef({0}, {1})"),
		BYTE      (Byte.TYPE,      "0",  "AsBytes", "AsByte",    "IntToStr({0})",      "StrToIntDef({0}, {1})"),
		SMALLINT  (SmallInt.TYPE,  "0",  "AsInteger", "AsSmallInt", "IntToStr({0})",      "StrToIntDef({0}, {1})"),
		WORD      (Word.TYPE,      "''", "AsInteger", "AsWord",    "WordToStr({0})",     "StrToWord({0})"),
		INTEGER   (Integer.TYPE,   "-1", "AsInteger", "AsInteger",  "IntToStr({0})",      "StrToIntDef({0}, {1})"),
		CARDINAL  (Cardinal.TYPE,  "0",  "AsInteger", "AsInteger", "IntToStr({0})",      "StrToIntDef({0}, {1})"),
		INT64     (Int64.TYPE,     "-1", "AsLargeInt", "AsLargeInt",  "IntToStr({0})",      "StrToInt64Def({0}, {1})"),
		UINT64    (UInt64.TYPE,    "0",  "AsLargeInt", "AsLargeInt", "IntToStr({0})",      "StrToInt64Def({0}, {1})"),
		UCS4Char  (com.gs.vd.metamodel.delphi.predef.system.UCS4Char.TYPE, "''", "AsString", "AsString", "{0}", "{0}"),
		STRING    (string.TYPE,    "''", "AsString", "AsString",   "{0}", "{0}"),
		ANSI_STRING  (AnsiString.TYPE,  "''", "AsString", "AsString",   "{0}", "{0}"),
		SHORT_STRING (ShortString.TYPE, "''", "AsString", "AsString",   "{0}", "{0}"),
		WIDE_STRING  (WideString.TYPE,  "''", "AsString", "AsString",   "{0}", "{0}"),
		;
		
		private final static Map<Type, DelphiTypeInfo> map = new HashMap<>();
		
		static {
			for (DelphiTypeInfo delphiTypeInfoEnum : DelphiTypeInfo.values()) {
				map.put(delphiTypeInfoEnum.getDelphiType(), delphiTypeInfoEnum);
			}
		}
		
		public static DelphiTypeInfo get(Type type) {
			return map.get(type);
		}
		
		public static String getDefaultValue(Type type) {
			DelphiTypeInfo delphiTypeInfo = get(type);
			return delphiTypeInfo != null ? delphiTypeInfo.getDefaultValue() : "0";
		}
		
		public static String getDbParameterValueForTField(Type type) {
			DelphiTypeInfo delphiTypeInfo = get(type);
			return delphiTypeInfo != null ? delphiTypeInfo.getDbParameterValueForTField() : "AsUnknown";
		}
		
		public static String getDbParameterValueForTParam(Type type) {
			DelphiTypeInfo delphiTypeInfo = get(type);
			return delphiTypeInfo != null ? delphiTypeInfo.getDbParameterValueForTParam() : "AsUnknown";
		}
		
		public static String getExpressionToConvertToString(Type type, String fieldName) {
			DelphiTypeInfo delphiTypeInfo = get(type);
			if (delphiTypeInfo != null) {
				return MessageFormat.format(delphiTypeInfo.getExpressionToConvertToString(), fieldName);
			} else {
				return fieldName;
			}
		}
		
		public static String getExpressionToConvertToType(Type type, String fieldName) {
			DelphiTypeInfo delphiTypeInfo = get(type);
			if (delphiTypeInfo != null) {
				return MessageFormat.format(delphiTypeInfo.getExpressionToConvertToType(), fieldName, delphiTypeInfo.getDefaultValue());
			} else {
				return fieldName;
			}
		}
		
		private final Type delphiType;
		private final String defaultValue;
		private final String dbParameterValueForTField;
		private final String dbParameterValueForTParam;
		private final String expressionToConvertToString;
		private final String expressionToConvertToType;
		
		private DelphiTypeInfo(Type delphiType, String defaultValue, String dbParameterValueForTField, String dbParameterValueForTParam,
				String expressionToConvertToString, String expressionToConvertToType) {
			this.delphiType = delphiType;
			this.defaultValue = defaultValue;
			this.dbParameterValueForTField = dbParameterValueForTField;
			this.dbParameterValueForTParam = dbParameterValueForTParam;
			this.expressionToConvertToString = expressionToConvertToString;
			this.expressionToConvertToType = expressionToConvertToType;
		}

		public Type getDelphiType() {
			return delphiType;
		}

		public String getDefaultValue() {
			return defaultValue;
		}

		public String getDbParameterValueForTField() {
			return dbParameterValueForTField;
		}

		public String getExpressionToConvertToString() {
			return expressionToConvertToString;
		}

		public String getExpressionToConvertToType() {
			return expressionToConvertToType;
		}

		public String getDbParameterValueForTParam() {
			return dbParameterValueForTParam;
		}
	}
}
