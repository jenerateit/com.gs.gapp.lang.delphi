package com.gs.gapp.converter.basic.delphi;

import java.io.Serializable;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.analytics.AbstractAnalyticsConverterOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;

public class BasicToDelphiConverterOptions extends AbstractAnalyticsConverterOptions {
	
	public static final OptionDefinitionString OPTION_DEF_NAMING_CLASS_PREFIX =
			new OptionDefinitionString("delphi.naming.class-prefix",
					                   "define a prefix that is going to be prepended to every name of a class",
					                    false,
					                    false);
	
	public static final OptionDefinitionString OPTION_DEF_NAMING_ENUMERATION_PREFIX =
			new OptionDefinitionString("delphi.naming.enumeration-prefix",
					                   "define a prefix that is going to be prepended to every name of an enumeration",
					                    false,
					                    false);
	
	public static final OptionDefinitionString OPTION_DEF_NAMING_EXCEPTION_PREFIX =
			new OptionDefinitionString("delphi.naming.exception-prefix",
					                   "define a prefix that is going to be prepended to every name of an exception",
					                    false,
					                    false);
	
	public BasicToDelphiConverterOptions(ModelConverterOptions options) {
		super(options);
	}
	
	/**
	 * @return
	 */
	public String getClassPrefix() {
		Serializable option = getOptions().get(OPTION_DEF_NAMING_CLASS_PREFIX.getKey());
		if (option != null) {
			return option.toString().trim();
		}
		return "";
	}
	
	/**
	 * @return
	 */
	public String getEnumerationPrefix() {
		Serializable option = getOptions().get(OPTION_DEF_NAMING_ENUMERATION_PREFIX.getKey());
		if (option != null) {
			return option.toString().trim();
		}
		return "";
	}
	
	/**
	 * @return
	 */
	public String getExceptionPrefix() {
		Serializable option = getOptions().get(OPTION_DEF_NAMING_EXCEPTION_PREFIX.getKey());
		if (option != null) {
			return option.toString().trim();
		}
		return "";
	}
}
