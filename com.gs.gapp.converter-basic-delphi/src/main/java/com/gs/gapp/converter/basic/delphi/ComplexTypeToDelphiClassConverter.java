package com.gs.gapp.converter.basic.delphi;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalMessage;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class ComplexTypeToDelphiClassConverter<S extends ComplexType, T extends Clazz>
		extends
		AbstractModelElementToDelphiConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ComplexTypeToDelphiClassConverter(
			AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	public ComplexTypeToDelphiClassConverter(AbstractConverter modelConverter,
			ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElementI, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S complexType, T clazz) {
		super.onConvert(complexType, clazz);
		
		// --- inheritance
		if (complexType.getParent() != null) {
			// We first have to convert to the unit since otherwise the converWithOther...() call for the class might return null.
			Unit unit = convertWithOtherConverter(Unit.class, complexType.getParent().getModule());
			if (unit != null) {
				Clazz parentClazz = convertWithOtherConverter(Clazz.class, complexType.getParent(), unit);
				clazz.setParent(parentClazz);
			} else {
				addWarning(ObjectPascalMessage.WARNING_CANNOT_SET_PARENT_TYPE_FOR_CLASS_DUE_TO_MISSING_UNIT.getMessageBuilder()
					.modelElement(clazz)
					.parameters(clazz.getName(), complexType.getParent().getName(), complexType.getParent().getModule().getName())
					.build().getMessage());
			}
		}
		
		// --- fields
		for (Field field : complexType.getFields()) {
			@SuppressWarnings("unused")
			Property property = convertWithOtherConverter(Property.class, field, clazz);
		}
	}

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected T onCreateModelElement(S complexType, ModelElementI previousResultingModelElement) {
		Unit existingUnit = null;
		
        if (previousResultingModelElement != null && previousResultingModelElement instanceof Unit) {
        	existingUnit = (Unit) previousResultingModelElement;
        	
        	
        	@SuppressWarnings("unchecked")
			T clazz = (T) new Clazz(getClassName(complexType), existingUnit);
        	
        	return clazz;
        }
        
        throw new ModelConverterException("Tried to convert a complex type to a delphi clazz, but there was no previous resulting element provided.", complexType);
	}
	
	/**
	 * @param complexType
	 * @return
	 */
	protected String getClassName(S complexType) {
		String className = null;
    	String classPrefix = getConverterOptions().getClassPrefix();
    	if (classPrefix == null) {
    		className = complexType.getName();  // take the original name, intentionally don't change the case
    	} else {
    		className = classPrefix + StringTools.firstUpperCase(complexType.getName());
    	}
    	return className;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected BasicToDelphiConverter getModelConverter() {
		return super.getModelConverter();
	}
	
	/**
	 * @return
	 */
	@Override
	protected BasicToDelphiConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}
}
