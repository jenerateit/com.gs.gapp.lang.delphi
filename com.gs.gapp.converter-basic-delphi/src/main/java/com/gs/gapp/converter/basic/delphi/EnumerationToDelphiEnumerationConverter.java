package com.gs.gapp.converter.basic.delphi;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.EnumerationEntry;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalMessage;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalNamingRulesEnforcer;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalTypeValue;

public class EnumerationToDelphiEnumerationConverter<S extends Enumeration, T extends com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration>
		extends
		AbstractModelElementToDelphiConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public EnumerationToDelphiEnumerationConverter(
			AbstractConverter modelConverter) {
		super(modelConverter, true, true, false);
	}

	
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElementI, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S enumeration, T delphiEnumeration) {
		super.onConvert(enumeration, delphiEnumeration);
	}

	
	/**
	 * @param enumeration
	 * @return
	 */
	protected Set<OrdinalTypeValue> convertEnumerationLiteralsToOrdinalTypeValues(S enumeration) {
		Set<OrdinalTypeValue> result = new LinkedHashSet<>();

		int counter = 0;
		for (EnumerationEntry enumEntry : enumeration.getEnumerationEntries()) {
			String identifier = enumEntry.getName();
			String normalizedIdentifier = normalizeIdentifier(enumEntry, identifier);
			
			if (normalizedIdentifier == null || normalizedIdentifier.isEmpty()) {
				String prefix = enumeration.getName();
				Message warningMessage = ObjectPascalMessage.WARNING_INVALID_ENUMERATION_ENTRY_NAME_IS_GOING_TO_BE_PREFIXED
				    .getMessageBuilder()
				    .modelElement(enumEntry)
				    .parameters(enumeration.getQualifiedName(),
				    		    identifier,
				    		    prefix)
				    .build();
				addWarning(warningMessage.getMessage());
				normalizedIdentifier = prefix + "_" + identifier;
				normalizedIdentifier = ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(normalizedIdentifier);
				if (normalizedIdentifier.equals(prefix + "_")) {
					normalizedIdentifier = normalizedIdentifier + counter;
				}
			}
			
			OrdinalTypeValue value = new OrdinalTypeValue(normalizedIdentifier);
			value.setBody(enumEntry.getBody());
			result.add(value);
			value.setOriginatingElement(enumEntry);
			counter++;
		}

		return result;
	}


	/**
	 * @param enumeration
	 * @return
	 */
	protected String getEnumerationName(S enumeration) {
		String enumerationName = null;
    	String enumerationPrefix = getConverterOptions().getEnumerationPrefix();
    	if (enumerationPrefix == null) {
    		enumerationName = enumeration.getName();  // take the original name, intentionally don't change the case
    	} else {
    		enumerationName = enumerationPrefix + StringTools.firstUpperCase(enumeration.getName());
    	}
    	return enumerationName;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected T onCreateModelElement(S enumeration, ModelElementI previousResultingModelElement) {
		Unit existingUnit = null;
		
        if (previousResultingModelElement != null && previousResultingModelElement instanceof Unit) {
        	existingUnit = (Unit) previousResultingModelElement;
        	
        	@SuppressWarnings("unchecked")
			T delphiEnumeration =
        			(T) new com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration(getEnumerationName(enumeration),
					                                                       existingUnit,
					                                                       convertEnumerationLiteralsToOrdinalTypeValues(enumeration).toArray(new OrdinalTypeValue[0]));
        	
        	return delphiEnumeration;
        }
        
        throw new ModelConverterException("Tried to convert an enumeration to a dephi enumeration, but there was no previous resulting element provided (unit required).", enumeration);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected BasicToDelphiConverter getModelConverter() {
		return super.getModelConverter();
	}
	
	/**
	 * @return
	 */
	@Override
	protected BasicToDelphiConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}
}
