/**
 * 
 */
package com.gs.gapp.converter.basic.delphi;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;


/**
 * @author mmt
 *
 */
public class ExceptionTypeToDelphiClassConverter<S extends ExceptionType, T extends Clazz> extends
		ComplexTypeToDelphiClassConverter<S, T> {

	public ExceptionTypeToDelphiClassConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	public ExceptionTypeToDelphiClassConverter(AbstractConverter modelConverter,
			ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	@Override
	protected void onConvert(S exceptionType, T clazz) {
		super.onConvert(exceptionType, clazz);
		
		if (clazz.getParent() == null) {
			// --- if there is no explicitely modeled parent available, we have to set the parent to the Exception type
			// --- see: http://docwiki.embarcadero.com/RADStudio/XE7/de/Exceptions
			clazz.setParent(com.gs.vd.metamodel.delphi.predef.system.sysutils.Exception.TYPE);
		}
	}

	/**
	 * @param exceptionType
	 * @return
	 */
	protected String getExceptionName(ExceptionType exceptionType) {
		String exceptionName = null;
    	String exceptionPrefix = getConverterOptions().getExceptionPrefix();
    	if (exceptionPrefix == null) {
    		exceptionName = exceptionType.getName();  // take the original name, intentionally don't change the case
    	} else {
    		exceptionName = exceptionPrefix + StringTools.firstUpperCase(exceptionType.getName());
    	}
    	return exceptionName;
	}


	@Override
	protected T onCreateModelElement(S exceptionType, ModelElementI previousResultingModelElement) {
        Unit existingUnit = null;
		
        if (previousResultingModelElement != null && previousResultingModelElement instanceof Unit) {
        	existingUnit = (Unit) previousResultingModelElement;
        	
        	@SuppressWarnings("unchecked")
			T clazz =
        			(T) new Clazz(getExceptionName(exceptionType), existingUnit);
        	
        	return clazz;
        }
        
        throw new ModelConverterException("Tried to convert a complex type to a Delphi class, but there was no previous resulting element provided (a unit is required).", exceptionType);
	}
}
