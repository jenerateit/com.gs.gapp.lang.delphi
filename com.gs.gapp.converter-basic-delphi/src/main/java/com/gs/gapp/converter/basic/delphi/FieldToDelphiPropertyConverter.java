/**
 *
 */
package com.gs.gapp.converter.basic.delphi;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.VisibilityDirective;
import com.gs.gapp.metamodel.objectpascal.member.Property;

/**
 * @author mmt
 *
 */
public class FieldToDelphiPropertyConverter<S extends Field, T extends Property> extends
		AbstractModelElementToDelphiConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public FieldToDelphiPropertyConverter(AbstractConverter modelConverter) {
		super(modelConverter, true, true, true);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S field, T property) {

		super.onConvert(field, property);
		
		// --- create setter and getter procedures for the property
		property.createFieldForRead();
		
		if (field.getReadOnly() == null || field.getReadOnly() == false) {
			property.createFieldForWrite();
		}
	}
	
	/**
	 * @param field
	 * @param type
	 * @return
	 */
	protected Type getCollectionType(S field, Type type) {
		Type result = null;
		if (field.getCollectionType() != null) {
			switch (field.getCollectionType()) {
			case LIST:
				// TODO should we use TObjecTList instead? or maybe both, depending on additional model information?
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TArray.TYPE, type);
				break;
			case SET:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TArray.TYPE, type);
				break;
			case SORTED_SET:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TArray.TYPE, type);
				break;
			case ARRAY:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TArray.TYPE, type);
				break;
			case KEYVALUE:
				Type keyType = this.convertWithOtherConverter(Type.class, field.getKeyType());
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TObjectDictionary.TYPE, keyType);
				break;
			default:
				throw new ModelConverterException("cannot convert basic field since it has an unknown collection type assigned", field);
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S field, ModelElementI previousResultingElement) {

		if (previousResultingElement instanceof Type) {
			Type owner = (Type) previousResultingElement;
			Type type = null;
			if (field.getType() instanceof PrimitiveType) {
				type = convertWithOtherConverter(Type.class, field.getType());
			} else {
				Unit unit = convertWithOtherConverter(Unit.class, field.getType().getModule());
				if (unit == null) unit = convertWithOtherConverter(Unit.class, field.getType());  // in case there is a converter that creates a unit for a given type
				if (unit == null) {
					throw new ModelConverterException("could not find created Unit instance for given module '" + field.getType().getModule() + "' (needed for type conversion)");
				}
				type = this.convertWithOtherConverter(Type.class, field.getType(), unit);
			}

			if (field.getCollectionType() != null) {
				type = getCollectionType(field, type);
			}

			@SuppressWarnings("unchecked")
			T result = (T) new Property(field.getName(), type, owner);
			result.setVisibility(VisibilityDirective.PUBLIC);
			// TODO result.setSpecifiers(specifiers);
			return result;
        }

		throw new ModelConverterException("original model element cannot be converted since the previous resulting model element is null or not of a delphi type", field);
	}
}
