/**
 *
 */
package com.gs.gapp.converter.basic.delphi;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.converter.AbstractConverter;


/**
 * @author mmt
 *
 */
public class NamespaceToDelphiNamespaceConverter<S extends Namespace, T extends com.gs.gapp.metamodel.objectpascal.Namespace> extends
    AbstractModelElementToDelphiConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public NamespaceToDelphiNamespaceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S namespace, T delphiNamespace) {
        super.onConvert(namespace, delphiNamespace);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S namespace, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) getModelConverter().getOrCreateDelphiNamespace(namespace);
		return result;
	}
}
