/**
 *
 */
package com.gs.gapp.converter.basic.delphi;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.TypeIdentifier;

/**
 * @author mmt
 *
 */
public class PrimitiveTypeToDelphiTypeConverter<S extends PrimitiveType, T extends Type> extends
		AbstractModelElementToDelphiConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public PrimitiveTypeToDelphiTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S primitiveType, T type) {
		super.onConvert(primitiveType, type);
	}
	
	

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generallyResponsible = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		
		if (generallyResponsible) {
			PrimitiveType primitiveType = (PrimitiveType) originalModelElement;
			Type mappedType = getModelConverter().getMappedType(primitiveType);
			return mappedType != null || primitiveType.getConstraints() != null;
		}
		
		return generallyResponsible;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
    @Override
	protected T onCreateModelElement(S primitiveType, ModelElementI previouslyResultingElement) {

		T result = (T) getModelConverter().getMappedType(primitiveType);
		
		if (result == null) {
		    // CHQDG-153
		    if (previouslyResultingElement != null && previouslyResultingElement instanceof Unit) {
	            Unit existingUnit = (Unit) previouslyResultingElement;
	            // in this case we have to create a type alias
	            Type delphiType = null;
	            PrimitiveType constrainedType = primitiveType.getConstraints().getConstrainedType();
	            Unit unit = null;
	            if (constrainedType.getModule() != null) {
	                unit = convertWithOtherConverter(Unit.class, constrainedType.getModule());
	            }
	            if (unit != null) {
                    delphiType = convertWithOtherConverter(Type.class, constrainedType, unit);
                } else {
                    delphiType = getModelConverter().getMappedType(constrainedType);
                }
	            
	            result = (T) new TypeIdentifier(primitiveType.getName(), delphiType, existingUnit);
	        } else {
	            throw new ModelConverterException("Tried to convert a primitive type to a delphi type identifier, but there was no previous resulting element provided.", primitiveType);
	        }
		}

		return result;
	}
}
