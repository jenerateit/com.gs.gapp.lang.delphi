package com.gs.gapp.converter.function.delphi;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.basic.delphi.ExceptionTypeToDelphiClassConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.AbstractBusinessException;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.BusinessExceptionClass;

/**
 * @author mmt
 *
 * @param <S>
 * @param <T>
 */
public class AbstractBusinessExceptionToBusinessExceptionClassConverter<S extends AbstractBusinessException, T extends BusinessExceptionClass> extends ExceptionTypeToDelphiClassConverter<S, T> {

	public AbstractBusinessExceptionToBusinessExceptionClassConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected void onConvert(S abstractBusinessException, T javaExceptionType) {
		super.onConvert(abstractBusinessException, javaExceptionType);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	public T onCreateModelElement(S abstractBusinessException, ModelElementI previouslyResultingElement) {
        Unit existingUnit = null;
		
        if (previouslyResultingElement != null && previouslyResultingElement instanceof Unit) {
        	existingUnit = (Unit) previouslyResultingElement;
        	@SuppressWarnings("unchecked")
			T clazz =
        			(T) new BusinessExceptionClass(getExceptionName(abstractBusinessException), existingUnit);
        	
        	return clazz;
        }
		
        throw new ModelConverterException("Tried to convert an abstract business exception to a Delphi business exception class, but there was no previous resulting element provided (a unit is required).", abstractBusinessException);
	}
}
