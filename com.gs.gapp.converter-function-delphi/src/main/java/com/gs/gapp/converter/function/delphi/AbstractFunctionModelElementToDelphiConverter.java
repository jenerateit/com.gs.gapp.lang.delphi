/**
 *
 */
package com.gs.gapp.converter.function.delphi;

import com.gs.gapp.converter.basic.delphi.AbstractModelElementToDelphiConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * This abstract class serves as the parent class for converters that create an instance of a type that inherits from CSharpTypeParameter.
 *
 * @author mmt
 *
 *
 */
public abstract class AbstractFunctionModelElementToDelphiConverter<S extends ModelElementI, T extends ObjectPascalModelElement> extends
		AbstractModelElementToDelphiConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AbstractFunctionModelElementToDelphiConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}



	/**
	 * @param modelConverter
	 * @param previousResultingElementRequired
	 */
	public AbstractFunctionModelElementToDelphiConverter(
			AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}
	
	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public AbstractFunctionModelElementToDelphiConverter(AbstractConverter modelConverter,
			ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected FunctionToDelphiConverter getModelConverter() {
		return (FunctionToDelphiConverter) super.getModelConverter();
	}
	
	/**
	 * @param functionModule
	 * @return
	 */
	protected Module getOrCreateModuleForOnTheFlyTypes(FunctionModule functionModule) {
		Module basicModule = convertWithOtherConverter(Module.class, functionModule);
		convertWithOtherConverter(Unit.class, basicModule);
		return basicModule;
	}
}