package com.gs.gapp.converter.function.delphi;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.basic.delphi.ExceptionTypeToDelphiClassConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.AbstractTechnicalException;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.TechnicalExceptionClass;

/**
 * TODO it could make senso to not create a business exception class for HTTP status codes where JAX-RS has built-in support and that support is always sufficient. (mmt 2017-Jun-22)
 * 
 * @author mmt
 *
 * @param <S>
 * @param <T>
 */
public class AbstractTechnicalExceptionToTechnicalExceptionClassConverter<S extends AbstractTechnicalException, T extends TechnicalExceptionClass> extends ExceptionTypeToDelphiClassConverter<S, T> {

	public AbstractTechnicalExceptionToTechnicalExceptionClassConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected void onConvert(S abstractTechnicalException, T javaExceptionType) {
		super.onConvert(abstractTechnicalException, javaExceptionType);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	public T onCreateModelElement(S abstractTechnicalException, ModelElementI previouslyResultingElement) {
        Unit existingUnit = null;
		
        if (previouslyResultingElement != null && previouslyResultingElement instanceof Unit) {
        	existingUnit = (Unit) previouslyResultingElement;
        	
        	@SuppressWarnings("unchecked")
			T clazz =
        			(T) new TechnicalExceptionClass(getExceptionName(abstractTechnicalException), existingUnit);
        	
        	return clazz;
        }
		
        throw new ModelConverterException("Tried to convert an abstract technical exception to a Delphi technical exception class, but there was no previous resulting element provided (a unit is required).", abstractTechnicalException);
	}
}
