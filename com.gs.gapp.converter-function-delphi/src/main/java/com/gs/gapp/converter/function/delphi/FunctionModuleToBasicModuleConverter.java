package com.gs.gapp.converter.function.delphi;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;

/**
 * This converter creates an additional basic module to hold complex types that are created
 * on the fly, while creating other model elements.
 * One example for this: the complex type that gets created when a function has more than one out parameters.
 * 
 * @author mmt
 *
 * @param <S>
 * @param <T>
 */
public class FunctionModuleToBasicModuleConverter <S extends FunctionModule, T extends Module> extends
    AbstractM2MModelElementConverter<S, T> {

	public FunctionModuleToBasicModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S functionModule, T basicModule) {
		super.onConvert(functionModule, basicModule);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S functionModule, ModelElementI previousResultingModelElement) {
		T result = (T) new Module(StringTools.firstUpperCase(functionModule.getName()) + "Derived");
		Namespace namespace = new Namespace(functionModule.getNamespace().getName() + ".types");
		result.setNamespace(namespace);
		return result;
	}
}
