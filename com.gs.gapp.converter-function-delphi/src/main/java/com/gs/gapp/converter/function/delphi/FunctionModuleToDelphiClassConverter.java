package com.gs.gapp.converter.function.delphi;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.BusinessException;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.product.Organization;

public class FunctionModuleToDelphiClassConverter <S extends FunctionModule, T extends Clazz> extends
   AbstractFunctionModelElementToDelphiConverter<S, T> {

	public FunctionModuleToDelphiClassConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S functionModule, T clazz) {
		super.onConvert(functionModule, clazz);

		Map<String, Clazz> businessExceptions = new LinkedHashMap<>();
		
		// --- functions to Java methods
		for (Function function : functionModule.getFunctions()) {
			@SuppressWarnings("unused")
			Method method = this.convertWithOtherConverter(Method.class, function, clazz);
            
            for (BusinessException businessException : function.getBusinessExceptions()) {
            	Clazz exception = null;
            			
            	if (businessExceptions.containsKey(businessException.getName().toLowerCase())) {
            		exception = businessExceptions.get(businessException.getName().toLowerCase());
            	}
            	
            	if (exception == null) {
                    exception = this.convertWithOtherConverter(Clazz.class, businessException);
                    if (exception != null) {
                		businessExceptions.put(businessException.getName().toLowerCase(), exception);
                	}
            	}
    		}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S functionModule, ModelElementI previousResultingModelElement) {
		Unit unit = null;
		if (previousResultingModelElement instanceof Unit) {
			unit = (Unit) previousResultingModelElement;
		} else {
			throw new ModelConverterException("no unit object given to be used as the owner of the class that should be created here", previousResultingModelElement);
		}
		
		T result = (T) new Clazz(StringTools.firstUpperCase(functionModule.getName()), unit);
		return result;
	}

	public Organization getOrganizationContext() {
		Set<Organization> organizations = getModel().getOriginatingElement(Model.class).getElements(Organization.class);
		return organizations != null && organizations.size() > 0 ? organizations.iterator().next() : null;
	}
}
