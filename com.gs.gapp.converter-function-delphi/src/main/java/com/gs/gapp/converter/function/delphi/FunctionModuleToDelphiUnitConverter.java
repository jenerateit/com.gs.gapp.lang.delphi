package com.gs.gapp.converter.function.delphi;

import java.util.Set;

import com.gs.gapp.converter.basic.delphi.BasicModuleToUnitConverter;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.objectpascal.Namespace;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.product.Organization;

public class FunctionModuleToDelphiUnitConverter <S extends FunctionModule, T extends Unit> extends
    BasicModuleToUnitConverter<S, T> {

	public FunctionModuleToDelphiUnitConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S functionModule, T unit) {
		super.onConvert(functionModule, unit);
		
		// --- create the class that is going to be owned by the given unit
		convertWithOtherConverter(Clazz.class, functionModule, unit);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S functionModule, ModelElementI previousResultingModelElement) {
		Namespace namespace = null;
		
    	@SuppressWarnings("unchecked")
		T unit = (T) new Unit(functionModule.getName() + "Function", namespace);
		return unit;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.basic.delphi.BasicModuleToUnitConverter#isModuleContainingElementsToJustifyConversion(com.gs.gapp.metamodel.basic.Module)
	 */
	@Override
	protected boolean isModuleContainingElementsToJustifyConversion(Module module) {
		boolean result = super.isModuleContainingElementsToJustifyConversion(module);
		if (!result) {
			Set<com.gs.gapp.metamodel.function.Function> functionsOfModule =
	                module.getElements(com.gs.gapp.metamodel.function.Function.class);
	        result = !functionsOfModule.isEmpty();
		}
		
		return result;
	}

	public Organization getOrganizationContext() {
		Set<Organization> organizations = getModel().getOriginatingElement(Model.class).getElements(Organization.class);
		return organizations != null && organizations.size() > 0 ? organizations.iterator().next() : null;
	}

}
