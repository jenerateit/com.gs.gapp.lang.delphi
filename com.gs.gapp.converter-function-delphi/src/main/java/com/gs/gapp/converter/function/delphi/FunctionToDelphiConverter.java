package com.gs.gapp.converter.function.delphi;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.basic.delphi.BasicToDelphiConverter;
import com.gs.gapp.converter.basic.delphi.BasicToDelphiConverterOptions;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;
import com.gs.gapp.metamodel.basic.typesystem.ValidatorDuplicateFields;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.persistence.ValidatorDuplicateEntityFields;

/**
 * @author mmt
 *
 */
public class FunctionToDelphiConverter extends BasicToDelphiConverter {

	private FunctionToDelphiConverterOptions converterOptions;
	
	/**
	 *
	 */
	public FunctionToDelphiConverter() {
		super();
	}
	
	public FunctionToDelphiConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.persistence.jpa.PersistenceToJPAConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		this.converterOptions = new FunctionToDelphiConverterOptions(getOptions());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters
		result.add( new FunctionModuleToDelphiUnitConverter<>(this) );
		
		// --- slave element converters
		result.add( new FunctionModuleToBasicModuleConverter<>(this) );
		result.add( new FunctionModuleToDelphiClassConverter<>(this) );
		
		result.add( new AbstractBusinessExceptionToBusinessExceptionClassConverter<>(this) );
		result.add( new BusinessExceptionToDelphiClassConverter<>(this) );
		result.add( new BusinessExceptionToBusinessExceptionClassConverter<>(this) );
		
		result.add( new AbstractTechnicalExceptionToTechnicalExceptionClassConverter<>(this) );
		result.add( new TechnicalExceptionToDelphiClassConverter<>(this) );
		result.add( new TechnicalExceptionToTechnicalExceptionClassConverter<>(this) );
		
		result.add( new FunctionToDelphiMethodConverter<>(this) );
		result.add( new NamespaceToDelphiNamespaceConverter<>(this) );
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.persistence.jpa.PersistenceToJPAConverter#getConverterOptions()
	 */
	@Override
	public FunctionToDelphiConverterOptions getConverterOptions() {
		return converterOptions;
	}
	
	/**
     * @param modelElements
     */
    @Override
    protected void onPerformValidationBeforeConversion(Set<Object> modelElements) {
        super.onPerformValidationBeforeConversion(modelElements);

        addMessages( new ValidatorDuplicateFields().validate(modelElements) );
        addMessages( new ValidatorDuplicateEntityFields().validate(modelElements) );
    }
	
	/**
	 * @author mmt
	 *
	 */
	public static class FunctionToDelphiConverterOptions extends BasicToDelphiConverterOptions {
		
		public static final String OPTION_MERGE_IN_PARAMETERS = "merge.in.parameters";
		
		public FunctionToDelphiConverterOptions(ModelConverterOptions options) {
			super(options);
		}
		
		/**
		 * @return true if the method's in parameters should be merged to a single parameter with a new type
		 */
		public boolean isMergeInParameters() {
			Serializable mergeInParameters =
					getOptions().get(OPTION_MERGE_IN_PARAMETERS);
			
			if (mergeInParameters != null) {
				validateBooleanOption(mergeInParameters.toString(), OPTION_MERGE_IN_PARAMETERS);
				return Boolean.parseBoolean(mergeInParameters.toString());
			}

			return false;
		}
		
		@Override
		public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
			Set<OptionDefinition<? extends Serializable>> optionDefinitions = super.getOptionDefinitions();
			
			optionDefinitions.add( new OptionDefinitionBoolean(OPTION_MERGE_IN_PARAMETERS, "set this to 'true' to combine all input parameter types of a method into one single type", false) );
			
			return optionDefinitions;
		}
	}
}
