/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.function.delphi;

import org.jenerateit.modelconverter.ModelConverterI;

import com.gs.gapp.converter.basic.delphi.AbstractDelphiConverterProvider;

/**
 * @author hrr
 *
 */
public class FunctionToDelphiConverterProvider extends AbstractDelphiConverterProvider {

	/**
	 *
	 */
	public FunctionToDelphiConverterProvider() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.ModelConverterProviderI#getModelConverter()
	 */
	@Override
	public ModelConverterI getModelConverter() {
		return new FunctionToDelphiConverter();
	}
}
