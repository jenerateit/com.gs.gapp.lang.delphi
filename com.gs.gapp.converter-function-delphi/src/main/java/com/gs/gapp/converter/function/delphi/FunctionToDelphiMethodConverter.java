package com.gs.gapp.converter.function.delphi;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.dsl.delphi.DelphiOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.objectpascal.ParameterKeyword;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.VisibilityDirective;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Parameter;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class FunctionToDelphiMethodConverter <S extends Function, T extends Method> extends
    AbstractFunctionModelElementToDelphiConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public FunctionToDelphiMethodConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);
		
		// note that business exceptions are going to be converted from within FunctionModuleToJavaClassConverter in order to circumvent the problem with duplicate exception names
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S function, ModelElementI potentialOwner) {
		Clazz owner = null;
		T result = null;
		if (potentialOwner instanceof Clazz) {
			owner = (Clazz) potentialOwner;
		}
		
		if (isHavingReturnParameter(function)) {
		    result = createFunction(function, owner);
		} else {
			result = createProcedure(function, owner);
		}
		return result;
	}
	
	/**
	 * @param function
	 * @return
	 */
	protected boolean isHavingReturnParameter(Function function) {
		return function.getFunctionOutParameters().size() > 0;
	}
	
	/**
	 * @param function
	 * @param owner
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected T createFunction(S function, Clazz owner) {
		T result = null;
		List<Parameter> parametersForInParameters = createParametersForInParameters(function);
		Type returnType = createTypeForOutParameters(function);
		result = (T) new com.gs.gapp.metamodel.objectpascal.member.Function(StringTools.firstUpperCase(function.getName()), owner, returnType, parametersForInParameters.toArray(new Parameter[0]));
		result.setVisibility(VisibilityDirective.PUBLIC);
		
		return result;
	}
	
	/**
	 * @param function
	 * @param owner
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected T createProcedure(S function, Clazz owner) {
		T result = null;
		List<Parameter> parametersForInParameters = createParametersForInParameters(function);
		result = (T) new com.gs.gapp.metamodel.objectpascal.member.Procedure(StringTools.firstUpperCase(function.getName()), owner, parametersForInParameters.toArray(new Parameter[0]));
		result.setVisibility(VisibilityDirective.PUBLIC);
		
		return result;
	}

	/**
	 * @param function
	 * @return
	 */
	protected List<Parameter> createParametersForInParameters(S function) {
		List<Parameter> result = new ArrayList<>();
		
		if (getModelConverter().getConverterOptions().isMergeInParameters()) {
			final ComplexType complexType = new ComplexType("ParametersFor" + StringTools.firstUpperCase(function.getName()));
			complexType.setOriginatingElement(function);
			Module basicModule = getOrCreateModuleForOnTheFlyTypes((FunctionModule) function.getModule());
			complexType.setModule(basicModule);
	
			for (FunctionParameter functionParameter : function.getFunctionInParameters()) {
				final Field field = new Field(functionParameter.getName(), complexType);
				field.setOriginatingElement(functionParameter);
				field.setType(functionParameter.getType());
				field.setCollectionType(functionParameter.getCollectionType());
			}
	
			Clazz typeWithFieldsForInParams = convertWithOtherConverter(Clazz.class, complexType, basicModule.getSingleExtensionElement(Unit.class));
			Parameter parameter = new Parameter("in", typeWithFieldsForInParams);
			result.add(parameter);
		} else {
			for (FunctionParameter functionParameter : function.getFunctionInParameters()) {
				Parameter parameter = createParameterForInParameter(function, functionParameter);
				if (parameter != null) {
				    result.add(parameter);
				}
			}
		}
		
		return result;
	}

	/**
	 * @param functionParameter
	 * @return the function/procedure parameter for a given function parameter, might return null if the in-parameter should not be resultin in a function/procedure parameter
	 */
	protected Parameter createParameterForInParameter(Function function, FunctionParameter functionParameter) {
		Type parameterType = createTypeForParameter(function, functionParameter);
		
		// --- adding key words depending on the availability of Delphi-specific modeling options
		Set<ParameterKeyword> parameterKeywords = new LinkedHashSet<>();
		OptionDefinition<String>.OptionValue keywords = functionParameter.getOption(DelphiOptionEnum.PARAMETER_KEYWORDS.getName(), String.class);
		if (keywords != null) {
		    for (String keyword : keywords.getOptionValues()) {
		    	ParameterKeyword parameterKeyword = ParameterKeyword.fromString(keyword);
		    	if (parameterKeyword == null) throw new ModelConverterException("could not find a ParameterKeyword enum entry for the string '" + keyword + '"');
		    	parameterKeywords.add(parameterKeyword);
		    }
		}

		Parameter parameter = new Parameter(functionParameter.getName(), parameterType, parameterKeywords);
		parameter.setOriginatingElement(functionParameter);
		return parameter;
	}

	/**
	 * @param function
	 * @return
	 */
	protected Type createTypeForOutParameters(S function) {
		Type result = null;
		
		if (function.getFunctionOutParameters().size() > 1) {
			final ComplexType complexType = new ComplexType("ResultFor" + StringTools.firstUpperCase(function.getName()));
			complexType.setOriginatingElement(function);
			Module basicModule = getOrCreateModuleForOnTheFlyTypes((FunctionModule) function.getModule());
			complexType.setModule(basicModule);
	
			for (FunctionParameter functionParameter : function.getFunctionOutParameters()) {
				final Field field = new Field(functionParameter.getName(), complexType);
				field.setOriginatingElement(functionParameter);
				field.setType(functionParameter.getType());
			}

			result = convertWithOtherConverter(Clazz.class, complexType, basicModule.getSingleExtensionElement(Unit.class));
		} else if (function.getFunctionOutParameters().size() == 1) {
			FunctionParameter outParam = function.getFunctionOutParameters().iterator().next();
			result = createTypeForParameter(function, outParam);
		} else {
			// no parameter, should be a Delphi function then
		}
		
		return result;
	}
	
	/**
	 * @param function
	 * @param functionParameter
	 * @return
	 */
	protected Type createTypeForParameter(Function function, FunctionParameter functionParameter) {
		Type result = null;
		
		Type type = null;
        Type collectionType = null;
        
		if (functionParameter.getType() instanceof PrimitiveType) {
			type = convertWithOtherConverter(Type.class, functionParameter.getType());
		} else {
			Unit unit = convertWithOtherConverter(Unit.class, functionParameter.getType().getModule());
			if (unit == null) unit = convertWithOtherConverter(Unit.class, functionParameter.getType());  // in case there is a converter that creates a unit for a given type
			if (unit == null) {
				throw new ModelConverterException("could not find created Unit instance for given module '" + functionParameter.getType().getModule() + "' (needed for type conversion)");
			}
			type = convertWithOtherConverter(Type.class, functionParameter.getType(), unit);
		}
		
		collectionType = getCollectionType(functionParameter, type);
        result = collectionType != null ? collectionType : type;
		
		return result;
	}
	
	/**
	 * @param functionParameter
	 * @param type
	 * @return
	 */
	protected Type getCollectionType(FunctionParameter functionParameter, Type type) {
		Type result = null;
		if (functionParameter.getCollectionType() != null) {
			switch (functionParameter.getCollectionType()) {
			case LIST:
				// TODO should we use TObjecTList instead? or maybe both, depending on additional model information? => http://docwiki.embarcadero.com/CodeExamples/Berlin/en/Generics_Collections_TList_(Delphi)
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TArray.TYPE, type);
				break;
			case SET:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TArray.TYPE, type);
				break;
			case SORTED_SET:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TArray.TYPE, type);
				break;
			case ARRAY:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TArray.TYPE, type);
				break;
			case KEYVALUE:
				Type keyType = this.convertWithOtherConverter(Type.class, functionParameter.getKeyType());
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.delphi.predef.system.generics.collections.TObjectDictionary.TYPE, keyType, type);
				break;
			default:
				throw new ModelConverterException("cannot convert basic field since it has an unknown collection type assigned", functionParameter);
			}
		}

		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected FunctionToDelphiConverter getModelConverter() {
		return super.getModelConverter();
	}
}
