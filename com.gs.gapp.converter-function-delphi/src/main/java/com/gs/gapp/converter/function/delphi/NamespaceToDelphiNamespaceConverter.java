/**
 *
 */
package com.gs.gapp.converter.function.delphi;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.Namespace;


/**
 * @author mmt
 *
 */
public class NamespaceToDelphiNamespaceConverter<S extends Namespace, T extends com.gs.gapp.metamodel.objectpascal.Namespace> extends
    com.gs.gapp.converter.basic.delphi.NamespaceToDelphiNamespaceConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public NamespaceToDelphiNamespaceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) getModelConverter().getOrCreateDelphiNamespace(originalModelElement);
		return result;
	}
}
