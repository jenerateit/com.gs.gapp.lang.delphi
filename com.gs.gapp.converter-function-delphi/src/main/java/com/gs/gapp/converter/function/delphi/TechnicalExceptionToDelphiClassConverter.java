/**
 *
 */
package com.gs.gapp.converter.function.delphi;

import com.gs.gapp.converter.basic.delphi.ExceptionTypeToDelphiClassConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.TechnicalException;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElementCreator;
import com.gs.gapp.metamodel.objectpascal.member.Constructor;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

/**
 * @author mmt
 *
 */
public class TechnicalExceptionToDelphiClassConverter<S extends TechnicalException, T extends Clazz> extends
    ExceptionTypeToDelphiClassConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public TechnicalExceptionToDelphiClassConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	public TechnicalExceptionToDelphiClassConverter(AbstractConverter modelConverter,
			ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	@Override
	protected void onConvert(S exceptionType, T delphiExceptionType) {
		super.onConvert(exceptionType, delphiExceptionType);
		
		// --- implement constructors for parent class constructors
		if (delphiExceptionType.getConstructors().size() == 0) {
			ObjectPascalModelElementCreator elementCreator = getModelConverter().getDelphiModelElementCreator();
			if (delphiExceptionType.getParent() != null && delphiExceptionType.getParent() instanceof Clazz) {
				Clazz clazz = (Clazz) delphiExceptionType.getParent();
				for (Constructor constructor : clazz.getConstructors()) {
			        elementCreator.duplicateConstructor(constructor, delphiExceptionType);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.basic.delphi.ExceptionTypeToDelphiClassConverter#onCreateModelElement(com.gs.gapp.metamodel.basic.typesystem.ExceptionType, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S technicalException, ModelElementI previousResultingModelElement) {
		 T result = null;
			
		if (getModelConverter().getCapabilityContext() != null) {
			TechnicalException abstractTechnicalException = getModelConverter().getCapabilityContext().getSingleExtensionElement(TechnicalException.class);
			if (abstractTechnicalException != null) {
				Clazz abstractDelphiTechnicalExceptionType = convertWithOtherConverter(Clazz.class, abstractTechnicalException);
				if (abstractDelphiTechnicalExceptionType != null) result = (T) abstractDelphiTechnicalExceptionType;
			}
		} else {
			result = super.onCreateModelElement(technicalException, previousResultingModelElement);
		}
		
		return result;
	}


}

