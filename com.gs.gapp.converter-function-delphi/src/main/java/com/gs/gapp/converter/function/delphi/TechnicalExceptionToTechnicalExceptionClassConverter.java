package com.gs.gapp.converter.function.delphi;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.TechnicalException;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.TechnicalExceptionClass;

/**
 * @author mmt
 *
 * @param <S>
 * @param <T>
 */
public class TechnicalExceptionToTechnicalExceptionClassConverter<S extends TechnicalException, T extends TechnicalExceptionClass>
    extends TechnicalExceptionToDelphiClassConverter<S, T> {
	
	public TechnicalExceptionToTechnicalExceptionClassConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected void onConvert(S businessException, T businessExceptionClass) {
		super.onConvert(businessException, businessExceptionClass);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	public T onCreateModelElement(S technicalException, ModelElementI previouslyResultingElement) {
        Unit existingUnit = null;
		
        if (previouslyResultingElement != null && previouslyResultingElement instanceof Unit) {
        	existingUnit = (Unit) previouslyResultingElement;
        	@SuppressWarnings("unchecked")
			T clazz =
        			(T) new TechnicalExceptionClass(getExceptionName(technicalException), existingUnit);
        	
        	return clazz;
        }
		
        throw new ModelConverterException("Tried to convert a technical exception to a Delphi technical exception class, but there was no previous resulting element provided (a unit is required).", technicalException);
	}
}
