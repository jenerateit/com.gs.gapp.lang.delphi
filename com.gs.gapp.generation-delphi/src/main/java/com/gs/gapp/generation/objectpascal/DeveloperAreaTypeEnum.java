package com.gs.gapp.generation.objectpascal;



/**
 * 
 * @author mmt
 *
 */
public enum DeveloperAreaTypeEnum {

	/**
	 * Allows for manual modifications within a constructor body
	 */
	CONSTRUCTOR_BODY,
	
	/**
	 * Allows for manual modifications within a destructor body
	 */
	DESTRUCTOR_BODY,
	
	/**
	 * Allows for manual modifications within a function or procedure body
	 */
	METHOD_BODY,
	
	/**
	 * Allows for manual modifications for annotations on a member
	 */
	ATTRIBUTE_FOR_MEMBER,
	
	/**
	 * Allows for manual modifications for annotations on a type (class, interface, record, enumeration, ...)
	 */
	ATTRIBUTE_FOR_TYPE,
	
	/**
     * Allows for manual modifications for annotations on a functions and procedures
     */
    ATTRIBUTE_FOR_METHOD,
    
    /**
     * Allows for manual modifications for annotations on a field
     */
    ATTRIBUTE_FOR_FIELD,
    
    /**
     * Allows for manual modifications for annotations on a property
     */
    ATTRIBUTE_FOR_PROPERTY,
	
	/**
	 * Allows for manual additions of enumeration entries in an enumeration
	 */
	ENUMERATION_ENTRIES,
	
	/**
	 * Allows for manual additions/modifications of type identifiers
	 */
	TYPE_IDENTIFIERS,
	
	BEFORE_PRIVATE_SECTION_IN_CLASS,
	
	ADDITIONAL_PRIVATE_FIELDS_IN_CLASS,
	ADDITIONAL_PRIVATE_MEMBERS_IN_CLASS,
	
	ADDITIONAL_STRICT_PRIVATE_FIELDS_IN_CLASS,
	ADDITIONAL_STRICT_PRIVATE_MEMBERS_IN_CLASS,
	
	ADDITIONAL_PROTECTED_FIELDS_IN_CLASS,
	ADDITIONAL_PROTECTED_MEMBERS_IN_CLASS,
	
	ADDITIONAL_STRICT_PROTECTED_FIELDS_IN_CLASS,
	ADDITIONAL_STRICT_PROTECTED_MEMBERS_IN_CLASS,
	
	ADDITIONAL_PUBLIC_FIELDS_IN_CLASS,
	ADDITIONAL_PUBLIC_MEMBERS_IN_CLASS,
	
	ADDITIONAL_PUBLISHED_FIELDS_IN_CLASS,
	ADDITIONAL_PUBLISHED_MEMBERS_IN_CLASS,
	
	ADDITIONAL_MEMBERS_IN_RECORD,
	
	ADDITIONAL_MEMBERS_IN_INTERFACE,
	
	ADDITIONAL_METHOD_IMPLEMENTATIONS,
	
	/**
	 * Allows for adding constants in a type definition
	 */
	ADDITIONAL_CONSTANTS_IN_UNIT,
	
	/**
	 * Allows for adding types in a unit
	 */
	ADDITIONAL_TYPES_IN_UNIT,
	
	/**
	 * Allows for adding vars in a unit
	 */
	ADDITIONAL_VARS_IN_UNIT,
	
	/**
	 * Allows for adding methods in a unit
	 */
	ADDITIONAL_METHODS_IN_UNIT,
	
	/**
	 * 
	 */
	ADDITIONAL_INITIALIZATIONS_IN_UNIT,
	
	/**
	 * 
	 */
	ADDITIONAL_FINALIZATIONS_IN_UNIT,
	

	// --- 6 new developer area types for issue CHQDG-178
	INTERFACE_SECTION_START,
	INTERFACE_SECTION_AFTER_USES,
	INTERFACE_SECTION_END,
	CLASS_DECLARATION,
	INTERFACE_DECLARATION,
	METHOD_DECLARATION,
	;
	
	public String getId() {
		return this.name().replace("_", ".").replace("-", ".").toLowerCase();
	}
}
