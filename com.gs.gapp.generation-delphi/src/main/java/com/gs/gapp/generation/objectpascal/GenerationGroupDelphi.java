/**
 * 
 */
package com.gs.gapp.generation.objectpascal;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.WriterLocatorI;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.generation.objectpascal.target.UnitTarget;
import com.gs.gapp.metamodel.basic.ModelElementCache;

/**
 * 
 */
public class GenerationGroupDelphi extends AbstractGenerationGroup implements GenerationGroupConfigI {

	private final WriterLocatorI writerLocator;
	
	
	public GenerationGroupDelphi() {
		super(new ModelElementCache());
		
		addTargetClass(UnitTarget.class);
		
		this.writerLocator = new WriterLocatorDelphi(getAllTargets());
	}

	
	/* (non-Javadoc)
	 * @see org.jenerateit.util.DetailsI#getName()
	 */
	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.util.DetailsI#getDescription()
	 */
	@Override
	public String getDescription() {
		return getClass().getName();
	}


	@Override
	public WriterLocatorI getWriterLocator() {
		return this.writerLocator;
	}

}
