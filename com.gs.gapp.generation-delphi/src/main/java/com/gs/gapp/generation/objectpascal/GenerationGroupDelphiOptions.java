package com.gs.gapp.generation.objectpascal;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.generation.objectpascal.target.DelphiTarget;
import com.gs.gapp.metamodel.basic.options.GenerationGroupOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionLong;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;
import com.gs.gapp.metamodel.objectpascal.DelphiVersionEnum;

public class GenerationGroupDelphiOptions extends GenerationGroupOptions {
	
	public static final OptionDefinitionString OPTION_DEF_AREA_RULE =
			new OptionDefinitionString("developer-area-rule",
					                   "determines the rule for developer areas", false, null,
					                   DeveloperAreaRule.getNames());
	
	public static final OptionDefinitionString OPTION_DEF_DELPHI_VERSION =
			new OptionDefinitionString("delphi-version",
					                   "defines the minimum delphi version of the target environment", false, null,
					                   DelphiVersionEnum.getNames());
	
	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_AFTER_USES =
			new OptionDefinitionLong("delphi.indentation.after.uses",
					                   "", false, 2L);
	
	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_AFTER_TYPE =
			new OptionDefinitionLong("delphi.indentation.after.type",
					                   "", false, 2L);
	
	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_AFTER_VAR =
			new OptionDefinitionLong("delphi.indentation.after.var",
					                   "", false, 2L);
	
	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_AFTER_CONST =
			new OptionDefinitionLong("delphi.indentation.after.const",
					                   "", false, 2L);
	
	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_BEFORE_VISIBILITY_DIRECTIVE =
			new OptionDefinitionLong("delphi.indentation.before.visibility.directive",
					                   "", false, 2L);

	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_BEFORE_MEMBERS =
			new OptionDefinitionLong("delphi.indentation.before.members",
					                   "", false, 2L);

	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_AFTER_INITIALIZATION =
			new OptionDefinitionLong("delphi.indentation.after.initialization",
					                   "", false, 2L);
	
	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_AFTER_FINALIZATION =
			new OptionDefinitionLong("delphi.indentation.after.finalization",
					                   "", false, 2L);

	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_BEFORE_BEGIN =
			new OptionDefinitionLong("delphi.indentation.before.begin",
					                   "", false, 2L);
	
	public static final OptionDefinitionLong OPTION_DEF_DELPHI_INDENTATION_AFTER_BEGIN =
			new OptionDefinitionLong("delphi.indentation.after.begin",
					                   "", false, 2L);
	
	private Integer indentationAfterUses;
	private Integer indentationAfterType;
	private Integer indentationAfterVar;
	private Integer indentationAfterConst;
	private Integer indentationBeforeVisibilityDirective;
	private Integer indentationBeforeMembers;
	private Integer indentationAfterInitialization;
	private Integer indentationAfterFinalization;
	private Integer indentationBeforeBegin;
	private Integer indentationAfterBegin;
	

	public GenerationGroupDelphiOptions(AbstractWriter writer) {
		super(writer);
		init();
	}

	public GenerationGroupDelphiOptions(BasicTextTarget<?> target) {
		super(target);
		init();
	}
	
	/**
	 * Initialize option values that are used often and thus should not be retrieved by
	 * calling getOptionValue() all the time. 
	 */
	private void init() {
		Long indentation = null;
		Serializable indentationOption = null;
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_USES.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_USES.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_USES.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationAfterUses = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_TYPE.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_TYPE.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_TYPE.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationAfterType = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_VAR.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_VAR.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_VAR.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationAfterVar = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_CONST.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_CONST.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_CONST.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationAfterConst = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_VISIBILITY_DIRECTIVE.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_VISIBILITY_DIRECTIVE.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_VISIBILITY_DIRECTIVE.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationBeforeVisibilityDirective = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_MEMBERS.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_MEMBERS.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_MEMBERS.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationBeforeMembers = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_INITIALIZATION.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_INITIALIZATION.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_INITIALIZATION.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationAfterInitialization = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_FINALIZATION.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_FINALIZATION.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_FINALIZATION.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationAfterFinalization = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_BEGIN.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_BEGIN.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_BEFORE_BEGIN.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationBeforeBegin = indentation.intValue();
		
		indentation = (Long) OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_BEGIN.getDefinition().getDefaultValue();
		indentationOption = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_BEGIN.getName());
		if (indentationOption != null) {
			validateNumericOption(indentationOption, OptionDefinitionEnum.OPTION_DELPHI_INDENTATION_AFTER_BEGIN.getName());
			indentation = new Long(indentationOption.toString());
		}
		indentationAfterBegin = indentation.intValue();
	}
	
	public Integer getIndentationAfterUses() {
		return indentationAfterUses;
	}
	
	public Integer getIndentationAfterType() {
		return indentationAfterType;
	}

	public Integer getIndentationAfterVar() {
		return indentationAfterVar;
	}

	public Integer getIndentationAfterConst() {
		return indentationAfterConst;
	}

	public Integer getIndentationBeforeVisibilityDirective() {
		return indentationBeforeVisibilityDirective;
	}

	public Integer getIndentationBeforeMembers() {
		return indentationBeforeMembers;
	}

	public Integer getIndentationAfterInitialization() {
		return indentationAfterInitialization;
	}

	public Integer getIndentationAfterFinalization() {
		return indentationAfterFinalization;
	}

	public Integer getIndentationBeforeBegin() {
		return indentationBeforeBegin;
	}

	public Integer getIndentationAfterBegin() {
		return indentationAfterBegin;
	}

	/**
	 * Affects the way developer area support is going to be gene generated.
	 * By default, developer areas are going to be used as defined within target classes {@link DelphiTarget#getActiveDeveloperAreaTypes()}.
	 * Using the option 'developer-area-rule', you can switch on or off all available developer areas.
	 * 
	 * @return
	 */
	public DeveloperAreaRule getDeveloperAreaRule() {
		DeveloperAreaRule defaultSetting = DeveloperAreaRule.DEFAULT;
		Serializable developerAreaRuleString = getOptionValue(OptionDefinitionEnum.OPTION_DEVELOPER_AREA_RULE.getName());
		
		if (developerAreaRuleString != null) {
			DeveloperAreaRule result = null;
			try {
		        result = DeveloperAreaRule.valueOf(developerAreaRuleString.toString().toUpperCase());
		        return result;
			} catch (IllegalArgumentException ex) {
				throwIllegalEnumEntryException(developerAreaRuleString.toString().toUpperCase(), OptionDefinitionEnum.OPTION_DEVELOPER_AREA_RULE.getName(), DeveloperAreaRule.values());
			}
		}
		
		return defaultSetting;		
	}
	
	/**
	 * Returns the minimal Delphi version to be supported by generated code. The main purpose of this
	 * option is to be able to disable newer language features in order to be able to still
	 * use older Delphi versions to work with generated code.
	 * 
	 * @return
	 */
	public DelphiVersionEnum getDelphiVersion() {
		DelphiVersionEnum defaultSetting = DelphiVersionEnum.getDefault();
		Serializable delphiVersionString = getOptionValue(OptionDefinitionEnum.OPTION_DELPHI_VERSION.getName());
		
		if (delphiVersionString != null) {
			DelphiVersionEnum result = null;
			try {
		        result = DelphiVersionEnum.valueOf(DelphiVersionEnum.class, delphiVersionString.toString().toUpperCase());
		        return result;
			} catch (IllegalArgumentException ex) {
				throwIllegalEnumEntryException(delphiVersionString.toString().toUpperCase(), OptionDefinitionEnum.OPTION_DELPHI_VERSION.getName(), DelphiVersionEnum.values());
			}
		}
		
		return defaultSetting;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.GenerationGroupOptions#getOptionDefinitions()
	 */
	@Override
	public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
		Set<OptionDefinition<? extends Serializable>> result = super.getOptionDefinitions();
		result.addAll(OptionDefinitionEnum.getDefinitions());
		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.GenerationGroupOptions#getDefaultTargetPrefix()
	 */
	@Override
	protected String getDefaultTargetPrefix() {
		return "/";
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.GenerationGroupOptions#getDefaultTargetPrefixForTests()
	 */
	@Override
	protected String getDefaultTargetPrefixForTests() {
		return "/";
	}
	
	/**
	 * @return
	 */
	@Override
	protected PlatformForLineBreakStyle getDefaultPlatformLineBreakStyle() {
		return PlatformForLineBreakStyle.WINDOWS;
	}

	/**
	 * @author mmt
	 *
	 */
	public static enum OptionDefinitionEnum {

		
		OPTION_DEVELOPER_AREA_RULE ( OPTION_DEF_AREA_RULE ),
		OPTION_DELPHI_VERSION ( OPTION_DEF_DELPHI_VERSION ),
		
		OPTION_DELPHI_INDENTATION_AFTER_USES ( OPTION_DEF_DELPHI_INDENTATION_AFTER_USES ),
		OPTION_DELPHI_INDENTATION_AFTER_TYPE ( OPTION_DEF_DELPHI_INDENTATION_AFTER_TYPE ),
		OPTION_DELPHI_INDENTATION_AFTER_VAR ( OPTION_DEF_DELPHI_INDENTATION_AFTER_VAR ),
		OPTION_DELPHI_INDENTATION_AFTER_CONST ( OPTION_DEF_DELPHI_INDENTATION_AFTER_CONST ),
		OPTION_DELPHI_INDENTATION_BEFORE_VISIBILITY_DIRECTIVE ( OPTION_DEF_DELPHI_INDENTATION_BEFORE_VISIBILITY_DIRECTIVE ),
		OPTION_DELPHI_INDENTATION_BEFORE_MEMBERS ( OPTION_DEF_DELPHI_INDENTATION_BEFORE_MEMBERS ),
		OPTION_DELPHI_INDENTATION_AFTER_INITIALIZATION ( OPTION_DEF_DELPHI_INDENTATION_AFTER_INITIALIZATION ),
		OPTION_DELPHI_INDENTATION_AFTER_FINALIZATION ( OPTION_DEF_DELPHI_INDENTATION_AFTER_FINALIZATION ),
		OPTION_DELPHI_INDENTATION_BEFORE_BEGIN ( OPTION_DEF_DELPHI_INDENTATION_BEFORE_BEGIN ),
		OPTION_DELPHI_INDENTATION_AFTER_BEGIN ( OPTION_DEF_DELPHI_INDENTATION_AFTER_BEGIN ),
		;

		private static final Map<String, OptionDefinitionEnum> stringToEnum = new HashMap<>();

		static {
			for (OptionDefinitionEnum m : values()) {
				stringToEnum.put(m.getName(), m);
			}
		}
		
		/**
		 * @return
		 */
		public static Set<OptionDefinition<? extends Serializable>> getDefinitions() {
			Set<OptionDefinition<? extends Serializable>> result = new LinkedHashSet<>();
			for (OptionDefinitionEnum m : values()) {
				result.add(m.getDefinition());
			}
			return result;
		}

		/**
		 * @param datatypeName
		 * @return
		 */
		public static OptionDefinitionEnum fromString(String datatypeName) {
			return stringToEnum.get(datatypeName);
		}

		private final OptionDefinition<? extends Serializable> definition;
		
		private OptionDefinitionEnum(OptionDefinition<? extends Serializable> definition) {
			this.definition = definition;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return this.getDefinition().getName();
		}

		public OptionDefinition<? extends Serializable> getDefinition() {
			return definition;
		}
	}
}
