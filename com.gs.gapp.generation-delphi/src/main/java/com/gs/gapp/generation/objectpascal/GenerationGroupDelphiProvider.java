/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.log.LogService;



/**
 * @author hrr
 *
 */
public class GenerationGroupDelphiProvider implements GenerationGroupProviderI {
	
	private LogService log = null;

	/**
	 * Constructor
	 */
	public GenerationGroupDelphiProvider() {
	}

    public void addLogService(LogService log) {
    	this.log = log;
    }
    
    public void removeLogService(LogService log) {
    	this.log = null;
    }
    
    
	
	public void startup(ComponentContext context) {
    	if (this.log != null) this.log.log(LogService.LOG_INFO, "startup of " + getClass().getSimpleName());
    }
    
    public void shutdown(ComponentContext context) {
    	if (this.log != null) this.log.log(LogService.LOG_INFO, "shutdown of " + getClass().getSimpleName());
    }

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupDelphi();
	}
}
