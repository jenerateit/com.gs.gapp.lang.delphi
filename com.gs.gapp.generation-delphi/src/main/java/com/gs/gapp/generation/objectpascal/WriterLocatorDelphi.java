/*
 * 
 */
package com.gs.gapp.generation.objectpascal;



import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.generation.basic.AbstractWriterLocator;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.generation.objectpascal.target.UnitTarget;
import com.gs.gapp.generation.objectpascal.writer.ArrayWriter;
import com.gs.gapp.generation.objectpascal.writer.AttributeUsageWriter;
import com.gs.gapp.generation.objectpascal.writer.AttributeWriter;
import com.gs.gapp.generation.objectpascal.writer.ClassReferenceWriter;
import com.gs.gapp.generation.objectpascal.writer.ClazzConstructorWriter;
import com.gs.gapp.generation.objectpascal.writer.ClazzDestructorWriter;
import com.gs.gapp.generation.objectpascal.writer.ClazzFunctionWriter;
import com.gs.gapp.generation.objectpascal.writer.ClazzProcedureWriter;
import com.gs.gapp.generation.objectpascal.writer.ClazzPropertyWriter;
import com.gs.gapp.generation.objectpascal.writer.ClazzWriter;
import com.gs.gapp.generation.objectpascal.writer.ConstantWriter;
import com.gs.gapp.generation.objectpascal.writer.ConstructedTypeWriter;
import com.gs.gapp.generation.objectpascal.writer.ConstructorWriter;
import com.gs.gapp.generation.objectpascal.writer.DestructorWriter;
import com.gs.gapp.generation.objectpascal.writer.EnumerationWriter;
import com.gs.gapp.generation.objectpascal.writer.FieldWriter;
import com.gs.gapp.generation.objectpascal.writer.FileWriter;
import com.gs.gapp.generation.objectpascal.writer.ForwardDeclarationWriter;
import com.gs.gapp.generation.objectpascal.writer.FunctionWriter;
import com.gs.gapp.generation.objectpascal.writer.InterfaceWriter;
import com.gs.gapp.generation.objectpascal.writer.ParameterWriter;
import com.gs.gapp.generation.objectpascal.writer.PointerWriter;
import com.gs.gapp.generation.objectpascal.writer.ProceduralWriter;
import com.gs.gapp.generation.objectpascal.writer.ProcedureWriter;
import com.gs.gapp.generation.objectpascal.writer.PropertyWriter;
import com.gs.gapp.generation.objectpascal.writer.PseudoTypeWriter;
import com.gs.gapp.generation.objectpascal.writer.PseudoUnitUsageWriter;
import com.gs.gapp.generation.objectpascal.writer.RecordWriter;
import com.gs.gapp.generation.objectpascal.writer.SetWriter;
import com.gs.gapp.generation.objectpascal.writer.SubRangeWriter;
import com.gs.gapp.generation.objectpascal.writer.TypeDeclarationWriter;
import com.gs.gapp.generation.objectpascal.writer.TypeIdentifierWriter;
import com.gs.gapp.generation.objectpascal.writer.TypedConstantWriter;
import com.gs.gapp.generation.objectpascal.writer.UnitUsageWriter;
import com.gs.gapp.generation.objectpascal.writer.UnitWriter;
import com.gs.gapp.generation.objectpascal.writer.VariableWriter;
import com.gs.gapp.metamodel.objectpascal.AttributeUsage;
import com.gs.gapp.metamodel.objectpascal.Constant;
import com.gs.gapp.metamodel.objectpascal.PseudoType;
import com.gs.gapp.metamodel.objectpascal.PseudoUnitUsage;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.UnitUsage;
import com.gs.gapp.metamodel.objectpascal.Variable;
import com.gs.gapp.metamodel.objectpascal.constant.TypedConstant;
import com.gs.gapp.metamodel.objectpascal.member.ClazzConstructor;
import com.gs.gapp.metamodel.objectpascal.member.ClazzDestructor;
import com.gs.gapp.metamodel.objectpascal.member.ClazzFunction;
import com.gs.gapp.metamodel.objectpascal.member.ClazzProcedure;
import com.gs.gapp.metamodel.objectpascal.member.ClazzProperty;
import com.gs.gapp.metamodel.objectpascal.member.Constructor;
import com.gs.gapp.metamodel.objectpascal.member.Destructor;
import com.gs.gapp.metamodel.objectpascal.member.Field;
import com.gs.gapp.metamodel.objectpascal.member.Function;
import com.gs.gapp.metamodel.objectpascal.member.Parameter;
import com.gs.gapp.metamodel.objectpascal.member.Procedure;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.Attribute;
import com.gs.gapp.metamodel.objectpascal.type.ConstructedType;
import com.gs.gapp.metamodel.objectpascal.type.ForwardDeclaration;
import com.gs.gapp.metamodel.objectpascal.type.Pointer;
import com.gs.gapp.metamodel.objectpascal.type.Procedural;
import com.gs.gapp.metamodel.objectpascal.type.TypeDeclaration;
import com.gs.gapp.metamodel.objectpascal.type.TypeIdentifier;
import com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration;
import com.gs.gapp.metamodel.objectpascal.type.simple.SubRange;
import com.gs.gapp.metamodel.objectpascal.type.structured.Array;
import com.gs.gapp.metamodel.objectpascal.type.structured.ClassReference;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.File;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;
import com.gs.gapp.metamodel.objectpascal.type.structured.Record;
import com.gs.gapp.metamodel.objectpascal.type.structured.Set;


/**
 * 
 * @author hrr
 *
 */
public class WriterLocatorDelphi extends AbstractWriterLocator implements WriterLocatorI {
	
	

	/**
	 * @param generationGroup
	 */
	public WriterLocatorDelphi(AbstractGenerationGroup generationGroup) {
		super(generationGroup);
		initWriterMappers();
	}

	/**
	 * Constructor.
	 * 
	 * @param targetClasses a collection of all target classes
	 */
	public WriterLocatorDelphi(java.util.Set<Class<? extends TargetI<?>>> targetClasses) {
		super(targetClasses);
	    initWriterMappers();	
	}
	
	private void initWriterMappers() {
		// --- generation decisions
		addWriterMapperForGenerationDecision(new WriterMapper(Unit.class, UnitTarget.class, UnitWriter.class));
		
		// --- generation delegations
		// - use-clause
		addWriterMapperForWriterDelegation(new WriterMapper(UnitUsage.class, UnitTarget.class, UnitUsageWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(PseudoUnitUsage.class, UnitTarget.class, PseudoUnitUsageWriter.class));
		
		// - types
		addWriterMapperForWriterDelegation(new WriterMapper(Record.class, UnitTarget.class, RecordWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Clazz.class, UnitTarget.class, ClazzWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Attribute.class, UnitTarget.class, AttributeWriter.class));  // --- attribute in Delphi is what an annotation is in Java
		addWriterMapperForWriterDelegation(new WriterMapper(Interface.class, UnitTarget.class, InterfaceWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(PseudoType.class, UnitTarget.class, PseudoTypeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Array.class, UnitTarget.class, ArrayWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Enumeration.class, UnitTarget.class, EnumerationWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(SubRange.class, UnitTarget.class, SubRangeWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(File.class, UnitTarget.class, FileWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Set.class, UnitTarget.class, SetWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(TypeIdentifier.class, UnitTarget.class, TypeIdentifierWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(TypeDeclaration.class, UnitTarget.class, TypeDeclarationWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(ConstructedType.class, UnitTarget.class, ConstructedTypeWriter.class));
		
		// - members
		addWriterMapperForWriterDelegation(new WriterMapper(Parameter.class, UnitTarget.class, ParameterWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Function.class, UnitTarget.class, FunctionWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(ClazzFunction.class, UnitTarget.class, ClazzFunctionWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Procedure.class, UnitTarget.class, ProcedureWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(ClazzProcedure.class, UnitTarget.class, ClazzProcedureWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Constructor.class, UnitTarget.class, ConstructorWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(ClazzConstructor.class, UnitTarget.class, ClazzConstructorWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Destructor.class, UnitTarget.class, DestructorWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(ClazzDestructor.class, UnitTarget.class, ClazzDestructorWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Property.class, UnitTarget.class, PropertyWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(ClazzProperty.class, UnitTarget.class, ClazzPropertyWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Field.class, UnitTarget.class, FieldWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Constant.class, UnitTarget.class, ConstantWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(TypedConstant.class, UnitTarget.class, TypedConstantWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Variable.class, UnitTarget.class, VariableWriter.class));
		
		// - others
		addWriterMapperForWriterDelegation(new WriterMapper(ForwardDeclaration.class, UnitTarget.class, ForwardDeclarationWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Pointer.class, UnitTarget.class, PointerWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(Procedural.class, UnitTarget.class, ProceduralWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(ClassReference.class, UnitTarget.class, ClassReferenceWriter.class));
		addWriterMapperForWriterDelegation(new WriterMapper(AttributeUsage.class, UnitTarget.class, AttributeUsageWriter.class));
	}
}
