/**
 * *************************************************************
 *   
 *
 ***************************************************************
 */

package com.gs.gapp.generation.objectpascal.target;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.target.TargetLifecycleListenerI;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.basic.TargetModelizer;
import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.objectpascal.GenerationGroupDelphiOptions;


/**
 * Note that the default line break style that is being used for the generation of delphi
 * target files is \r\n (WINDOWS). You can configure this by using the generation option
 * line.break.style (enumerated, one of 'MAC', 'MAC_OLD', 'WINDOWS' or 'UNIX'). 
 * 
 * @author mmt
 *
 */
public abstract class DelphiTarget extends BasicTextTarget<DelphiTargetDocument> {

	private static final TargetLifecycleListenerI USES_MERGER = new UsesMerger();
	private static final TargetLifecycleListenerI DIRECTIVES_MERGER = new DirectivesMerger();
	private static final TargetLifecycleListenerI INTERFACE_GUIDS_MERGER = new InterfaceGuidsMerger();
	private static final TargetLifecycleListenerI TARGET_MODELIZER = new TargetModelizer();
	private static final TargetLifecycleListenerI DEVELOPER_DOCUMENTATION_MERGER = new DeveloperDocumentationMerger();
	
	private GenerationGroupDelphiOptions generationGroupOptions;
	
	/**
	 * Constructor.
	 */
	public DelphiTarget() {
		super();
		addTargetLifecycleListener(USES_MERGER);
		addTargetLifecycleListener(DIRECTIVES_MERGER);
		addTargetLifecycleListener(INTERFACE_GUIDS_MERGER);
		addTargetLifecycleListener(TARGET_MODELIZER);
		addTargetLifecycleListener(DEVELOPER_DOCUMENTATION_MERGER);
	}
	
	/**
	 * @return
	 */
	public Set<DeveloperAreaTypeEnum> getActiveDeveloperAreaTypes() {
		return new LinkedHashSet<>( Arrays.asList( DeveloperAreaTypeEnum.values()) );
	}

	/**
	 * @return
	 */
	@Override
	public TargetSection getCurrentTransformationSection() {
		return super.getCurrentTransformationSection();
	}
	
	/**
	 * @return
	 */
	@Override
	public GenerationGroupDelphiOptions getGenerationGroupOptions() {
		if (this.generationGroupOptions == null) {
			this.generationGroupOptions = new GenerationGroupDelphiOptions(this);
		}
		return generationGroupOptions;
	}
}
