/*
 * 
 */

package com.gs.gapp.generation.objectpascal.target;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.regex.Pattern;

import org.jenerateit.target.AbstractTextTargetDocument;
import org.jenerateit.target.DeveloperAreaInfoI;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.target.TextFormattingConfigItem;
import org.jenerateit.target.TextFormattingConfigItemDefinition;
import org.jenerateit.target.TextTargetDocumentI;
import org.jenerateit.util.StringTools;

import com.gs.gapp.generation.objectpascal.writer.DelphiWriter;

/**
 * @author hr
 *
 */
public class DelphiTargetDocument extends AbstractTextTargetDocument {

	private static final long serialVersionUID = -8845520671736296893L;
	
	public static final String DEVDOC_HINT = "// Here you can add comment lines (starting with '//') that will not be overwritten by the code generation.";
	
	@SuppressWarnings("unused")
	private static final Pattern LTRIM = Pattern.compile("^\\s+");
	private static final Pattern RTRIM = Pattern.compile("\\s+$");
	
	private boolean usesSectionInInterfaceStarted = false;
	private boolean usesSectionInInterfaceEnded = false;
	private StringBuilder usesSectionInInterfaceContent = new StringBuilder();
	
	private boolean usesSectionInImplementationStarted = false;
	private boolean usesSectionInImplementationEnded = false;
	private StringBuilder usesSectionInImplementationContent = new StringBuilder();
	
	private boolean insideInterfaceSection = false;
	private boolean insideImplementationSection = false;
	
	private final ArrayList<String> linesBetweenUnitAndFirstDevDoc = new ArrayList<>();
	private final ArrayList<String> directivesBetweenUnitAndInterfaceSection = new ArrayList<>();
	private final ArrayList<String> potentialLinesBeforeNextDirective = new ArrayList<>();
	private boolean sectionBetweenUnitAndInterfaceSectionStarted = false;
	private boolean sectionBetweenUnitAndInterfaceSectionEnded = false;
	
	private final LinkedHashMap<String, String> interfaceGuids = new LinkedHashMap<>();
	private String currentInterfaceType;
	
	private final List<String> developerDocumentation = new ArrayList<>();
	private Boolean firstCommentAfterUnitKeyword = null;
	@SuppressWarnings("unused")
    private boolean firstDevDocFound = false;
	private boolean unitSupplementLinesCollected = false;
	private int numberOfEmptyLinesBetweenDeveloperDocumentation = 0;
	
	/**
	 * The map's key is the identifier being used for the model element, e.g. a method name.
	 * The pattern to identify the key is "// ************* [the key] *************
	 * That key is being used to try to identify an element within the unit that is being generated.
	 * That element could be anything, a function, a procedure, a property.
	 * If is is not possible to uniquely identify an element within the unit, a WriterException is being thrown.
	 */
	private final Map<String,List<String>> developerDocumentationForElement = new LinkedHashMap<>();
	private String currentKeyForDeveloperDocumentation;
	
    private final Map<DelphiTargetSectionEnum,Boolean> writtenCompilerDirectiveInUseSectionMap = new LinkedHashMap<>();
    
    private final Map<DelphiTargetSectionEnum,Boolean> previousUseWrittenAsIsMap = new LinkedHashMap<>();
    
    private String unitNameLine;
    
    private final Map<DelphiTargetSectionEnum, List<String>> currentUses = new HashMap<>();
    
    @Override
	public List<TextFormattingConfigItem> getTextFormattingConfigurationItems() {
		List<TextFormattingConfigItem> result = new ArrayList<>();
		result.add(new TextFormattingConfigItem(TextFormattingConfigItemDefinition.WRITE_INDENTATION_ON_EMPTY_LINE, Boolean.FALSE));
		return result;
	}
    
	/**
	 * 'currentUses' caches the list of uses of the Delphi unit that is currently transformed.
	 * Whenever a new unit-use is added, the list of 'currentUses' is reset to an empty list
	 * in order to let the program logic re-construct that list from scratch.
	 * 
	 * This mechanism has been introduced in order to improve the generation performance.
	 * 
	 * @return the currentUses
	 */
	public List<String> getCurrentUses(DelphiTargetSectionEnum tsEnumEntry) {
		return currentUses.get(tsEnumEntry);
	}

	/**
	 * Clear all instance variables and call the super class method {@link AbstractTextTargetDocument#analyzeDocument()}
	 * 
	 * @see AbstractTextTargetDocument#analyzeDocument()
	 */
	@Override
	protected void analyzeDocument() {
		// this has to be done before the analyzeDocument(), it ensures that the logic works fine even when a target document instance is being cached/reused (mmt 17-Aug-2017)
		initializeVariables();
		
		super.analyzeDocument();
	}


	/**
	 * Resets all instance variables
	 */
	private void initializeVariables() {
		this.usesSectionInInterfaceStarted = false;
		this.usesSectionInInterfaceEnded = false;
		this.usesSectionInInterfaceContent = new StringBuilder();
		
		this.unitSupplementLinesCollected = false;
		
		this.usesSectionInImplementationStarted = false;
		this.usesSectionInImplementationEnded = false;
		this.usesSectionInImplementationContent = new StringBuilder();
		this.insideInterfaceSection = false;
		this.insideImplementationSection = false;
		
		this.linesBetweenUnitAndFirstDevDoc.clear();
		this.directivesBetweenUnitAndInterfaceSection.clear();
		this.potentialLinesBeforeNextDirective.clear();
		this.sectionBetweenUnitAndInterfaceSectionStarted = false;
		this.sectionBetweenUnitAndInterfaceSectionEnded = false;
		
		this.interfaceGuids.clear();
		this.currentInterfaceType = null;
		
		this.developerDocumentation.clear();
		this.firstCommentAfterUnitKeyword = null;
		this.numberOfEmptyLinesBetweenDeveloperDocumentation = 0;
		
		this.developerDocumentationForElement.clear();
		this.currentKeyForDeveloperDocumentation = null;
		
		this.writtenCompilerDirectiveInUseSectionMap.clear();
		this.previousUseWrittenAsIsMap.clear();
		
		
		this.unitNameLine = null;
	}


	/**
	 * Calls the super class method to check for developer areas and 
	 * then check the given line for an import statement.
	 * 
	 * @param lineNumber number of the line of the current file
	 * @param line the line to check
	 * @param start the start position in the target document of the line
	 * @param end the end position in the target document of the line
	 * @see AbstractTextTargetDocument#analyzeLine(int, java.lang.String, int, int)
	 */
	@Override
	protected void analyzeLine(int lineNumber, String line, int start, int end) {
		super.analyzeLine(lineNumber, line, start, end);
		String trimmedLine = line.trim();
		String trimmedLineLowercase = trimmedLine.toLowerCase();
		String trimmedLineLowercaseWithoutWhitespaces = trimmedLineLowercase.replaceAll("\\s", "");
		String lineWithoutLineBreak = RTRIM.matcher(line).replaceAll("");
		
		if (this.sectionBetweenUnitAndInterfaceSectionStarted && !sectionBetweenUnitAndInterfaceSectionEnded) {
			if (trimmedLine.startsWith("//")) {
				 if (this.firstCommentAfterUnitKeyword == null) {
					 this.firstCommentAfterUnitKeyword = true;
				 } else {
					 this.firstCommentAfterUnitKeyword = false;
				 }
			 }
		}
		
		if (this.sectionBetweenUnitAndInterfaceSectionStarted &&
	            !this.sectionBetweenUnitAndInterfaceSectionEnded &&
	            !this.unitSupplementLinesCollected) {
		    if (isDevDocLine(trimmedLine)) {
		        this.unitSupplementLinesCollected = true;
		    } else {
                // Here we collect the manually written lines between the unit declaration (first line in file) and the begining of the first comment (devdoc).
                this.linesBetweenUnitAndFirstDevDoc.add(line);
            }
        }
		
		
		if (trimmedLine.startsWith("unit ")) {
			this.sectionBetweenUnitAndInterfaceSectionStarted = true;
			this.unitNameLine = lineWithoutLineBreak.trim();
		}
		if (trimmedLine.startsWith("interface")) {
			sectionBetweenUnitAndInterfaceSectionEnded = true;
			insideInterfaceSection = true;
		}
		if (trimmedLine.startsWith("implementation")) {
			insideInterfaceSection = false;
			insideImplementationSection = true;
		}
		
		if (insideInterfaceSection) {
			if (!this.usesSectionInInterfaceEnded && trimmedLine.startsWith("uses")) {
				this.usesSectionInInterfaceStarted = true;
			}
			
			if (this.usesSectionInInterfaceStarted && !this.usesSectionInInterfaceEnded) {
				this.usesSectionInInterfaceContent.append(line);
			}
			
			if (this.usesSectionInInterfaceStarted && (line.indexOf(";") >= 0 || line.startsWith("const") || line.startsWith("type"))) {
				this.usesSectionInInterfaceEnded = true;
			}
		}
		
		if (insideImplementationSection) {
			if (!this.usesSectionInImplementationEnded && trimmedLine.startsWith("uses")) {
				this.usesSectionInImplementationStarted = true;
			}
			
			if (this.usesSectionInImplementationStarted && !this.usesSectionInImplementationEnded) {
				this.usesSectionInImplementationContent.append(line);
			}
			
			if (this.usesSectionInImplementationStarted && line.indexOf(";") >= 0) {
				this.usesSectionInImplementationEnded = true;
			}
		}
		
		// --- compiler directives (e.g. includes)
		if (this.sectionBetweenUnitAndInterfaceSectionStarted && !this.sectionBetweenUnitAndInterfaceSectionEnded &&
		        !firstDevDocFound) {
			
			if (getDirectivesBetweenUnitAndInterfaceSection().size() == 0) {
				if (trimmedLine.startsWith("\u007B\u0024")) {
					// --- the very first time a directive has been found
					directivesBetweenUnitAndInterfaceSection.add(lineWithoutLineBreak);
				}
			} else {
				if (trimmedLine.startsWith("\u007B\u0024")) {
					// --- found a subsequent directive
					directivesBetweenUnitAndInterfaceSection.addAll(potentialLinesBeforeNextDirective);
					potentialLinesBeforeNextDirective.clear();
					directivesBetweenUnitAndInterfaceSection.add(lineWithoutLineBreak);
				} else {
					potentialLinesBeforeNextDirective.add(lineWithoutLineBreak);
				}
			}
		}
		
		// --- interface GUIDs
		if (insideInterfaceSection) {
			if (trimmedLineLowercaseWithoutWhitespaces.indexOf("=interface") > 0) {
			    currentInterfaceType = trimmedLineLowercaseWithoutWhitespaces.split("[=]")[0].trim();
			} else if (currentInterfaceType != null && currentInterfaceType.length() > 0 && trimmedLineLowercaseWithoutWhitespaces.indexOf("\u005B\u0027\u007B") >= 0) {  // ['{
				interfaceGuids.put(currentInterfaceType, trimmedLine);
				currentInterfaceType = null;
			}
		}

		if (trimmedLine.startsWith("//") && !trimmedLine.startsWith("///")
				&& !trimmedLine.contains(DeveloperAreaInfoI.DA_START)
				&& !trimmedLine.contains(DeveloperAreaInfoI.DA_ELSE)
				&& !trimmedLine.contains(DeveloperAreaInfoI.DA_END)) {
		    // the following check takes the asteriskd into account that are generated around the key for the developer documentation areas (ADELPHI-25), (mmt 04-Apr-2019)
			if (trimmedLine.indexOf("[") > 0 && trimmedLine.indexOf("]") > 0 && trimmedLine.indexOf(DelphiWriter.DEVELOPER_DOCUMENTATION_ASTERISKS) > 0) {
			    firstDevDocFound = true;
				// we found a developer documentation - lets try to find out its key
				int startIndex = trimmedLine.indexOf("[");
				int endIndex = trimmedLine.indexOf("]");
				if (endIndex > startIndex) {
				    this.currentKeyForDeveloperDocumentation = trimmedLine.substring(startIndex+1, endIndex).trim();
//				    System.out.println("trimmed line:" + trimmedLine);
//				    System.out.println("found key for dev doc:" + this.currentKeyForDeveloperDocumentation);
				}
			} else if (this.currentKeyForDeveloperDocumentation != null) {
				if (!this.insideInterfaceSection && !this.insideImplementationSection) {
			    	// developer documentation for the unit
					if (this.numberOfEmptyLinesBetweenDeveloperDocumentation > 0) {
						for (int ii=0; ii<this.numberOfEmptyLinesBetweenDeveloperDocumentation; ii++) {
							this.developerDocumentation.add("");
				    	}
				    	this.numberOfEmptyLinesBetweenDeveloperDocumentation = 0;
					}
		    		this.developerDocumentation.add(trimmedLine);
			    } else {
			    	if (trimmedLine.equals(DEVDOC_HINT)) {
			    		// Do not handle the hint for developer documentation logic
			    	} else {
				    	// developer documentation for a language concept
				    	String currentLowercaseKeyForDeveloperDocumentation = this.currentKeyForDeveloperDocumentation.toLowerCase();
				    	List<String> documentationLines = this.developerDocumentationForElement.get(currentLowercaseKeyForDeveloperDocumentation);
				    	if (documentationLines == null) {
				    		documentationLines = new ArrayList<>();
				    		this.developerDocumentationForElement.put(currentLowercaseKeyForDeveloperDocumentation, documentationLines);
				    	}
				    	if (this.numberOfEmptyLinesBetweenDeveloperDocumentation > 0) {
					    	for (int ii=0; ii<this.numberOfEmptyLinesBetweenDeveloperDocumentation; ii++) {
					    		documentationLines.add("");
					    	}
					    	this.numberOfEmptyLinesBetweenDeveloperDocumentation = 0;
				    	}
				    	documentationLines.add(trimmedLine);
			    	}
			    }
			}
		} else if (this.currentKeyForDeveloperDocumentation != null && trimmedLine.length() == 0) {
			// we have empty lines after or between developer documentation, lets count the number of empty lines in order to be able to restore the identical developer documentation
			this.numberOfEmptyLinesBetweenDeveloperDocumentation++;
		} else {
			this.currentKeyForDeveloperDocumentation = null;
			this.numberOfEmptyLinesBetweenDeveloperDocumentation = 0;
		}
	}
	
	/**
	 * @param trimmedLine
	 * @return
	 */
	private boolean isDevDocLine(String trimmedLine) {
	    if (trimmedLine.startsWith("//") && !trimmedLine.startsWith("///")
                && !trimmedLine.contains(DeveloperAreaInfoI.DA_START)
                && !trimmedLine.contains(DeveloperAreaInfoI.DA_ELSE)
                && !trimmedLine.contains(DeveloperAreaInfoI.DA_END)) {
            // the following check takes the asteriskd into account that are generated around the key for the developer documentation areas (ADELPHI-25), (mmt 04-Apr-2019)
            if (trimmedLine.indexOf("[") > 0 && trimmedLine.indexOf("]") > 0 && trimmedLine.indexOf(DelphiWriter.DEVELOPER_DOCUMENTATION_ASTERISKS) > 0) {
                return true;
            }
	    }
	    
	    return false;
	}

	public StringBuilder getUsesSectionInInterfaceContent() {
		return usesSectionInInterfaceContent;
	}
	
	public StringBuilder getUsesSectionInImplementationContent() {
		return usesSectionInImplementationContent;
	}

	/**
	 * 
	 * @return
	 * @see TextTargetDocumentI#getCommentEnd()
	 */
	@Override
	public String getCommentEnd() {
		return StringTools.EMPTY_STRING;
	}

	/**
	 * 
	 * @return
	 * @see TextTargetDocumentI#getCommentStart()
	 */
	@Override
	public String getCommentStart() {
		return "//";
	}

	/**
	 * 
	 * @return
	 * @see TargetDocumentI#getTargetSections()
	 */
	@Override
	public SortedSet<TargetSection> getTargetSections() {
		return DelphiTargetSectionEnum.getTargetSections();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TextTargetDocumentI#getPrefixChar()
	 */
	@Override
	public char getPrefixChar() {
		return ' ';
	}
	
	/**
	 * @param tsEnumEntry
	 * @return
	 */
	public List<String> getUses(DelphiTargetSectionEnum tsEnumEntry) {
	    return getUses(tsEnumEntry,
	                   true);  // uses are trimmed
	}
	
	/**
	 * @param tsEnumEntry
	 * @param trimUses
	 * @return
	 */
	public List<String> getUses(DelphiTargetSectionEnum tsEnumEntry, boolean trimUses) {
	    return getUses(tsEnumEntry,
	                   trimUses,
	                   false);  // false => returned uses do not contain commas
	}
	
	public List<String> getUses(DelphiTargetSectionEnum tsEnumEntry, boolean trim, boolean keepCommas) {
		return getUses(tsEnumEntry, trim, keepCommas, false);
	}
	
	/**
	 * @param tsEnumEntry
	 * @param trim
	 * @param keepCommas
	 * @param ignoreCachedUses
	 * @return
	 */
	public List<String> getUses(DelphiTargetSectionEnum tsEnumEntry, boolean trim, boolean keepCommas, boolean ignoreCachedUses) {
	    if (!isDocumentAnalyzed()) {
            analyzeDocument();
        }
		byte[] currentContent = toByte(tsEnumEntry.getTargetSection());
		if (tsEnumEntry == DelphiTargetSectionEnum.INTERFACE_USES) {
		    return getUses(tsEnumEntry, currentContent != null && currentContent.length > 0 ? new String(currentContent) : this.usesSectionInInterfaceContent.toString(), trim, keepCommas, ignoreCachedUses);
		} else if (tsEnumEntry == DelphiTargetSectionEnum.IMPLEMENTATION_USES) {
			String content = currentContent != null && currentContent.length > 0 ? new String(currentContent) : this.usesSectionInImplementationContent.toString();
			return getUses(tsEnumEntry, content, trim, keepCommas, ignoreCachedUses);
		}
		
		return Collections.emptyList();
	}
	
	/**
	 * @param tsEnumEntry
	 * @param content
	 * @return
	 */
	protected List<String> getUses(DelphiTargetSectionEnum tsEnumEntry, String content) {
	    return getUses(tsEnumEntry, content, false);
	}
	
	/**
	 * @param tsEnumEntry
	 * @param content
	 * @param trim
	 * @return
	 */
	protected List<String> getUses(DelphiTargetSectionEnum tsEnumEntry, String content, boolean trim) {
	    return getUses(tsEnumEntry, content, trim, false);
	}
	
	/**
	 * @param tsEnumEntry
	 * @param content
	 * @param trimUses
	 * @param keepCommas
	 * @return
	 */
	protected List<String> getUses(DelphiTargetSectionEnum tsEnumEntry, String content, boolean trimUses, boolean keepCommas) {
		return getUses(tsEnumEntry, content, trimUses, keepCommas, false);
	}
	
	/**
	 * Parse the given content for unit names in the uses statement.
	 * 
	 * @param tsEnumEntry
	 * @param content the content to parse
	 * @param trimUses
	 * @param keepCommas
	 * @param ignoreCachedUses
	 * @return a list of unit names
	 */
	protected List<String> getUses(DelphiTargetSectionEnum tsEnumEntry, String content, boolean trimUses, boolean keepCommas, boolean ignoreCachedUses) {
		if ( (!ignoreCachedUses) && (currentUses.get(tsEnumEntry) != null && !currentUses.get(tsEnumEntry).isEmpty()) ) {
			return currentUses.get(tsEnumEntry);
		}
		
		List<String> listOfCurrentUses = currentUses.get(tsEnumEntry);
		if (listOfCurrentUses == null) {
			listOfCurrentUses = new ArrayList<>();
			currentUses.put(tsEnumEntry, listOfCurrentUses);
		}
		
		String usesPattern = "uses";
		int usesStart = content.indexOf(usesPattern);
		
		if (usesStart >= 0) {
			// found the uses clause
			int usesEnd = content.indexOf(";", usesStart);
			
			if (usesEnd < 0) usesEnd = content.length();
			String allUses = content.substring(usesStart+usesPattern.length(), usesEnd);
			String allUsesWithoutLineBreaks = allUses.replaceAll("\\r\\n|\\r|\\n", "");
			
			// TODO improve this logic (CHQDG-139, CHQDG-140) (mmt 10-Jul-2019)
			for (String commaSection : allUsesWithoutLineBreaks.split("[,]")) {
			    if (commaSection.trim().length() == 0) continue;
			    if (commaSection.contains("}")) {
			        for (String closingBracketSection : commaSection.trim().split("[}]")) {
			            if (closingBracketSection.trim().length() == 0) continue;
			            if (closingBracketSection.contains("{$") && closingBracketSection.trim().indexOf("{$") != 0) {
			                String[] openingBracketSections = closingBracketSection.trim().split("[{][$]");
			                if (openingBracketSections.length != 2) {
			                    throw new RuntimeException("preservation of compiler directives within uses found an existing uses that could not be parsed:" + content);
			                }
			                listOfCurrentUses.add(trimUses ? openingBracketSections[0].trim() : openingBracketSections[0]);
			                listOfCurrentUses.add("{$" + openingBracketSections[1].trim() + "}");
			            } else {
			                if (closingBracketSection.contains("{$")) {
			                	listOfCurrentUses.add(closingBracketSection.trim() + "}");
			                } else {
			                	listOfCurrentUses.add(trimUses ? closingBracketSection.trim() : closingBracketSection);
			                }
			            }
			        }
			        
			        // the fragment that got added at last might need to get the closing curly bracket re-added
			        String lastEntry = listOfCurrentUses.remove(listOfCurrentUses.size()-1);
		            if (lastEntry.contains("{$") && !lastEntry.contains("}")) {
		            	listOfCurrentUses.add(lastEntry + "}");
		            } else {
		            	listOfCurrentUses.add(lastEntry);
		            }

			    } else {
			    	listOfCurrentUses.add(trimUses ? commaSection.trim() : commaSection);
			    }
			    
			
			    if (listOfCurrentUses.size() > 0) {
    			    String lastEntry = listOfCurrentUses.remove(listOfCurrentUses.size()-1);
    			    if (keepCommas) {
    			    	if (!lastEntry.contains(",")) {
    			    	    listOfCurrentUses.add(lastEntry + ",");  // re-add the comma
    			    	} else {
    			    		listOfCurrentUses.add(lastEntry);
    			    	}
    			    } else {
    			    	listOfCurrentUses.add(lastEntry);
    			    }
			    }
			}
			
			// the very last entry must not have a comma
			if (listOfCurrentUses.size() > 0) {
    			String lastEntry = listOfCurrentUses.remove(listOfCurrentUses.size()-1);
    			if (lastEntry.endsWith(",")) {
    				listOfCurrentUses.add(lastEntry.replaceAll(",", ""));
    			} else {
    				listOfCurrentUses.add(lastEntry);
    			}
			}
		}
		
		StringBuilder sb = new StringBuilder("trimUses:" + trimUses + ", keepCommas:" + keepCommas);
		for (String use : listOfCurrentUses) {
		    sb.append(System.lineSeparator()).append(use);
		}

		return listOfCurrentUses;
	}


	public ArrayList<String> getDirectivesBetweenUnitAndInterfaceSection() {
		return directivesBetweenUnitAndInterfaceSection;
	}
	
	/**
	 * @return
	 */
	public ArrayList<String> getDirectivesBetweenUnitAndInterfaceSectionWithoutDuplicates() {
		if (!isDocumentAnalyzed()) {
            analyzeDocument();
        }
		
	    final ArrayList<String> result;
	    
	    if (getLinesBetweenUnitAndFirstDevDoc().isEmpty()) {
	        result = getDirectivesBetweenUnitAndInterfaceSection();
	    } else {
	        result = new ArrayList<>();
            getDirectivesBetweenUnitAndInterfaceSection().stream()
                .filter(directive -> !getLinesBetweenUnitAndFirstDevDoc().stream().filter(line -> line.contains(directive)).findFirst().isPresent())
                .forEach(directive -> result.add(directive));
        }
	    
        return result;
	}


	public LinkedHashMap<String, String> getInterfaceGuids() {
		if (!isDocumentAnalyzed()) {
            analyzeDocument();
        }
		return interfaceGuids;
	}

	public List<String> getDeveloperDocumentation() {
		if (!isDocumentAnalyzed()) {
            analyzeDocument();
        }
		return developerDocumentation;
	}


	public Map<String,List<String>> getDeveloperDocumentationForElement() {
		if (!isDocumentAnalyzed()) {
            analyzeDocument();
        }
		return developerDocumentationForElement;
	}

    public ArrayList<String> getLinesBetweenUnitAndFirstDevDoc() {
    	if (!isDocumentAnalyzed()) {
            analyzeDocument();
        }
        return linesBetweenUnitAndFirstDevDoc;
    }

    /**
     * @param targetSectionEnum
     * @return
     */
    public boolean isWrittenCompilerDirectiveInUseSection(DelphiTargetSectionEnum targetSectionEnum) {
        Boolean writtenCompilerDirectiveInUseSection = writtenCompilerDirectiveInUseSectionMap.get(targetSectionEnum);
        return writtenCompilerDirectiveInUseSection != null && writtenCompilerDirectiveInUseSection;
    }

    /**
     * @param targetSectionEnum
     * @param writtenCompilerDirectiveInUseSection
     */
    public void setWrittenCompilerDirectiveInUseSection(DelphiTargetSectionEnum targetSectionEnum, boolean writtenCompilerDirectiveInUseSection) {
        this.writtenCompilerDirectiveInUseSectionMap.put(targetSectionEnum, writtenCompilerDirectiveInUseSection);
    }

    /**
     * @param targetSectionEnum
     * @return
     */
    public boolean isPreviousUseWrittenAsIs(DelphiTargetSectionEnum targetSectionEnum) {
        Boolean previousUseWrittenAsIs = previousUseWrittenAsIsMap.get(targetSectionEnum);
        return previousUseWrittenAsIs != null && previousUseWrittenAsIs;
    }

    /**
     * @param targetSectionEnum
     * @param previousUseWrittenAsIs
     */
    public void setPreviousUseWrittenAsIs(DelphiTargetSectionEnum targetSectionEnum, boolean previousUseWrittenAsIs) {
        this.previousUseWrittenAsIsMap.put(targetSectionEnum, previousUseWrittenAsIs);
    }


    public String getUnitNameLine() {
        return unitNameLine;
    }
}
