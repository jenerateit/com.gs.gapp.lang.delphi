/**
 * 
 */
package com.gs.gapp.generation.objectpascal.target;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jenerateit.target.TargetSection;

/**
 * @author mmt
 *
 */
public enum DelphiTargetSectionEnum {

    UNIT            (new TargetSection("unit",            10, 0)),
    
	INTERFACE_USES     (new TargetSection("interface_uses",   20, 0)),
	INTERFACE_TYPE     (new TargetSection("interface_type",   30, 0)),
	INTERFACE_CONSTANT (new TargetSection("interface_const",  40, 0)),
	INTERFACE_VAR      (new TargetSection("interface_var",    50, 0)),
	INTERFACE_METHOD   (new TargetSection("interface_method", 60, 0)),
	
	IMPLEMENTATION_USES       (new TargetSection("implementation_uses",       70,  0)),
	IMPLEMENTATION_AFTER_USES (new TargetSection("implementation_after_uses", 80,  0)),
	IMPLEMENTATION_TYPE       (new TargetSection("implementation_type",       90,  0)),
	IMPLEMENTATION_CONSTANT   (new TargetSection("implementation_const",      100, 0)),
	IMPLEMENTATION_VAR        (new TargetSection("implementation_var",        110, 0)),
	IMPLEMENTATION_METHOD     (new TargetSection("implementation_method",     120, 0)),
	
	INITIALIZATION (new TargetSection("initialization", 130, 0)),
	FINALIZATION   (new TargetSection("finalization",   140, 0)),
	
	END_OF_UNIT (new TargetSection("end_of_unit",   150, 0)),
	;
    
	
    private static final SortedSet<TargetSection> SECTIONS = new TreeSet<>();
    private static final Map<TargetSection, DelphiTargetSectionEnum> MAP = new HashMap<>();
    private static final EnumSet<DelphiTargetSectionEnum> INTERFACE_SECTIONS;
    private static final EnumSet<DelphiTargetSectionEnum> IMPLEMENTATION_SECTIONS;
    private static final EnumSet<DelphiTargetSectionEnum> SECTIONS_FOR_USES;
    
    static {
    	for (DelphiTargetSectionEnum targetSectionEnumEntry : values()) {
			SECTIONS.add(targetSectionEnumEntry.getTargetSection());
			MAP.put(targetSectionEnumEntry.getTargetSection(), targetSectionEnumEntry);
		}
    	
    	INTERFACE_SECTIONS = EnumSet.of(INTERFACE_USES, INTERFACE_CONSTANT, INTERFACE_TYPE, INTERFACE_VAR, INTERFACE_METHOD);
    	IMPLEMENTATION_SECTIONS = EnumSet.of(IMPLEMENTATION_USES, IMPLEMENTATION_CONSTANT, IMPLEMENTATION_TYPE, IMPLEMENTATION_VAR, IMPLEMENTATION_METHOD);
    	SECTIONS_FOR_USES = EnumSet.of(INTERFACE_USES, IMPLEMENTATION_USES);
    }
    
	private final TargetSection targetSection;
	
	DelphiTargetSectionEnum(TargetSection targetSection) {
		this.targetSection = targetSection;
	}

	public TargetSection getTargetSection() {
		return targetSection;
	}
	
	/**
	 * @return
	 */
	public static SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}
	
	public static DelphiTargetSectionEnum get(TargetSection ts) {
		return MAP.get(ts);
	}
	
	public static boolean isInterfaceSection(TargetSection ts) {
		DelphiTargetSectionEnum delphiTargetSectionEnum = get(ts);
		return INTERFACE_SECTIONS.contains(delphiTargetSectionEnum);
	}
	
	public static boolean isImplementationSection(TargetSection ts) {
		DelphiTargetSectionEnum delphiTargetSectionEnum = get(ts);
		return IMPLEMENTATION_SECTIONS.contains(delphiTargetSectionEnum);
	}
	
	public static DelphiTargetSectionEnum getUsesSection(TargetSection ts) {
		DelphiTargetSectionEnum delphiTargetSectionEnum = get(ts);
		if (INTERFACE_SECTIONS.contains(delphiTargetSectionEnum)) {
			return INTERFACE_USES;
		} else if (IMPLEMENTATION_SECTIONS.contains(delphiTargetSectionEnum)) {
			return DelphiTargetSectionEnum.IMPLEMENTATION_USES;
		}
		
		return null;
	}
	
	public static boolean isSectionForUses(TargetSection ts) {
		DelphiTargetSectionEnum delphiTargetSectionEnum = get(ts);
		return SECTIONS_FOR_USES.contains(delphiTargetSectionEnum);
	}
}
