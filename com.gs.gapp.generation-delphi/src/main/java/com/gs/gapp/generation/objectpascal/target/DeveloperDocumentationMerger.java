/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal.target;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.writer.DelphiWriter;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.member.Function;
import com.gs.gapp.metamodel.objectpascal.member.Procedure;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Member;
import com.gs.gapp.metamodel.objectpascal.type.structured.Record;

/**
 * Merger class to merge manually added developer documentation to a generated unit
 * 
 * @author mmt
 *
 */
public class DeveloperDocumentationMerger implements TargetLifecycleListenerI {

	/**
	 * Merge manually added interface GUIDs to a generated unit
	 * 
	 * @param target the target to work on
	 * @see org.jenerateit.target.TargetLifecycleListenerI#preTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void preTransform(TargetI<?> target) {
		if (!DelphiTarget.class.isInstance(target)) {
			throw new JenerateITException("Got a postTransform(TargetI) event with a target of type '" +
					target.getClass() + "' but I expect a target of type '" + DelphiTarget.class + "'");

		} else {
			WriterI writer = target.getBaseWriter();
			if (!DelphiWriter.class.isInstance(writer)) {
				throw new JenerateITException("Got a postTransform(TargetI) event with a writer of type '" +
						writer.getClass() + "' but I expect a target of type '" + DelphiWriter.class + "'");

			} else {
				
				DelphiTarget delphiTarget = DelphiTarget.class.cast(target);
				@SuppressWarnings("unused")
				DelphiWriter delphiWriter = DelphiWriter.class.cast(writer);
				
				@SuppressWarnings("unused")
				DelphiTargetDocument newTargetDocument = delphiTarget.getNewTargetDocument();
				DelphiTargetDocument prevTargetDocument = delphiTarget.getPreviousTargetDocument();
				
				if (delphiTarget instanceof UnitTarget) {
					UnitTarget unitTarget = (UnitTarget) delphiTarget;
					unitTarget.getUnit().getDeveloperDocumentation().addAll(prevTargetDocument.getDeveloperDocumentation());

					Map<String, List<String>> developerDocumentationForElement = prevTargetDocument.getDeveloperDocumentationForElement();
					Set<String> matchedDeveloperDocumentationHeaders = new LinkedHashSet<>();
					
					for (Function function : unitTarget.getUnit().getFunctions()) {
						String key = function.getDeveloperDocumentationHeader().trim().toLowerCase();
						List<String> developerDocumentation = developerDocumentationForElement.get(key);
						matchedDeveloperDocumentationHeaders.add(key);
						function.getDeveloperDocumentation().addAll(developerDocumentation != null ? developerDocumentation : Collections.emptyList());
					}
					
                    for (Procedure procedure : unitTarget.getUnit().getProcedures()) {
                    	String key = procedure.getDeveloperDocumentationHeader().trim().toLowerCase();
                    	matchedDeveloperDocumentationHeaders.add(key);
                    	List<String> developerDocumentation = developerDocumentationForElement.get(key);
						procedure.getDeveloperDocumentation().addAll(developerDocumentation != null ? developerDocumentation : Collections.emptyList());
					}
                    
                    for (Type type : unitTarget.getUnit().getTypes()) {
                    	String key = type.getDeveloperDocumentationHeader().trim().toLowerCase();
                    	matchedDeveloperDocumentationHeaders.add(key);
                    	List<String> developerDocumentationForType = developerDocumentationForElement.get(key);
                    	type.getDeveloperDocumentation().addAll(developerDocumentationForType != null ? developerDocumentationForType : Collections.emptyList());
                    	
                    	if (type instanceof Clazz) {
							Clazz clazz = (Clazz) type;
							for (Member member : clazz.getMembers()) {
	                    		String memberKey = member.getDeveloperDocumentationHeader().trim().toLowerCase();
	                    		matchedDeveloperDocumentationHeaders.add(memberKey);
	                    		List<String> developerDocumentationForMember = developerDocumentationForElement.get(memberKey);
	    						member.getDeveloperDocumentation().addAll(developerDocumentationForMember != null ? developerDocumentationForMember : Collections.emptyList());
	                    	}
                    	} else if (type instanceof Record) {
							Record record = (Record) type;
							for (Member member : record.getMembers()) {
	                    		String memberKey = member.getDeveloperDocumentationHeader().trim().toLowerCase();
	                    		matchedDeveloperDocumentationHeaders.add(memberKey);
	                    		List<String> developerDocumentationForMember = developerDocumentationForElement.get(memberKey);
	    						member.getDeveloperDocumentation().addAll(developerDocumentationForMember != null ? developerDocumentationForMember : Collections.emptyList());
	                    	}
                    	}
                    }
                    
                    String commentToAdd = "// ******* developer documentation that could not be matched to a type, function or procedure (" + new SimpleDateFormat().format(new Date()) + ") *******";
                    Set<String> keys = new LinkedHashSet<>(developerDocumentationForElement.keySet());
                    keys.removeAll(matchedDeveloperDocumentationHeaders);
                    for (String key : keys) {
                    	List<String> unmatchedDevDocs = developerDocumentationForElement.get(key);
                    	if (unmatchedDevDocs != null) {
                    		if (commentToAdd != null) {
                    			unitTarget.getUnit().getDeveloperDocumentation().add(commentToAdd);
                    			commentToAdd = null;  // in order to add this only once
                    		}
	                    	unitTarget.getUnit().getDeveloperDocumentation().add("// *** " + key);
	                    	unitTarget.getUnit().getDeveloperDocumentation().addAll(unmatchedDevDocs);
                    	}
                    }
				}
			}
		}
	}

	@Override
	public void postTransform(TargetI<?> target) {}

	@Override
	public void onLoaded(TargetI<?> target) {}

	@Override
	public void onFound(TargetI<?> target) {}

}
