/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal.target;

import java.util.ArrayList;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.writer.DelphiWriter;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalMessage;

/**
 * Merger class to merge the previous uses statements into the current working file.
 * 
 * @author mmt
 *
 */
public class DirectivesMerger implements TargetLifecycleListenerI {

	/**
	 * Merge previously manually added directives to the unit that is about to be generated
	 * 
	 * @param target the target to work on
	 * @see org.jenerateit.target.TargetLifecycleListenerI#preTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void preTransform(TargetI<?> target) {
		if (!DelphiTarget.class.isInstance(target)) {
			throw new JenerateITException("Got a postTransform(TargetI) event with a target of type '" +
					target.getClass() + "' but I expect a target of type '" + DelphiTarget.class + "'");

		} else {
			WriterI writer = target.getBaseWriter();
			if (!DelphiWriter.class.isInstance(writer)) {
				throw new JenerateITException("Got a postTransform(TargetI) event with a writer of type '" +
						writer.getClass() + "' but I expect a target of type '" + DelphiWriter.class + "'");

			} else {
				
				DelphiTarget delphiTarget = DelphiTarget.class.cast(target);
				@SuppressWarnings("unused")
				DelphiWriter delphiWriter = DelphiWriter.class.cast(writer);
				
				@SuppressWarnings("unused")
				DelphiTargetDocument newTargetDocument = delphiTarget.getNewTargetDocument();
				DelphiTargetDocument prevTargetDocument = delphiTarget.getPreviousTargetDocument();
				
				if (delphiTarget instanceof UnitTarget) {
					UnitTarget unitTarget = (UnitTarget) delphiTarget;
					ArrayList<String> previousDirectvies = prevTargetDocument.getDirectivesBetweenUnitAndInterfaceSectionWithoutDuplicates();
					if (!previousDirectvies.isEmpty()) {
					    unitTarget.getUnit().addDirectivesBetweenUnitAndInterfaceSection(previousDirectvies);
					}
					
					// manually added lines between unit declaration and first dev doc
					ArrayList<String> manuallyAddedLines = prevTargetDocument.getLinesBetweenUnitAndFirstDevDoc();
					if (!manuallyAddedLines.isEmpty()) {
						Message infoMessage = ObjectPascalMessage.INFO_FOUND_MANUALLY_ADDED_LINES_BETWEEN_UNIT_AND_DEV_DOC.getMessageBuilder()
							.modelElement(unitTarget.getUnit())
							.parameters(unitTarget.getUnit().getName(), manuallyAddedLines.size())
							.build();
						unitTarget.addInfo(infoMessage.getMessage());
					    unitTarget.getUnit().addLinesBetweenUnitAndFirstDevDoc(manuallyAddedLines);
					}
				}
			}
		}
	}

	@Override
	public void postTransform(TargetI<?> target) {}

	@Override
	public void onLoaded(TargetI<?> target) {}

	@Override
	public void onFound(TargetI<?> target) {}

}
