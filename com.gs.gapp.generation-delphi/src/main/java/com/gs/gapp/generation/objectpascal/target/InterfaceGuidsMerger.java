/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal.target;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.writer.DelphiWriter;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;

/**
 * Merger class to merge manually added interface GUIDs to a generated unit
 * 
 * @author hrr
 *
 */
public class InterfaceGuidsMerger implements TargetLifecycleListenerI {

	/**
	 * Merge manually added interface GUIDs to a generated unit
	 * 
	 * @param target the target to work on
	 * @see org.jenerateit.target.TargetLifecycleListenerI#preTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void preTransform(TargetI<?> target) {
		if (!DelphiTarget.class.isInstance(target)) {
			throw new JenerateITException("Got a postTransform(TargetI) event with a target of type '" +
					target.getClass() + "' but I expect a target of type '" + DelphiTarget.class + "'");

		} else {
			WriterI writer = target.getBaseWriter();
			if (!DelphiWriter.class.isInstance(writer)) {
				throw new JenerateITException("Got a postTransform(TargetI) event with a writer of type '" +
						writer.getClass() + "' but I expect a target of type '" + DelphiWriter.class + "'");

			} else {
				
				DelphiTarget delphiTarget = DelphiTarget.class.cast(target);
				@SuppressWarnings("unused")
				DelphiWriter delphiWriter = DelphiWriter.class.cast(writer);
				
				@SuppressWarnings("unused")
				DelphiTargetDocument newTargetDocument = delphiTarget.getNewTargetDocument();
				DelphiTargetDocument prevTargetDocument = delphiTarget.getPreviousTargetDocument();
				
				if (delphiTarget instanceof UnitTarget) {
					UnitTarget unitTarget = (UnitTarget) delphiTarget;
					for (Interface interf : unitTarget.getUnit().getInterfaces()) {
						if (prevTargetDocument.getInterfaceGuids().containsKey(interf.getName()) && (interf.getGuid() == null || interf.getGuid().length() == 0)) {
							interf.setGuid(prevTargetDocument.getInterfaceGuids().get(interf.getName()));
						}
					}
				}
			}
		}
	}

	@Override
	public void postTransform(TargetI<?> target) {}

	@Override
	public void onLoaded(TargetI<?> target) {}

	@Override
	public void onFound(TargetI<?> target) {}

}
