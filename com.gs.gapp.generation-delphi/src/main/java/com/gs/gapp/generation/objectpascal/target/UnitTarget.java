package com.gs.gapp.generation.objectpascal.target;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.EnumSet;
import java.util.Set;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.objectpascal.Unit;




/**
 * 
 * @author hrr
 *
 */
public class UnitTarget extends DelphiTarget {

	@ModelElement
	private Unit unit;
	
	/**
	 * 24 enum entries of 38 in total {@link DeveloperAreaTypeEnum}
	 *
	 * @return
	 */
	@Override
	public Set<DeveloperAreaTypeEnum> getActiveDeveloperAreaTypes() {
		return EnumSet.of(DeveloperAreaTypeEnum.CONSTRUCTOR_BODY,
				          DeveloperAreaTypeEnum.METHOD_BODY,
				          DeveloperAreaTypeEnum.BEFORE_PRIVATE_SECTION_IN_CLASS,
				          
				          DeveloperAreaTypeEnum.ADDITIONAL_PRIVATE_FIELDS_IN_CLASS,
				          DeveloperAreaTypeEnum.ADDITIONAL_PRIVATE_MEMBERS_IN_CLASS,
				          
				          DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PRIVATE_FIELDS_IN_CLASS,
				          DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PRIVATE_MEMBERS_IN_CLASS,
				          
				          DeveloperAreaTypeEnum.ADDITIONAL_PROTECTED_FIELDS_IN_CLASS,
				          DeveloperAreaTypeEnum.ADDITIONAL_PROTECTED_MEMBERS_IN_CLASS,
				          
				          DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PROTECTED_FIELDS_IN_CLASS,
				          DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PROTECTED_MEMBERS_IN_CLASS,
				          
				          DeveloperAreaTypeEnum.ADDITIONAL_PUBLIC_FIELDS_IN_CLASS,
				          DeveloperAreaTypeEnum.ADDITIONAL_PUBLIC_MEMBERS_IN_CLASS,
				          
				          DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_FIELDS_IN_CLASS,
				          DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_MEMBERS_IN_CLASS,
				          
				          DeveloperAreaTypeEnum.ADDITIONAL_MEMBERS_IN_INTERFACE,
				          DeveloperAreaTypeEnum.ADDITIONAL_MEMBERS_IN_RECORD,
				          DeveloperAreaTypeEnum.ADDITIONAL_METHOD_IMPLEMENTATIONS,
				          
				          DeveloperAreaTypeEnum.INTERFACE_SECTION_START,
				          DeveloperAreaTypeEnum.INTERFACE_SECTION_AFTER_USES,
				          DeveloperAreaTypeEnum.INTERFACE_SECTION_END,
				          DeveloperAreaTypeEnum.CLASS_DECLARATION,
				          DeveloperAreaTypeEnum.INTERFACE_DECLARATION,
				          DeveloperAreaTypeEnum.METHOD_DECLARATION);
	}
	
	@Override
	public URI getTargetURI() {
		
		StringBuilder sb = new StringBuilder();

		sb.append(getTargetRoot()).append("/").append(getTargetPrefix()).append("/");
		sb.append(getUnit().getQualifiedName()).append(".pas");

		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, unit);
		}
	}

	public Unit getUnit() {
		return unit;
	}
}
