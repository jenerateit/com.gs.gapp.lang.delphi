/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal.target;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.writer.DelphiWriter;

/**
 * Merger class to merge the previous uses statements into the current working file.
 * 
 * @author hrr
 *
 */
public class UsesMerger implements TargetLifecycleListenerI {

	/**
	 * Not implemented.
	 * 
	 * @param target the target to work on
	 * @see org.jenerateit.target.TargetLifecycleListenerI#preTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void preTransform(TargetI<?> target) {
	}

	/**
	 * Merges the old uses statements into the new target document.
	 * 
	 * @param target the target to work on
	 * @see org.jenerateit.target.TargetLifecycleListenerI#postTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void postTransform(TargetI<?> target) {
		if (!DelphiTarget.class.isInstance(target)) {
			throw new JenerateITException("Got a postTransform(TargetI) event with a target of type '" +
					target.getClass() + "' but I expect a target of type '" + DelphiTarget.class + "'");

		} else {
			WriterI writer = target.getBaseWriter();
			if (!DelphiWriter.class.isInstance(writer)) {
				throw new JenerateITException("Got a postTransform(TargetI) event with a writer of type '" +
						writer.getClass() + "' but I expect a target of type '" + DelphiWriter.class + "'");

			} else {
				
				DelphiTarget delphiTarget = DelphiTarget.class.cast(target);
				DelphiWriter delphiWriter = DelphiWriter.class.cast(writer);
				
				DelphiTargetDocument newTargetDocument = delphiTarget.getNewTargetDocument();
				@SuppressWarnings("unused")
                DelphiTargetDocument prevTargetDocument = delphiTarget.getPreviousTargetDocument();
				
				if (newTargetDocument == null) {
					throw new JenerateITException("The new target document is not set, can not write delphi uses -> is the merger called outside the transform?");
				}
				
				// The functionality of merging of uses has been moved to UnitWriter#transformPreviousUsesForInterface. 
//				List<String> interfaceUses = prevTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES);
//				if (prevTargetDocument != null && !interfaceUses.isEmpty()) {
//					for (String s : interfaceUses) {
//						delphiWriter.wUse(s, DelphiTargetSectionEnum.INTERFACE_USES);
//					}
//				}
				if (!newTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES).isEmpty()) {
					delphiWriter.wNL(DelphiTargetSectionEnum.INTERFACE_USES.getTargetSection(), ";");
				}

                // The functionality of merging of uses has been moved to UnitWriter#transformPreviousUsesForInterface.
//				List<String> implementationUses = prevTargetDocument.getUses(DelphiTargetSectionEnum.IMPLEMENTATION_USES);
//				if (prevTargetDocument != null && !implementationUses.isEmpty()) {
//					for (String s : implementationUses) {
//						delphiWriter.wUse(s, DelphiTargetSectionEnum.IMPLEMENTATION_USES);
//					}
//				}
				
				// This functionality is still required here, since it is the only reliable way to end the "uses" section with a semicolon. (mmt 11-Jul-2019)
				if (!newTargetDocument.getUses(DelphiTargetSectionEnum.IMPLEMENTATION_USES).isEmpty()) {
					delphiWriter.wNL(DelphiTargetSectionEnum.IMPLEMENTATION_USES.getTargetSection(), ";");
				}
			}
		}
	}

	@Override
	public void onLoaded(TargetI<?> target) {
		
	}

	@Override
	public void onFound(TargetI<?> target) {
		
	}

}
