/*
 * 
 */

package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.metamodel.objectpascal.Keyword;
import com.gs.gapp.metamodel.objectpascal.type.structured.Array;


/**
 * @author mmt
 *
 */
public class ArrayWriter extends StructuredTypeWriter {

	@ModelElement
	private Array array;
	
	
	/**
	 * @param ts
	 */
	protected void wTypeClause(TargetSection ts) {
	    String packed = "";
        if (this.array.getPacked() != null && this.array.getPacked()) {
            packed = Keyword.PACKED.getName() + " ";
        }
        
		wNL(getTypeName(), " = ", packed, array.getArrayDef(), ";");
	}
	
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		wTypeClause(ts);
	}
}
