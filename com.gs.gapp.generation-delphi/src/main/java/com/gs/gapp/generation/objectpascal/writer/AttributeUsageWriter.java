/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.objectpascal.target.DelphiTargetSectionEnum;
import com.gs.gapp.metamodel.objectpascal.AttributeUsage;

/**
 * @author mmt
 *
 */
public class AttributeUsageWriter extends DelphiWriter {

	@ModelElement
	private AttributeUsage attributeUsage;

	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		
		wAttributeUsage(ts);
	}
	
	/**
	 * @param ts
	 */
	public void wAttributeUsage(TargetSection ts) {
		
		if (DelphiTargetSectionEnum.isInterfaceSection(ts)) {
			StringBuilder parameters = new StringBuilder();
			if (attributeUsage.getAttributeParameters().size() > 0) {
				parameters.append("(");
				String comma = "";
				for (String parameter : attributeUsage.getAttributeParameters()) {
					parameters.append(comma).append(parameter);
					comma = ", ";
				}
				parameters.append(")");
			}
			
			wUse(attributeUsage.getAttribute());
			String attributeName = attributeUsage.getAttribute().getTypeAsString();
			if (attributeName.endsWith("Attribute")) {
				attributeName = attributeName.substring(0, attributeName.lastIndexOf("Attribute"));
			}
			
			w("[", attributeName, parameters, "]");
		}
	}
	
	@Override
	protected void wDeveloperDocumentation(TargetSection ts) { /* nothing written here for AttributeUsage */ }
    
    @Override
    protected void wDeveloperDocumentationHeader(TargetSection ts) { /* nothing written here for AttributeUsage */ }
}
