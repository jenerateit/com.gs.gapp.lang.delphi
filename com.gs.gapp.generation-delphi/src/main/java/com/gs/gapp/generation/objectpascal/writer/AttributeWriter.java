package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.metamodel.objectpascal.type.Attribute;
import com.gs.vd.metamodel.delphi.predef.system.TCustomAttribute;


/**
 * @author hr
 *
 */
public class AttributeWriter extends ClazzWriter {

	@ModelElement
	private Attribute attribute;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		if (attribute.getParent() != TCustomAttribute.TYPE) {
			attribute.setParent(TCustomAttribute.TYPE);
		}
		super.transform(ts);
	}
}
