package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.metamodel.objectpascal.type.structured.ClassReference;


/**
 * @author mmt
 *
 */
public class ClassReferenceWriter extends StructuredTypeWriter {

	@ModelElement
	private ClassReference classReference;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		wTypeName(); w(" = class of ", classReference.getReferencedClass().getName()); wNL(";");
	}
	
	@Override
	public String getTypeName() {
		return classReference.getNonReservedWordName();
	}
}
