/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.member.ClazzConstructor;



/**
 * @author mmt
 *
 */
public class ClazzConstructorWriter extends ProcedureWriter {

	@ModelElement
	private ClazzConstructor clazzConstructor;
	
	@Override
	protected void wMethodKeyword() {
		w("class constructor ");
	}
}
