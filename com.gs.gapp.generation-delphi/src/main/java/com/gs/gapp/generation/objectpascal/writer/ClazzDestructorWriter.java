/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.member.ClazzDestructor;



/**
 * @author mmt
 *
 */
public class ClazzDestructorWriter extends ProcedureWriter {

	@ModelElement
	private ClazzDestructor clazzDestructor;
	
	@Override
	protected void wMethodKeyword() {
		w("class destructor ");
	}
}
