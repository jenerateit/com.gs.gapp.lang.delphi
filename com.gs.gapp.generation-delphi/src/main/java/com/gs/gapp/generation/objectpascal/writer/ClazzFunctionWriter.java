/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.member.ClazzFunction;



/**
 * @author mmt
 *
 */
public class ClazzFunctionWriter extends FunctionWriter {

	@ModelElement
	private ClazzFunction clazzFunction;
	
	@Override
	protected void wMethodKeyword() {
		w("class function ");
	}
}
