/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.member.ClazzProcedure;



/**
 * @author mmt
 *
 */
public class ClazzProcedureWriter extends ProcedureWriter {

	@ModelElement
	private ClazzProcedure clazzProcedure;
	

	@Override
	protected void wMethodKeyword() {
		w("class procedure ");
	}
}
