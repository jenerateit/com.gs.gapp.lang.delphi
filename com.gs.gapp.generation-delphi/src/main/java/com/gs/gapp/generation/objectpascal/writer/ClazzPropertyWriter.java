/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.member.ClazzProperty;


/**
 * @author hrr
 *
 */
public class ClazzPropertyWriter extends PropertyWriter {

	@ModelElement
	private ClazzProperty clazzProperty;

	@Override
	protected void wKeyword() {
        w("class property ");
	}
}
