package com.gs.gapp.generation.objectpascal.writer;

import java.util.Set;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.objectpascal.DelphiVersionEnum;
import com.gs.gapp.metamodel.objectpascal.Directive;
import com.gs.gapp.metamodel.objectpascal.Keyword;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.VisibilityDirective;
import com.gs.gapp.metamodel.objectpascal.member.Field;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.InterfaceTypeI;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Member;


/**
 * @author hr
 *
 */
public class ClazzWriter extends StructuredTypeWriter {

	@ModelElement
	private Clazz clazz;
	
    /**
     * @param ts
     */
    private void wStrictProtectedMembers(TargetSection ts) {
    	if (getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
    	    
    	    String developerAreaIdStrictProtectedFieldsInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PROTECTED_FIELDS_IN_CLASS.getId()).toString();
            String developerAreaIdStrictProtectedMembersInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PROTECTED_MEMBERS_IN_CLASS.getId()).toString();
            
    		wNL(Directive.STRICT.getName(), " ", VisibilityDirective.PROTECTED.getName()).indent(getGenerationGroupOptions().getIndentationBeforeMembers());
	    		// CHQDG-152
	            Set<Field> strictProtectedFields = clazz.getStrictProtectedFields();
	            Set<Method> strictProtectedMethods = clazz.getStrictProtectedMethods();
	            Set<Property> strictProtectedProperties = clazz.getStrictProtectedProperties();
	            
	            
	            // --- fields
	            for (final Field field : strictProtectedFields) {
	                WriterI writer = getTransformationTarget().getWriterInstance(field);
	                if (writer != null) writer.transform(ts);
	            }
	            if (strictProtectedMethods.size() > 0 || strictProtectedProperties.size() > 0) {
	                if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PROTECTED_FIELDS_IN_CLASS)) {
                        bDA(developerAreaIdStrictProtectedFieldsInClass);
                        eDA();
	                }
	            }
	            
	            // --- methods
	            for (final Method method : strictProtectedMethods) {
	                WriterI writer = getTransformationTarget().getWriterInstance(method);
	                if (writer != null) writer.transform(ts);
	            }
	            
	            // --- properties
	            for (final Property property : strictProtectedProperties) {
	                WriterI writer = getTransformationTarget().getWriterInstance(property);
	                if (writer != null) writer.transform(ts);
	            }
	            
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PROTECTED_MEMBERS_IN_CLASS)) {
					bDA(developerAreaIdStrictProtectedMembersInClass);
				    eDA();
				}
			outdent().wNL(getGenerationGroupOptions().getIndentationBeforeMembers());
    	} else {
			// in this case the protected members are getting generated in wProtectedMembers()
		}
	}

	/**
	 * @param ts
	 */
	private void wStrictPrivateMembers(TargetSection ts) {
		if (getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
		    
		    String developerAreaIdStrictPrivateFieldsInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PRIVATE_FIELDS_IN_CLASS.getId()).toString();
	        String developerAreaIdStrictPrivateMembersInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PRIVATE_MEMBERS_IN_CLASS.getId()).toString();
	        
    	    wNL(Directive.STRICT.getName(), " ", VisibilityDirective.PRIVATE.getName()).indent(getGenerationGroupOptions().getIndentationBeforeMembers());
    	    
    	        // CHQDG-152
                Set<Field> strictPrivateFields = clazz.getStrictPrivateFields();
                Set<Method> strictPrivateMethods = clazz.getStrictPrivateMethods();
                Set<Property> strictPrivateProperties = clazz.getStrictPrivateProperties();
                
                
                // --- fields
                for (final Field field : strictPrivateFields) {
                    WriterI writer = getTransformationTarget().getWriterInstance(field);
                    if (writer != null) writer.transform(ts);
                }
                if (strictPrivateMethods.size() > 0 || strictPrivateProperties.size() > 0) {
                    if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PRIVATE_FIELDS_IN_CLASS)) {
                        bDA(developerAreaIdStrictPrivateFieldsInClass);
                        eDA();
                    }
                }
                
                // --- methods
                for (final Method method : strictPrivateMethods) {
                    WriterI writer = getTransformationTarget().getWriterInstance(method);
                    if (writer != null) writer.transform(ts);
                }

                // --- properties
                for (final Property property : strictPrivateProperties) {
                    WriterI writer = getTransformationTarget().getWriterInstance(property);
                    if (writer != null) writer.transform(ts);
                }
                
    	    
	    	    
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_STRICT_PRIVATE_MEMBERS_IN_CLASS)) {
					bDA(developerAreaIdStrictPrivateMembersInClass);
				    eDA();
				}
	        outdent(getGenerationGroupOptions().getIndentationBeforeMembers()).wNL();
		} else {
			// in this case the private members are getting generated in wPrivateMembers()
		}
	}

	/**
	 * @param ts
	 */
	private void wPublishedMembers(TargetSection ts) {
		String developerAreaIdPublishedFieldsInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_FIELDS_IN_CLASS.getId()).toString();
		String developerAreaIdPublishedMembersInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_MEMBERS_IN_CLASS.getId()).toString();
		
		if (clazz.getPublishedMembers().size() > 0) {
		    
	        // CHQDG-152
	        Set<Field> publishedFields = clazz.getPublishedFields();
	        Set<Method> publishedMethods = clazz.getPublishedMethods();
	        Set<Property> publishedProperties = clazz.getPublishedProperties();
	        
			// only add a 'published' section when there are published members
	    	wNL(VisibilityDirective.PUBLISHED.getName()).indent(getGenerationGroupOptions().getIndentationBeforeMembers());
	    	
	    	
	        // --- fields
            for (final Field field : publishedFields) {
                WriterI writer = getTransformationTarget().getWriterInstance(field);
                if (writer != null) writer.transform(ts);
            }
            if (publishedMethods.size() > 0 || publishedProperties.size() > 0) {
                if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_FIELDS_IN_CLASS)) {
                    // CHQDG-146
                    bDA(developerAreaIdPublishedFieldsInClass);
                    eDA();
                }
            }
	    	
            // --- methods
            for (final Method method : publishedMethods) {
                WriterI writer = getTransformationTarget().getWriterInstance(method);
                if (writer != null) writer.transform(ts);
            }
            
            // --- properties
            for (final Property property : publishedProperties) {
                WriterI writer = getTransformationTarget().getWriterInstance(property);
                if (writer != null) writer.transform(ts);
            }
			
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_MEMBERS_IN_CLASS)) {
				bDA(developerAreaIdPublishedMembersInClass);
			    eDA();
			}
	        outdent(getGenerationGroupOptions().getIndentationBeforeMembers()).wNL();
		} else {
		    if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_FIELDS_IN_CLASS) ||
		        isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_MEMBERS_IN_CLASS)) {
		        wComment("in one of the following developer areas you can manually add a '" + VisibilityDirective.PUBLISHED.getName() + "' section");
		    }
			// there are no published members in this class, but we still generate the corresponding developer areas in order to be able to manually add a 'published' section
		    if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_FIELDS_IN_CLASS)) {
                // CHQDG-146
                bDA(developerAreaIdPublishedFieldsInClass);
                eDA();
            }
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PUBLISHED_MEMBERS_IN_CLASS)) {
				bDA(developerAreaIdPublishedMembersInClass);
			    eDA();
			}
		}
	}

	/**
	 * @param ts
	 */
	private void wPublicMembers(TargetSection ts) {
	    String developerAreaIdPublicFieldsInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_PUBLIC_FIELDS_IN_CLASS.getId()).toString();
        String developerAreaIdPublicMembersInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_PUBLIC_MEMBERS_IN_CLASS.getId()).toString();
        
		wNL(VisibilityDirective.PUBLIC.getName()).indent(getGenerationGroupOptions().getIndentationBeforeMembers());
			for (final Type type : clazz.getPublicNestedTypes()) {
				WriterI writer = getTransformationTarget().getWriterInstance(type);
				if (writer != null) writer.transform(ts);
			}
			
			// CHQDG-152
			Set<Field> publicFields = clazz.getPublicFields();
			Set<Method> publicMethods = clazz.getPublicMethods();
			Set<Property> publicProperties = clazz.getPublicProperties();
			
			
			// --- fields
			for (final Field field : publicFields) {
				WriterI writer = getTransformationTarget().getWriterInstance(field);
				if (writer != null) writer.transform(ts);
			}
			if (publicMethods.size() > 0 || publicProperties.size() > 0) {
                if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PUBLIC_FIELDS_IN_CLASS)) {
                    bDA(developerAreaIdPublicFieldsInClass);
                    eDA();
                }
            }
			
			// --- methods
			for (final Method method : publicMethods) {
                WriterI writer = getTransformationTarget().getWriterInstance(method);
                if (writer != null) writer.transform(ts);
            }
			
			// --- properties
			for (final Property property : publicProperties) {
                WriterI writer = getTransformationTarget().getWriterInstance(property);
                if (writer != null) writer.transform(ts);
            }
			
			
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PUBLIC_MEMBERS_IN_CLASS)) {
				bDA(developerAreaIdPublicMembersInClass);
			    eDA();
			}
        outdent(getGenerationGroupOptions().getIndentationBeforeMembers()).wNL();
	}

	/**
	 * @param ts
	 */
	private void wProtectedMembers(TargetSection ts) {
	    String developerAreaIdProtectedFieldsInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_PROTECTED_FIELDS_IN_CLASS.getId()).toString();
        String developerAreaIdProtectedMembersInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_PROTECTED_MEMBERS_IN_CLASS.getId()).toString();
        
		wNL(VisibilityDirective.PROTECTED.getName()).indent(getGenerationGroupOptions().getIndentationBeforeMembers());
			for (final Type type : clazz.getProtectedNestedTypes()) {
				WriterI writer = getTransformationTarget().getWriterInstance(type);
				if (writer != null) writer.transform(ts);
			}
			
			// CHQDG-152
            Set<Field> protectedFields = clazz.getProtectedFields();
            Set<Field> strictProtectedFields = clazz.getStrictProtectedFields();
            Set<Method> protectedMethods = clazz.getProtectedMethods();
            Set<Method> strictProtectedMethods = clazz.getStrictProtectedMethods();
            Set<Property> protectedProperties = clazz.getProtectedProperties();
            Set<Property> strictProtectedProperties = clazz.getStrictProtectedProperties();
            
            
            // --- fields
            for (final Field field : protectedFields) {
                WriterI writer = getTransformationTarget().getWriterInstance(field);
                if (writer != null) writer.transform(ts);
            }
            if (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
                // write strict protected fields to the same protected section as the non-strict members, since we are generating for an older Delphi version
                for (final Field field : strictProtectedFields) {
                    WriterI writer = getTransformationTarget().getWriterInstance(field);
                    if (writer != null) writer.transform(ts);
                }
            }
            if (protectedMethods.size() > 0 || protectedProperties.size() > 0 ||
                (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07) && (strictProtectedMethods.size() > 0 || strictProtectedProperties.size() > 0))) {
                if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PROTECTED_FIELDS_IN_CLASS)) {
                    bDA(developerAreaIdProtectedFieldsInClass);
                    eDA();
                }
            }
            
            // --- methods
            for (final Method method : protectedMethods) {
                WriterI writer = getTransformationTarget().getWriterInstance(method);
                if (writer != null) writer.transform(ts);
            }
            if (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
                // write strict protected methods to the same protected section as the non-strict members, since we are generating for an older Delphi version
                for (final Method method : strictProtectedMethods) {
                    WriterI writer = getTransformationTarget().getWriterInstance(method);
                    if (writer != null) writer.transform(ts);
                }
            }
            
            // --- properties
            for (final Property property : protectedProperties) {
                WriterI writer = getTransformationTarget().getWriterInstance(property);
                if (writer != null) writer.transform(ts);
            }
            if (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
                // write strict protected properties to the same protected section as the non-strict members, since we are generating for an older Delphi version
                for (final Property property : strictProtectedProperties) {
                    WriterI writer = getTransformationTarget().getWriterInstance(property);
                    if (writer != null) writer.transform(ts);
                }
            }
			
			if (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
				// write strict protected members to the same protected section as the non-strict members, since we are generating for an older Delphi version
				for (final Member member : clazz.getStrictProtectedMembers()) {
					WriterI writer = getTransformationTarget().getWriterInstance(member);
					if (writer != null) writer.transform(ts);
				}
			}
			
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PROTECTED_MEMBERS_IN_CLASS)) {
				bDA(developerAreaIdProtectedMembersInClass);
			    eDA();
			}
        outdent(getGenerationGroupOptions().getIndentationBeforeMembers()).wNL();
	}

	/**
	 * @param ts
	 */
	private void wPrivateMembers(TargetSection ts) {
	    String developerAreaIdPrivateFieldsInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_PRIVATE_FIELDS_IN_CLASS.getId()).toString();
        String developerAreaIdPrivateMembersInClass = new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_PRIVATE_MEMBERS_IN_CLASS.getId()).toString();
        
		wNL(VisibilityDirective.PRIVATE.getName()).indent(getGenerationGroupOptions().getIndentationBeforeMembers());
			for (final Type type : clazz.getPrivateNestedTypes()) {
				WriterI writer = getTransformationTarget().getWriterInstance(type);
				if (writer != null) writer.transform(ts);
			}
			
			// CHQDG-152
            Set<Field> privateFields = clazz.getPrivateFields();
            Set<Field> strictPrivateFields = clazz.getStrictPrivateFields();
            Set<Method> privateMethods = clazz.getPrivateMethods();
            Set<Method> strictPrivateMethods = clazz.getStrictPrivateMethods();
            Set<Property> privateProperties = clazz.getPrivateProperties();
            Set<Property> strictPrivateProperties = clazz.getStrictPrivateProperties();
            
            
            // --- fields
            for (final Field field : privateFields) {
                WriterI writer = getTransformationTarget().getWriterInstance(field);
                if (writer != null) writer.transform(ts);
            }
            if (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
                // write strict private fields to the same private section as the non-strict members, since we are generating for an older Delphi version
                for (final Field field : strictPrivateFields) {
                    WriterI writer = getTransformationTarget().getWriterInstance(field);
                    if (writer != null) writer.transform(ts);
                }
            }
            if (privateMethods.size() > 0 || privateProperties.size() > 0 ||
                (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07) && (strictPrivateMethods.size() > 0 || strictPrivateProperties.size() > 0))) {
                if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PRIVATE_FIELDS_IN_CLASS)) {
                    bDA(developerAreaIdPrivateFieldsInClass);
                    eDA();
                }
            }
            
            // --- methods
            for (final Method method : privateMethods) {
                WriterI writer = getTransformationTarget().getWriterInstance(method);
                if (writer != null) writer.transform(ts);
            }
            if (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
                // write strict private methods to the same private section as the non-strict members, since we are generating for an older Delphi version
                for (final Method method : strictPrivateMethods) {
                    WriterI writer = getTransformationTarget().getWriterInstance(method);
                    if (writer != null) writer.transform(ts);
                }
            }
            
            // --- properties
            for (final Property property : privateProperties) {
                WriterI writer = getTransformationTarget().getWriterInstance(property);
                if (writer != null) writer.transform(ts);
            }
            if (!getGenerationGroupOptions().getDelphiVersion().isNewerVersionThan(DelphiVersionEnum.DELPHI_07)) {
                // write strict private properties to the same private section as the non-strict members, since we are generating for an older Delphi version
                for (final Property property : strictPrivateProperties) {
                    WriterI writer = getTransformationTarget().getWriterInstance(property);
                    if (writer != null) writer.transform(ts);
                }
            }
			
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_PRIVATE_MEMBERS_IN_CLASS)) {
				bDA(developerAreaIdPrivateMembersInClass);
			    eDA();
			}
	    outdent(getGenerationGroupOptions().getIndentationBeforeMembers()).wNL();
	}

	/**
	 * @param ts
	 */
	protected void wTypeClause(TargetSection ts) {
		
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.CLASS_DECLARATION)) {
			// CHQDG-178
		    bDA(new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.CLASS_DECLARATION.getId()).toString());
	    }
		
	    String packed = "";
        if (this.clazz.getPacked() != null && this.clazz.getPacked()) {
            packed = Keyword.PACKED.getName() + " ";
        }
        
		w(getTypeName(), clazz.getTypeParametersAsString(), " = ", packed, "class");
		if (clazz.getDirective() != null) {
			w(" ", clazz.getDirective().getDirective().getName(), " ");
		}
		
		StringBuilder parentString = new StringBuilder();
		if (clazz.getParent() != null) {
			wUse(clazz.getParent().getDeclaringUnit().getQualifiedName());
			parentString.append(clazz.getParent().getTypeAsString()).append(clazz.getParent().getTypeParametersAsString());
		}
		
		StringBuilder interfacesString = new StringBuilder();
		if (clazz.getInterfaces().size() > 0) {
			// at least one interface is implemented, construct the string that represents the implemented interfaces
			String comma = "";
			for (InterfaceTypeI interfaceType : clazz.getInterfaces()) {
				wUse(interfaceType.getDeclaringUnit().getQualifiedName());
				interfacesString.append(comma).append(interfaceType.getTypeAsString());
				comma = ", ";
			}
		}

		String commaBeforeInterfaces = (parentString.length() > 0 && interfacesString.length() > 0) ? ", " : "";
		
		if (parentString.length() > 0 || interfacesString.length() > 0) {
		    w("(", parentString, commaBeforeInterfaces, interfacesString, ")");
		}
		
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.CLASS_DECLARATION)) {
			// CHQDG-178
		    eDA();
	    }
	}
	
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		wTypeClause(ts);
		wNL();indent(getGenerationGroupOptions().getIndentationBeforeVisibilityDirective());
		    if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.BEFORE_PRIVATE_SECTION_IN_CLASS)) {
		        // CHQDG-145 (mmt 10-Jul-2019)
    		    bDA(new StringBuffer(clazz.getName()).append(".").append(DeveloperAreaTypeEnum.BEFORE_PRIVATE_SECTION_IN_CLASS.getId()).toString());
    		    eDA();
		    }
			wPrivateMembers(ts);
			wStrictPrivateMembers(ts);
			wProtectedMembers(ts);
			wStrictProtectedMembers(ts);
			wPublicMembers(ts);
			wPublishedMembers(ts);
		outdent(getGenerationGroupOptions().getIndentationBeforeVisibilityDirective());wNL("end;");
		wNL();
	}

	@Override
	public String getTypeName() {
		return clazz.getNonReservedWordName();
	}
}
