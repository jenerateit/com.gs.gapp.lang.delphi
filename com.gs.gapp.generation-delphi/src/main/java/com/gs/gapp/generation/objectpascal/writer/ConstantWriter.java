/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.objectpascal.Constant;

/**
 * @author mmt
 *
 */
public class ConstantWriter extends DelphiWriter {
	
	@ModelElement
	private Constant constant;

	/**
	 * 
	 */
	public ConstantWriter() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		bDA(constant.getDeveloperAreaKey());
		wNL(constant.getNonReservedWordName(), getType(), " = ", getConstantExpression(ts), ";");
		eDA();
	}
	
	/**
	 * @param ts
	 * @return
	 */
	protected String getConstantExpression(TargetSection ts) {
		return constant.getExpression();
	}
	
	/**
	 * @return
	 */
	protected String getType() {
		return "";
	}
	
	@Override
	protected void wDeveloperDocumentation(TargetSection ts) { /* nothing written here for Constant */ }
    
    @Override
    protected void wDeveloperDocumentationHeader(TargetSection ts) { /* nothing written here for Constant */ }
}
