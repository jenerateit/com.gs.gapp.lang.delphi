package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.objectpascal.type.ConstructedType;

/**
 * @author mmt
 *
 */
public class ConstructedTypeWriter extends TypeWriter {

	private ConstructedType constructedType;
	
	/**
	 * @param ts
	 */
	protected void wTypeClause(TargetSection ts) {
		
		throw new WriterException("the method wTypeClause() in ConstructedTypeWriter is not yet implemented");
	}
	
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		wTypeClause(ts);
	}

	@Override
	public String getTypeName() {
		return constructedType.getTypeAsString();
	}
}
