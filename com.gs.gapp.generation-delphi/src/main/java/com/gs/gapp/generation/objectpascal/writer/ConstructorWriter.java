/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.member.Constructor;



/**
 * @author mmt
 *
 */
public class ConstructorWriter extends ProcedureWriter {

	@ModelElement
	private Constructor constructor;
	
	@Override
	protected void wMethodKeyword() {
		w("constructor ");
	}
}
