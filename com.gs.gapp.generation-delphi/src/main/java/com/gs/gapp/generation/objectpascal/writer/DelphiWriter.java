package com.gs.gapp.generation.objectpascal.writer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.basic.writer.ModelElementWriter;
import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.objectpascal.GenerationGroupDelphiOptions;
import com.gs.gapp.generation.objectpascal.target.DelphiTarget;
import com.gs.gapp.generation.objectpascal.target.DelphiTargetDocument;
import com.gs.gapp.generation.objectpascal.target.DelphiTargetSectionEnum;
import com.gs.gapp.metamodel.basic.GeneratorInfo;
import com.gs.gapp.metamodel.objectpascal.DelphiVersionEnum;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.UnitUsage;
import com.gs.gapp.metamodel.objectpascal.type.ConstructedType;
import com.gs.gapp.metamodel.objectpascal.type.Pointer;
import com.gs.gapp.metamodel.objectpascal.type.Procedural;
import com.gs.gapp.metamodel.objectpascal.type.structured.Record;
import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;
import com.gs.vd.metamodel.delphi.predef.System_SysUtils_Unit;
import com.gs.vd.metamodel.delphi.predef.System_Unit;
import com.gs.vd.metamodel.delphi.predef.System_Variants_Unit;


/**
 * @author hr
 *
 */
public abstract class DelphiWriter extends ModelElementWriter {

    private final static String SYSTEM_UNIT_NAME = System_Unit.UNIT.getQualifiedName();
    
    public final static String DEVELOPER_DOCUMENTATION_ASTERISKS = "**********";
    public final static int MAX_COMMENT_LINE_LENGTH = 250;
    public final static int BLANK_NUMERIC = ' ';
	
	@ModelElement
	private ObjectPascalModelElement modelElement;
	
	private GenerationGroupDelphiOptions generationGroupOptions;
	
	/**
	 * @param ts
	 * @return
	 */
	protected DelphiWriter wDocumentation(TargetSection ts) {
	    final StringBuilder documentation = new StringBuilder();
		if (StringTools.isText(modelElement.getBody())) {
			documentation.append(getWrappedText(modelElement.getBody(), ""));
		}
		
		if (modelElement instanceof Unit) {
            // --- add information about the generator and the model, to help users understand how things are related to each other
            Unit unit = (Unit) modelElement;
            StringBuilder generatorInfo = GeneratorInfo.formatGeneratorInfo(unit);
            if (generatorInfo != null && generatorInfo.length() > 0) {
                if (documentation.length() > 0) {
                    documentation.append(System.lineSeparator()).append(generatorInfo);
                } else {
                    documentation.append(generatorInfo);
                }
            }
        }
		
		String docString = documentation.toString();
		if (StringTools.isText(docString)) {
		    wBlockCommentForDeveloper(documentation.toString());
		}
        
		return this;
	}

	/**
	 * @return
	 */
	public DelphiWriter wBegin() {
		wNL("begin").indent(getGenerationGroupOptions().getIndentationAfterBegin());
		return this;
	}
	
	/**
	 * @return
	 */
	public DelphiWriter wEnd() {
		outdent(getGenerationGroupOptions().getIndentationAfterBegin()).wNL("end;");
		return this;
	}
	
	public DelphiWriter wForLoop(CharSequence... text) {
		w("for ").w(text).wNL(" do");
		wBegin();
		return this;
	}
	
	public DelphiWriter wEndForLoop() {
		wEnd();
		return this;
	}
	
	public DelphiWriter wIf(CharSequence... text) {
		w("if (").w(text).wNL(") then");
		wBegin();
		return this;
	}
	
	public DelphiWriter wElseIf(CharSequence... text) {
		outdent().wNL("end");
		w("else if (").w(text).wNL(") then");
		wBegin();
		return this;
	}

	public DelphiWriter wEndIf() {
		wEnd();
		return this;
	}
	
	public DelphiWriter wTry() {
		wNL("try").indent();
		return this;
	}
	
	public DelphiWriter wExcept() {
		outdent().wNL("except").indent();
		return this;
	}
	
	public DelphiWriter wFinally() {
		outdent().wNL("finally").indent();
		return this;
	}
	
	public DelphiWriter wTryEnd() {
		outdent().wNL("end;");
		return this;
	}
	
	/**
	 * @param type
	 * @return
	 */
	public DelphiWriter wUse(Type type) {
		final TargetSection currentTargetSection = getDelphiTarget().getCurrentTransformationSection();
		return wUse(type, currentTargetSection);
	}
		
	public DelphiWriter wUse(Type type, DelphiTargetSectionEnum tsEnumEntry) {
		
        Unit declaringUnit = type.getDeclaringUnit();
        
		if (declaringUnit != null && declaringUnit != System_Unit.UNIT) {
			DelphiTargetSectionEnum usesSectionEnumEntry = DelphiTargetSectionEnum.getUsesSection(tsEnumEntry.getTargetSection());
			if (usesSectionEnumEntry != null) {
		        wUse(declaringUnit.getQualifiedName(), usesSectionEnumEntry);
			} else {
				throw new WriterException("Transformtion section '" + tsEnumEntry.getTargetSection() + "' is not valid", getTransformationTarget(), this);
			}	
		}
		
		return this;
	}
	
	/**
	 * @param type
	 * @param targetSection
	 * @return
	 */
	public DelphiWriter wUse(Type type, TargetSection targetSection) {
		
		wUse(type, DelphiTargetSectionEnum.get(targetSection));
		return this;
	}
	
	public DelphiWriter wUse(UnitUsage unitUsage) {
		final TargetSection currentTargetSection = getDelphiTarget().getCurrentTransformationSection();
	    wUse(unitUsage, DelphiTargetSectionEnum.getUsesSection(currentTargetSection));
	    return this;
	}
	
	public DelphiWriter wUse(UnitUsage unitUsage, DelphiTargetSectionEnum tsEnumEntry) {
	    wUse(unitUsage.getQualifiedName(), tsEnumEntry);
	    return this;
	}
	
	public DelphiWriter wUse(Unit unit) {
		final TargetSection currentTargetSection = getDelphiTarget().getCurrentTransformationSection();
	    wUse(unit, DelphiTargetSectionEnum.getUsesSection(currentTargetSection));
	    return this;
	}
	
	public DelphiWriter wUse(Unit unit, DelphiTargetSectionEnum tsEnumEntry) {
	    wUse(unit.getQualifiedName(), tsEnumEntry);
	    return this;
	}
	
	public DelphiWriter wUse(String unitName) {
		final TargetSection currentTargetSection = getDelphiTarget().getCurrentTransformationSection();
		return wUse(unitName, DelphiTargetSectionEnum.getUsesSection(currentTargetSection));
	}
	
	public DelphiWriter wUse(String unitName, boolean removeCommaFromUnitName) {
        final TargetSection currentTargetSection = getDelphiTarget().getCurrentTransformationSection();
        return wUse(unitName, DelphiTargetSectionEnum.getUsesSection(currentTargetSection), removeCommaFromUnitName);
    }
	
	/**
	 * @param unitName
	 * @param tsEnumEntry
	 * @return
	 */
	public DelphiWriter wUse(String unitName, DelphiTargetSectionEnum tsEnumEntry) {
	    return wUse(unitName,
	                tsEnumEntry,
	                true);  // true => remove comma from unit name
	}
	
	/**
	 * @param unitName
	 * @param tsEnumEntry
	 * @param removeCommaFromUnitName
	 * @return
	 */
	public DelphiWriter wUse(String unitName, DelphiTargetSectionEnum tsEnumEntry, boolean removeCommaFromUnitName) {
		if (tsEnumEntry == null) throw new NullPointerException("parameter 'tsEnumEntry' must not be null");
		if (DelphiTargetSectionEnum.isSectionForUses(tsEnumEntry.getTargetSection()) == false) throw new IllegalArgumentException("target section enum entry '" + tsEnumEntry + "' cannot be used for writing a use statement");
		
		// --- first we have to do some preliminary tasks
		if (getGenerationGroupOptions().getDelphiVersion().isSameOrOlderVersionThan(DelphiVersionEnum.DELPHI_07)) {
            if (unitName.contains(".")) {
                // a use-clause with qualified namespaces are not allowed in Delphi 7 or older versions
                unitName = unitName.substring(unitName.lastIndexOf(".")+1);
                unitName = unitName.substring(unitName.lastIndexOf(".")+1);
            }
        }
		
		String unitNameWithCommaStripped = unitName.replace(",", "");
		boolean isCompilerDirective = unitNameWithCommaStripped.trim().contains("{");
		
		if (unitNameWithCommaStripped.trim().equals(SYSTEM_UNIT_NAME)) {
		    // The "System" unit never has to be in the uses section since it is automatically added by Delphi.
		    // This is similar to the Java package "java.lang" that never needs an import statement.
		    return this;
		}
		
		
		// --- from here on the real work begins
		if (!Unit.class.isInstance(getTransformationTarget().getBaseElement()) ||
				Unit.class.cast(getTransformationTarget().getBaseElement()).getQualifiedName().compareToIgnoreCase(unitNameWithCommaStripped) != 0) {
			
		    @SuppressWarnings("unused")
            List<String> previousUses = null;
		    List<String> newUses = null;
		    DelphiTargetDocument previousTargetDocument = getDelphiTarget().getPreviousTargetDocument();
		    if (previousTargetDocument != null) {
                previousUses = previousTargetDocument.getUses(tsEnumEntry);
		    }
		    
			DelphiTargetDocument newTargetDocument = getDelphiTarget().getNewTargetDocument();
			if (newTargetDocument != null) {
			    newUses = newTargetDocument.getUses(tsEnumEntry);
			}
			
			if (!isCompilerDirective && tsEnumEntry == DelphiTargetSectionEnum.IMPLEMENTATION_USES && newTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES).contains(unitNameWithCommaStripped)) {
				// do nothing if there is already a "use" in the interface use section
			} else {
			    @SuppressWarnings("unused")
                String targetPath = getDelphiTarget().getTargetPath().toString();
				if (newUses == null || newUses.isEmpty()) {
//				    System.out.println(targetPath + ", writing use '" + unitNameWithCommaStripped + "', newUses:" + newUses);
					wNL(tsEnumEntry.getTargetSection(), "uses");indent(tsEnumEntry.getTargetSection(), getGenerationGroupOptions().getIndentationAfterUses());
					if (removeCommaFromUnitName) {
					    w(tsEnumEntry.getTargetSection(), unitNameWithCommaStripped.trim());
					} else {
					    w(tsEnumEntry.getTargetSection(), unitName.trim());					    
					}
					
					// now keep some information about writing of the current use to be used for the writing of the next use
					newTargetDocument.setPreviousUseWrittenAsIs(tsEnumEntry, !removeCommaFromUnitName);
					newTargetDocument.setWrittenCompilerDirectiveInUseSection(tsEnumEntry, isCompilerDirective);
					newTargetDocument.getCurrentUses(tsEnumEntry).clear();  // clear current uses in order to be able to rebuild them
				} else if (!isCompilerDirective && newUses.contains(unitNameWithCommaStripped)) {
					// do nothing, the unit name is already present
//				    System.out.println(targetPath + ", newUses contains '" + unitName + "', newUses:" + newUses);
				} else {
				    // find out, whether a manually commented use is there
				    boolean doWriteNewUse = true;
				    if (!isCompilerDirective) {
    				    for (String aUse : newUses) {
    				        if (aUse.endsWith(" " + unitNameWithCommaStripped) || aUse.endsWith("/" + unitNameWithCommaStripped)) {
    				            doWriteNewUse = false;
    				            break;
    				        }
    				    }
				    }
				    
				    if (doWriteNewUse) {
//    				    System.out.println(targetPath + ", writing use '" + unitName + "', newUses:" + newUses);
    				    if (newTargetDocument.isWrittenCompilerDirectiveInUseSection(tsEnumEntry) || !removeCommaFromUnitName || newTargetDocument.isPreviousUseWrittenAsIs(tsEnumEntry)) {
    				        wNL(tsEnumEntry.getTargetSection());
    				    } else {
    				        wNL(tsEnumEntry.getTargetSection(), ",");
    				    }
    				    
    				    if (removeCommaFromUnitName) {
                            w(tsEnumEntry.getTargetSection(), unitNameWithCommaStripped.trim());
                        } else {
                            w(tsEnumEntry.getTargetSection(), unitName.trim());                     
                        }

                        // now keep some information about writing of the current use to be used for the writing of the next use
    				    newTargetDocument.setPreviousUseWrittenAsIs(tsEnumEntry, !removeCommaFromUnitName);
                        newTargetDocument.setWrittenCompilerDirectiveInUseSection(tsEnumEntry, isCompilerDirective);
                        newTargetDocument.getCurrentUses(tsEnumEntry).clear();  // clear current uses in order to be able to rebuild them
				    }
				}
			}
		}
		return this;
	}
	
	private DelphiTarget getDelphiTarget() {
		TargetI<?> t = getTextTransformationTarget();
		if (DelphiTarget.class.isInstance(t)) {
			return DelphiTarget.class.cast(t);
		} else {
			throw new WriterException("The target used by this writer is not of type DelphiTarget", getTransformationTarget(), this);
		}
	}
	
	public DelphiWriter wBlockCommentForDocumentation(String... comments) {
		return wBlockCommentForDocumentation(null, comments);
	}
	
	/**
	 * @param ts
	 * @param comments
	 * @return
	 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Fundamental_Syntactic_Elements#Comments_and_Compiler_Directives">Delphi XE7 - Comments and Compiler Directives</a>
	 */
	public DelphiWriter wBlockCommentForDocumentation(TargetSection ts, String... comments) {
		if (comments != null && comments.length > 0) {
			w("(* ");
			int ii=0;
			for (String c : comments) {
				if (ii == comments.length-1) {
					if (c == null || c.trim().length() == 0) continue;
				}
				for (CharSequence s : getLines(c)) {
					if (ts != null) { 
						wNL(ts, "  ", s);
					} else {
						wNL("  ", s);
					}
				}
				ii++;
			}
			wNL(" *)");
		}
		return this;
	}
	
	public DelphiWriter wBlockCommentForDeveloper(String... comments) {
		return wBlockCommentForDocumentation(null, comments);
	}
	
	/**
	 * @param ts
	 * @param comments
	 * @return
	 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Fundamental_Syntactic_Elements#Comments_and_Compiler_Directives">Delphi XE7 - Comments and Compiler Directives</a>
	 */
	public DelphiWriter wBlockCommentForDeveloper(TargetSection ts, String... comments) {
		if (comments != null && comments.length > 0) {
			w("{ ");
			int ii=0;
			for (String c : comments) {
				if (ii == comments.length-1) {
					if (c == null || c.trim().length() == 0) continue;
				}
				for (CharSequence s : getLines(c)) {
					if (ts != null) { 
						wNL(ts, "  ", s);
					} else {
						wNL("  ", s);
					}
				}
				ii++;
			}
			wNL(" }");
		}
		return this;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.writer.ModelElementWriter#getGenerationGroupOptions()
	 */
	@Override
	public GenerationGroupDelphiOptions getGenerationGroupOptions() {
		if (this.generationGroupOptions == null) {
			this.generationGroupOptions = new GenerationGroupDelphiOptions(this);
		}
		
		return this.generationGroupOptions;
	}
	
	
	/**
	 * @param devAreaType the developer area type for which it is checked, whether the currently generating target allowing to use it
	 * @return true, if the given developer area type is active according to the settings in the target
	 */
	protected boolean isDeveloperAreaTypeActive(DeveloperAreaTypeEnum devAreaType) {
		if (devAreaType == null) throw new NullPointerException("parameter 'devArea' must not be null");
		
		boolean result = false;
		
		switch (this.getGenerationGroupOptions().getDeveloperAreaRule()) {
		case ALL:
			result = true;
			break;
		case DEFAULT:
			if (getTransformationTarget() instanceof DelphiTarget) {
				DelphiTarget delphiTarget = (DelphiTarget) getTransformationTarget();
				result = delphiTarget.getActiveDeveloperAreaTypes().contains(devAreaType);
			}
			break;
		case NONE:
			result = false;
			break;
		default:
			throw new WriterException("unhandled developer area rule '" + this.getGenerationGroupOptions().getDeveloperAreaRule() + "' found", getTransformationTarget(), this);
		}
		
		return result;
	}

	@Override
	public void transform(TargetSection ts) throws WriterException {
		wDeveloperDocumentationHeader(ts);
		wDeveloperDocumentation(ts);
		wDocumentation(ts);
	}

	/**
     * This method writes a developer documentation, which is content that got
     * manually added to a generated source file.
     * Note that this method only writes those lines that
     * start with "//", thus represent comments.
     * 
     * @param ts
     */
    protected void wDeveloperDocumentation(TargetSection ts) {
    	if (this.modelElement.isDeveloperDocumentationGenerated()) {
	    	if (this.modelElement.getDeveloperDocumentation().size() > 0) {
	    		for (String devDoc : this.modelElement.getDeveloperDocumentation()) {
	    			String trimmedDevDoc = devDoc.trim();
	    			if (trimmedDevDoc.startsWith("//") || trimmedDevDoc.length() == 0) {
	    				wNL(trimmedDevDoc);
	    			}
	    		}
	    	} else {
	    		wNL(DelphiTargetDocument.DEVDOC_HINT);
	    	}
    	}
	}
    
    /**
     * 
     */
    protected void wDeveloperDocumentationHeader(TargetSection ts) {
    	if (this.modelElement.isDeveloperDocumentationGenerated()) {
    	    wNL("// ", DEVELOPER_DOCUMENTATION_ASTERISKS, " [", this.modelElement.getDeveloperDocumentationHeader(), "] ", DEVELOPER_DOCUMENTATION_ASTERISKS);
    	}
    }
    
	/* (non-Javadoc)
	 * @see org.jenerateit.writer.AbstractTextWriter#getTabSize()
	 */
	@Override
	protected int getTabSize() {
        return 2;  // default indentation for Delphi is 2 characters
	}
	
	/**
	 * @param type
	 * @return
	 */
	/**
	 * @param type
	 * @return
	 */
	protected String getDefaultValueForType(Type type) {
	    String result = null;
	    
        if (type == com.gs.vd.metamodel.delphi.predef.system.Boolean.TYPE ||
            type == com.gs.vd.metamodel.delphi.predef.system.ByteBool.TYPE ||
            type == com.gs.vd.metamodel.delphi.predef.system.WordBool.TYPE ||
            type == com.gs.vd.metamodel.delphi.predef.system.LongBool.TYPE ||
            type instanceof com.gs.gapp.metamodel.objectpascal.type.simple.Boolean) {
            // here we need a special initialization value, the compiler would neither accept a Null nor a nil
            result = "False";
        } else if (type == com.gs.vd.metamodel.delphi.predef.system.Integer.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.Int64.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.Cardinal.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.UInt64.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.ShortInt.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.SmallInt.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.Byte.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.Word.TYPE) {
            result = "0";
        } else if (type == com.gs.vd.metamodel.delphi.predef.system.TDate.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.TTime.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.TDateTime.TYPE) {
            result = "0";
        } else if (type == com.gs.vd.metamodel.delphi.predef.system.Currency.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.Double.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.Single.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.Extended.TYPE) {
            result = "0";
        } else if (type == com.gs.vd.metamodel.delphi.predef.system.string.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.AnsiString.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.ShortString.TYPE ||
                   type == com.gs.vd.metamodel.delphi.predef.system.WideString.TYPE ||
                   type instanceof com.gs.gapp.metamodel.objectpascal.type.string.AnsiString ||
                   type instanceof com.gs.gapp.metamodel.objectpascal.type.string.ShortString ||
                   type instanceof com.gs.gapp.metamodel.objectpascal.type.string.StringType ||
                   type instanceof com.gs.gapp.metamodel.objectpascal.type.string.WideString) {
            result = "EmptyStr";
            wUse(System_SysUtils_Unit.UNIT);  // otherwise using the 'EmptyStr' won't compile
        } else {
            if (type instanceof Pointer) {
                result = "Nil";
            } else if (type instanceof Record) {
            	// see https://stackoverflow.com/questions/38393309/delphi-default-keyword-with-record-types-in-older-delphi-versions
            	result = "Default(" + type.getName() + ")";
            } else if (type instanceof StructuredType ||
                type instanceof ConstructedType ||
                type instanceof Procedural) {
                result = "Nil";  // asked Eric Schlegel (highQ), he said he would use "Nil" (mmt 19-Jul-2019)
            } else {
                result = "Null";
                wUse(System_Variants_Unit.UNIT);  // otherwise using the 'Null' command won't compile
            }
        }
        
        return result;
	}
	
	private class TextSegment {
		private final int start;
		private final int end;
		
		TextSegment(int start, int end) {
			this.start = start;
			this.end = end;
		}

		/**
		 * @return the start
		 */
		public int getStart() {
			return start;
		}

		/**
		 * @return the end
		 */
		public int getEnd() {
			return end;
		}
	}
	
	/**
	 * @param text
	 * @param prefixForNewLines
	 * @return
	 */
	protected StringBuilder getWrappedText(String text, final String prefixForNewLines) {
		StringBuilder result = new StringBuilder();
		
		if (text == null) {
			return result;
		}
		
		String[] lines = text.split("\\r?\\n");
		if (lines != null) {
			int lineCounter = 0;
			for (final String line : lines) {
				lineCounter++;
				String newLineOrBlank = lineCounter == lines.length ? "" : StringTools.NEWLINE;
				if (line.length() < MAX_COMMENT_LINE_LENGTH) {
					result.append(line == null ? "" : line).append(newLineOrBlank);
				} else {
					// --- 1. determine the coordinates of the text segments
					// --- 2. cut out the text segments and create new lines with them
					final ArrayList<TextSegment> textSegments = new ArrayList<>();
					textSegments.clear();
					
					final AtomicInteger indexStart = new AtomicInteger(0);
					final AtomicInteger indexEnd = new AtomicInteger(0);
					final AtomicInteger counter = new AtomicInteger(0);
					final int lineLength = (int) line.chars().count();
					
					final AtomicInteger previousCharacter = new AtomicInteger(0);
					line.chars().forEach( character -> {
						if (BLANK_NUMERIC == character && previousCharacter.get() != BLANK_NUMERIC) {
							if (counter.get() - indexStart.get() >= MAX_COMMENT_LINE_LENGTH && indexEnd.get() > indexStart.get()) {
								TextSegment textSegment = new TextSegment(indexStart.get(), indexEnd.get());
								textSegments.add(textSegment);
								indexStart.set(indexEnd.get()+1);
							} else {
								indexEnd.set(counter.get());
							}
						}
						counter.incrementAndGet();
						
						if (counter.get() == lineLength) {
							// --- end of line reached
							TextSegment textSegment = new TextSegment(indexStart.get(), lineLength);
							textSegments.add(textSegment);
						}
						
						previousCharacter.set(character);
					});
					
					textSegments.forEach(textSegment -> {
						String newLine = line.substring(textSegment.getStart(), textSegment.getEnd());
						result.append(textSegment.getStart() == 0 || prefixForNewLines == null ? "" : prefixForNewLines).append(newLine).append(StringTools.NEWLINE);
					});
				}
			}
		}
		
		return result;
	}
}
