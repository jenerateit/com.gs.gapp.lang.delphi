/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.member.Destructor;



/**
 * @author mmt
 *
 */
public class DestructorWriter extends ProcedureWriter {

	@ModelElement
	private Destructor destructor;
	
	@Override
	protected void wMethodKeyword() {
		w("destructor ");
	}
}
