package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration;
import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalTypeValue;



/**
 * @author mmt
 *
 */
public class EnumerationWriter extends TypeWriter {

	@ModelElement
	private Enumeration enumeration;

	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
	    String postfixEqual = " = (";
		wTypeName().w(postfixEqual);
		
		int additionalIndentation = getTypeName().length() + postfixEqual.length(); 
		
		String comma = "";
		for (OrdinalTypeValue value : enumeration.getValues()) {
			w(comma);
			if (comma.length() > 0)  wNL();
			w(value.getName(), value.getExplicitOrdinality() != null ? " = "+value.getExplicitOrdinality() : "");
			if (comma.length() == 0) {
				comma = ",";
				indent(additionalIndentation);
			}
		}
		if (comma.length() > 0) outdent(additionalIndentation);
		
		wNL(");");
	}
	
	@Override
	public String getTypeName() {
		return enumeration.getNonReservedWordName();
	}
}
