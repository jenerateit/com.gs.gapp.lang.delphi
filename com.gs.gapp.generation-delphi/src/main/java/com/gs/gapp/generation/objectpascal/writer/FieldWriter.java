/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.objectpascal.member.Field;


/**
 * @author hrr
 *
 */
public class FieldWriter extends MemberWriter {

	@ModelElement
	private Field field;

	/**
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		wField();
	}

	/**
	 * 
	 * @return
	 */
	public FieldWriter wField() {
		wUse(field.getType());
		TypeWriter tw = getTransformationTarget().getWriterInstance(field.getType(), TypeWriter.class);
		wNL(field.getNonReservedWordName(), ": ", tw.getTypeName(), ";");
		return this;
	}
}
