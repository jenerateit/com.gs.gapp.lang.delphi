package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.metamodel.objectpascal.type.structured.File;


/**
 * @author hr
 *
 */
public class FileWriter extends StructuredTypeWriter {

	@ModelElement
	private File fileType;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		wTypeName(); w(" = file"); if (fileType.getTypeStoredInFile() != null) w(" of ", fileType.getTypeStoredInFile().getName()); wNL(";");
	}
}
