package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.objectpascal.target.DelphiTargetSectionEnum;
import com.gs.gapp.metamodel.objectpascal.type.ForwardDeclaration;



/**
 * @author hr
 *
 */
public class ForwardDeclarationWriter extends TypeWriter {

	@ModelElement
	private ForwardDeclaration forwardDeclaration;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		DelphiTargetSectionEnum targetSectionEnum = DelphiTargetSectionEnum.get(ts);
		if (targetSectionEnum == DelphiTargetSectionEnum.INTERFACE_TYPE) {
			wNL(getTypeName(), " = class;");
		}
	}

	@Override
	public ForwardDeclarationWriter wTypeName() {
		wUse(forwardDeclaration);
		w(getTypeName());
		return this;
	}

	@Override
	public String getTypeName() {
		if (forwardDeclaration.getDeclaredClazz() != null) {
			return forwardDeclaration.getDeclaredClazz().getTypeAsString();
		} else if (forwardDeclaration.getDeclaredInterface() != null) {
			return forwardDeclaration.getDeclaredInterface().getTypeAsString();
		}
		
		throw new WriterException("tried to get type name for a forward declaration that neither has a class nor an interface defined");
	}
}
