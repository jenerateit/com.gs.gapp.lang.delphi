/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.objectpascal.member.Function;



/**
 * @author mmt
 *
 */
public class FunctionWriter extends MethodWriter {

	@ModelElement
	private Function function;
	
	

	@Override
	protected void wReturnType() {
		if (function.getReturnType() != null) {
			wUse(function.getReturnType());
			TypeWriter tw = getTransformationTarget().getWriterInstance(function.getReturnType(), TypeWriter.class);
			if (tw != null) {
				w(": ", tw.getTypeName(), ";");
			} else {
				throw new WriterException("function " + function.getName() + " has return type '" + function.getReturnType().getName() + "', but now writer could be found for the return type");
			}
		} else {
			super.wReturnType();
		}
	}
	
	@Override
	protected void wMethodKeyword() {
		w("function ");
	}

	/**
	 * 
	 */
	@Override
	protected void wMethodBody() {
		if (function.getDefaultImplementation().size() > 0) {
			// the default implementation has been set within a model element converter => use it here by simply writing it
			wDefaultImplementation();
		} else {
			if (function.getRelatedProperty() == null) {
			    wNL("Result := ", getDefaultValueForType(function.getReturnType()), ";");
			} else {
				super.wMethodBody();  // let the parent's method handle the logic for the property
			}
		}
	}
}
