package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;


/**
 * @author hr
 *
 */
public class InterfaceWriter extends StructuredTypeWriter {

	@ModelElement
	private Interface interf;
	
    /**
     * @param ts
     */
    private void wProperties(TargetSection ts) {
		for (final Property property : interf.getProperties()) {
			WriterI writer = getTransformationTarget().getWriterInstance(property);
			if (writer != null) writer.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	private void wMethods(TargetSection ts) {
		for (final Method method : interf.getMethods()) {
			WriterI writer = getTransformationTarget().getWriterInstance(method);
			if (writer != null) writer.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wTypeClause(TargetSection ts) {
		
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.INTERFACE_DECLARATION)) {
			// CHQDG-178
		    bDA(new StringBuffer(interf.getName()).append(".").append(DeveloperAreaTypeEnum.INTERFACE_DECLARATION.getId()).toString());
	    }
		
		w(getTypeName(), " = interface");
		if (interf.getParent() != null) {
			InterfaceWriter interfaceWriter = getTransformationTarget().getWriterInstance(interf.getParent(), InterfaceWriter.class);
			if (interfaceWriter != null) {
				wUse(interf.getParent().getDeclaringUnit().getQualifiedName());
				w("(", interfaceWriter.getTypeName(), ")");
			}
		} 
		if (interf.getGuid() != null && interf.getGuid().length() > 0) {
			wNL();
			wNL(interf.getGuid());
		}
		
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.INTERFACE_DECLARATION)) {
			// CHQDG-178
			eDA();
	    }
	}
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		wTypeClause(ts);
		wNLI();
			wMethods(ts);
			wProperties(ts);
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_MEMBERS_IN_INTERFACE)) {
				bDA(new StringBuffer(interf.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_MEMBERS_IN_INTERFACE.getId()).toString());
			    eDA();
			}
		wONL("end;");
		wNL();
	}

	@Override
	public String getTypeName() {
		return interf.getNonReservedWordName();
	}
}
