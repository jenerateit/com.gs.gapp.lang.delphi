package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.objectpascal.target.DelphiTargetSectionEnum;
import com.gs.gapp.metamodel.objectpascal.AttributeUsage;
import com.gs.gapp.metamodel.objectpascal.type.structured.Member;



/**
 * @author hr
 *
 */
public class MemberWriter extends DelphiWriter {

	@ModelElement
	private Member member;

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.objectpascal.writer.DelphiWriter#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		
		if (DelphiTargetSectionEnum.isInterfaceSection(ts)) {
		    
		    String developerAreaIdAttributeForMember = new StringBuffer(member.getDeveloperAreaKey()).append(".").append(DeveloperAreaTypeEnum.ATTRIBUTE_FOR_MEMBER.getId()).toString();
		    if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ATTRIBUTE_FOR_MEMBER)) {
		        bDA(developerAreaIdAttributeForMember);
		    }
		    
			if (!member.getAttributeUsages().isEmpty()) {
				String separator = "";
				for (AttributeUsage attributeUsage : member.getAttributeUsages()) {
					WriterI writer = getTransformationTarget().getWriterInstance(attributeUsage);
					if (writer != null) {
						w(separator);
						writer.transform(ts);
						separator = StringTools.NEWLINE;
					}
				}
				wNL();
			}
			
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ATTRIBUTE_FOR_MEMBER)) {
                eDA();
            }
		}
	}

	@Override
	protected void wDeveloperDocumentation(TargetSection ts) {
		if (DelphiTargetSectionEnum.isInterfaceSection(ts)) {
			// the interface section does not need a developer documentation for members
		} else {
			super.wDeveloperDocumentation(ts);
    	}
	}

	@Override
	protected void wDeveloperDocumentationHeader(TargetSection ts) {
		if (DelphiTargetSectionEnum.isInterfaceSection(ts)) {
			// the interface section does not need a developer documentation for members
		} else {
			super.wDeveloperDocumentationHeader(ts);
    	}
	}
	
	/**
	 * adapts the member name in case the member name is a reserved word in Delphi
	 * 
	 * @return the member name prepended with ampersand in case the member name is a reserved word in Delphi
	 */
	protected String getNonReservedWordMemberName() {
		return member.getNonReservedWordName();
	}
}
