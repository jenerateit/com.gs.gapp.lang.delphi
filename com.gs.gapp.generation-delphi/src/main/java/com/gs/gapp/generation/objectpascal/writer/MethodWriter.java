/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import java.util.Iterator;
import java.util.Set;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.objectpascal.target.DelphiTargetSectionEnum;
import com.gs.gapp.metamodel.objectpascal.MethodDirective;
import com.gs.gapp.metamodel.objectpascal.member.Field;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Parameter;
import com.gs.gapp.metamodel.objectpascal.member.Property;





/**
 * @author mmt
 *
 */
public abstract class MethodWriter extends MemberWriter {

	@ModelElement
	private Method method;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		if (ts == DelphiTargetSectionEnum.INTERFACE_TYPE.getTargetSection() ||
			ts == DelphiTargetSectionEnum.INTERFACE_METHOD.getTargetSection()) {
			writeMethodInterface(ts);
			
		} else if (ts == DelphiTargetSectionEnum.IMPLEMENTATION_TYPE.getTargetSection() ||
				   ts == DelphiTargetSectionEnum.IMPLEMENTATION_METHOD.getTargetSection()) {
			if (!method.getDirectives().contains(MethodDirective.ABSTRACT)) {
				writeMethodSignatureForImplementation(ts);
				indent(getGenerationGroupOptions().getIndentationBeforeBegin());
				    writeMethodImplementationWithOwnDeveloperAreas();
				outdent(getGenerationGroupOptions().getIndentationBeforeBegin());
			}
		}
	}
	
	/**
	 * Overwrite this method instead of writeMethodImplementation() when you want to
	 * fully control the developer areas being available within the method body.
	 */
	protected void writeMethodImplementationWithOwnDeveloperAreas() {
		bDA(method.getDeveloperAreaKey());
	    writeMethodImplementation();
	    eDA();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.objectpascal.writer.DelphiWriter#wDocumentation(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected DelphiWriter wDocumentation(TargetSection ts) {
		if (ts == DelphiTargetSectionEnum.INTERFACE_METHOD.getTargetSection() || ts == DelphiTargetSectionEnum.INTERFACE_TYPE.getTargetSection()) {
			String xmlDocumentation = method.getXmlDocumentation();
			if (xmlDocumentation != null && xmlDocumentation.length() > 0) {
			    wNL(getWrappedText(xmlDocumentation, "/// "));
			}
		}
		return this;
	}

	/**
	 * 
	 */
	protected void writeMethodInterface(TargetSection ts) {
		
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.METHOD_DECLARATION)) {
			// CHQDG-178
			bDA(method.getDeveloperAreaKey() + "." + DeveloperAreaTypeEnum.METHOD_DECLARATION.getId());
		}
		wMethodKeyword(); w(getNonReservedWordMemberName()); wMethodParameters(ts, method.getParameters()); wReturnType(); wDirectives(); wNL();
        if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.METHOD_DECLARATION)) {
        	// CHQDG-178
			eDA();
		}
	}
	
	protected void wReturnType() {
		w(";");
	}
	
	protected void writeMethodSignatureForImplementation(TargetSection ts) {
		String methodTypeQualifier = getMethodTypeQualifier();
		// note that in the implementation section, no directives (abstract, override, overload, ...) are going to be used
		wMethodKeyword(); w(methodTypeQualifier != null ? methodTypeQualifier+"." : ""); w(getNonReservedWordMemberName()); wMethodParameters(ts, method.getParameters()); wReturnType(); wNL();
	}
	
	/**
	 * You can overwrite this method to provide your own method implementation.
	 * Note that you cannot add additional developer areas by overwriting this method
	 * since the caller of this method controls this and enforces exactly _one_
	 * developer area. If you need more, you have to override writeMethodImplementationWithOwnDeveloperAreas()
	 * instead.
	 */
	protected void writeMethodImplementation() {
        wLocalDeclarations();
		wBegin();
		    wMethodBody();
		wEnd();
	}
	
	private String getMethodTypeQualifier() {
		if (method.getOwner() != null) {
			return method.getOwner().getQualifiedTypeName();
		} else {
			return null;
		}
	}
	
	/**
	 * 
	 */
	protected void wLocalDeclarations() {
		if (method.getLocalDeclarations().size() > 0) {
			wNL("var");
		    indent(getGenerationGroupOptions().getIndentationAfterBegin());
				for (String localDeclaration : method.getLocalDeclarations()) {
					wNL(localDeclaration);
				}
			outdent(getGenerationGroupOptions().getIndentationAfterBegin());
		}
	}
	
	protected abstract void wMethodKeyword();
	
	/**
	 * 
	 */
	protected void wDefaultImplementation() {
		for (String lineOfCode : method.getDefaultImplementation()) {
			wNL(lineOfCode);
		}
	}
	
	/**
	 * 
	 */
	protected void wMethodBody() {
		if (method.getDefaultImplementation().size() > 0) {
			// the default implementation has been set within a model element converter => use it here by simply writing it
			wDefaultImplementation();
		} else {
		    if (method.getRelatedProperty() != null) {
		    	// here we might have to generate code to set or read a field
		    	Property property = method.getRelatedProperty();
		    	if (property.getFieldOrMethodForRead() == method) {
		    		// --- read access
		    		if (property.getFieldOrMethodForWrite() != null && property.getFieldOrMethodForWrite() instanceof Field) {
						Field field = (Field) property.getFieldOrMethodForWrite();
		    			wNL("Result := ", field.getNonReservedWordName(), ";");
		    		}
		    	} else if (property.getFieldOrMethodForWrite() == method) {
		    		// --- write access
		    		if (property.getFieldOrMethodForRead() != null && property.getFieldOrMethodForRead() instanceof Field) {
						Field field = (Field) property.getFieldOrMethodForRead();
		    			wNL(field.getNonReservedWordName(), " := ", method.getParameter(0).getNonReservedWordName(), ";");
		    		}
		    	}
		    }
		}
	}
	
	/**
	 * @param params the list of parameters
	 */
	protected MethodWriter wMethodParameters(TargetSection ts, Set<Parameter> params) {
		if (!params.isEmpty()) {
			w("(");
			ParameterWriter pw = null;
			Iterator<Parameter> i = params.iterator();
			while (i.hasNext()) {
				pw = getTransformationTarget().getWriterInstance(i.next(), ParameterWriter.class);
				pw.wParameter(ts);
				if (i.hasNext()) {
					w("; ");  // in Delphi, parameters are separated by semicolons, in contrast to Java, where parameters are separated by commas
				}
			}
			w(")");
		}
		return this;
	}

	/**
	 * @return
	 */
	protected MethodWriter wDirectives() {
		method.getDirectives().stream().forEach(directive -> {
			switch (directive) {
			case ABSTRACT:
			case CDECL:
			case VIRTUAL:
			case OVERLOAD:
			case OVERRIDE:
			case PASCAL:
			case REGISTER:
			case SAFECALL:
			case STDCALL:
			case VARARGS:
			case REINTRODUCE:
				w( " ", directive.getDirective().getName(), ";");
				break;
			case EXTERNAL:
				// TODO here we have to write some extra code - see documentation of 'external'
				w( " ", directive.getDirective().getName(), ";");
				break;
			case DEPRECATED:
			    w( " ", directive.getDirective().getName(), ";");
                break;
                
			default:
				throw new WriterException("found a directive that is not yet handled in the method writer:" + directive);
			}
		});
		
		return this;
	}
}
