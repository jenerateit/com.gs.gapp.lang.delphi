package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalType;



/**
 * @author hr
 *
 */
public class OrdinalTypeWriter extends TypeWriter {

	@ModelElement
	private OrdinalType type;
	
}
