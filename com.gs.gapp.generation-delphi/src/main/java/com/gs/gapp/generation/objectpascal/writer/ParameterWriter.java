/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal.writer;


import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.target.DelphiTargetSectionEnum;
import com.gs.gapp.metamodel.objectpascal.AttributeUsage;
import com.gs.gapp.metamodel.objectpascal.member.Parameter;


/**
 * @author hrr
 *
 */
public class ParameterWriter extends DelphiWriter {

	@ModelElement
	private Parameter parameter;

	/**
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		if (ts == DelphiTargetSectionEnum.INTERFACE_TYPE.getTargetSection() ||
				ts == DelphiTargetSectionEnum.IMPLEMENTATION_METHOD.getTargetSection()) {
			wParameter(ts);
		}
	}

	/**
	 * 
	 * @return
	 */
	public ParameterWriter wParameter(TargetSection ts) {
		boolean canUseDefaultValue = true;
		
		if (parameter.isVar()) {
			canUseDefaultValue = false;
			w("var ");
			
		} else if (parameter.isConst()) {
			w((parameter.isRef() ? "[ref] " : ""), "const ");
			
		} else if (parameter.isOut()) {
			canUseDefaultValue = false;
			w("out ");
		}
		
		if (DelphiTargetSectionEnum.isInterfaceSection(ts)) {
			// parameter attributes 
			if (!parameter.getAttributeUsages().isEmpty()) {
				String separator = "";
				for (AttributeUsage attributeUsage : parameter.getAttributeUsages()) {
					WriterI writer = getTransformationTarget().getWriterInstance(attributeUsage);
					if (writer != null) {
						w(separator);
						writer.transform(ts);
						separator = " ";
					}
				}
				w(" ");  // a blank in between the param's attribute and its name
			}
		}

		w(getNonReservedWordParameterName(), ": ");
		
		wUse(parameter.getType());
		getTransformationTarget().getWriterInstance(parameter.getType(), TypeWriter.class).wTypeName();
		
		if (canUseDefaultValue && parameter.getDefaultValue() != null && parameter.getDefaultValue().length() > 0) {
			w(" = ", parameter.getDefaultValue());
		}
		
		return this;
	}
	
	/**
	 * 
	 * @return
	 */
	public ParameterWriter wParameterName() {
		w(getNonReservedWordParameterName());
		return this;
	}
	
	/**
	 * Getter for the name of the Parameter.
	 * 
	 * @return the name of the parameter
	 */
	public String getParameterName() {
		return parameter.getName();
	}
	
	/**
	 * adapts the member name in case the member name is a reserved word in Delphi
	 * 
	 * @return the member name prepended with ampersand in case the member name is a reserved word in Delphi
	 */
	public String getNonReservedWordParameterName() {
		return parameter.getNonReservedWordName();
	}
	
	@Override
	protected void wDeveloperDocumentation(TargetSection ts) { /* nothing written here for Parameter */ }
    
    @Override
    protected void wDeveloperDocumentationHeader(TargetSection ts) { /* nothing written here for Parameter */ }
}
