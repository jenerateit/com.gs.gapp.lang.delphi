package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.metamodel.objectpascal.type.Pointer;



/**
 * @author hr
 *
 */
public class PointerWriter extends TypeWriter {

	@ModelElement
	private Pointer pointer;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
        wTypeName().wNL(" = ^", pointer.getReferencedType().getName(), ";");
	}
	
	@Override
	public String getTypeName() {
		return pointer.getNonReservedWordName();
	}
}
