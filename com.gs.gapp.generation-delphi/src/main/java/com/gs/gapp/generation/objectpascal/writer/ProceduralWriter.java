package com.gs.gapp.generation.objectpascal.writer;

import java.util.Iterator;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.objectpascal.member.Parameter;
import com.gs.gapp.metamodel.objectpascal.type.Procedural;



/**
 * @author mmt
 *
 */
public class ProceduralWriter extends TypeWriter {

	@ModelElement
	private Procedural procedural;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		wTypeName(); w(" = "); wMethodKeyword(); wMethodParameters(ts); wReturnType(); w(procedural.isOfObject() ? " of object" : ""); wNL(";");
	}

	/**
	 * 
	 */
	private void wReturnType() {
		if (procedural.getReturnType() != null) {
			wUse(procedural.getReturnType());
			TypeWriter tw = getTransformationTarget().getWriterInstance(procedural.getReturnType(), TypeWriter.class);
			if (tw != null) {
				w(": ", tw.getTypeName());
			} else {
				throw new WriterException("procedural type " + procedural.getName() + " has return type '" + procedural.getReturnType().getName() + "', but no writer could be found for the return type");
			}
		}
	}

	/**
	 * 
	 */
	private void wMethodParameters(TargetSection ts) {
		if (!procedural.getParameters().isEmpty()) {
			w("(");
			ParameterWriter pw = null;
			Iterator<Parameter> iterator = procedural.getParameters().iterator();
			while (iterator.hasNext()) {
				pw = getTransformationTarget().getWriterInstance(iterator.next(), ParameterWriter.class);
				pw.wParameter(ts);
				if (iterator.hasNext()) {
					w(", ");
				}
			}
			w(")");
		}
	}

	/**
	 * 
	 */
	private void wMethodKeyword() {
		if (procedural.getReturnType() == null) {
			w("procedure ");
		} else {
			w("function ");
		}
	}
}
