/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.member.Procedure;



/**
 * @author mmt
 *
 */
public class ProcedureWriter extends MethodWriter {

	@ModelElement
	private Procedure procedure;
	

	@Override
	protected void wMethodKeyword() {
		w("procedure ");
	}
	
}
