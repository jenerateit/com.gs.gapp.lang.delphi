/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.metamodel.objectpascal.member.Parameter;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.structured.Member;


/**
 * @author hrr
 *
 */
public class PropertyWriter extends MemberWriter {

	@ModelElement
	private Property property;

	/**
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		wProperty(ts);
	}

	/**
	 * 
	 * @return
	 */
	public PropertyWriter wProperty(TargetSection ts) {
		TypeWriter tw = getTransformationTarget().getWriterInstance(property.getType(), TypeWriter.class);
		wUse(property.getType());
		wKeyword(); w(property.getNonReservedWordName());
		if (property.getIndexParameters().size() > 0) {
			w("[");
			String comma = "";
			for (Parameter parameter : property.getIndexParameters()) {
				WriterI writer = getTransformationTarget().getWriterInstance(parameter);
				if (writer != null) {
					w(comma); writer.transform(ts);
					comma = ", ";
				}
			}
			w("]");
		}
		w(": ", tw.getTypeName());
		
		final Member memberForReadAccess = this.property.getFieldOrMethodForRead();
		if (memberForReadAccess != null) {
			MemberWriter memberWriter = getTransformationTarget().getWriterInstance(memberForReadAccess, MemberWriter.class);
			if (memberWriter != null) {
				if (this.property.getEnclosingFieldOrMethod() == null) {
					w(" read ", memberWriter.getNonReservedWordMemberName());
				} else {
					MemberWriter enclosingMemberWriter = getTransformationTarget()
							.getWriterInstance(this.property.getEnclosingFieldOrMethod(), MemberWriter.class);
					if (enclosingMemberWriter != null) {
						w(" read ", enclosingMemberWriter.getNonReservedWordMemberName(), ".", memberWriter.getNonReservedWordMemberName());
					} else {
						w(" read ", memberWriter.getNonReservedWordMemberName());
					}
				}
			    
			}
		}
		
		final Member memberForWriteAccess = this.property.getFieldOrMethodForWrite();
		if (memberForWriteAccess != null) {
			MemberWriter memberWriter = getTransformationTarget().getWriterInstance(memberForWriteAccess, MemberWriter.class);
			if (memberWriter != null) {
				if (this.property.getEnclosingFieldOrMethod() == null) {
					w(" write ", memberWriter.getNonReservedWordMemberName());
				} else {
					MemberWriter enclosingMemberWriter = getTransformationTarget()
							.getWriterInstance(this.property.getEnclosingFieldOrMethod(), MemberWriter.class);
					if (enclosingMemberWriter != null) {
						w(" write ", enclosingMemberWriter.getNonReservedWordMemberName(), ".", memberWriter.getNonReservedWordMemberName());
					} else {
				        w(" write ", memberWriter.getNonReservedWordMemberName());
					}
				}
			}
		}
		wNL(";");
		
		return this;
	}
	
	/**
	 * 
	 */
	protected void wKeyword() {
		w("property ");
	}
}
