/*  
 *
 */

package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.PseudoType;



/**
 *
 * 
 * @author mmt
 *
 */
public class PseudoTypeWriter extends TypeWriter {

	@ModelElement
	private PseudoType pseudoType;

}
