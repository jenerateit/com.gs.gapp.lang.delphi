package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.PseudoUnitUsage;


/**
 * @author mmt
 *
 */
public class PseudoUnitUsageWriter extends UnitUsageWriter {
	
	@ModelElement
	private PseudoUnitUsage pseudoUsage;

}
