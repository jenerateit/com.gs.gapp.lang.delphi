package com.gs.gapp.generation.objectpascal.writer;

import java.util.LinkedHashSet;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.objectpascal.Keyword;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.member.Field;
import com.gs.gapp.metamodel.objectpascal.type.structured.Member;
import com.gs.gapp.metamodel.objectpascal.type.structured.Record;
import com.gs.gapp.metamodel.objectpascal.type.structured.Record.VariantPart;


/**
 * @author hr
 *
 */
public class RecordWriter extends StructuredTypeWriter {

	@ModelElement
	private Record record;
	
    /**
     * @param ts
     */
    protected void wMembers(TargetSection ts) {
    	indent();
	    	for (final Type type : record.getNestedTypes()) {
				WriterI writer = getTransformationTarget().getWriterInstance(type);
				if (writer != null) writer.transform(ts);
			}
			for (final Member member : record.getMembers()) {
				WriterI writer = getTransformationTarget().getWriterInstance(member);
				if (writer != null) writer.transform(ts);
			}
			
			if (record.getVariantPart() != null) {
				VariantPart variantPart = record.getVariantPart();
				wNLI("case ", variantPart.getField().getName(), ": ", variantPart.getField().getType().getTypeAsString(), " of");
				boolean firstKey = true;
				for (String key : variantPart.getFieldMap().keySet()) {
					LinkedHashSet<Field> fields = variantPart.getFieldMap().get(key);
					if (!firstKey) wNL();
					w(key, ": (");
					boolean firstField = true;
					for (Field field : fields) {
						if (!firstField) wNL();
						w(field.getName(), ": ", field.getType().getTypeAsString(), ";");
						firstField = false;
					}
					w(");");
					firstKey = false;
				}
				outdent();
			}
			
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_MEMBERS_IN_RECORD)) {
				bDA(new StringBuffer(record.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_MEMBERS_IN_RECORD.getId()).toString());
			    eDA();
			}
        outdent();
	}

	
	/**
	 * @param ts
	 */
	protected void wTypeClause(TargetSection ts) {
	    String packed = "";
	    if (this.record.getPacked() != null && this.record.getPacked()) {
	        packed = Keyword.PACKED.getName() + " ";
	    }
		w(getTypeName(), " = ", packed, "record");
	}
	
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		wTypeClause(ts);
		wNL().indent();
			wMembers(ts);
		wONL("end;");
		wNL();
	}

	@Override
	public String getTypeName() {
		return record.getNonReservedWordName();
	}
}
