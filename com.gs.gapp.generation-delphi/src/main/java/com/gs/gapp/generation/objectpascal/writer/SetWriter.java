package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.objectpascal.type.structured.Set;


/**
 * @author hr
 *
 */
public class SetWriter extends StructuredTypeWriter {

	@ModelElement
	private Set set;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		if (set.getBaseType() == null) {  // added this writer exception due to a relaxation of checks in the constructors of Sets (mmt 2015-Nov-23)
			throw new WriterException("base type of a Set must not be null (Set:" + set.getName() + ")");
		}
		
		wNL();
		w(getTypeName(), " = set of ", set.getBaseType().getTypeAsString(), ";");
	}
}
