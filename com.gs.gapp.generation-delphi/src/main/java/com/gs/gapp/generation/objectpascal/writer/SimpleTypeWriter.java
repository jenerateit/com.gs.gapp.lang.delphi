package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.type.simple.SimpleType;



/**
 * @author hr
 *
 */
public class SimpleTypeWriter extends TypeWriter {

	@ModelElement
	private SimpleType type;
	
}
