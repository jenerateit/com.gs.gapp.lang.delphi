package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;



/**
 * @author hr
 *
 */
public abstract class StructuredTypeWriter extends TypeWriter {

	@ModelElement
	private StructuredType structuredType;

	@Override
	public String getTypeName() {
		return this.structuredType.getTypeAsString();
	}
}
