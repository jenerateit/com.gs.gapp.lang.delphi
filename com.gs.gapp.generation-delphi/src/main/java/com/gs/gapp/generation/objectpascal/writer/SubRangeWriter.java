package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.objectpascal.type.simple.SubRange;



/**
 * @author hr
 *
 */
public class SubRangeWriter extends OrdinalTypeWriter {

	@ModelElement
	private SubRange subRange;

	@Override
	public void transform(TargetSection ts) {
        super.transform(ts);
		
		wTypeName().w(" = "); w(subRange.getMinValue().getName(), "..", subRange.getMaxValue().getName()); wNL(";");
	}
}
