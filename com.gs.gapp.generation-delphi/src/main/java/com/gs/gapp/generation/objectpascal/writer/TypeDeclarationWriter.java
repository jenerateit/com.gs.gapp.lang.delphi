package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.metamodel.objectpascal.type.TypeDeclaration;



/**
 * @author hr
 *
 */
public class TypeDeclarationWriter extends TypeWriter {

	@ModelElement
	private TypeDeclaration typeDeclaration;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
        wTypeName().wNL(" = type ", typeDeclaration.getDenotedType().getName(), ";");
	}
}
