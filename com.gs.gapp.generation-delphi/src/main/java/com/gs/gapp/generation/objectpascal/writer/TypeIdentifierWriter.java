package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.metamodel.objectpascal.type.TypeIdentifier;



/**
 * @author hr
 *
 */
public class TypeIdentifierWriter extends TypeWriter {

	@ModelElement
	private TypeIdentifier typeIdentifier;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		wUse(typeIdentifier.getDenotedType());
        wTypeName().wNL(" = ", typeIdentifier.getDenotedType().getName(), ";");
	}
}
