/*  
 *
 */

package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.target.DelphiTargetSectionEnum;
import com.gs.gapp.metamodel.objectpascal.AttributeUsage;
import com.gs.gapp.metamodel.objectpascal.Type;



/**
 * @author hr
 *
 */
public class TypeWriter extends DelphiWriter {

	@ModelElement
	private Type type;
	
	/**
	 * 
	 * @param ts
	 * @see AbstractWriter#transform(TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		super.transform(ts);
		
		if (DelphiTargetSectionEnum.isInterfaceSection(ts)) {
			if (!type.getAttributeUsages().isEmpty()) {
				String separator = "";
				for (AttributeUsage attributeUsage : type.getAttributeUsages()) {
					WriterI writer = getTransformationTarget().getWriterInstance(attributeUsage);
					if (writer != null) {
						w(separator);
						writer.transform(ts);
						separator = StringTools.NEWLINE;
					}
				}
				wNL();
			}
		}
	}

	/**
	 * @return
	 */
	public TypeWriter wTypeName() {
		wUse(type);
		w(type.getTypeAsString());
		return this;
	}
	
	/**
	 * Getter to the name of this type.
	 * 
	 * @return the type name
	 */
	public String getTypeName() {
		return type.getTypeAsString();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.objectpascal.writer.DelphiWriter#wDocumentation(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected DelphiWriter wDocumentation(TargetSection ts) {
		String xmlDocumentation = type.getXmlDocumentation();
		if (xmlDocumentation != null && xmlDocumentation.length() > 0) {
		    wNL(getWrappedText(xmlDocumentation, "/// "));
		}
		
		return this;
	}
}
