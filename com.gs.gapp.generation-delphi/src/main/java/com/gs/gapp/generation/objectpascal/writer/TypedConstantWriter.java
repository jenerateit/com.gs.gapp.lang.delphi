/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.objectpascal.constant.TypedConstant;

/**
 * @author mmt
 *
 */
public class TypedConstantWriter extends ConstantWriter {
	
	@ModelElement
	private TypedConstant typedConstant;
	
	

	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		wUse(typedConstant.getType());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.objectpascal.writer.ConstantWriter#getType()
	 */
	@Override
	protected String getType() {
		return new StringBuilder(": ").append(typedConstant.getType().getTypeAsString()).toString();
	}
}