package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.objectpascal.UnitUsage;


/**
 * @author mmt
 *
 */
public class UnitUsageWriter extends DelphiWriter {
	
	@ModelElement
	private UnitUsage unitUsage;

	@Override
	public void transform(TargetSection ts) throws WriterException {
		super.transform(ts);
		wUse(getUse());
	}

	@Override
	protected void wDeveloperDocumentation(TargetSection ts) { /* nothing written here for UnitUsage */ }
    
    @Override
    protected void wDeveloperDocumentationHeader(TargetSection ts) { /* nothing written here for UnitUsage */ }
    
    public String getUse() {
        return unitUsage.getQualifiedName();
    }
}
