package com.gs.gapp.generation.objectpascal.writer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.objectpascal.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.objectpascal.target.DelphiTarget;
import com.gs.gapp.generation.objectpascal.target.DelphiTargetDocument;
import com.gs.gapp.generation.objectpascal.target.DelphiTargetSectionEnum;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.objectpascal.Constant;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalMessage;
import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.ThreadLocalVariable;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.TypeVisibility;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.UnitUsage;
import com.gs.gapp.metamodel.objectpascal.Variable;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.type.ForwardDeclaration;
import com.gs.gapp.metamodel.objectpascal.type.TypeIdentifier;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;
import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;


/**
 * @author mmt
 *
 */
public class UnitWriter extends DelphiWriter {
    
	@ModelElement
	private Unit unit;
	
	/**
     * @param ts
     */
    protected void writeInterfaceHeader(TargetSection ts) {
        wNL();
        wNL("{================================================================}");
        wNL("interface");
        wNL("{================================================================}");
        if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.INTERFACE_SECTION_START)) {
        	// CHQDG-178
	        final String developerAreaIdForTypeIdentifiers = new StringBuffer(unit.getName()).append(".")
	    			.append(DeveloperAreaTypeEnum.INTERFACE_SECTION_START.getId()).toString();
		    bDA(developerAreaIdForTypeIdentifiers);
		    eDA();
        }
        wNL();
    }
    
    /**
     * @param ts
     */
    protected void writeImplementationHeader(TargetSection ts) {
        wNL();
        wNL();
        wNL("{================================================================}");
        wNL("implementation");
        wNL("{================================================================}");
        wNL();
    }
    
	/**
	 * @param ts
	 */
	protected void transformTypes(TargetSection ts) {
		TypeVisibility typeVisibility =
				ts == DelphiTargetSectionEnum.INTERFACE_TYPE.getTargetSection() ? TypeVisibility.GLOBAL : TypeVisibility.LOCAL;
		
		LinkedHashSet<TypeIdentifier> typeIdentifiers = unit.getTypes(TypeIdentifier.class, typeVisibility);
		LinkedHashSet<Type> allTypes = unit.getTypes(Type.class, typeVisibility);
	    Collection<StructuredType> typesToWriteFirstDueToInheritanceDependencies = unit.getTypesInvolvedInInheritance();
			
		if (!allTypes.isEmpty() || !typeIdentifiers.isEmpty()) {
		
	    	wNL();
		    wNL("type");indent(getGenerationGroupOptions().getIndentationAfterType());
		    
		    if (ts == DelphiTargetSectionEnum.INTERFACE_TYPE.getTargetSection()) {
		    	
			    // --- ForwardDeclaration types first (that's the real sense of forward declarations)
		    	final Set<StructuredType> forwardDeclaredTypes = unit.getForwardDeclaredTypes();
		    	Set<StructuredType> typesThatRequiredSyntheticForwardDeclaration = unit.getStructuredTypesThatMightNeedForwardDeclaration();
		    	if (!typesThatRequiredSyntheticForwardDeclaration.isEmpty()) {
		    		wComment("--- Forward declarations for types that are declared _and_ used in this unit");
		    		typesThatRequiredSyntheticForwardDeclaration.stream()
		    		    .filter(structuredType -> !forwardDeclaredTypes.contains(structuredType))
		    		    .forEach(structuredType -> {
				    		// add forward declarations in order to be sure that the generated code compiles
				    		if (structuredType instanceof Interface) {
				                Interface interf = (Interface) structuredType;
				                @SuppressWarnings("unused")
								ForwardDeclaration forwardDeclaration = new ForwardDeclaration(interf.getName() + "ForwardDeclaration", interf, unit);
				    		} else if (structuredType instanceof Clazz) {
				                Clazz clazz = (Clazz) structuredType;
				                @SuppressWarnings("unused")
								ForwardDeclaration forwardDeclaration = new ForwardDeclaration(clazz.getName() + "ForwardDeclaration", clazz, unit);
				    		}
				    	});
		    	}
		    	
		    	boolean written = false;
		    	LinkedHashSet<ForwardDeclaration> forwardDeclarations = unit.getTypes(ForwardDeclaration.class, typeVisibility);
			    for (final ForwardDeclaration forwardDeclaration : forwardDeclarations) {
			    	if (forwardDeclaration.isGenerated() == false) continue;
			    	
		    		WriterI writerInstance = getTransformationTarget().getWriterInstance(forwardDeclaration);
		    		if (writerInstance != null) {
		    			writerInstance.transform(ts);
		    			written = true;
		    		}
		    	}
			    if (written) wNL();
			    
			    // --- TypeIdentifier types second
			    final String developerAreaIdForTypeIdentifiers = new StringBuffer(unit.getName()).append(".")
		    			.append(DeveloperAreaTypeEnum.TYPE_IDENTIFIERS.getId())
		    			.append(".")
		    			.append(ts.getCode().replace("_", ".").replace("-", ".")).toString();
			    bDA(developerAreaIdForTypeIdentifiers);
			    for (final Type type : typeIdentifiers) {
			    	if (type.isGenerated() == false) continue;
			    	
		    		WriterI writerInstance = getTransformationTarget().getWriterInstance(type);
		    		if (writerInstance != null) {
		    			writerInstance.transform(ts);
		    		}
		    	}
			    eDA();
			    wNL();
		    }
		    
		    // --- all other types follow
		    final Collection<Type> intersection = allTypes.stream().filter(type -> type instanceof StructuredType).collect(Collectors.toCollection(ArrayList::new));
		    intersection.retainAll(typesToWriteFirstDueToInheritanceDependencies);
		    boolean orderingOfTypeDeclarationForInheritanceIsCorrect = intersection.equals(typesToWriteFirstDueToInheritanceDependencies);
		    
		    if ((!orderingOfTypeDeclarationForInheritanceIsCorrect) && (!typesToWriteFirstDueToInheritanceDependencies.isEmpty())) {
		    	// First write types that must come first and in a certain order to allow the Delphi compiler
		    	// to compile the unit successfully.
		    	wComment("--- First some type declarations that are involved in inheritance relationships");
		    	typesToWriteFirstDueToInheritanceDependencies.stream().forEach(structuredType -> {
		    		WriterI writerInstance = getTransformationTarget().getWriterInstance(structuredType);
		    		if (writerInstance != null) writerInstance.transform(ts);
		    	});
		    }
		    
		    if ((!orderingOfTypeDeclarationForInheritanceIsCorrect) && (!typesToWriteFirstDueToInheritanceDependencies.isEmpty())) {
		        wComment("--- Now the type declarations that are _not_ involved in inheritance relationships");
		    }
		    for (final Type type : allTypes) {
		    	if (type.isGenerated() == false) continue;
		    	if (type instanceof TypeIdentifier) continue;
		    	if (type instanceof ForwardDeclaration) continue;
		    	if ((!orderingOfTypeDeclarationForInheritanceIsCorrect) && typesToWriteFirstDueToInheritanceDependencies.contains(type)) continue;
		    	
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(type);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
		    
		}//if there are types to be generated
	    
		// Note that for the additional types in the unit we need a developer area
		// for the interface _and_ the implementation section.
    	bDA(new StringBuffer(unit.getName()).append(".")
    			.append(DeveloperAreaTypeEnum.ADDITIONAL_TYPES_IN_UNIT.getId())
    			.append(".")
    			.append(ts.getCode().replace("_", ".").replace("-", ".")).toString());
    	eDA();
	}
	
	/**
	 * @param ts
	 */
	protected void transformPublicVariables(TargetSection ts) {
		// --- "normal" variables
		Set<Variable> variablesWithPublicVisibility = unit.getVariablesWithPublicVisibility();
    	if (!variablesWithPublicVisibility.isEmpty()) {
    		wNL();
	    	wNL("var");indent(getGenerationGroupOptions().getIndentationAfterVar());
	    	for (Variable variable : variablesWithPublicVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(variable);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
	    	outdent(getGenerationGroupOptions().getIndentationAfterVar());
    	}
    	
		// --- thread local variables
		Set<ThreadLocalVariable> threadLocalVariablesWithPublicVisibility = unit.getThreadLocalVariablesWithPublicVisibility();
    	if (!threadLocalVariablesWithPublicVisibility.isEmpty()) {
    		wNL();
	    	wNL("threadvar");indent(getGenerationGroupOptions().getIndentationAfterVar());
	    	for (Variable variable : threadLocalVariablesWithPublicVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(variable);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
	    	outdent(getGenerationGroupOptions().getIndentationAfterVar());
    	}
	}
	
    /**
     * @param ts
     */
    protected void transformPublicConstants(TargetSection ts) {
    	// --- regular constants
    	Set<Constant> constantsWithPublicVisibility = unit.getConstantsWithPublicVisibility();
    	
    	if (constantsWithPublicVisibility.size() > 0) {
    		wNL();
			wNL("const");indent(getGenerationGroupOptions().getIndentationAfterConst());
	    	for (final Constant constant : constantsWithPublicVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(constant);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
	    	outdent(getGenerationGroupOptions().getIndentationAfterConst());
		}
    	
    	// --- resource strings
        Set<Constant> resourceStringsWithPublicVisibility = unit.getResourceStringsWithPublicVisibility();
    	
    	if (resourceStringsWithPublicVisibility.size() > 0) {
    		wNL();
			wNL("resourcestring");indent(getGenerationGroupOptions().getIndentationAfterConst());
	    	for (final Constant constant : resourceStringsWithPublicVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(constant);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
			outdent(getGenerationGroupOptions().getIndentationAfterConst());
		}
	}
	
    /**
     * @param ts
     */
    protected void transformPublicMethods(TargetSection ts) {
		Set<Method> methodsWithPublicVisibility = unit.getMethodsWithPublicVisibility();
		
		if (methodsWithPublicVisibility.size() > 0) {
			wNL();
	    	for (Method method : methodsWithPublicVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(method);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
		}
	}
    
    /**
     * @param ts
     */
    protected void transformMethodImplementations(TargetSection ts) {
		Set<Method> methods = unit.getMethods();
		Set<Method> methodsOfClasses = unit.getMethodsOfAllClasses();
		
		if (methods.size() > 0 || methodsOfClasses.size() > 0) {
			wNL();
	    	for (Method method : methods) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(method);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
	    	
	    	for (Type type : unit.getTypes(Type.class)) {
	    		if (type instanceof OuterTypeI) {
	    		    transformMethodImplementationsForTypesWithMethods(ts, (OuterTypeI) type);
	    		}
			}
		}
		
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_METHOD_IMPLEMENTATIONS)) {
			bDA(new StringBuffer(unit.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_METHOD_IMPLEMENTATIONS.getId()).toString());
		    eDA();
		}
	}
    
    /**
     * @param ts
     * @param outerType
     */
    private void transformMethodImplementationsForTypesWithMethods(TargetSection ts, OuterTypeI outerType) {
    	
    	for (Method method : outerType.getMethods()) {
    		WriterI writerInstance = getTransformationTarget().getWriterInstance(method);
    		if (writerInstance != null) writerInstance.transform(ts);
    	}
    	
    	// --- now recursively calling nested classes and nested records method implementation generation
    	for (Type nestedType : outerType.getNestedTypes(Type.class)) {
    		if (nestedType instanceof OuterTypeI) {
    		    transformMethodImplementationsForTypesWithMethods(ts, (OuterTypeI) nestedType);
    		}
    	}
    }
    
    /**
     * @param ts
     */
    protected void transformPrivateConstants(TargetSection ts) {
    	// --- regular constants
		Set<Constant> constantsWithPrivateVisibility = unit.getConstantsWithPrivateVisibility();
		
		if (constantsWithPrivateVisibility.size() > 0) {
			wNL();
			wNL("const");indent(getGenerationGroupOptions().getIndentationAfterConst());
	    	for (final Constant constant : constantsWithPrivateVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(constant);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
			outdent(getGenerationGroupOptions().getIndentationAfterConst());
		}
    	
    	// --- resource strings
		Set<Constant> resourceStringsWithPrivateVisibility = unit.getResourceStringsWithPrivateVisibility();
		
		if (resourceStringsWithPrivateVisibility.size() > 0) {
			wNL();
			wNL("resourcestring");indent(getGenerationGroupOptions().getIndentationAfterConst());
	    	for (final Constant constant : resourceStringsWithPrivateVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(constant);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
			outdent(getGenerationGroupOptions().getIndentationAfterConst());
		}
	}
    
	/**
     * 
     * @param ts
     * @see AbstractWriter#transform(TargetSection)
     */
    @Override
    public void transform(TargetSection ts) {
    	DelphiTargetSectionEnum tsEnumEntry = DelphiTargetSectionEnum.get(ts);
    	
    	switch (tsEnumEntry) {
    	case UNIT:
    		
    	    // --- Before doing anything else, we are going to check for duplicate type entries.
    		Map<String, List<Type>> duplicateForwardDeclarations = this.unit.getDuplicateTypes(ForwardDeclaration.class);
    		if (!duplicateForwardDeclarations.isEmpty()) {
    			Message duplicateForwaredDeclarationMessage = ObjectPascalMessage.ERROR_DUPLICATE_TYPE_IN_UNIT.getMessageBuilder()
	    			.modelElement(this.unit)
	    			.parameters(this.unit.getName(), duplicateForwardDeclarations.keySet().stream().collect(Collectors.joining(", ")))
	    			.build();
    			throw new WriterException(duplicateForwaredDeclarationMessage.getMessage());
    		}
    		Map<String, List<Type>> duplicateStructuredTypes = this.unit.getDuplicateTypes(StructuredType.class);
    		if (!duplicateStructuredTypes.isEmpty()) {
    			Message duplicateStructuredTypesMessage = ObjectPascalMessage.ERROR_DUPLICATE_TYPE_IN_UNIT.getMessageBuilder()
	    			.modelElement(this.unit)
	    			.parameters(this.unit.getName(), duplicateStructuredTypes.keySet().stream().collect(Collectors.joining(", ")))
	    			.build();
    			throw new WriterException(duplicateStructuredTypesMessage.getMessage());
    		}
    		
			wUnitHeading(ts);
			wUnitHeadingSupplement(ts);
			wDeveloperDocumentationHeader(ts);
    		wDeveloperDocumentation(ts);
	    	wUnitComment(ts);
	    	wDirectivesBetweenUnitAndInterfaceSection(ts);
			break;

    	case INTERFACE_USES:
			writeInterfaceHeader(ts);
			transformPreviousUsesForInterface(ts);
	    	transformUsesForInterface(ts);
			break;
		case INTERFACE_CONSTANT:
			transformPublicConstants(ts);
			break;
		case INTERFACE_TYPE:
			// The INTERFACE_TYPE section comes right after INTERFACE_USES and the terminating ';'
			// of the 'uses' part in the interface section is written by the uses merger instead
			// of the UnitWriter. For this reason we have to write the developer area that comes
			// right after the interface uses here instead of "at the end" of INTERFACE_USES. (mmt 26-May-2023)
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.INTERFACE_SECTION_AFTER_USES)) {
				// CHQDG-178
				wNL();
		    	final String developerAreaIdForTypeIdentifiers = new StringBuffer(unit.getName()).append(".")
		    			.append(DeveloperAreaTypeEnum.INTERFACE_SECTION_AFTER_USES.getId()).toString();
			    bDA(developerAreaIdForTypeIdentifiers);
			    eDA();
			}
			
			transformTypes(ts);
			break;
		case INTERFACE_VAR:
			transformPublicVariables(ts);
			break;
		case INTERFACE_METHOD:
			transformPublicMethods(ts);
			
			// The following has to be done here since INTERFACE_METHOD is the very last section
			// of the interface part. There is no section INTERFACE_END. (mmt 26-May-2023)
			if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.INTERFACE_SECTION_END)) {
	    		// CHQDG-178
				final String developerAreaIdForTypeIdentifiers = new StringBuffer(unit.getName()).append(".")
						.append(DeveloperAreaTypeEnum.INTERFACE_SECTION_END.getId())
						.append(".").toString().replace("_", ".").replace("-", ".");
			    bDA(developerAreaIdForTypeIdentifiers);
			    eDA();
	    	}
			break;
			
		
		case IMPLEMENTATION_USES:
	    	writeImplementationHeader(ts);
	    	transformPreviousUsesForImplementation(ts);
	    	transformUsesForImplementation(ts);
			break;
		case IMPLEMENTATION_AFTER_USES:
		    transformAfterUsesForImplementation(ts);
		    break;
		case IMPLEMENTATION_CONSTANT:
			transformPrivateConstants(ts);
			break;
		case IMPLEMENTATION_TYPE:
			transformTypes(ts);
			break;
		
		case IMPLEMENTATION_VAR:
			transformPrivateVariables(ts);
			break;
		case IMPLEMENTATION_METHOD:
			transformMethodImplementations(ts);
			break;
			
			
		case INITIALIZATION:
			transformInitialization(ts);
			break;
		case FINALIZATION:
			transformFinalization(ts);
			break;			
		
		
		case END_OF_UNIT:
			wNL("end.");
			break;
		default:
			throw new RuntimeException("cannot handle this, unexpected target section found:" + ts);
    	}
    }   

	/**
	 * @param ts
	 */
	protected void wUnitHeadingSupplement(TargetSection ts) {
	    DelphiTargetDocument previousTargetDocument = (DelphiTargetDocument)getTextTransformationTarget().getPreviousTargetDocument();
	    if (previousTargetDocument != null) {
	    	if (!previousTargetDocument.getLinesBetweenUnitAndFirstDevDoc().isEmpty()) {
	    		StringBuilder rewrittenContent = new StringBuilder();
	    		int counterForNonEmptyLines = 0;
		        for (String line : previousTargetDocument.getLinesBetweenUnitAndFirstDevDoc()) {
		            w(line);
		            if (!line.trim().isEmpty()) {
		            	counterForNonEmptyLines++;
		            }
		            rewrittenContent.append(line);
		        }
		        Message infoMessage = ObjectPascalMessage.INFO_REWRITTEN_CONTENT_BETWEEN_UNIT_AND_DEV_DOC.getMessageBuilder()
			        .modelElement(this.unit)
			        .parameters(previousTargetDocument.getLinesBetweenUnitAndFirstDevDoc().size(),
			        		    counterForNonEmptyLines,
			        		    this.unit.getName(),
			        		    rewrittenContent.toString())
			        .build();
		        getTextTransformationTarget().addInfo(infoMessage.getMessage());
	    	} else {
	    		Message infoMessage = ObjectPascalMessage.INFO_NO_PREVIOUS_CONTENT_AVAILABLE_BETWEEN_UNIT_AND_DEV_DOC.getMessageBuilder()
			        .modelElement(this.unit)
			        .parameters(this.unit.getName(), previousTargetDocument.length())
			        .build();
		        getTextTransformationTarget().addInfo(infoMessage.getMessage());
	    	}
	    } else {
	    	Message infoMessage = ObjectPascalMessage.INFO_NO_PREVIOUS_CONTENT_AVAILABLE.getMessageBuilder()
		        .modelElement(this.unit)
		        .parameters(this.unit.getName())
		        .build();
	        getTextTransformationTarget().addInfo(infoMessage.getMessage());
	    }
	}

    /**
	 * @param ts
	 */
	protected void transformAfterUsesForImplementation(TargetSection ts) {}

    /**
	 * @param ts
	 */
	protected void transformPreviousUsesForImplementation(TargetSection ts) {
        DelphiTarget delphiTarget = DelphiTarget.class.cast(getTransformationTarget());
        DelphiTargetDocument prevTargetDocument = delphiTarget.getPreviousTargetDocument();
       
        List<String> implementationUses = prevTargetDocument.getUses(DelphiTargetSectionEnum.IMPLEMENTATION_USES,
                                                                     false,  // false => do not trim the unit name
                                                                     true,   // true => keep commas as part of the unit name
                                                                     true);  // ignore cached uses
        if (prevTargetDocument != null && !implementationUses.isEmpty()) {
            for (String previousUse : new ArrayList<>(implementationUses)) {
                wUse(previousUse,
                     false);  // false => do not remove a comma from the unit name
            }
        }
    }

    /**
	 * @param ts
	 */
	protected void wDirectivesBetweenUnitAndInterfaceSection(TargetSection ts) {
		if (!this.unit.getDirectivesBetweenUnitAndInterfaceSection().isEmpty()) {
			wNL();
			getTextTransformationTarget().addInfo("Rewriting " + this.unit.getDirectivesBetweenUnitAndInterfaceSection().size() +
					" lines of code, typically directives, between unit declaration and interface section in unit " + this.unit.getName() + ".");
			for (String directive : this.unit.getDirectivesBetweenUnitAndInterfaceSection()) {
				wNL(directive);
			}
		}
	}

	/**
	 * @param ts
	 */
	private void transformUsesForImplementation(TargetSection ts) {
		for (UnitUsage usage : unit.getUsesForImplementation()) {
			if (this.unit == usage.getUnit()) {
				continue;  // no need to add a use clause when the unit uses itself
			}
			WriterI writer = getTransformationTarget().getWriterInstance(usage);
			if (writer != null) writer.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	private void transformUsesForInterface(TargetSection ts) {
		for (UnitUsage usage : unit.getUsesForInterface()) {
			if (this.unit == usage.getUnit()) {
				continue;  // no need to add a use clause when the unit uses itself 
			}
			WriterI writer = getTransformationTarget().getWriterInstance(usage);
			if (writer != null) writer.transform(ts);
		}
	}
	
	/**
	 * @param ts
	 */
	protected void transformPreviousUsesForInterface(TargetSection ts) {
	    DelphiTarget delphiTarget = DelphiTarget.class.cast(getTransformationTarget());
        DelphiTargetDocument prevTargetDocument = delphiTarget.getPreviousTargetDocument();
       
        List<String> interfaceUses = prevTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES,
                                                               false,  // do not trim the uses
                                                               true,   // true => keep commas as part of the unit name
                                                               true);  // ignore cached uses
        if (prevTargetDocument != null && !interfaceUses.isEmpty()) {
            for (String previousUse : new ArrayList<>(interfaceUses)) {
                wUse(previousUse,
                     false);  // false => do not remove a comma from the unit name
            }
        }
	}

	/**
	 * @param ts
	 */
	protected void transformFinalization(TargetSection ts) {
		wNL("finalization");indent(getGenerationGroupOptions().getIndentationAfterFinalization());
		    bDA(new StringBuffer(unit.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_FINALIZATIONS_IN_UNIT.getId()).toString());
		    for (String statement : unit.getFinalizationStatements()) {
		    	wNL(statement);
		    }
		    eDA();
		outdent(getGenerationGroupOptions().getIndentationAfterFinalization());
	}

	/**
	 * @param ts
	 */
	protected void transformInitialization(TargetSection ts) {
		wNL("initialization");indent(getGenerationGroupOptions().getIndentationAfterInitialization());
		    bDA(new StringBuffer(unit.getName()).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_INITIALIZATIONS_IN_UNIT.getId()).toString());
			for (String statement : unit.getInitializationStatements()) {
		    	wNL(statement);
		    }
			eDA();
		outdent(getGenerationGroupOptions().getIndentationAfterInitialization());
	}

	private void transformPrivateVariables(TargetSection ts) {
		// --- "normal" variables
		Set<Variable> variablesWithPrivateVisibility = unit.getVariablesWithPrivateVisibility();
    	if (!variablesWithPrivateVisibility.isEmpty()) {
    		wNL();
	    	wNL("var");indent(getGenerationGroupOptions().getIndentationAfterVar());
	    	for (Variable variable : variablesWithPrivateVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(variable);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
	    	outdent(getGenerationGroupOptions().getIndentationAfterVar());
    	}
    	
    	// --- thread local variables
    	Set<ThreadLocalVariable> threadLocalVariablesWithPrivateVisibility = unit.getThreadLocalVariablesWithPrivateVisibility();
    	if (!threadLocalVariablesWithPrivateVisibility.isEmpty()) {
    		wNL();
	    	wNL("threadvar");indent(getGenerationGroupOptions().getIndentationAfterVar());
	    	for (ThreadLocalVariable variable : threadLocalVariablesWithPrivateVisibility) {
	    		WriterI writerInstance = getTransformationTarget().getWriterInstance(variable);
	    		if (writerInstance != null) writerInstance.transform(ts);
	    	}
	    	outdent(getGenerationGroupOptions().getIndentationAfterVar());
    	}
	}

	/**
	 * @param ts
	 * @return
	 */
	public UnitWriter wUnitHeading(TargetSection ts) {
    	wNL(ts, "unit ", unit.getQualifiedName(), ";");
    	return this;
    }
    
    /**
     * 
     * @return
     */
    public UnitWriter wUnitComment(TargetSection ts) {
    	wDocumentation(ts);
    	return this;
    }

	public String getQualifiedUnitName() {
    	return this.unit.getQualifiedName();
    }
    
	public String getDescription() {
		return "Writer for a Delphi unit";
	}
}
