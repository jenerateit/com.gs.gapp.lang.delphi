/**
 * 
 */
package com.gs.gapp.generation.objectpascal.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.objectpascal.Variable;

/**
 * @author mmt
 *
 */
public class VariableWriter extends DelphiWriter {
	
	@ModelElement
	private Variable variable;

	/**
	 * 
	 */
	public VariableWriter() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		
		wUse(variable.getType());
		wNL(variable.getNonReservedWordName(), variable.getType() != null ? ": "+variable.getType().getTypeAsString() : "", hasInitialization() ? " = " : "",	getInitialization(), ";");
	}
	
	/**
	 * Keep in mind that only variables that are defined in a unit can be initialized in one and the same statement.
	 * 
	 * @return
	 */
	private String getInitialization() {
		return variable.getDeclaringUnit() != null ? variable.getInitialization() : "";
	}
	
	/**
	 * @return
	 */
	private boolean hasInitialization() {
		return getInitialization() != null && getInitialization().length() > 0;
	}
	
	@Override
	protected void wDeveloperDocumentation(TargetSection ts) { /* nothing written here for Variable */ }
    
    @Override
    protected void wDeveloperDocumentationHeader(TargetSection ts) { /* nothing written here for Variable */ }
}
