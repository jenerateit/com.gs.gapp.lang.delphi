package com.gs.gapp.generation.objectpascal.target;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

public class DelphiTargetDocumentTest {

    private final String IF_ELSE_COMPILER_DIRECTIVE = "uses\r\n" + 
            "{$ifdef Delphi10_Umstellung}\r\n" + 
            "System.SysUtils,\r\n" + 
            "System.UITypes\r\n" + 
            "{$else}\r\n" + 
            "SysUtils,\r\n" + 
            "Controls\r\n" + 
            "{$endif}\r\n" + 
            ";";
    
    private final String MULTIPLE_COMPILER_DIRECTIVES = "uses\r\n" + 
            "{$ifdef Delphi10_Umstellung}\r\n" + 
            "System.SysUtils,\r\n" + 
            "System.UITypes,\r\n" + 
            "{$ifdef Delphi20_Umstellung}\r\n" + 
            "unit20,\r\n" + 
            "unit20A\r\n" + 
            "{$else}\r\n" + 
            "unit20not,\r\n" + 
            "unit20notA\r\n" + 
            "{$endif}\r\n" + 
            "{$else}\r\n" + 
            "SysUtils,\r\n" + 
            "Controls\r\n" + 
            "{$endif}\r\n" + 
            ";";
    
    private final String COMMENTED_USES = "uses\r\n" + 
            "Use1,\r\n" + 
            "Use2,\r\n" +
            "//Use3,\r\n" +
            "Use4,\r\n" +
            "// Use5,\r\n" +
            ";";
    
    @BeforeClass
    public static void setupBeforeClass() {}

    @Test
    public void testParsingUseForIfElseCompilerDirectives() {
        
        
        DelphiTargetDocument delphiTargetDocument = new DelphiTargetDocument() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<String> getUses(DelphiTargetSectionEnum tsEnum, String content, boolean trim, boolean withComma) {
                return super.getUses(tsEnum, content, trim, withComma);
            }
        };
        
        List<String> usesWithComma = delphiTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES, IF_ELSE_COMPILER_DIRECTIVE, true, true, true);
        int totalNumberOfOpeningCurlyBraces = 0;
        int totalNumberOfClosingCurlyBraces = 0;
        System.out.println("IF_ELSE_COMPILER_DIRECTIVE, with comma");
        for (String use : usesWithComma) {
            int numberOfOpeningCurlyBraces = use.length() - use.replace("{", "").length();
            int numberOfClosingCurlyBraces = use.length() - use.replace("}", "").length();
            totalNumberOfOpeningCurlyBraces += numberOfOpeningCurlyBraces;
            totalNumberOfClosingCurlyBraces += numberOfClosingCurlyBraces;
            System.out.println(use);
        }
        
        assertTrue("wrong number of uses parsed", usesWithComma.size() == 7);
        assertTrue("wrong number of opening curly braces", totalNumberOfOpeningCurlyBraces == 3);
        assertTrue("wrong number of closing curly braces", totalNumberOfClosingCurlyBraces == 3);
        
        
        totalNumberOfOpeningCurlyBraces = 0;
        totalNumberOfClosingCurlyBraces = 0;
        List<String> usesWithoutComma = delphiTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES, IF_ELSE_COMPILER_DIRECTIVE, true, false);
        System.out.println("IF_ELSE_COMPILER_DIRECTIVE, without comma");
        for (String use : usesWithoutComma) {
            int numberOfOpeningCurlyBraces = use.length() - use.replace("{", "").length();
            int numberOfClosingCurlyBraces = use.length() - use.replace("}", "").length();
            totalNumberOfOpeningCurlyBraces += numberOfOpeningCurlyBraces;
            totalNumberOfClosingCurlyBraces += numberOfClosingCurlyBraces;
            System.out.println(use);
        }
        
        assertTrue("wrong number of uses parsed", usesWithComma.size() == 7);
        assertTrue("wrong number of opening curly braces", totalNumberOfOpeningCurlyBraces == 3);
        assertTrue("wrong number of closing curly braces", totalNumberOfClosingCurlyBraces == 3);
    }
    
    /**
     * 
     */
    @Test
    public void testParsingUseForMultipleCompilerDirectives() {
        
        
        DelphiTargetDocument delphiTargetDocument = new DelphiTargetDocument() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<String> getUses(DelphiTargetSectionEnum tsEnum, String content, boolean trim, boolean withComma) {
                return super.getUses(tsEnum, content, trim, withComma);
            }
        };
        
        List<String> usesWithComma = delphiTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES, MULTIPLE_COMPILER_DIRECTIVES, true, true, true);
        System.out.println("MULTIPLE_COMPILER_DIRECTIVES, with comma");
        int totalNumberOfOpeningCurlyBraces = 0;
        int totalNumberOfClosingCurlyBraces = 0;
        for (String use : usesWithComma) {
            int numberOfOpeningCurlyBraces = use.length() - use.replace("{", "").length();
            int numberOfClosingCurlyBraces = use.length() - use.replace("}", "").length();
            totalNumberOfOpeningCurlyBraces += numberOfOpeningCurlyBraces;
            totalNumberOfClosingCurlyBraces += numberOfClosingCurlyBraces;
            System.out.println(use);
        }
        assertTrue("wrong number of uses parsed", usesWithComma.size() == 14);
        assertTrue("wrong number of opening curly braces", totalNumberOfOpeningCurlyBraces == 6);
        assertTrue("wrong number of closing curly braces", totalNumberOfClosingCurlyBraces == 6);
        
        
        totalNumberOfOpeningCurlyBraces = 0;
        totalNumberOfClosingCurlyBraces = 0;
        List<String> usesWithoutComma = delphiTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES, MULTIPLE_COMPILER_DIRECTIVES, true, false);
        System.out.println("MULTIPLE_COMPILER_DIRECTIVES, without comma");
        for (String use : usesWithoutComma) {
            int numberOfOpeningCurlyBraces = use.length() - use.replace("{", "").length();
            int numberOfClosingCurlyBraces = use.length() - use.replace("}", "").length();
            totalNumberOfOpeningCurlyBraces += numberOfOpeningCurlyBraces;
            totalNumberOfClosingCurlyBraces += numberOfClosingCurlyBraces;
            System.out.println(use);
        }
        
        assertTrue("wrong number of uses parsed", usesWithComma.size() == 14);
        assertTrue("wrong number of opening curly braces", totalNumberOfOpeningCurlyBraces == 6);
        assertTrue("wrong number of closing curly braces", totalNumberOfClosingCurlyBraces == 6);
    }
    
    /**
     * 
     */
    @Test
    public void testParsingUseForCommentedUses() {
        DelphiTargetDocument delphiTargetDocument = new DelphiTargetDocument() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<String> getUses(DelphiTargetSectionEnum tsEnum, String content, boolean trim, boolean withComma) {
                return super.getUses(tsEnum, content, trim, withComma);
            }
        };
        
        List<String> usesWithComma = delphiTargetDocument.getUses(DelphiTargetSectionEnum.INTERFACE_USES, COMMENTED_USES, true, true, true);
        System.out.println("COMMENTED_USES, with comma");
        int totalNumberOfComments = 0;
        for (String use : usesWithComma) {
            if (use.contains("//")) totalNumberOfComments++;
            System.out.println(use);
        }
        assertTrue("wrong number of uses parsed", usesWithComma.size() == 5);
        assertTrue("wrong number of commented uses", totalNumberOfComments == 2);
    }
}
