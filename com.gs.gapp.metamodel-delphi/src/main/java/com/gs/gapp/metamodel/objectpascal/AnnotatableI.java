package com.gs.gapp.metamodel.objectpascal;

import java.util.Set;

/**
 * @author mmt
 *
 */
public interface AnnotatableI {

	/**
	 * @return
	 */
	Set<AttributeUsage> getAttributeUsages();
	
	/**
	 * @param attributeUsage
	 * @return
	 */
	boolean addAttributeUsage(AttributeUsage attributeUsage);
}
