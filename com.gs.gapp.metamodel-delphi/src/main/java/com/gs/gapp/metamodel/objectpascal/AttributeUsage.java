/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.gs.gapp.metamodel.objectpascal.type.Attribute;

/**
 * @author mmt
 *
 */
public class AttributeUsage extends ObjectPascalModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8088209880574677761L;
	
	private final Attribute attribute;
	private final AnnotatableI annotatedElement;
	private final Set<String> attributeParameters = new LinkedHashSet<>();


	/**
	 * @param attribute
	 * @param annotatedElement
	 */
	public AttributeUsage(Attribute attribute, AnnotatableI annotatedElement) {
		this(attribute, annotatedElement, new String[0]);
	}
	
	/**
	 * @param attribute
	 * @param annotatedElement
	 * @param parameters
	 */
	public AttributeUsage(Attribute attribute, AnnotatableI annotatedElement, String... parameters) {
		super(attribute.getName());
		this.attribute = attribute;
		this.annotatedElement = annotatedElement;
		
		annotatedElement.addAttributeUsage(this);
		
		if (parameters != null) {
			List<String> parametersList = Stream.of(parameters)
				.filter(paramName -> paramName != null && paramName.trim().length() > 0)
			    .collect(Collectors.toList());
			attributeParameters.addAll(parametersList);
		}
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public AnnotatableI getAnnotatedElement() {
		return annotatedElement;
	}

	public Set<String> getAttributeParameters() {
		return attributeParameters;
	}
}
