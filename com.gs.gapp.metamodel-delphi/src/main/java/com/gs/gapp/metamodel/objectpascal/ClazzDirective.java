/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashMap;
import java.util.Map;



/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Fundamental_Syntactic_Elements#Directives">Delphi XE7 - Directives</a>
 *
 */
public enum ClazzDirective {
	
	
	ABSTRACT (Directive.ABSTRACT),
	SEALED (Directive.SEALED),
	;
	
	private static final Map<Directive, ClazzDirective> stringToEnum = new LinkedHashMap<>();
	
	static {
		for (ClazzDirective visibilityDirective : values()) {
			stringToEnum.put(visibilityDirective.getDirective(), visibilityDirective);
		}
	}
	
	public static ClazzDirective fromString(String name) {
	    Directive directive = Directive.fromString(name);
	    if (directive != null) {
		    return stringToEnum.get(directive);
	    }
	    
	    return null;
	}
	
	private final Directive directive;
	
	private ClazzDirective(Directive directive) {
		this.directive = directive;
	}

	public Directive getDirective() {
		return directive;
	}
	
	public String getName() {
		return directive.getName();
	}
}
