/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Declared_Constants">Delphi XE7 - Constants</a>
 */
public abstract class Constant extends ObjectPascalModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1649222934132151494L;
	
	private final Unit declaringUnit;
	
	private final boolean onlyVisibleInImplementationSection;
	
	private final String expression;

	/**
	 * @param name
	 */
	public Constant(String name, String expression) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		declaringUnit = null;
		this.onlyVisibleInImplementationSection = false;
		this.expression = expression;
		
		init();
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Constant(String name, String expression, Unit declaringUnit) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		this.declaringUnit = declaringUnit;
		this.onlyVisibleInImplementationSection = false;
		this.expression = expression;
		
		init();
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Constant(String name, String expression, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		this.declaringUnit = declaringUnit;
		this.onlyVisibleInImplementationSection = onlyVisibleInImplementationSection;
		this.expression = expression;
		
		init();
	}
	
	private void init() {
		if (declaringUnit != null) {
			declaringUnit.addConstant(this);
		}
	}

	public Unit getDeclaringUnit() {
		return declaringUnit;
	}

	public boolean isOnlyVisibleInImplementationSection() {
		return onlyVisibleInImplementationSection;
	}

	public String getExpression() {
		return expression;
	}
	
	/**
	 * @return
	 */
	public String getDeveloperAreaKey() {
		StringBuilder sb = new StringBuilder(getDeclaringUnit() != null ? getDeclaringUnit().getQualifiedName() : "").append(".constant.").append(getName().toLowerCase());
		return sb.toString();
	}
	
	/**
	 * adapts the member name in case the member name is a reserved word in Delphi
	 * 
	 * @return the member name prepended with ampersand in case the member name is a reserved word in Delphi
	 */
	public String getNonReservedWordName() {
		return Keyword.adaptIdentifierIfNeeded(getName());
	}
}
