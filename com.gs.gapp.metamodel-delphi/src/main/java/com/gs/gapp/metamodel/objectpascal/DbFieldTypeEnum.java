package com.gs.gapp.metamodel.objectpascal;

import java.util.HashMap;
import java.util.Map;

import com.gs.gapp.dsl.delphi.DelphiOptionEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;

/**
 * TODO There is not yet a default mapping for the following primitive types:
 * UINT64, FLOAT16, FLOAT128, CHARACTER
 * @author mmt
 *
 */
public enum DbFieldTypeEnum {
	
    FT_UNKNOWN ("ftUnknown"),
    FT_STRING ("ftString", PrimitiveTypeEnum.STRING),
    FT_SMALLINT ("ftSmallint", PrimitiveTypeEnum.SINT16),
    FT_INTEGER ("ftInteger", PrimitiveTypeEnum.SINT32),
    FT_WORD ("ftWord", PrimitiveTypeEnum.UINT16),
    FT_BOOLEAN ("ftBoolean", PrimitiveTypeEnum.UINT1),
    FT_FLOAT ("ftFloat", PrimitiveTypeEnum.FLOAT64),
    FT_CURRENCY ("ftCurrency", PrimitiveTypeEnum.SINT128),
    FT_BCD ("ftBCD", PrimitiveTypeEnum.UINT128),
    FT_DATE ("ftDate", PrimitiveTypeEnum.DATE),
    FT_TIME ("ftTime", PrimitiveTypeEnum.TIME),
    FT_DATE_TIME ("ftDateTime", PrimitiveTypeEnum.DATETIME),
    FT_BYTEST ("ftBytes", PrimitiveTypeEnum.STRING),
    FT_VAR_BYTES ("ftVarBytes", PrimitiveTypeEnum.STRING),
    FT_AUTO_INC ("ftAutoInc", PrimitiveTypeEnum.UINT32),
    FT_BLOB ("ftBlob", PrimitiveTypeEnum.STRING),
    FT_MEMO ("ftMemo", PrimitiveTypeEnum.STRING),
    FT_GRAPHIC ("ftGraphic"),
    FT_FMT_MEMO ("ftFmtMemo", PrimitiveTypeEnum.STRING),
    FT_PARADOX_OLE ("ftParadoxOle"),
    FT_DBASE_OLE ("ftDBaseOle"),
    FT_TYPED_BINARY ("ftTypedBinary"),
    FT_CURSOR ("ftCursor"),
    FT_FIXED_CHAR ("ftFixedChar"),
    FT_WIDE_STRING ("ftWideString", DelphiPrimitiveTypeEnum.WIDE_STRING),
    FT_LARGEINT ("ftLargeint", PrimitiveTypeEnum.SINT64),
    FT_ADT ("ftADT"),
    FT_ARRAY ("ftArray"),
    FT_REFERENCE ("ftReference"),
    FT_DATA_SET ("ftDataSet"),
    FT_ORA_BLOB ("ftOraBlob", PrimitiveTypeEnum.STRING),
    FT_ORA_CLOB ("ftOraClob", PrimitiveTypeEnum.STRING),
    FT_VARIANT ("ftVariant"),
    FT_INTERFACE ("ftInterface"),
    FT_IDISPATCH ("ftIDispatch"),
    FT_GUID ("ftGuid", PrimitiveTypeEnum.STRING),
    FT_TIME_STAMP ("ftTimeStamp"),
    FT_FMT_BCD ("ftFMTBcd"),
    FT_FIXED_WIDE_CHAR ("ftFixedWideChar", DelphiPrimitiveTypeEnum.WIDE_STRING),
    FT_WIDE_MEMO ("ftWideMemo", DelphiPrimitiveTypeEnum.WIDE_STRING),
    FT_ORA_TIME_STAMP ("ftOraTimeStamp"),
    FT_ORA_INTERVAL ("ftOraInterval"),
    FT_LONG_WORD ("ftLongWord"),
    FT_SHORTINT ("ftShortint", PrimitiveTypeEnum.SINT8),
    FT_BYTE ("ftByte", PrimitiveTypeEnum.UINT8),
    FT_EXTENDED ("ftExtended"),
    FT_CONNECTION ("ftConnection"),
    FT_PARAMS ("ftParams"),
    FT_STREAM ("ftStream", PrimitiveTypeEnum.STRING),
    FT_TIME_STAMP_OFFSET ("ftTimeStampOffset"),
    FT_OBJECT ("ftObject"),
    FT_SINGLE ("ftSingle", PrimitiveTypeEnum.FLOAT32),
	;
	
	private static final Map<String,DbFieldTypeEnum> map = new HashMap<>();
	
	static {
		for (DbFieldTypeEnum enumEntry : DbFieldTypeEnum.values()) {
			map.put(enumEntry.getName(), enumEntry);
		}
	}
	
	public static DbFieldTypeEnum get(String name) {
		return map.get(name);
	}
	
	public static DbFieldTypeEnum getDefaultDbFieldType(PrimitiveTypeEnum primitiveType) {
		if (primitiveType != null) {
			for (DbFieldTypeEnum enumEntry : DbFieldTypeEnum.values()) {
				if (enumEntry.getPrimitiveType() == primitiveType) return enumEntry;
			}
		}
		
		return null;
	}
	
	public static DbFieldTypeEnum getDefaultDbFieldType(DelphiPrimitiveTypeEnum delphiPrimitiveType) {
		if (delphiPrimitiveType != null) {
			for (DbFieldTypeEnum enumEntry : DbFieldTypeEnum.values()) {
				if (enumEntry.getDelphiPrimitiveType() == delphiPrimitiveType) return enumEntry;
			}
		}
		
		return null;
	}
	
	/**
	 * @param field
	 * @return
	 */
	public static DbFieldTypeEnum getDbFieldType(Field field) {
		DbFieldTypeEnum result = null;
		if (field.getType() instanceof PrimitiveType) {
			PrimitiveType primitiveType = (PrimitiveType) field.getType();
			if (primitiveType.getConstraints() != null) {
			    primitiveType = primitiveType.getConstraints().getRootConstrainedType(); // CHQDG-153
			}
			PrimitiveTypeEnum primitiveTypeEnum = primitiveType.getOriginatingElement(PrimitiveTypeEnum.class);
			if (primitiveTypeEnum != null) {
				result = getDefaultDbFieldType(primitiveTypeEnum);
			} else {
				// in this case we have a Delphi specific primitive type modeled
				DelphiPrimitiveTypeEnum delphiPrimitiveTypeEnum = DelphiPrimitiveTypeEnum.get(primitiveType.getName());
				result = getDefaultDbFieldType(delphiPrimitiveTypeEnum);
			}
			
			OptionDefinition<String>.OptionValue dbFieldTypeOptionValue = field.getOption(DelphiOptionEnum.DB_FIELD_TYPE.getName(), String.class);
			if (dbFieldTypeOptionValue != null && dbFieldTypeOptionValue.getOptionValue() != null && dbFieldTypeOptionValue.getOptionValue().length() > 0) {
				DbFieldTypeEnum modeledDbFieldType = DbFieldTypeEnum.get(dbFieldTypeOptionValue.getOptionValue());
				if (modeledDbFieldType != null) result = modeledDbFieldType;
			}
		} else if (field.getType() instanceof Enumeration) {
			result = DbFieldTypeEnum.FT_STRING; // Enumeration types are always going
		}
		
		return result;
	}
	
	private final String name;
	private final PrimitiveTypeEnum primitiveType;
	private final DelphiPrimitiveTypeEnum delphiPrimitiveType;
	
	private DbFieldTypeEnum(String name) {
		this(name, (PrimitiveTypeEnum)null);
	}
	
	private DbFieldTypeEnum(String name, PrimitiveTypeEnum primitiveType) {
		this.name = name;
		this.primitiveType = primitiveType;
		this.delphiPrimitiveType = null;
	}
	
	private DbFieldTypeEnum(String name, DelphiPrimitiveTypeEnum delphiPrimitiveType) {
		this.name = name;
		this.primitiveType = null;
		this.delphiPrimitiveType = delphiPrimitiveType;
	}

	public String getName() {
		return name;
	}

	public PrimitiveTypeEnum getPrimitiveType() {
		return primitiveType;
	}

	public DelphiPrimitiveTypeEnum getDelphiPrimitiveType() {
		return delphiPrimitiveType;
	}
	
	/**
	 * @param field
	 * @return
	 */
	public boolean isUnusualMapping(Field field) {
		boolean result = false;
		if (field.getType() != null) {
			if (getPrimitiveType() != null && getPrimitiveType().getPrimitiveType() != field.getType()) {
				result = true;
			} else if (getDelphiPrimitiveType() != null && !getDelphiPrimitiveType().getDelphiTypeName().equalsIgnoreCase(field.getType().getName())) {
				result = true;
			}
		}
		
		return result;
	}
}
