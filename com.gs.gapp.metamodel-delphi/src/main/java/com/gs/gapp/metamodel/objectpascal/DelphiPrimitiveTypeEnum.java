package com.gs.gapp.metamodel.objectpascal;

import java.util.HashMap;
import java.util.Map;

public enum DelphiPrimitiveTypeEnum {
	
    SHORT_STRING ("Delphi_ShortString", "ShortString"),
    ANSI_STRING ("Delphi_AnsiString", "AnsiString"),
    UNICODE_STRING ("Delphi_UnicodeString", "UnicodeString"),
    WIDE_STRING ("Delphi_WideString", "WideString"),
	;
	
	private static final Map<String,DelphiPrimitiveTypeEnum> map = new HashMap<>();
	
	static {
		for (DelphiPrimitiveTypeEnum enumEntry : DelphiPrimitiveTypeEnum.values()) {
			map.put(enumEntry.getName(), enumEntry);
		}
	}
	
	public static DelphiPrimitiveTypeEnum get(String name) {
		return map.get(name);
	}
	
	public static String getDelphiTypeName(String name) {
		DelphiPrimitiveTypeEnum delphiPrimitiveTypeEnum = get(name);
		return delphiPrimitiveTypeEnum != null ? delphiPrimitiveTypeEnum.getDelphiTypeName() : null;
	}
	
	private final String name;
	private final String delphiTypeName;
	
	
	private DelphiPrimitiveTypeEnum(String name, String delphiTypeName) {
		this.name = name;
		this.delphiTypeName = delphiTypeName;
	}

	public String getName() {
		return name;
	}

	public String getDelphiTypeName() {
		return delphiTypeName;
	}
}
