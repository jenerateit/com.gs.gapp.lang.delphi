package com.gs.gapp.metamodel.objectpascal;

import java.util.ArrayList;
import java.util.List;

public enum DelphiVersionEnum {
	
	DELPHI_07,
	DELPHI_08,
	DELPHI_09,
	DELPHI_10,
	DELPHI_11,
	DELPHI_12,
	DELPHI_14,
	DELPHI_XE,
	DELPHI_XE2,
	DELPHI_XE3,
	DELPHI_XE4,
	DELPHI_XE5,
	DELPHI_XE6,
	DELPHI_XE7,
	DELPHI_10_SEATTLE,
	;
	

	/**
	 * @param delphiVersionEnum
	 * @return true if this delphi version is the same or a newer version than the one provided through the param 'delhpiVersionEnum'
	 */
	public boolean isSameOrNewerVersionThan(DelphiVersionEnum delphiVersionEnum) {
		return this.ordinal() >= delphiVersionEnum.ordinal();
	}
	
	public boolean isNewerVersionThan(DelphiVersionEnum delphiVersionEnum) {
		return this.ordinal() > delphiVersionEnum.ordinal();
	}
	
	public boolean isOlderVersionThan(DelphiVersionEnum delphiVersionEnum) {
		return this.ordinal() < delphiVersionEnum.ordinal();
	}
	
	public boolean isSameOrOlderVersionThan(DelphiVersionEnum delphiVersionEnum) {
		return this.ordinal() <= delphiVersionEnum.ordinal();
	}
	
	/**
	 * @return
	 */
	public static List<String> getNames() {
		List<String> result = new ArrayList<>();
		
		for (DelphiVersionEnum enumEntry : values()) {
			result.add(enumEntry.name());
		}
		
		return result;
	}
	
	public static DelphiVersionEnum getDefault() {
		DelphiVersionEnum[] values = values();
		return values[values.length-1];
	}
}
