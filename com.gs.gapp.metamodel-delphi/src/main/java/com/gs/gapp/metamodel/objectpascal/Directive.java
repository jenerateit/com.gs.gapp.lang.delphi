/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;



/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Fundamental_Syntactic_Elements#Directives">Delphi XE7 - Directives</a>
 *
 */
public enum Directive {
	
	ABSOLUTE ("absolute"),
	ABSTRACT ("abstract"),
	ASSEMBLER ("assembler"),
	AUTOMATED ("automated"),
	CDECL ("cdecl"),
	CONTAINS ("contains"),
	DEFAULT ("default"),
	DELAYED ("delayed"),
	DEPRECATED ("deprecated"),
	DISPID ("dispid"),
	DYNAMIC ("dynamic"),
	EXPERIMENTAL ("experimental"),
	
	EXPORT ("export"),
	EXTERNAL ("external"),
	FAR ("far"),
	FINAL ("final"),
	FORWARD ("forward"),
	HELPER ("helper"),
	IMPLEMENTS ("implements"),
	INDEX ("index"),
	INLINE ("inline"),
	LIBRARY ("library"),
	LOCAL ("local"),
	MESSAGE ("message"),
	
	NAME ("name"),
	NEAR ("near"),
	NODEFAULT ("nodefault"),
	OPERATOR ("operator"),
	OUT ("out"),
	OVERLOAD ("overload"),
	OVERRIDE ("override"),
	PACKAGE ("package"),
	PASCAL ("pascal"),
	PLATFORM ("platform"),
	PRIVATE ("private"),
	PROTECTED ("protected"),
	
	PUBLIC ("public"),
	PUBLISHED ("published"),
	READ ("read"),
	READONLY ("readonly"),
	REFERENCE ("reference"),
	REGISTER ("register"),
	REINTRODUCE ("reintroduce"),
	REQUIRES ("requires"),
	RESIDENT ("resident"),
	SAFECALL ("safecall"),
	SEALED ("sealed"),
	STATIC ("static"),
	
	STDCALL ("stdcall"),
	STRICT ("strict"),
	STORED ("stored"),
	UNSAFE ("unsafe"),
	VARARGS ("varargs"),
	VIRTUAL ("virtual"),
	WINAPI ("winapi"),
	WRITE ("write"),
	WRITEONLY ("writeonly"),
	;
	
	private static final Map<String, Directive> stringToEnum =
			new LinkedHashMap<>();
	
	private static final Set<Directive> propertySpecifiers =
			EnumSet.of(READ, WRITE, STORED, DEFAULT, NODEFAULT, IMPLEMENTS);
	
	private static final Set<Directive> visibilityScopeSpecifiers =
			EnumSet.of(PRIVATE, PROTECTED, PUBLIC, PUBLISHED, STRICT);
	
	static {
		for (Directive directive : values()) {
			stringToEnum.put(directive.name, directive);
		}
	}
	
	public static Directive fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
	
	private final String name;
	
	private Directive(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static Set<Directive> getPropertyspecifiers() {
		return propertySpecifiers;
	}

	public static Set<Directive> getVisibilityscopespecifiers() {
		return visibilityScopeSpecifiers;
	}
	
	public boolean isVisibilityScopeSpecifier() {
		return visibilityScopeSpecifiers.contains(this);
	}
	
	public boolean isPropertySpecifier() {
		return propertySpecifiers.contains(this);
	}
}
