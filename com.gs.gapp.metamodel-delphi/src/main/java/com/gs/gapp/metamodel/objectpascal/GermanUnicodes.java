/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashMap;
import java.util.Map;



/**
 *
 */
public enum GermanUnicodes {
	
	UPPERCASE_AE ("#196"),
	LOWERCASE_AE ("#228"),
	UPPERCASE_OE ("#214"),
	LOWERCASE_OE ("#246"),
	UPPERCASE_UE ("#220"),
	LOWERCASE_UE ("#252"),
	SCHARF_S ("#223"),
	EURO ("#8364"),
	;
	
	private static final Map<String, GermanUnicodes> stringToEnum =
			new LinkedHashMap<>();
	
	static {
		for (GermanUnicodes keyword : values()) {
			stringToEnum.put(keyword.code, keyword);
		}
	}
	
	public static GermanUnicodes fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
	
	private final String code;
	
	private GermanUnicodes(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
}
