/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashMap;
import java.util.Map;



/**
 *
 */
public enum Keyword {
	
	AND ("and"),
	ARRAY ("array"),
	AS ("as"),
	ASM ("asm"),
	BEGIN ("begin"),
	CASE ("case"),
	CLASS ("class"),
	CONST ("const"),
	CONSTRUCTOR ("constructor"),
	DESTRUCTOR ("destructor"),
	DISPINTERFACE ("dispinterface"),
	DIV ("div"),
	DO ("do"),
	DOWNTO ("downto"),
	ELSE ("else"),
	END ("end"),
	EXCEPT ("except"),
	EXPORTS ("exports"),
	FILE ("file"),
	FINALIZATION ("finalization"),
	FINALLY ("finally"),
	FOR ("for"),
	FUNCTION ("function"),
	GOTO ("goto"),
	IF ("if"),
	IMPLEMENTATION ("implementation"),
	IN ("in"),
	INHERITED ("inherited"),
	INITIALIZATION ("initialization"),
	INLINE ("inline"),
	INTERFACE ("interface"),
	IS ("is"),
	LABEL ("label"),
	LIBRARY ("library"),
	MOD ("mod"),
	NIL ("nil"),
	NOT ("not"),
	OBJECT ("object"),
	OF ("of"),
	OR ("or"),
	OUT ("out"),
	PACKED ("packed"),
	PROCEDURE ("procedure"),
	PROGRAM ("program"),
	PROPERTY ("property"),
	RAISE ("raise"),
	RECORD ("record"),
	REPEAT ("repeat"),
	RESOURCESTRING ("resourcestring"),
	SET ("set"),
	SHL ("shl"),
	SHR ("shr"),
	STRING ("string"),
	THEN ("then"),
	THREADVAR ("threadvar"),
	TO ("to"),
	TRY ("try"),
	TYPE ("type"),
	UNIT ("unit"),
	UNTIL ("until"),
	USES ("uses"),
	VAR ("var"),
	WHILE ("while"),
	WITH ("with"),
	XOR ("xor"),
	;
	
	
	
	private static final Map<String, Keyword> stringToEnum =
			new LinkedHashMap<>();
	
	static {
		for (Keyword keyword : values()) {
			stringToEnum.put(keyword.name, keyword);
		}
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static Keyword fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
	
	/**
	 * @param identifier
	 * @return
	 */
	public static boolean isKeyword(String identifier) {
		return identifier != null && stringToEnum.containsKey(identifier.toLowerCase());
	}
	
	public static String adaptIdentifierIfNeeded(String identifier) {
		if (identifier != null && identifier.trim().length() > 0 && !identifier.startsWith("&")) {
		    Keyword keyword = Keyword.fromString(identifier.trim().toLowerCase());
		    if (keyword != null) {
		    	return "&" + identifier;
		    }
		}
		
		return identifier;
	}
	
	private final String name;
	
	private Keyword(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
