/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashMap;
import java.util.Map;



/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Fundamental_Syntactic_Elements#Directives">Delphi XE7 - Directives</a>
 *
 */
public enum MethodDirective {
	
	
	ABSTRACT (Directive.ABSTRACT),
	OVERLOAD (Directive.OVERLOAD),
	OVERRIDE (Directive.OVERRIDE),
	VIRTUAL (Directive.VIRTUAL),
	REINTRODUCE (Directive.REINTRODUCE),
	
	// --- calling conventions
	REGISTER (Directive.REGISTER),
	PASCAL (Directive.PASCAL),
	CDECL (Directive.CDECL),
	STDCALL (Directive.STDCALL),
	SAFECALL (Directive.SAFECALL),
	// backwards compatibility for 16-bit Windows programming - not supported here
//	NEAR (Directive.NEAR),  
//	FAR (Directive.FAR),
//	EXPORT (Directive.EXPORT),
	
	VARARGS (Directive.VARARGS),
	
	/**
	 * this is used to import routines from DLLs (needs this syntax: external stringConstant1 name stringConstant2;)
	 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Libraries_and_Packages#Delayed_Loading">Delphi XE7 - Delayed Loading of DLLs</a>
	 */
	EXTERNAL (Directive.EXTERNAL),
	
	DEPRECATED (Directive.DEPRECATED),
	;
	
	private static final Map<Directive, MethodDirective> stringToEnum = new LinkedHashMap<>();
	
	static {
		for (MethodDirective visibilityDirective : values()) {
			stringToEnum.put(visibilityDirective.getDirective(), visibilityDirective);
		}
	}
	
	public static MethodDirective fromString(String name) {
	    Directive directive = Directive.fromString(name);
        if (directive != null) {
		    return stringToEnum.get(directive);
        }
        
        return null;
	}
	
	private final Directive directive;
	
	private MethodDirective(Directive directive) {
		this.directive = directive;
	}

	public Directive getDirective() {
		return directive;
	}
	
	public String getName() {
		return directive.getName();
	}
}
