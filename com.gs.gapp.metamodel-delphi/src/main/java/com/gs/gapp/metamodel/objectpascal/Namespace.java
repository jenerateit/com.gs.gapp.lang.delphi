/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class Namespace extends ObjectPascalModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2500473828438689908L;
	
	/**
	 * @param nsString
	 * @return
	 */
	public static Namespace convertToNamespaceObject(String nsString) {
		Namespace result = null;
		String[] nsArr = nsString.split("[.]");
		for (String nsPart : nsArr) {
			result = new Namespace(nsPart, result);
		}
		return result;
	}
	
	private final Set<Unit> units = new LinkedHashSet<>();
	private final Namespace parentNamespace;

	/**
	 * This constructor is intended to be used for root namespaces
	 * 
	 * @param name
	 */
	public Namespace(String name) {
		super(name);
		
		this.parentNamespace = null;
	}
	
	/**
	 * @param name
	 * @param parentNamespace
	 */
	public Namespace(String name, Namespace parentNamespace) {
		super(name);
		
		if (parentNamespace == null) throw new NullPointerException("parameter 'parentNamespace' must not be null");
		
		this.parentNamespace = parentNamespace;
	}

	public Set<Unit> getUnits() {
		return units;
	}
	
	boolean addUnit(Unit unit) {
		if (unit == null) throw new NullPointerException("parameter 'unit' must not be null");
		
		return this.units.add(unit);
	}

	public Namespace getParentNamespace() {
		return parentNamespace;
	}

	@Override
	public String getQualifiedName() {
        StringBuilder result = new StringBuilder();
        
        Namespace currentNamespace = this;

        do {
        	if (result.length() > 0) result.insert(0,  ".");
        	result.insert(0, currentNamespace.getName());
        	currentNamespace = currentNamespace.getParentNamespace();
        } while (currentNamespace != null);
        
		return result.toString();
	}
	
	
}
