/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * @author mmt
 *
 */
public enum ObjectPascalConstant {

	DEFAULT_NAMESPACE ("Default"),
	;
	
	private String name;
	
	private ObjectPascalConstant(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
