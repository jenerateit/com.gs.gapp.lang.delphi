package com.gs.gapp.metamodel.objectpascal;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum ObjectPascalMessage implements MessageI {

	// --------- ERRORS ------------
	UNKNOWN_ERROR (MessageStatus.ERROR,
			"0001",
			"An unexpected, unknown error has occurred: %s",
			null),
	
	ERROR_INVALID_DEFAULT_VALUE_FOR_FIELD (MessageStatus.ERROR,
			"0002",
			"The field '%s' has a default value '%s' modeled that is invalid for the field's type %s",
			"Please change the default value in the model to a valid value for the given type. Alternatively, you might want to change the field's type."),
	
	ERROR_EMPTY_IDENTIFIER (MessageStatus.ERROR,
			"0003",
			"The normalization of a model element with the name '%s' has led to an empty Delphi identifier. This can for instance happen when a model element name is a number.",
			"Please search for the model element that has the mentioned name and modify that name to get a valid Delphi identifier after the normalization procedure."),
	
	ERROR_DUPLICATE_TYPE_IN_UNIT (MessageStatus.ERROR,
			"0004",
			"The unit %s contains duplicate type declarations: %s",
			"Please contact the generator provider's support since this issue most probably is a bug in the generator."),
	
	// --------- WARNINGS ------------
	WARNING_INVALID_ENUMERATION_ENTRY_NAME_IS_GOING_TO_BE_PREFIXED (MessageStatus.WARNING,
			"1001",
			"During the internal generator processing an attempt to create an entry in the enumeration '%s' with an invalid Delphi identifier '%s' has been detected." +
			" The enumeration entry is automatically prefixed with %s.",
			null),
	
	WARNING_CANNOT_SET_PARENT_TYPE_FOR_CLASS_DUE_TO_MISSING_UNIT (MessageStatus.WARNING,
			"1002",
			"The class '%s' normally would have a parent class '%s'. But it apparently is not possible to generate that parent class since the generator cannot determine the parent's unit." +
			" That unit should have the unit name '%s'.",
			"Please contact the generator provider's support since this issue most probably is a bug in the generator."),
	
	// --------- INFOS ------------
	INFO_REWRITTEN_CONTENT_BETWEEN_UNIT_AND_DEV_DOC (MessageStatus.INFO,
			"2001",
			"Rewriting %s lines of code (%s thereof are non-empty lines), typically directives, between unit declaration and first dev doc in unit %s:" + System.lineSeparator() + "%s",
			null),
	INFO_NO_PREVIOUS_CONTENT_AVAILABLE (MessageStatus.INFO,
			"2002",
			"The unit %s is generated without applying any previously generated content. Most probably the unit is generated for the very first time.",
			null),
	INFO_NO_PREVIOUS_CONTENT_AVAILABLE_BETWEEN_UNIT_AND_DEV_DOC (MessageStatus.INFO,
			"2003",
			"The unit %s doesn't have any manually added lines between unit declaration and the very first dev doc. The previous document's size is %s bytes.",
			null),
	INFO_FOUND_MANUALLY_ADDED_LINES_BETWEEN_UNIT_AND_DEV_DOC (MessageStatus.INFO,
			"2004",
			"There is previously existing content for the unit %s. There are %s lines of code found between the unit declaration and the first dev doc.",
			null),
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "OBJECTPASCAL";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private ObjectPascalMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 