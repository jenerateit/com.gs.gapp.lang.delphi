/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.ArrayList;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public abstract class ObjectPascalModelElement extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8955690030384920915L;
	
	private final java.util.List<String> developerDocumentation = new ArrayList<>();
	
	private boolean developerDocumentationGenerated = true;
	
	public ObjectPascalModelElement(String name) {
		super(name);
	}

	/**
	 * @return
	 */
	public String getQualifiedName() {
		return this.getName();
	}
	
	public java.util.List<String> getDeveloperDocumentation() {
		return developerDocumentation;
	}
	
	/**
	 * The purpose of developer documentation is to re-add those comment lines
	 * to the generated file that previously were manually added by a developer.
	 * 
	 * @param developerDocumentation
	 * @return
	 */
	public boolean addDeveloperDocumentation(String developerDocumentation) {
		return this.developerDocumentation.add(developerDocumentation);
	}
	
	public String getDeveloperDocumentationHeader() {
		return this.getQualifiedName() + ".devdoc"; 
	}

	/**
	 * @return the developerDocumentationGenerated
	 */
	public boolean isDeveloperDocumentationGenerated() {
		return developerDocumentationGenerated;
	}

	/**
	 * @param developerDocumentationGenerated the developerDocumentationGenerated to set
	 */
	public void setDeveloperDocumentationGenerated(boolean developerDocumentationGenerated) {
		this.developerDocumentationGenerated = developerDocumentationGenerated;
	}
}
