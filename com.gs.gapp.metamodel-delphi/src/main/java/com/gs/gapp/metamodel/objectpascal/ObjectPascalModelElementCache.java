/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import com.gs.gapp.metamodel.basic.ModelElementCache;

/**
 * The CSharpModelElementCache is a facade to the basic ModelElementCache.
 * @author mmt
 *
 */
public class ObjectPascalModelElementCache extends ModelElementCache {


	/**
	 * 
	 */
	public ObjectPascalModelElementCache() {
		super();
	}
}
