package com.gs.gapp.metamodel.objectpascal;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import com.gs.gapp.metamodel.objectpascal.member.Constructor;
import com.gs.gapp.metamodel.objectpascal.member.Function;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Parameter;
import com.gs.gapp.metamodel.objectpascal.member.Parameter.ParamInfo;
import com.gs.gapp.metamodel.objectpascal.member.Procedure;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.ConstructedType;
import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalTypeValue;
import com.gs.gapp.metamodel.objectpascal.type.simple.SubRange;
import com.gs.gapp.metamodel.objectpascal.type.structured.Array;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;

public class ObjectPascalModelElementCreator {

	private final Map<BoundTypeKey,ConstructedType> constructedGenericTypeCache =
			new LinkedHashMap<>();

	/**
	 * @param type
	 * @param boundTypes
	 * @return the constructed type, if the given type is a generic type, null otherwise
	 */
	public ConstructedType makeGenericType(Type type, Type... boundTypes) {
		ConstructedType result = null;

//		if (type.getTypeParameters().size() > 0) {  TODO reverse engineering for delphi standard types does not give use infos about generic type parameters => do not check this
			synchronized (constructedGenericTypeCache) {

				BoundTypeKey key = new BoundTypeKey(type, boundTypes);
				result = constructedGenericTypeCache.get(key);

				if (result == null) {
					result = new ConstructedType(type, boundTypes);
					constructedGenericTypeCache.put(key, result);
				}
		    }
//		}
		return result;
	}

	/**
	 * @param name
	 * @param size
	 * @param indexType
	 * @param arrayType
	 * @param unit
	 * @return
	 */
	public static Array createStandardArray(String name, int size, com.gs.gapp.metamodel.objectpascal.type.simple.Integer indexType, Type arrayType, Unit unit) {
		Integer startIndex = 0;
		Integer endIndex = size-1;
		SubRange subRange = new SubRange(name + "-sub-range", indexType, new OrdinalTypeValue(startIndex.toString()), new OrdinalTypeValue(endIndex.toString()), unit);
		Array array = new Array(name, subRange, unit);
		
		return array;
	}
	
	/**
	 * @param name
	 * @param size
	 * @param indexType
	 * @param arrayType
	 * @param outerType
	 * @return
	 */
	public static Array createStandardArray(String name, int size, com.gs.gapp.metamodel.objectpascal.type.simple.Integer indexType, Type arrayType, OuterTypeI outerType) {
		Integer startIndex = 0;
		Integer endIndex = size-1;
		SubRange subRange = new SubRange(name + "-sub-range", indexType, new OrdinalTypeValue(startIndex.toString()), new OrdinalTypeValue(endIndex.toString()), outerType);
		Array array = new Array(name, subRange, outerType);
		
		return array;
	}

	/**
	 * @author mmt
	 *
	 */
	private final class BoundTypeKey {
		private final Type genericTypeDefinition;
		private final LinkedHashSet<Type> boundTypes = new LinkedHashSet<>();

		/**
		 * @param genericTypeDefinition
		 * @param boundTypes
		 */
		public BoundTypeKey (Type genericTypeDefinition, Type... boundTypes) {
			// TODO for the moment we have to ignore these checks since we do not have a way to reverse engineer generic type parameters by means of Delphi RTTI
//			if (genericTypeDefinition.getTypeParameters().size() == 0) throw new IllegalArgumentException("parameter 'genericTypeDefinition' is not a generic type definition");
//			if (boundTypes == null || boundTypes.length != genericTypeDefinition.getTypeParameters().size()) throw new IllegalArgumentException("parameter 'boundTypes' is either null or the number of bound types does not match the number of generic arguments of the generic type definition");

			this.genericTypeDefinition = genericTypeDefinition;
			if (boundTypes != null && boundTypes.length > 0) {
				for (final Type t : boundTypes) {
					if (t != null) {
						this.boundTypes.add(t);
					} else {
						throw new RuntimeException("tried to create a bound type key with one of its bound types being null");
					}
				}
			}
		}

		/**
		 * @return the genericTypeDefinition
		 */
		@SuppressWarnings("unused")
		public Type getGenericTypeDefinition() {
			return genericTypeDefinition;
		}
	}
	
    /**
     * @param constructor
     * @param ownerOfNewConstructor
     * @param paramNames
     * @return
     */
    public Constructor duplicateConstructor(Constructor constructor, Clazz ownerOfNewConstructor, String... paramNames) {
    	Constructor duplicatedConstructor = null;

    	List<ParamInfo> paramInfos = new ArrayList<>();
    	int paramCounter = 0;
    	for (Parameter param : constructor.getParameters()) {
    		String name = paramNames != null && paramNames.length > paramCounter ? paramNames[paramCounter] : param.getName();
    		paramInfos.add( new ParamInfo(param.getType(),
    				                      name,
    				                      param.getBody() == null || param.getBody().length() == 0 ? " the " + name : param.getBody(),
    				                      param.getDefaultValue()) );
    		paramCounter++;
    	}

    	duplicatedConstructor = createConstructor(constructor.getOriginatingElement(), ownerOfNewConstructor, paramInfos.toArray(new ParamInfo[0]));

    	Parameter[] originalParameters = constructor.getParameters().toArray(new Parameter[0]);
    	Parameter[] newParameters = duplicatedConstructor.getParameters().toArray(new Parameter[0]);

    	for (int ii=0; ii<originalParameters.length; ii++) {
    		newParameters[ii].setOriginatingElement(originalParameters[ii].getOriginatingElement());
    	}

    	return duplicatedConstructor;
    }
    
    /**
     * @param function
     * @param ownerOfNewFunction
     * @param paramNames
     * @return the duplicated function
     */
    public Function duplicateFunction(Function function, Type ownerOfNewFunction, String... paramNames) {
    	Function duplicatedFunction = null;

    	List<ParamInfo> paramInfos = new ArrayList<>();
    	int paramCounter = 0;
    	for (Parameter param : function.getParameters()) {
    		String name = paramNames != null && paramNames.length > paramCounter ? paramNames[paramCounter] : param.getName();
    		paramInfos.add( new ParamInfo(param.getType(),
    				                      name,
    				                      param.getBody() == null || param.getBody().length() == 0 ? " the " + name : param.getBody(),
    				                      param.getDefaultValue(),
    				                      param.getKeywords()) );
    		paramCounter++;
    	}

    	duplicatedFunction = Function.createFunction(function.getReturnType(),
    			                                     function.getDirectives(),
    			                                     function.getName(),
    			                                     ownerOfNewFunction,
    			                                     paramInfos.toArray(new ParamInfo[0]));
    	duplicatedFunction.setBody(function.getBody());
    	duplicatedFunction.getRemarks().addAll(function.getRemarks());
    	duplicatedFunction.setVisibility(function.getVisibility());
    	duplicatedFunction.setOriginatingElement(function);

    	Parameter[] originalParameters = function.getParameters().toArray(new Parameter[0]);
    	Parameter[] newParameters = duplicatedFunction.getParameters().toArray(new Parameter[0]);

    	for (int ii=0; ii<originalParameters.length; ii++) {
    		newParameters[ii].setOriginatingElement(originalParameters[ii].getOriginatingElement());
    	}

    	return duplicatedFunction;
    }
    
    /**
     * @param procedure
     * @param ownerOfNewProcedure
     * @param paramNames
     * @return
     */
    public Procedure duplicateProcedure(Procedure procedure, Type ownerOfNewProcedure, String... paramNames) {
    	Procedure duplicatedProcedure = null;

    	List<ParamInfo> paramInfos = new ArrayList<>();
    	int paramCounter = 0;
    	for (Parameter param : procedure.getParameters()) {
    		String name = paramNames != null && paramNames.length > paramCounter ? paramNames[paramCounter] : param.getName();
    		paramInfos.add( new ParamInfo(param.getType(),
    				                      name,
    				                      param.getBody() == null || param.getBody().length() == 0 ? " the " + name : param.getBody(),
    				                      param.getDefaultValue(),
    				                      param.getKeywords()) );
    		paramCounter++;
    	}

    	duplicatedProcedure = Procedure.createProcedure(procedure.getDirectives(),
    			                                       procedure.getName(),
    			                                       ownerOfNewProcedure,
    			                                       paramInfos.toArray(new ParamInfo[0]));
    	duplicatedProcedure.setBody(procedure.getBody());
    	duplicatedProcedure.getRemarks().addAll(procedure.getRemarks());
    	duplicatedProcedure.setOriginatingElement(procedure);
    	duplicatedProcedure.setVisibility(procedure.getVisibility());

    	Parameter[] originalParameters = procedure.getParameters().toArray(new Parameter[0]);
    	Parameter[] newParameters = duplicatedProcedure.getParameters().toArray(new Parameter[0]);

    	for (int ii=0; ii<originalParameters.length; ii++) {
    		newParameters[ii].setOriginatingElement(originalParameters[ii].getOriginatingElement());
    	}

    	return duplicatedProcedure;
    }
    
    /**
     * @param property
     * @param ownerOfNewProperty
     * @param paramNames
     * @return
     */
    public Property duplicateProperty(Property property, Type ownerOfNewProperty, String... paramNames) {
    	Property duplicatedProperty = null;

    	duplicatedProperty = new Property(property.getName(), property.getType(), ownerOfNewProperty);

    	duplicatedProperty.setBody(property.getBody());
    	duplicatedProperty.setOriginatingElement(property);
    	duplicatedProperty.setVisibility(property.getVisibility());

    	return duplicatedProperty;
    }
    
    /**
     * Creates a new interface or uses given interface to add methods to it
     * that are originally part of the given clazz.
     * 
     * @param clazz
     * @param delphiInterface
     * @param unit
     * @return
     */
    public Interface extractInterface(Clazz clazz, Interface delphiInterface, Unit unit) {
    	if (delphiInterface == null) {
    	    delphiInterface = new Interface("I"+clazz.getName(), unit == null ? clazz.getDeclaringUnit() : unit);
    	}
    	
    	Map<Method,Property> propertyReadMap = new LinkedHashMap<>();
    	Map<Method,Property> propertyWriteMap = new LinkedHashMap<>();
    	
    	for (Property property : clazz.getProperties(VisibilityDirective.PUBLIC)) {
   			Property duplicatedProperty = duplicateProperty(property, delphiInterface);
   			duplicatedProperty.setVisibility(VisibilityDirective.PUBLIC);
   			
   			if (property.getFieldOrMethodForRead() instanceof Method) {
   			    propertyReadMap.put((Method) property.getFieldOrMethodForRead(), duplicatedProperty);
   			}
   			if (property.getFieldOrMethodForWrite() instanceof Method) {
   			    propertyWriteMap.put((Method) property.getFieldOrMethodForWrite(), duplicatedProperty);
   			}
    	}
    	
    	for (Function function : clazz.getFunctions()) {
    		if (function.getVisibility() == VisibilityDirective.PUBLIC) {
    			Function duplicatedFunction = duplicateFunction(function, delphiInterface);
    			if (propertyReadMap.containsKey(function)) {
       				propertyReadMap.get(function).setFieldOrMethodForRead(duplicatedFunction);
       			}
       			if (propertyWriteMap.containsKey(function)) {
       				propertyWriteMap.get(function).setFieldOrMethodForWrite(duplicatedFunction);
       			}
    		}
    		
    	}
    	
    	for (Procedure procedure : clazz.getProcedures()) {
    		if (procedure.getVisibility() == VisibilityDirective.PUBLIC) {
    			Procedure duplicatedProcedure = duplicateProcedure(procedure, delphiInterface);
    			if (propertyReadMap.containsKey(procedure)) {
       				propertyReadMap.get(procedure).setFieldOrMethodForRead(duplicatedProcedure);
       			}
       			if (propertyWriteMap.containsKey(procedure)) {
       				propertyWriteMap.get(procedure).setFieldOrMethodForWrite(duplicatedProcedure);
       			}
    		}
    	}
    	
    	return delphiInterface;
    }
    
    /**
     * Let the given clazz get all the functions and procedures
     * that are part of the interface.
     *
     * @param delphiInterface
     * @param clazz
     * @return
     */
    public Clazz implementInterface(Interface delphiInterface, Clazz clazz) {
    	
    	Map<Method,Property> propertyReadMap = new LinkedHashMap<>();
    	Map<Method,Property> propertyWriteMap = new LinkedHashMap<>();
    	
    	for (Property property : delphiInterface.getProperties()) {
   			Property duplicatedProperty = duplicateProperty(property, clazz);
   			duplicatedProperty.setVisibility(VisibilityDirective.PUBLIC);
   			
   			if (property.getFieldOrMethodForRead() instanceof Method) {
   			    propertyReadMap.put((Method) property.getFieldOrMethodForRead(), duplicatedProperty);
   			}
   			if (property.getFieldOrMethodForWrite() instanceof Method) {
   			    propertyWriteMap.put((Method) property.getFieldOrMethodForWrite(), duplicatedProperty);
   			}
    	}
    	
    	for (Function function : delphiInterface.getFunctions()) {
   			Function duplicatedFunction = duplicateFunction(function, clazz);
   			duplicatedFunction.setVisibility(VisibilityDirective.PUBLIC);
   			if (propertyReadMap.containsKey(function)) {
   				propertyReadMap.get(function).setFieldOrMethodForRead(duplicatedFunction);
   			}
   			if (propertyWriteMap.containsKey(function)) {
   				propertyWriteMap.get(function).setFieldOrMethodForWrite(duplicatedFunction);
   			}
    	}
    	
    	for (Procedure procedure : delphiInterface.getProcedures()) {
   			Procedure duplicatedProcedure = duplicateProcedure(procedure, clazz);
   			duplicatedProcedure.setVisibility(VisibilityDirective.PUBLIC);
   			if (propertyReadMap.containsKey(procedure)) {
   				propertyReadMap.get(procedure).setFieldOrMethodForRead(duplicatedProcedure);
   			}
   			if (propertyWriteMap.containsKey(procedure)) {
   				propertyWriteMap.get(procedure).setFieldOrMethodForWrite(duplicatedProcedure);
   			}
    	}
    	
    	
    	
    	return clazz;
    }
    
    /**
     * @param origin
     * @param owner
     * @param paramInfos
     * @return
     */
    public Constructor createConstructor(Object origin, Type owner, ParamInfo... paramInfos) {

    	if (paramInfos != null) {
	    	for (ParamInfo paramInfo : paramInfos) {
	    		if (paramInfo == null) throw new NullPointerException("a passed param info object must not be null");
	    	}
    	}

    	List<Parameter> parameters = new ArrayList<>();
    	StringBuilder constructorName = new StringBuilder("constructor");
    	if (paramInfos != null) {
	    	for (ParamInfo paramInfo : paramInfos) {
				Parameter methodParameter =
	    			new Parameter(paramInfo.getName(), paramInfo.getType(), paramInfo.getKeywords(), paramInfo.getDefaultValue());
				if (paramInfo.getDoc() != null) {
					methodParameter.setBody(paramInfo.getDoc());
				} else {
					methodParameter.setBody(" the " + methodParameter.getName());
				}
				parameters.add(methodParameter);
				constructorName.append("_").append(paramInfo.getName());
	    	}
    	}

    	Constructor constructor = new Constructor(owner, parameters.toArray(new Parameter[0]));

    	if (origin != null) {
    		constructor.setOriginatingElement(origin);
    	}

    	return constructor;
    }
}
