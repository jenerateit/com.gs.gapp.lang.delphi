package com.gs.gapp.metamodel.objectpascal;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author marcu
 *
 */
public enum ObjectPascalNamingRulesEnforcer {

	INSTANCE,
	;
	
	private static final Set<Character> SPECIAL_CHARACTERS =
			"#$&'()*+,-./:;<=>@[]^{}".chars().mapToObj(e -> (char)e).collect(Collectors.toSet());
	
	private static final Set<String> BUILT_IN_TYPES = Arrays.stream(new String[]{"string"}).collect(Collectors.toSet());
	
	/**
	 * @param unitName
	 * @return
	 */
	public String normalizeUnitName(String unitName) {
		return normalizeIdentifier(unitName);
	}
	
	/**
	 * This method normalizes a given identifier by replacing critical characters by an underscore.
	 * The method never throws an exception but silently converts a given identifier to
	 * a valid identifier.
	 * 
	 * If the given identifier represents a reserved keyword, the identifier is going to be prefixed with "{@literal &}".
	 * 
	 * @see <a href='http://docwiki.embarcadero.com/RADStudio/Sydney/en/Fundamental_Syntactic_Elements_(Delphi)'>Fundamental Syntactic Elements</a>
	 * 
	 * @param identifier
	 * @return
	 */
	public String normalizeIdentifier(String identifier) {
		Objects.requireNonNull(identifier, "the indentifier to normalize must not be null");

    	if (Keyword.isKeyword(identifier)) {
    		// ADELPHI-34
    		return "&" + identifier;
    	} else {
	    	StringBuilder result = new StringBuilder();
			for (int ii=0; ii<identifier.length(); ii++) {
				char charOfIdentifier = identifier.charAt(ii);
				
				if (ii == 0) {
					// Note that a unicode character is not allowed to start an identifier. But we
					// do not check this here.
				    if (!Character.isWhitespace(charOfIdentifier) && !SPECIAL_CHARACTERS.contains(charOfIdentifier)) {
				    	// It is a valid first character
				    	result.append(charOfIdentifier);
				    } else {
				    	result.append("_");
				    }
				} else {
					// all remaining characters
					if (!Character.isWhitespace(charOfIdentifier) && !SPECIAL_CHARACTERS.contains(charOfIdentifier)) {
				    	// It is a valid remaining character
						result.append(charOfIdentifier);
				    } else {
				    	result.append("_");
				    }
				}
			}
			
			String resultingIdentifier = result.toString();
	    	if (resultingIdentifier.equals(identifier)) {
	    		// original identifier is unmodified
	    		return identifier;
	    	} else {
	    		// continue normalization (recursive call)
	    		return normalizeIdentifier(resultingIdentifier);
	    	}
    	}
	}
	
	/**
	 * @param typeName
	 * @return
	 */
	public String normalizeTypeName(String typeName) {
		if (BUILT_IN_TYPES.contains(typeName)) {
			return typeName;
		}
		return normalizeIdentifier(typeName);
	}
}
