package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.objectpascal.member.Method;

/**
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Nested_Type_Declarations">Delphi XE7 - Nested Type Declarations</a>
 * @author mmt
 *
 */
public interface OuterTypeI extends ModelElementI {
	
	/**
	 * @return
	 */
	OuterTypeI getOuterType();

	/**
	 * @return
	 */
	Set<Type> getNestedTypes();
	
	/**
	 * @param nestedType
	 * @return
	 */
	boolean addNestedType(Type nestedType);
	
	/**
	 * @param clazz
	 * @return
	 */
	<T extends Type> LinkedHashSet<T> getNestedTypes(Class<T> clazz);
	
	/**
	 * @return
	 */
	Set<Method> getMethods();
}
