/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashMap;
import java.util.Map;



/**
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Parameters_%28Delphi%29#Parameter_Semantics">Delphi XE7 - Keywords for method parameters</a>
 *
 */
public enum ParameterKeyword {
	
	
	CONST (Keyword.CONST),
	OUT   (Keyword.OUT),
	VAR   (Keyword.VAR),
	;
	
	private static final Map<String, ParameterKeyword> stringToEnum = new LinkedHashMap<>();
	
	static {
		for (ParameterKeyword parameterKeyword : values()) {
			stringToEnum.put(parameterKeyword.getName().toLowerCase(), parameterKeyword);
		}
	}
	
	public static ParameterKeyword fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
	
	private final Keyword keyword;
	
	private ParameterKeyword(Keyword keyword) {
		this.keyword = keyword;
	}

	public Keyword getKeyword() {
		return keyword;
	}
	
	public String getName() {
		return keyword.getName();
	}
}
