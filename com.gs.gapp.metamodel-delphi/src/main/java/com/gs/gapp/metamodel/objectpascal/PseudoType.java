/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * A pseudo type is a simple means to provide a type name for the code generation without
 * having to specify a unit or a predefined type. A pseudo type has nothing else than
 * a name.
 * 
 * @author mmt
 *
 */
public class PseudoType extends Type {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3403622669330915075L;

	/**
	 * @param name
	 */
	public PseudoType(String name) {
		super(name);
	}
}
