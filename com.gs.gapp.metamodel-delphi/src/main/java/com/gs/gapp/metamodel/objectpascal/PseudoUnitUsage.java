/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * @author mmt
 *
 */
public class PseudoUnitUsage extends UnitUsage {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -575676324790737368L;

	/**
	 * @param pseudoUnitName
	 */
	public PseudoUnitUsage(String pseudoUnitName) {
		this(pseudoUnitName, false);
	}

	/**
	 * @param pseudoUnitName
	 * @param implementationOnly
	 */
	public PseudoUnitUsage(String pseudoUnitName, boolean implementationOnly) {
		super(pseudoUnitName, implementationOnly);
	}
}
