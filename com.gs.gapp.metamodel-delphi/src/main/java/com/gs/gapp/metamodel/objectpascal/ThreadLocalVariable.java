/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Variables#Thread-local_Variables">Delphi XE7 - Thread-local Variables</a>
 * @author mmt
 *
 */
public class ThreadLocalVariable extends Variable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3930108570622615792L;

	public ThreadLocalVariable(String name, Type type, String initialization,
			Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, type, initialization, declaringUnit,
				onlyVisibleInImplementationSection);
	}

	public ThreadLocalVariable(String name, Type type, String initialization) {
		super(name, type, initialization);
	}

	public ThreadLocalVariable(String name, Type type, Unit declaringUnit,
			boolean onlyVisibleInImplementationSection) {
		super(name, type, declaringUnit, onlyVisibleInImplementationSection);
	}

	public ThreadLocalVariable(String name, Type type, Unit declaringUnit) {
		super(name, type, declaringUnit);
	}

	public ThreadLocalVariable(String name, Type type) {
		super(name, type);
	}
}
