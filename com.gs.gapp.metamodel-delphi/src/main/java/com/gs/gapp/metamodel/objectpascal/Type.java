/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.type.TypeParameter;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;

/**
 * @author mmt
 *
 */
public abstract class Type extends ObjectPascalModelElement implements AnnotatableI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8305089089730541193L;
	
	private final Set<TypeParameter> typeParameters = new LinkedHashSet<>();
	private final Unit declaringUnit;
	private final OuterTypeI outerType;
	private final boolean onlyVisibleInImplementationSection;
	private TypeVisibility typeVisibility;
	
	private VisibilityDirective visibilityAsNestedType = VisibilityDirective.PUBLIC;
	
	private final Set<AttributeUsage> attributeUsages = new LinkedHashSet<>();
	
	private final List<String> remarks = new ArrayList<>();
	
	/**
	 * @param name
	 */
	protected Type(String name) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeTypeName(name));
		this.declaringUnit = null;
		this.outerType = null;
		this.onlyVisibleInImplementationSection = false;
		
		init();
	}

	/**
	 * @param name
	 * @param declaringUnit
	 */
	protected Type(String name, Unit declaringUnit) {
		this(name, declaringUnit, false);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	protected Type(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeTypeName(name));
		
		if (declaringUnit == null) throw new NullPointerException("parameter 'declaringUnit' must not be null");
		
		this.declaringUnit = declaringUnit;
		this.outerType = null;
		this.onlyVisibleInImplementationSection = onlyVisibleInImplementationSection;
		
		this.declaringUnit.addType(this);  // set up the bidirectional relationship
		
		init();
	}
	
	/**
	 * @param name
	 * @param outerType
	 */
	protected Type(String name, OuterTypeI outerType) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeTypeName(name));
		this.declaringUnit = null;
		this.outerType = outerType;
		this.onlyVisibleInImplementationSection = false;
		
		init();
	}
	
	private void init() {
		if (onlyVisibleInImplementationSection) {
			typeVisibility = TypeVisibility.LOCAL;
		} else {
			typeVisibility = TypeVisibility.GLOBAL;
		}
		
		if (outerType != null) {
			outerType.addNestedType(this);
		}
	}
	
	/**
	 * @return
	 */
	public String getTypeAsString() {
		if (this.isGenerated()) {
		    return Keyword.adaptIdentifierIfNeeded(getName());
		}
		
		return getName();
	}
	
	/**
	 * @return
	 */
	public String getTypeParametersAsString() {
	    StringBuilder sb = new StringBuilder("");
	    
	    for (TypeParameter typeParameter : typeParameters) {
	        if (sb.length() <= 0) {
	            sb.append("<");
	        } else if (sb.length() > 0) {
	            sb.append(", ");
	        }
	        
	        sb.append(typeParameter.getTypeAsString());
	        
	        if (typeParameter.isClassConstraint()) {
	            sb.append(": class");
	        } else if (typeParameter.isRecordConstraint()) {
                sb.append(": record");
	        } else if (typeParameter.isConstructorConstraint()) {
                sb.append(": constructor");
            } else {
                if (typeParameter.getInterfaces().size() > 0) {
                    sb.append(": ");
                    String comma = "";
                    for (Interface interf : typeParameter.getInterfaces()) {
                        sb.append(comma).append(interf.getTypeAsString());
                        comma = ", ";
                    }
                } else if (typeParameter.getClazz() != null && ("TObject".equals(typeParameter.getClazz().getName()) == false)) {
                    sb.append(": ").append(typeParameter.getClazz().getTypeAsString());
                }
            }
	    }
	    
	    if (sb.length() > 0) sb.append(">");
	    return sb.toString();
	}

	public Set<TypeParameter> getTypeParameters() {
		return typeParameters;
	}
	
	public boolean addTypeParameter(TypeParameter typeParameter) {
		return this.typeParameters.add(typeParameter);
	}
	
	public Unit getDeclaringUnit() {
		return declaringUnit;
	}

	public OuterTypeI getOuterType() {
		return outerType;
	}

	public boolean isOnlyVisibleInImplementationSection() {
		return onlyVisibleInImplementationSection;
	}

	public TypeVisibility getTypeVisibility() {
		return typeVisibility;
	}

	public VisibilityDirective getVisibilityAsNestedType() {
		return visibilityAsNestedType;
	}

	public void setVisibilityAsNestedType(VisibilityDirective visibilityAsNestedType) {
		this.visibilityAsNestedType = visibilityAsNestedType;
	}
	
	public String getQualifiedTypeName() {
		StringBuilder sb = new StringBuilder(this.getName());
		
		OuterTypeI theOuterType = outerType;
		
		while (theOuterType != null) {
			sb.insert(0, ".").insert(0, theOuterType.getName());
			theOuterType = theOuterType.getOuterType();
		}
		
		return sb.toString();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.AnnotatableI#getAttributeUsages()
	 */
	@Override
	public Set<AttributeUsage> getAttributeUsages() {
		return attributeUsages;
	}
	
	/**
	 * @param attributeUsage
	 * @return
	 */
	@Override
	public boolean addAttributeUsage(AttributeUsage attributeUsage) {
		return this.attributeUsages.add(attributeUsage);
	}
	
	/**
     * For each remark, the code generator creates a remark tag for
     * the XML Doc feature of Delphi.
     * 
     * @return
     */
    public List<String> getRemarks() {
		return remarks;
	}
    
    /**
     * @param remark
     */
    public void addRemark(String remark) {
    	this.remarks.add(remark);
    }
    
    /**
	 * This method returns a pre-formatted XML string that includes Delphi XML Doc tags
	 * like for instance summary and remarks.
	 * 
	 * @return the Xml documentation to be written to the source code file
	 */
	public String getXmlDocumentation() {
		StringBuilder result = new StringBuilder();
		
		final String tripleSlash = "/// ";
		
		if (getBody() != null && getBody().length() > 0) {
		    result.append(tripleSlash).append("<summary>");
		    result.append(System.lineSeparator()).append(Method.prefixWithTripleSlashes(getBody()));
		    result.append(System.lineSeparator());
		    result.append(tripleSlash).append("</summary>");
		}
		
		if (remarks.size() > 0) {
			result.append(System.lineSeparator());
			result.append(tripleSlash).append("<remarks>");
			for (String remark : remarks) {
				result.append(System.lineSeparator());
				result.append(tripleSlash).append("<para>");
				result.append(System.lineSeparator()).append(Method.prefixWithTripleSlashes(remark)).append("</para>");
			}
			result.append(System.lineSeparator());
			result.append(tripleSlash).append("</remarks>");
		}
		
		return result.toString();
	}
}
