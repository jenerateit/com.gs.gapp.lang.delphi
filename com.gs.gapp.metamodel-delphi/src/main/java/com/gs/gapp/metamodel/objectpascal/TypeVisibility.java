/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * @author mmt
 *
 */
public enum TypeVisibility {

	GLOBAL,
	LOCAL,
	;
}
