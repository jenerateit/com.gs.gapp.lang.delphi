/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.objectpascal.constant.ResourceString;
import com.gs.gapp.metamodel.objectpascal.member.Function;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Procedure;
import com.gs.gapp.metamodel.objectpascal.type.ClassTypeI;
import com.gs.gapp.metamodel.objectpascal.type.ConstructedType;
import com.gs.gapp.metamodel.objectpascal.type.ForwardDeclaration;
import com.gs.gapp.metamodel.objectpascal.type.InterfaceTypeI;
import com.gs.gapp.metamodel.objectpascal.type.Pointer;
import com.gs.gapp.metamodel.objectpascal.type.Procedural;
import com.gs.gapp.metamodel.objectpascal.type.TypeDeclaration;
import com.gs.gapp.metamodel.objectpascal.type.TypeIdentifier;
import com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration;
import com.gs.gapp.metamodel.objectpascal.type.simple.SubRange;
import com.gs.gapp.metamodel.objectpascal.type.string.AnsiString;
import com.gs.gapp.metamodel.objectpascal.type.string.ShortString;
import com.gs.gapp.metamodel.objectpascal.type.structured.Array;
import com.gs.gapp.metamodel.objectpascal.type.structured.ClassReference;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.File;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;
import com.gs.gapp.metamodel.objectpascal.type.structured.Record;
import com.gs.gapp.metamodel.objectpascal.type.structured.Set;
import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;

/**
 * @author mmt
 *
 */
public class Unit extends ObjectPascalModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4343833281607025964L;
	
	private final String scope;
	
	private final java.util.Set<UnitUsage> uses = new LinkedHashSet<>();
	private final java.util.Set<Constant> constants = new LinkedHashSet<>();
	private final java.util.Set<Type> types = new LinkedHashSet<>();
	private final java.util.Set<Variable> variables = new LinkedHashSet<>();
	private final java.util.Set<Method> methods = new LinkedHashSet<>();
	
	private final Namespace namespace;

	private final List<String> initializationStatements = new ArrayList<>();
	private final List<String> finalizationStatements = new ArrayList<>();
	
	private final List<String> directivesBetweenUnitAndInterfaceSection = new ArrayList<>();
	private final ArrayList<String> linesBetweenUnitAndFirstDevDoc = new ArrayList<>();
	
	/**
	 * @param name
	 * @param namespace
	 */
	public Unit(String name, Namespace namespace) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeUnitName(name));
		
		this.scope = null;
		this.namespace = namespace;
		
		init();
	}
	
	/**
	 * @param name
	 * @param namespace
	 * @param scope
	 */
	public Unit(String name, Namespace namespace, String scope) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeUnitName(name));
		this.scope = scope;
		this.namespace = namespace;
		
		init();
	}
	
	private void init() {
		if (this.namespace != null) {
			this.namespace.addUnit(this);
		}
	}

	public String getScope() {
		return scope;
	}

	public java.util.Set<Type> getTypes() {
		return types;
	}
	
	public java.util.Set<Interface> getInterfaces() {
		return getTypes(Interface.class);
	}
	
	public java.util.Set<Clazz> getClasses() {
		return getTypes(Clazz.class);
	}
	
	public java.util.Set<Array> getArrays() {
		return getTypes(Array.class);
	}
	
	public java.util.Set<Record> getRecords() {
		return getTypes(Record.class);
	}
	
	public java.util.Set<ClassReference> getClassReferences() {
		return getTypes(ClassReference.class);
	}
	
	public java.util.Set<File> getFileTypes() {
		return getTypes(File.class);
	}
	
	public java.util.Set<Set> getSets() {
		return getTypes(Set.class);
	}
	
	public java.util.Set<ConstructedType> getConstructedTypes() {
		return getTypes(ConstructedType.class);
	}
	
	public java.util.Set<ForwardDeclaration> getForwardDeclarations() {
		return getTypes(ForwardDeclaration.class);
	}
	
	public java.util.Set<Pointer> getPointers() {
		return getTypes(Pointer.class);
	}
	
	public java.util.Set<Procedural> getProcedurals() {
		return getTypes(Procedural.class);
	}
	
	public java.util.Set<TypeDeclaration> getTypeDeclarations() {
		return getTypes(TypeDeclaration.class);
	}
	
	public java.util.Set<TypeIdentifier> getTypeIdentifiers() {
		return getTypes(TypeIdentifier.class);
	}
	
	public java.util.Set<AnsiString> getAnsiStrings() {
		return getTypes(AnsiString.class);
	}
	
	public java.util.Set<ShortString> getShortStrings() {
		return getTypes(ShortString.class);
	}
	
	public java.util.Set<Enumeration> getEnumerations() {
		return getTypes(Enumeration.class);
	}
	
	public java.util.Set<SubRange> getSubRanges() {
		return getTypes(SubRange.class);
	}
	
	
	/**
	 * @param clazz
	 * @return
	 */
	public <T extends Type> LinkedHashSet<T> getTypes(Class<T> clazz) {
		return getTypes(clazz, null);
	}
	
	/**
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends Type> LinkedHashSet<T> getTypes(Class<T> clazz, TypeVisibility typeVisibility) {
        LinkedHashSet<T> result = new LinkedHashSet<>();
        
        for (Type type : types) {
        	if (clazz.isAssignableFrom(type.getClass()) && (typeVisibility == null || type.getTypeVisibility() == typeVisibility)) {
        		result.add((T) type);
        	}
        }
		
		return result;
	}
	
	boolean addType(Type type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		if (type.getDeclaringUnit() != this) throw new IllegalArgumentException("parameter 'type' has declaringUnit '" + type.getDeclaringUnit() + "', which is different from '" + this + "'");
		
		return this.types.add(type);
	}

	/**
	 * @return
	 */
	public java.util.Set<UnitUsage> getUses() {
		return uses;
	}
	
	/**
	 * @return
	 */
	public java.util.Set<UnitUsage> getUsesForInterface() {
		java.util.Set<UnitUsage> result = new LinkedHashSet<>();
		for (UnitUsage usage : this.uses) {
			if (usage.isImplementationOnly() == false) result.add(usage);
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public java.util.Set<UnitUsage> getUsesForImplementation() {
		java.util.Set<UnitUsage> result = new LinkedHashSet<>();
		for (UnitUsage usage : this.uses) {
			if (usage.isImplementationOnly() == true) result.add(usage);
		}
		return result;
	}
	
	/**
	 * @param unitUsage
	 * @return
	 */
	public boolean addUse(UnitUsage unitUsage) {
		if (unitUsage == null) throw new NullPointerException("parameter 'unitUsage' must not be null");
		return this.uses.add(unitUsage);
	}

	public Namespace getNamespace() {
		return namespace;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
		StringBuilder result = new StringBuilder();
		
		if (getNamespace() != null && getNamespace().getQualifiedName().length() > 0) {
			result.append(getNamespace().getQualifiedName());
			if (getNamespace() != null && getNamespace().getName().endsWith(getName())) {  // TODO the if-clause is a fix of the issue that predefined types have wrong namespace set (same as unit name)
				// nothing to be done here
			} else {
				result.append(".").append(getName());
			}
		} else {
			result.append(getName());
		}
		
		return result.toString();
	}

	public java.util.Set<Constant> getConstants() {
		return constants;
	}
	
	/**
	 * @return
	 */
	public java.util.Set<Constant> getConstantsWithPublicVisibility() {
        java.util.Set<Constant> result = new LinkedHashSet<>();
		
		for (Constant constant : constants) {
			if (!(constant instanceof ResourceString) && constant.isOnlyVisibleInImplementationSection() == false) result.add(constant);
		}
		return result;
	}
	
	public java.util.Set<Constant> getResourceStringsWithPublicVisibility() {
        java.util.Set<Constant> result = new LinkedHashSet<>();
		
		for (Constant constant : constants) {
			if (constant instanceof ResourceString && constant.isOnlyVisibleInImplementationSection() == false) result.add(constant);
		}
		return result;
	}
	
	public java.util.Set<Constant> getConstantsWithPrivateVisibility() {
        java.util.Set<Constant> result = new LinkedHashSet<>();
		
		for (Constant constant : constants) {
			if (!(constant instanceof ResourceString) && constant.isOnlyVisibleInImplementationSection() == true) result.add(constant);
		}
		return result;
	}
	
	public java.util.Set<Constant> getResourceStringsWithPrivateVisibility() {
        java.util.Set<Constant> result = new LinkedHashSet<>();
		
		for (Constant constant : constants) {
			if (constant instanceof ResourceString && constant.isOnlyVisibleInImplementationSection() == true) result.add(constant);
		}
		return result;
	}
	
	boolean addConstant(Constant constant) {
		if (constant == null) throw new NullPointerException("parameter 'constant' must not be null");

		return this.constants.add(constant);
	}


	public java.util.Set<Variable> getVariables() {
		return variables;
	}
	
	boolean addVariable(Variable variable) {
		if (variable == null) throw new NullPointerException("parameter 'variable' must not be null");

		return this.variables.add(variable);
	}
	
	/**
	 * @return
	 */
	public java.util.Set<Variable> getVariablesWithPublicVisibility() {
        java.util.Set<Variable> result = new LinkedHashSet<>();
		
		for (Variable variable : variables) {
			if (!(variable instanceof ThreadLocalVariable) && variable.isOnlyVisibleInImplementationSection() == false) result.add(variable);
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public java.util.Set<Variable> getVariablesWithPrivateVisibility() {
        java.util.Set<Variable> result = new LinkedHashSet<>();
		
		for (Variable variable : variables) {
			if (!(variable instanceof ThreadLocalVariable) && variable.isOnlyVisibleInImplementationSection() == true) result.add(variable);
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public java.util.Set<ThreadLocalVariable> getThreadLocalVariablesWithPublicVisibility() {
        java.util.Set<ThreadLocalVariable> result = new LinkedHashSet<>();
		
		for (Variable variable : variables) {
			if (variable instanceof ThreadLocalVariable && variable.isOnlyVisibleInImplementationSection() == false) result.add((ThreadLocalVariable) variable);
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public java.util.Set<ThreadLocalVariable> getThreadLocalVariablesWithPrivateVisibility() {
        java.util.Set<ThreadLocalVariable> result = new LinkedHashSet<>();
		
		for (Variable variable : variables) {
			if (variable instanceof ThreadLocalVariable && variable.isOnlyVisibleInImplementationSection() == true) result.add((ThreadLocalVariable) variable);
		}
		return result;
	}

	/**
	 * @param method must not be null and must not yet have a declaring unit set that is different from this unit
	 * @return
	 */
	public boolean addMethod(Method method) {
		if (method == null) throw new NullPointerException("parameter 'method' must not be null");
		if (method.getDeclaringUnit() != this) throw new IllegalArgumentException("parameter 'method' has declaringUnit '" + method.getDeclaringUnit() + "', which is different from '" + this + "'");

		return this.methods.add(method);
	}
	
	public java.util.Set<Method> getMethods() {
		return this.methods;
	}
	
	/**
	 * @return
	 */
	public java.util.Set<Function> getFunctions() {
		java.util.Set<Function> result = new LinkedHashSet<>();
		for (Method method : methods) {
			if (method instanceof Function) {
				result.add((Function) method);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public java.util.Set<Procedure> getProcedures() {
		java.util.Set<Procedure> result = new LinkedHashSet<>();
		for (Method method : methods) {
			if (method instanceof Procedure) {
				result.add((Procedure) method);
			}
		}
		return result;
	}
	
	/**
	 * This method collects all methods of all classes in the unit and returns
	 * a single set containting all the methods.
	 * 
	 * @return
	 */
	public java.util.Set<Method> getMethodsOfAllClasses() {
		java.util.Set<Method> result = new LinkedHashSet<>();
		
		for (Clazz clazz : getTypes(Clazz.class)) {
			result.addAll(clazz.getMethods());
		}
		
		return result;
	}
	
	public java.util.Set<Method> getMethodsWithPublicVisibility() {
        java.util.Set<Method> result = new LinkedHashSet<>();
		
		for (Method method : methods) {
			if (method.getVisibility() == VisibilityDirective.PUBLIC) result.add(method);
		}
		return result;
	}
	
	public java.util.Set<Method> getMethodsWithLocalVisibility() {
		java.util.Set<Method> result = new LinkedHashSet<>();
		
		for (Method method : methods) {
			if (method.isOnlyVisibleInImplementationSection()) result.add(method);
		}
		return result;
	}

	public List<String> getInitializationStatements() {
		return initializationStatements;
	}
	
	public boolean addInitializationStatement(String initializationStatement) {
		return initializationStatements.add(initializationStatement);
	}

	public List<String> getFinalizationStatements() {
		return finalizationStatements;
	}
	
	public boolean addFinalizationStatement(String finalizationStatement) {
		return initializationStatements.add(finalizationStatement);
	}

	public List<String> getDirectivesBetweenUnitAndInterfaceSection() {
		return directivesBetweenUnitAndInterfaceSection;
	}
	
	public void addDirectivesBetweenUnitAndInterfaceSection(Collection<String> directives) {
		this.directivesBetweenUnitAndInterfaceSection.addAll(directives);
	}
	
	public ArrayList<String> getLinesBetweenUnitAndFirstDevDoc() {
		return linesBetweenUnitAndFirstDevDoc;
	}

	public void addLinesBetweenUnitAndFirstDevDoc(Collection<String> lines) {
		this.linesBetweenUnitAndFirstDevDoc.addAll(lines);
	}
	
	/**
	 * Collects and returns all structured types that are used by this
	 * unit's types, excluding the unit's structured types (unless they _are_
	 * used).
	 * 
	 * @return
	 */
	public final java.util.Set<StructuredType> getUsedStructuredTypes() {
		final java.util.Set<StructuredType> result = new LinkedHashSet<>();
		getTypes(StructuredType.class).stream().forEach(structuredType -> result.addAll(structuredType.getUsedStructuredTypes()));
		return result;
	}
	
	public final java.util.Set<StructuredType> getForwardDeclaredTypes() {
		return getTypes(ForwardDeclaration.class).stream()
		    .map(forwardDeclaration -> forwardDeclaration.getDeclaredStructuredType())
		    .collect(Collectors.toCollection(LinkedHashSet::new));
	}
	
	/**
	 * @return
	 */
	public java.util.Set<StructuredType> getStructuredTypesThatMightNeedForwardDeclaration() {
		java.util.Set<StructuredType> structuredTypesUsedByTypesDeclaredInInit = getUsedStructuredTypes();
		LinkedHashSet<StructuredType> structuredTypesDeclaredInUnit = this.getTypes(StructuredType.class);
		
		final java.util.Set<StructuredType> result = new LinkedHashSet<>(structuredTypesDeclaredInUnit);
		result.retainAll(structuredTypesUsedByTypesDeclaredInInit);
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Collection<StructuredType> getTypesInvolvedInInheritance() {
		final List<StructuredType> preliminaryResult = new ArrayList<>();
		
		// --- interface inheritance
		getTypes(Interface.class).stream().forEach(interf -> {
			if (interf.getParent() != null) {
				ArrayList<InterfaceTypeI> reversedInheritance = new ArrayList<>(interf.getAllParents());
				Collections.reverse(reversedInheritance);
				Object aParent = null;
				for (InterfaceTypeI interfType : reversedInheritance) {
					if (interfType instanceof StructuredType) {
						StructuredType structuredType = (StructuredType) interfType;
						int idxOfParent = aParent == null ? -1 : preliminaryResult.indexOf(aParent);
						int idx = preliminaryResult.indexOf(structuredType);
						
						if (idx >= 0) {
							// the type is already in the list => go on
						} else if (idxOfParent >= 0) {
							// type is not yet in the list but its parent is => add the child type right after its parent
							preliminaryResult.add(idxOfParent+1, structuredType);
						} else if (aParent != null) {
							// type is not yet in the list _and_ its parent isn't either => this must not happen in this algorithm
							throw new ModelConverterException("error in algorithm to determine the type declaration order for inheritance, interface type currently under work: '" + structuredType + "'");
						} else {
							preliminaryResult.add(structuredType);
						}
					}
					aParent = interfType;
				}
			}
		});
		
		getTypes(Clazz.class).stream().forEach(clazz -> {
			if (clazz.getParent() != null) {
				ArrayList<ClassTypeI> reversedInheritance = new ArrayList<>(clazz.getAllParents());
				Collections.reverse(reversedInheritance);
				Object aParent = null;
				for (ClassTypeI clazzType : reversedInheritance) {
					if (clazzType instanceof StructuredType) {
						StructuredType structuredType = (StructuredType) clazzType;
						int idxOfParent = aParent == null ? -1 : preliminaryResult.indexOf(aParent);
						int idx = preliminaryResult.indexOf(structuredType);
						
						if (idx >= 0) {
							// the type is already in the list => go on
						} else if (idxOfParent >= 0) {
							// type is not yet in the list but its parent is => add the child type right after its parent
							preliminaryResult.add(idxOfParent+1, structuredType);
						} else if (aParent != null) {
							// type is not yet in the list _and_ its parent isn't either => this must not happen in this algorithm
							throw new ModelConverterException("error in algorithm to determine the type declaration order for inheritance, class type currently under work: '" + structuredType + "'");
						} else {
							preliminaryResult.add(structuredType);
						}
					}
					aParent = clazzType;
				}
			}
		});
		
		// --- Finally, remove types that are _not_ declared in this unit since we won't generate them.
		Collection<StructuredType> finalResult = preliminaryResult.stream().filter(structuredType -> structuredType.getDeclaringUnit().equals(this)).collect(Collectors.toList());
		
		return finalResult;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#isGenerated()
	 */
	@Override
	public boolean isGenerated() {
		return super.isGenerated() && !isEmpty();
	}

	/**
	 * This method rearranges the set of this unit's types. It only makes
	 * changes when both passed types are actually contained in the unit's set of types.
	 * The typeToMove is removed from the collection, then the index of the
	 * otherType is determined and the typeToMove is re-inserted right before
	 * the otherType.
	 * 
	 * Note that this method makes ordering changes on a LinkedHashSet. It can only do this
	 * by working with lists instead of sets and completely clearing and rebuilding the set
	 * of types. So this method should only be called in rare cases, when you must make sure
	 * that a certain type is defined before another type is defined. An example when it could
	 * make sense to call this method: When a record needs to be defined before another class.
	 * That case cannot be resolved by using forward declarations, since Delphi doesn't support
	 * forward declarations for records.
	 * 
	 * @param typeToMove
	 * @param otherType
	 */
	public void moveTypeBeforeOtherType(Type typeToMove, Type otherType) {
	    if (this.types.contains(typeToMove) && this.types.contains(otherType)) {
    	    ArrayList<Type> typesList = new ArrayList<>(this.types);
    	    typesList.remove(typeToMove);
    	    typesList.add(typesList.indexOf(otherType), typeToMove);
    	    this.types.clear();
    	    this.types.addAll(typesList);
	    }
	}
	
	/**
	 * This method checks the unit for duplicate entries and returns them if it finds any.
	 * 
	 * <p>The algorithm works with all names being made lowercase. So even if two types'
	 * names differ in case, they are considered to be a duplicate.
	 * 
	 * @param typesToBeConsideredArr
	 * @return
	 */
	@SafeVarargs
	public final Map<String,List<Type>> getDuplicateTypes(Class<? extends Type>...typesToBeConsideredArr) {
		if (typesToBeConsideredArr == null) return Collections.emptyMap();
		
		Map<String,List<Type>> result = new HashMap<>();
		
		final java.util.Set<Class<? extends Type>> typesToBeConsidered = new HashSet<>(Arrays.asList(typesToBeConsideredArr));
		final java.util.Set<Type> typesToBeChecked = types.stream()
				.filter(type -> typesToBeConsidered.size() == 0 || isAssignableFrom(typesToBeConsidered, type))
				.collect(Collectors.toSet());
				
		typesToBeChecked.stream()
		    .forEach(type -> {
		    	List<Type> typesForName = result.computeIfAbsent(type.getName().toLowerCase(), k -> new ArrayList<>());
		    	typesForName.add(type);
		    });
		
		// --- remove entries that have at most one list entry
		new HashSet<>(result.keySet()).forEach(name -> {
			if (result.get(name).size() <= 1) {
				result.remove(name);
			}
		});
		
		return result;
	}
	
	private boolean isAssignableFrom(java.util.Set<Class<? extends Type>> typesToBeConsidered, Type type) {
		Optional<Class<? extends Type>> assignableClass = typesToBeConsidered.stream()
				.filter(typeToBeConsidered -> typeToBeConsidered.isAssignableFrom(type.getClass()))
		        .findFirst();
		return assignableClass.isPresent();
	}
	
	/**
	 * @return
	 */
	public boolean isEmpty() {
		return  (this.getClasses().isEmpty()             || this.getClasses().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getAnsiStrings().isEmpty()         || this.getAnsiStrings().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getArrays().isEmpty()              || this.getArrays().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getClassReferences().isEmpty()     || this.getClassReferences().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getConstants().isEmpty()           || this.getConstants().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getConstructedTypes().isEmpty()    || this.getConstructedTypes().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getEnumerations().isEmpty()        || this.getEnumerations().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getFileTypes().isEmpty()           || this.getFileTypes().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getForwardDeclarations().isEmpty() || this.getForwardDeclarations().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getFunctions().isEmpty()           || this.getFunctions().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getInterfaces().isEmpty()          || this.getInterfaces().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getMethods().isEmpty()             || this.getMethods().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getPointers().isEmpty()            || this.getPointers().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getProcedurals().isEmpty()         || this.getProcedurals().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getRecords().isEmpty()             || this.getRecords().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getSets().isEmpty()                || this.getSets().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getShortStrings().isEmpty()        || this.getShortStrings().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getSubRanges().isEmpty()           || this.getSubRanges().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getTypeDeclarations().isEmpty()    || this.getTypeDeclarations().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getTypeIdentifiers().isEmpty()     || this.getTypeIdentifiers().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getTypes().isEmpty()               || this.getTypes().stream().allMatch(modelElement -> !modelElement.isGenerated())) &&
				(this.getVariables().isEmpty()           || this.getVariables().stream().allMatch(modelElement -> !modelElement.isGenerated()));
	}
}
