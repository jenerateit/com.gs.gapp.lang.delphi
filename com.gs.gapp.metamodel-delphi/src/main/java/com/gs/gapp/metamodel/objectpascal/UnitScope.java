/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * @author mmt
 *
 */
public class UnitScope extends ObjectPascalModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5063042224491984735L;

	/**
	 * @param name
	 */
	public UnitScope(String name) {
		super(name);
	}

}
