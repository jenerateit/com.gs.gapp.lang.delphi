/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * @author mmt
 *
 */
public class UnitUsage extends ObjectPascalModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5275536263680725209L;
	
	private final Unit unit;
	
	private final String fileLocation;
	
	private final boolean implementationOnly;
	
	private final boolean writeQualifiedName = false;
	
	/**
	 * @param pseudoUnitName
	 */
	protected UnitUsage(String pseudoUnitName) {
		this(pseudoUnitName, false);
	}
	
	/**
	 * @param pseudoUnitName
	 * @param implementationOnly
	 */
	protected UnitUsage(String pseudoUnitName, boolean implementationOnly) {
		super(pseudoUnitName);
		this.unit = null;
		this.fileLocation = null;
		this.implementationOnly = implementationOnly;
	}

	/**
	 * @param unit
	 */
	public UnitUsage(Unit unit) {
		this(unit, false);
	}
	
	/**
	 * @param unit
	 * @param implementationOnly
	 */
	public UnitUsage(Unit unit, boolean implementationOnly) {
		super(unit != null ? unit.getName() : null);
		this.unit = unit;
		this.fileLocation = null;
		this.implementationOnly = implementationOnly;
	}
	
	/**
	 * @param unit
	 * @param fileLocation
	 */
	public UnitUsage(Unit unit, String fileLocation) {
		this(unit, fileLocation, false);
	}
	
	/**
	 * @param unit
	 * @param fileLocation
	 * @param implementationOnly
	 */
	public UnitUsage(Unit unit, String fileLocation, boolean implementationOnly) {
		super(unit != null ? unit.getName() : null);
		this.unit = unit;
		this.fileLocation = fileLocation;
		this.implementationOnly = implementationOnly;
	}
	
	public Unit getUnit() {
		return unit;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public boolean isImplementationOnly() {
		return implementationOnly;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
		StringBuilder qualifiedName = new StringBuilder();
		if (unit != null && unit.getNamespace() != null) {
			qualifiedName.append(unit.getNamespace().getQualifiedName());
		}
		
		if (unit.getNamespace() != null && unit.getNamespace().getName().equals(getName())) {
			// avoid duplicated, incorrect use
		} else {
			if (qualifiedName.length() > 0) qualifiedName.append(".");
		    qualifiedName.append(getName());
		}
		return qualifiedName.toString();
	}

	/**
	 * TODO fix generation of standard uses-clause
	 * @return
	 */
	public boolean isWriteQualifiedName() {
		return writeQualifiedName;
	}

	@Override
	public String toString() {
		return super.toString() + ", used unit:" + unit;
	}
	
}
