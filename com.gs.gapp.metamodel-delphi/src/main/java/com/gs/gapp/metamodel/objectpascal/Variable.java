/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

/**
 * @author mmt
 *
 */
public class Variable extends ObjectPascalModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4269213049749998549L;
	
	private final Type type;
	
	private final String initialization;
	
	private final Unit declaringUnit;
	
	private final boolean onlyVisibleInImplementationSection;
	
	/**
	 * @param name
	 * @param type
	 */
	public Variable(String name, Type type) {
		this(name, type, (Unit)null);
	}
	
	/**
	 * @param name
	 * @param type
	 * @param declaringUnit
	 */
	public Variable(String name, Type type, Unit declaringUnit) {
		this(name, type, declaringUnit, false);
	}
	
	/**
	 * @param name
	 * @param type
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Variable(String name, Type type, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		this(name, type, null, declaringUnit, onlyVisibleInImplementationSection);
	}
	
	/**
	 * @param name
	 * @param type
	 * @param initialization
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Variable(String name, Type type, String initialization, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		this.type = type;
		this.declaringUnit = declaringUnit;
		this.initialization = initialization;
		this.onlyVisibleInImplementationSection = onlyVisibleInImplementationSection;
		
		init();
	}
	
	/**
	 * @param name
	 * @param type
	 * @param initialization
	 */
	public Variable(String name, Type type, String initialization) {
		this(name, type, initialization, null, false);
	}
	
	private void init() {
		if (getDeclaringUnit() != null) {
			getDeclaringUnit().addVariable(this);
		}
	}

	public Type getType() {
		return type;
	}

	public String getInitialization() {
		return initialization == null ? "" : initialization;
	}

	public boolean isOnlyVisibleInImplementationSection() {
		return onlyVisibleInImplementationSection;
	}

	public Unit getDeclaringUnit() {
		return declaringUnit;
	}
	
	/**
	 * adapts the member name in case the member name is a reserved word in Delphi
	 * 
	 * @return the member name prepended with ampersand in case the member name is a reserved word in Delphi
	 */
	public String getNonReservedWordName() {
		return Keyword.adaptIdentifierIfNeeded(getName());
	}
}
