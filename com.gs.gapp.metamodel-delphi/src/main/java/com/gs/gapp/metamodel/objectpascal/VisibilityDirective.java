/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal;

import java.util.LinkedHashMap;
import java.util.Map;



/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Fundamental_Syntactic_Elements#Directives">Delphi XE7 - Directives</a>
 *
 */
public enum VisibilityDirective {
	
	
	PRIVATE (Directive.PRIVATE),
	PROTECTED (Directive.PROTECTED),
	PUBLIC (Directive.PUBLIC),
	PUBLISHED (Directive.PUBLISHED),
	;
	
	private static final Map<Directive, VisibilityDirective> stringToEnum = new LinkedHashMap<>();
	
	static {
		for (VisibilityDirective visibilityDirective : values()) {
			stringToEnum.put(visibilityDirective.getDirective(), visibilityDirective);
		}
	}
	
	public static VisibilityDirective fromString(String name) {
	    Directive directive = Directive.fromString(name);
        if (directive != null) {
		    return stringToEnum.get(directive);
        }
        return null;
	}
	
	private final Directive directive;
	
	private VisibilityDirective(Directive directive) {
		this.directive = directive;
	}

	public Directive getDirective() {
		return directive;
	}
	
	public String getName() {
		return directive.getName();
	}
}
