/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.constant;

import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 *
 */
public class ConstantExpression extends TrueConstant {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7036443566968839396L;
	
	/**
	 * @param name
	 * @param expression
	 */
	public ConstantExpression(String name, String expression) {
		super(name, expression);
	}

	public ConstantExpression(String name, String expression,
			Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, expression, declaringUnit, onlyVisibleInImplementationSection);
	}

	public ConstantExpression(String name, String expression, Unit declaringUnit) {
		super(name, expression, declaringUnit);
	}
}
