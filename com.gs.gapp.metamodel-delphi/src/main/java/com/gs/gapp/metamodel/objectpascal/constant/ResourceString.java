/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.constant;

import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 *
 */
public class ResourceString extends ConstantExpression {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6318944471331910449L;

	/**
	 * @param name
	 */
	public ResourceString(String name, String expression) {
		super(name, expression);
	}

	public ResourceString(String name, String expression, Unit declaringUnit,
			boolean onlyVisibleInImplementationSection) {
		super(name, expression, declaringUnit, onlyVisibleInImplementationSection);
	}

	public ResourceString(String name, String expression, Unit declaringUnit) {
		super(name, expression, declaringUnit);
	}
}
