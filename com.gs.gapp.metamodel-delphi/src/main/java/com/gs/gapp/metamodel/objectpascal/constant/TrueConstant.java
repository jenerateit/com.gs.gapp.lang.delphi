/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.constant;

import com.gs.gapp.metamodel.objectpascal.Constant;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 *
 */
public abstract class TrueConstant extends Constant {

	/**
	 * 
	 */
	private static final long serialVersionUID = -86980601090620478L;

	/**
	 * @param name
	 * @param expression
	 * 
	 */
	public TrueConstant(String name, String expression) {
		super(name, expression);
	}

	public TrueConstant(String name, String expression, Unit declaringUnit,
			boolean onlyVisibleInImplementationSection) {
		super(name, expression, declaringUnit, onlyVisibleInImplementationSection);
	}

	public TrueConstant(String name, String expression, Unit declaringUnit) {
		super(name, expression, declaringUnit);
	}
}
