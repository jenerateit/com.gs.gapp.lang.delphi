/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.constant;

import com.gs.gapp.metamodel.objectpascal.Constant;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * Can be one of
 * - typed constant, e.g. const Max: Integer = 100;
 * - array constant, e.g. const Digits: array[0..2] of Char = ('0', '1', '2');
 * - record constant, e.g. const
 *       Origin: TPoint = (X: 0.0; Y: 0.0);
 *       Line: TVector = ((X: -3.1; Y: 1.5), (X: 5.8; Y: 3.0));
 *       SomeDay: TDate = (D: 2; M: Dec; Y: 1960);
 * - procedural constant (function or procedure), e.g. const MyFunction: TFunction = Calc;
 * - pointer constant, e.g. const PI: ^Integer = @I;
 * 
 * @author mmt
 */
public class TypedConstant extends Constant {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8026141645221936028L;
	
	private final Type type;
	
	/**
	 * @param name
	 */
	public TypedConstant(String name, Type type, String expression) {
		super(name, expression);
		
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		this.type = type;
	}
	
	

	/**
	 * @param name
	 * @param type
	 * @param expression
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public TypedConstant(String name, Type type, String expression, Unit declaringUnit,
			boolean onlyVisibleInImplementationSection) {
		super(name, expression, declaringUnit, onlyVisibleInImplementationSection);
		
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		this.type = type;
	}



	/**
	 * @param name
	 * @param type
	 * @param expression
	 * @param declaringUnit
	 */
	public TypedConstant(String name, Type type, String expression, Unit declaringUnit) {
		super(name, expression, declaringUnit);

		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		this.type = type;
	}


	public Type getType() {
		return type;
	}
}
