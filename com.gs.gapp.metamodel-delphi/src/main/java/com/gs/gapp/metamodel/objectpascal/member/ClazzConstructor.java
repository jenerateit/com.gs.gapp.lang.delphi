/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 *
 */
public class ClazzConstructor extends Constructor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3351761774914234623L;

	/**
	 * @param owner
	 */
	public ClazzConstructor(Type owner) {
		super(owner);
	}
}
