/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 *
 */
public class ClazzDestructor extends Destructor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 717195791428032544L;

	/**
	 * @param owner
	 */
	public ClazzDestructor(Type owner) {
		super(owner);
	}
}
