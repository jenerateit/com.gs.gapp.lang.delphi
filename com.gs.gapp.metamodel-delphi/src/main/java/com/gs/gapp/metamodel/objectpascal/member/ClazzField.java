/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 *
 */
public class ClazzField extends Field {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6252456683511394571L;

	/**
	 * @param name
	 * @param type
	 * @param owner
	 */
	public ClazzField(String name, Type type, Type owner) {
		super(name, type, owner);
	}
}
