/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 *
 */
public class ClazzFunction extends Function {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1925736163010771107L;

	/**
	 * This is a convenience constructor to create a function with no parameters
	 * 
	 * @param name
	 * @param owner
	 */
	public ClazzFunction(String name, Type owner, Type returnType) {
		super(name, owner, returnType);
	}
	
	/**
	 * @param name
	 * @param owner
	 * @param parameters
	 */
	public ClazzFunction(String name, Type owner, Type returnType, Parameter... parameters) {
		super(name, owner, returnType, parameters);
	}
}
