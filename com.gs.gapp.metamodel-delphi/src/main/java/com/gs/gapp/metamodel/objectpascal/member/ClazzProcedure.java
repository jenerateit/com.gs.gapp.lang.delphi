/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 *
 */
public class ClazzProcedure extends Procedure {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1125851102961591514L;

	/**
	 * This is a convenience constructor to create a procedure with no parameters
	 * 
	 * @param name
	 * @param owner
	 */
	public ClazzProcedure(String name, Type owner) {
		super(name, owner);
	}
	
	/**
	 * @param name
	 * @param owner
	 * @param parameters
	 */
	public ClazzProcedure(String name, Type owner, Parameter... parameters) {
		super(name, owner, parameters);
	}
}
