/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 *
 */
public class ClazzProperty extends Property {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1052380207303267302L;

	/**
	 * @param name
	 * @param owner
	 */
	public ClazzProperty(String name, Type type, Type owner) {
		super(name, type, owner);
	}
}
