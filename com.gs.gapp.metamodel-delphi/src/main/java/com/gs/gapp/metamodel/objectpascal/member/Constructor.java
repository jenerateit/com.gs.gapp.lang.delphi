/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 *
 */
public class Constructor extends Procedure {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7645508520956361661L;

	/**
	 * This is a convenience constructor to create a constructor with no parameters
	 * 
	 * @param owner
	 */
	public Constructor(Type owner) {
		super("Create", owner);
	}
	
	/**
	 * @param name
	 * @param owner
	 */
	public Constructor(String name, Type owner) {
		super(name, owner);
	}

	/**
	 * @param owner
	 * @param parameters
	 */
	public Constructor(Type owner, Parameter... parameters) {
		super("Create", owner, parameters);
	}
	
	/**
	 * @param name
	 * @param owner
	 * @param parameters
	 */
	public Constructor(String name, Type owner, Parameter... parameters) {
		super(name, owner, parameters);
	}
}
