/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 *
 */
public class Destructor extends Procedure {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6244930236907035845L;

	/**
	 * This is a convenience constructor to create a destructor with no parameters
	 * 
	 * @param owner
	 */
	public Destructor(Type owner) {
		super("Destroy", owner);
	}
	
	/**
	 * @param name
	 * @param owner
	 */
	public Destructor(String name, Type owner) {
		super(name, owner);
	}

	/**
	 * @param owner
	 * @param parameters
	 */
	public Destructor(Type owner, Parameter... parameters) {
		super("Destroy", owner, parameters);
	}
	
	/**
	 * @param name
	 * @param owner
	 * @param parameters
	 */
	public Destructor(String name, Type owner, Parameter... parameters) {
		super(name, owner, parameters);
	}
}
