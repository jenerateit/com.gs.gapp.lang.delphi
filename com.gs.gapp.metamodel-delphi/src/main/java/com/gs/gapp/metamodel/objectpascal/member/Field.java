/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import java.util.Collections;

import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.type.structured.Member;
import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Fields">Delphi XE7 - Fields</a>
 */
public class Field extends Member {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4595524620752805684L;
	
	private final Type type;
	
	private Property property;
	
	/**
	 * @param name
	 * @param type
	 * @param owner
	 */
	public Field(String name, Type type, Type owner) {
		super(name, owner);
		
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		
		this.type = type;
	}

	public Type getType() {
		return type;
	}

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    /**
	 * @return
	 */
    @Override
	public java.util.Set<StructuredType> getStructuredTypes() {
    	if (type instanceof StructuredType) {
		    return Collections.singleton((StructuredType)this.type);
    	}
    	
    	return Collections.emptySet();
	}
    
    /**
     * @return
     */
    @Override
    public String getDeveloperAreaKey() {
        StringBuilder sb = new StringBuilder(getOwner() != null ? getOwner().getQualifiedName() : getDeclaringUnit().getName()).append(".").append(getName().toLowerCase());
        return sb.toString();
    }
}
