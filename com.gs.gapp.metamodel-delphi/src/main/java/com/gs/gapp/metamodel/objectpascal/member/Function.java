/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;

/**
 * @author mmt
 *
 */
public class Function extends Method {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2587759860591361442L;
	
	
	private final Type returnType;
	
	private String returnTypeDocumentation;

	/**
	 * This is a convenience constructor to create a function with no parameters
	 * 
	 * @param name
	 * @param owner
	 */
	public Function(String name, Type owner, Type returnType) {
		super(name, owner);
		this.returnType = returnType;
		
		init();
	}
	
	/**
	 * This is a convenience constructor to create a global function with no parameters
	 * 
	 * @param name
	 * @param declaringUnit
	 */
	public Function(String name, Unit declaringUnit, Type returnType) {
		super(name, declaringUnit);
		this.returnType = returnType;
		
		init();
	}
	
	/**
	 * @param name
	 * @param owner
	 * @param parameters
	 */
	public Function(String name, Type owner, Type returnType, Parameter... parameters) {
		super(name, owner, parameters);
		this.returnType = returnType;
		
		init();
	}
	
	/**
	 * Creates a global or unit-level function with the given parameters
	 * 
	 * @param name
	 * @param declaringUnit
	 * @param parameters
	 */
	public Function(String name, Unit declaringUnit, Type returnType, Parameter... parameters) {
		super(name, declaringUnit, parameters);
		this.returnType = returnType;
		
		init();
	}
	
	private void init() {
		if (this.returnType == null) {
			throw new RuntimeException("field 'returnType' of Function class must not be null");
		}
	}

	public Type getReturnType() {
		return returnType;
	}

	public String getReturnTypeDocumentation() {
		return returnTypeDocumentation;
	}

	public void setReturnTypeDocumentation(String returnTypeDocumentation) {
		this.returnTypeDocumentation = returnTypeDocumentation;
	}
	
	/**
	 * @return
	 */
	@Override
	protected String getXmlDocumentationForReturnType() {
		if (returnType != null) {
			final String tripleSlash = "/// ";
			
			if (returnTypeDocumentation != null && returnTypeDocumentation.length() > 0) {
				return new StringBuilder(System.lineSeparator()).append(tripleSlash).append("<returns>")
						.append(returnTypeDocumentation)
						.append("</returns>").toString();
			}
			
			return new StringBuilder(System.lineSeparator()).append(tripleSlash)
					.append("<returns>").append(returnType.getName()).append("</returns>").toString();
		}
		
		return "";
	}
	
	/**
	 * @return
	 */
    @Override
	public java.util.Set<StructuredType> getStructuredTypes() {
    	LinkedHashSet<StructuredType> result = new LinkedHashSet<>(super.getStructuredTypes());
    	if (this.returnType instanceof StructuredType) {
    		result.add((StructuredType) returnType);
    	}
    	return result;
	}
}
