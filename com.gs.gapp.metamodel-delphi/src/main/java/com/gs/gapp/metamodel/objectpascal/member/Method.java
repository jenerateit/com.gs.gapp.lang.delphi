/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.MethodDirective;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.VisibilityDirective;
import com.gs.gapp.metamodel.objectpascal.member.Parameter.ParamInfo;
import com.gs.gapp.metamodel.objectpascal.type.TypeParameter;
import com.gs.gapp.metamodel.objectpascal.type.structured.Member;
import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;

/**
 * @author mmt
 *
 */
public abstract class Method extends Member {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7988102187450004964L;
	
	/**
	 * @param resultType
	 * @param directives
	 * @param name
	 * @param owner
	 * @param paramInfos
	 * @return
	 */
	public static Function createFunction(Type resultType, Set<MethodDirective> directives, String name, Type owner, ParamInfo ...paramInfos) {
        return createFunction(resultType, directives, null, name, owner, paramInfos);
	}
	
	/**
	 * @param resultType
	 * @param directives
	 * @param typeParameters
	 * @param name
	 * @param owner
	 * @param paramInfos
	 * @return
	 */
	public static Function createFunction(Type resultType, Set<MethodDirective> directives, Set<TypeParameter> typeParameters,
			String name, Type owner, ParamInfo ...paramInfos) {

		@SuppressWarnings("unchecked")
		Set<Parameter> parameters = (Set<Parameter>) (paramInfos == null ? new LinkedHashSet<>() : constructParameters(paramInfos));
        Function function = new Function(name, owner, resultType, parameters.toArray(new Parameter[0]));
		if (typeParameters != null) function.addTypeParameters(typeParameters);
		addDirectives(function, directives);

		return function;
	}
	
	private static void addDirectives(Method method, Set<MethodDirective> directives) {
		if (directives != null) {
			for (MethodDirective directive : directives) {
				method.addDirective(directive);
			}
		}
	}
	
	/**
	 * @param resultType
	 * @param directives
	 * @param name
	 * @param owningUnit
	 * @param paramInfos
	 * @return
	 */
	public static Function createFunction(Type resultType, Set<MethodDirective> directives, String name, Unit owningUnit, ParamInfo ...paramInfos) {

		Set<Parameter> parameters = paramInfos == null ? new LinkedHashSet<>() : constructParameters(paramInfos);
        Function function = new Function(name, owningUnit, resultType, parameters.toArray(new Parameter[0]));
        function.setVisibility(VisibilityDirective.PUBLIC);
        addDirectives(function, directives);
        
		return function;
	}
	
	
	/**
	 * @param directives
	 * @param name
	 * @param owner
	 * @param paramInfos
	 * @return
	 */
	public static Procedure createProcedure(Set<MethodDirective> directives, String name, Type owner, ParamInfo ...paramInfos) {
        return createProcedure(directives, null, name, owner, paramInfos);
	}
	
	/**
	 * @param directives
	 * @param typeParameters
	 * @param name
	 * @param owner
	 * @param paramInfos
	 * @return
	 */
	public static Procedure createProcedure(Set<MethodDirective> directives, Set<TypeParameter> typeParameters,
			String name, Type owner, ParamInfo ...paramInfos) {

		Set<Parameter> parameters = paramInfos == null ? new LinkedHashSet<>() : constructParameters(paramInfos);
		Procedure procedure = new Procedure(name, owner, parameters.toArray(new Parameter[0]));
		if (typeParameters != null) procedure.addTypeParameters(typeParameters);
		addDirectives(procedure, directives);
		
		return procedure;
	}
	

	/**
	 * @param directives
	 * @param name
	 * @param owningUnit
	 * @param paramInfos
	 * @return
	 */
	public static Procedure createProcedure(Set<MethodDirective> directives, String name, Unit owningUnit, ParamInfo ...paramInfos) {
		
		Set<Parameter> parameters = paramInfos == null ? new LinkedHashSet<>() : constructParameters(paramInfos);
		Procedure procedure = new Procedure(name, owningUnit, parameters.toArray(new Parameter[0]));
		procedure.setVisibility(VisibilityDirective.PUBLIC);
		addDirectives(procedure, directives);
		
		return procedure;
	}
	
	/**
	 * @param paramInfos
	 * @return
	 */
	private static LinkedHashSet<Parameter> constructParameters(ParamInfo ...paramInfos) {
		LinkedHashSet<Parameter> parameters = new LinkedHashSet<>();
		
		if (paramInfos != null) {
			for (ParamInfo paramInfo : paramInfos) {
				Parameter param = new Parameter(paramInfo.getName(), paramInfo.getType(), paramInfo.getKeywords(), paramInfo.getDefaultValue());
				parameters.add(param);
			}
		}
		
		return parameters;
	}
	
	private final Set<Parameter> parameters = new LinkedHashSet<>();
	
	private final Set<TypeParameter> typeParameters = new LinkedHashSet<>();
	
	private final Set<MethodDirective> directives = new LinkedHashSet<>();
	
	private final List<String> localDeclarations = new ArrayList<>();
	
	private final List<String> defaultImplementation = new ArrayList<>();
	
	private final List<String> remarks = new ArrayList<>();
	
	/**
	 * This is a convenience constructor to create a method with no parameters
	 * 
	 * @param name
	 * @param owner
	 */
	protected Method(String name, Type owner) {
		super(name, owner);
		
		init();
	}
	
	/**
	 * This is a convenience constructor to create a global method with no parameters
	 * 
	 * @param name
	 * @param declaringUnit
	 */
	protected Method(String name, Unit declaringUnit) {
		super(name, declaringUnit);
		
		init();
	}

	/**
	 * @param name
	 * @param owner
	 * @param parameters
	 */
	protected Method(String name, Type owner, Parameter... parameters) {
		super(name, owner);
		
		if (parameters == null) throw new NullPointerException("parameter 'parameters' must not be null");
		
		this.parameters.addAll(Arrays.asList(parameters));
		
		init();
	}
	
	/**
	 * Constructs a global method with the given parameters
	 * 
	 * @param name
	 * @param declaringUnit
	 * @param parameters
	 */
	protected Method(String name, Unit declaringUnit, Parameter... parameters) {
		super(name, declaringUnit);
		
		if (parameters == null) throw new NullPointerException("parameter 'parameters' must not be null");
		
		this.parameters.addAll(Arrays.asList(parameters));
		
		init();
	}
	
	/*
	 * Initializtations that are required for all constructors of this class
	 */
	private void init() {
		for (Parameter parameter : parameters) {
			parameter.setMethod(this);
		}
		
		if (getDeclaringUnit() != null) {
		    getDeclaringUnit().addMethod(this);
		}
	}

	public Set<TypeParameter> getTypeParameters() {
		return typeParameters;
	}
	
	public void addTypeParameters(Collection<TypeParameter> typeParameters) {
		this.typeParameters.addAll(typeParameters);
	}

	public boolean addTypeParameter(TypeParameter typeParameter) {
		if (typeParameter == null) throw new NullPointerException("parameter 'typeParameter' must not be null");
		
		return this.typeParameters.add(typeParameter);
	}

	public Set<Parameter> getParameters() {
		return parameters;
	}
	
	/**
	 * @param idx
	 * @return
	 */
	public Parameter getParameter(int idx) {
		if (idx >= 0 && idx < this.parameters.size()) {
		    return this.parameters.toArray(new Parameter[0])[idx];
		}
		
		throw new IllegalArgumentException("there is no parameter at index " + idx +", number of parameters:" + this.parameters.size());
	}

	public Set<MethodDirective> getDirectives() {
		return directives;
	}
	
	public boolean addDirective(MethodDirective directive) {
		if (directive == null) throw new NullPointerException("parameter 'directive' must not be null");
		
		return this.directives.add(directive);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
        return this.getDeveloperAreaKey();
	}
	
	/**
	 * @return
	 */
	@Override
	public String getDeveloperAreaKey() {
		StringBuilder sb = new StringBuilder(getOwner() != null ? getOwner().getQualifiedName() : getDeclaringUnit().getName()).append(".").append(getName().toLowerCase());
		for (Parameter parameter : this.getParameters()) {
			sb.append(".").append(parameter.getName().toLowerCase());
		}
		
		return sb.toString();
	}
	
	/**
	 * @return
	 */
	public String getDeveloperAreaKeyForLocalDelcarations() {
		return getDeveloperAreaKey() + "-LocalDeclarations";
	}

	public List<String> getLocalDeclarations() {
		return localDeclarations;
	}
	
	/**
	 * @param localDeclaration
	 * @return
	 */
	public boolean addLocalDeclaration(String localDeclaration) {
		return localDeclarations.add(localDeclaration);
	}
	
	/**
	 * @return the defaultImplementation
	 */
	public List<String> getDefaultImplementation() {
		return Collections.unmodifiableList(defaultImplementation);
	}
	
    /**
     * For each remark, the code generator creates a remark tag for
     * the XML Doc feature of Delphi.
     * 
     * @return
     */
    public List<String> getRemarks() {
		return remarks;
	}
    
    /**
     * @param remark
     */
    public void addRemark(String remark) {
    	this.remarks.add(remark);
    }

	/**
	 * @param defaultImplementationLine the defaultImplementation to set
	 */
	public void addToDefaultImplementation(String defaultImplementationLine) {
		if (defaultImplementationLine != null && defaultImplementationLine.length() > 0) {
		    this.defaultImplementation.add(defaultImplementationLine);
		}
	}
	
	/**
	 * This method returns a pre-formatted XML string that includes Delphi XML Doc tags
	 * like for instance summary and remarks.
	 * 
	 * @return the Xml documentation to be written to the source code file
	 */
	public String getXmlDocumentation() {
		StringBuilder result = new StringBuilder();
		
		final String tripleSlash = "/// ";
		
		if (getBody() != null && getBody().length() > 0) {
		    result.append(tripleSlash).append("<summary>");
		    result.append(System.lineSeparator()).append(prefixWithTripleSlashes(getBody()));
		    result.append(System.lineSeparator());
		    result.append(tripleSlash).append("</summary>");
		}
		
		if (remarks.size() > 0) {
			if (result.length() > 0) result.append(System.lineSeparator());
			result.append(tripleSlash).append("<remarks>");
			for (String remark : remarks) {
				result.append(System.lineSeparator());
				result.append(tripleSlash).append("<para>");
				result.append(System.lineSeparator()).append(prefixWithTripleSlashes(remark)).append("</para>");
			}
			result.append(System.lineSeparator());
			result.append(tripleSlash).append("</remarks>");
		}
		
		for (Parameter parameter : parameters) {
			String paramDoc = parameter.getBody() != null && parameter.getBody().length() > 0 ? parameter.getBody() : " the " + parameter.getName();
			if (result.length() > 0) result.append(System.lineSeparator());
			// adding the <param> tag in one single line
			result.append(tripleSlash).append("<param name=\"").append(parameter.getName()).append("\">");
			result.append(paramDoc).append("</param>");
		}
		
		result.append(getXmlDocumentationForReturnType());
		
		return result.toString();
	}
	
	/**
	 * @param text
	 * @return
	 */
	public static String prefixWithTripleSlashes(String text) {
		final String tripleSlash = "/// ";
	    String lines[] = text.split("\\r\\n|\\n|\\r");
		StringBuilder result = new StringBuilder();
		for (String lineOfText : lines) {
			result.append(tripleSlash).append(lineOfText);
			if (lineOfText != lines[lines.length-1]) result.append(System.lineSeparator());  // only at the last line do _not_ add a line break
		}
		return result.toString();
	}

	/**
	 * @return
	 */
	protected String getXmlDocumentationForReturnType() {
		return "";
	}
	
	/**
	 * @return
	 */
    @Override
	public java.util.Set<StructuredType> getStructuredTypes() {
    	LinkedHashSet<StructuredType> result = new LinkedHashSet<>();
    	this.parameters.stream().filter(parameter -> parameter.getType() instanceof StructuredType).forEach(parameter -> result.add((StructuredType) parameter.getType()));
    	return result;
	}
}
