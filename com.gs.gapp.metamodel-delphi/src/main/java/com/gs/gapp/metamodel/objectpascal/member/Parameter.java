/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.objectpascal.AnnotatableI;
import com.gs.gapp.metamodel.objectpascal.AttributeUsage;
import com.gs.gapp.metamodel.objectpascal.Keyword;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalNamingRulesEnforcer;
import com.gs.gapp.metamodel.objectpascal.ParameterKeyword;
import com.gs.gapp.metamodel.objectpascal.Type;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Parameters_%28Delphi%29">Delphi XE7 - Parameters</a>
 *
 */
public class Parameter extends ObjectPascalModelElement implements AnnotatableI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 356832618631229594L;
	
	private Method method;
	private final Type type;
	private final Set<ParameterKeyword> keywords = new LinkedHashSet<>();
	private final String defaultValue;
	private boolean ref = false;
	
	private final Set<AttributeUsage> attributeUsages = new LinkedHashSet<>();
	
	/**
	 * @param name
	 * @param type
	 */
	public Parameter(String name, Type type) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		
		this.type = type;
		this.defaultValue = null;
	}

	/**
	 * @param name
	 * @param type
	 * @param keywords
	 */
	public Parameter(String name, Type type, Set<ParameterKeyword> keywords) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		
		this.type = type;
		this.defaultValue = null;
		if (keywords != null) this.keywords.addAll(keywords);
	}
	
	/**
	 * @param name
	 * @param type
	 * @param defaultValue
	 */
	public Parameter(String name, Type type, String defaultValue) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		
		this.type = type;
		this.defaultValue = defaultValue;
	}
	
	/**
	 * @param name
	 * @param type
	 * @param keywords
	 * @param defaultValue
	 */
	public Parameter(String name, Type type, Set<ParameterKeyword> keywords, String defaultValue) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		
		this.type = type;
		this.defaultValue = defaultValue;
		if (keywords != null) this.keywords.addAll(keywords);
	}

	public Method getMethod() {
		return method;
	}

	void setMethod(Method method) {
		if (method == null) throw new NullPointerException("parameter 'method' must not be null");
		if (this.method != null) throw new IllegalArgumentException("detected attempt to override an already set method as an owner of a parameter");
		
		this.method = method;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public Type getType() {
		return type;
	}
	
	public boolean isVar() {
		return this.keywords.contains(ParameterKeyword.VAR);
	}
	
	public boolean isConst() {
		return this.keywords.contains(ParameterKeyword.CONST);
	}
	
	public boolean isOut() {
		return this.keywords.contains(ParameterKeyword.OUT);
	}

	public boolean isRef() {
		return ref;
	}

	public void setRef(boolean ref) {
		this.ref = ref;
	}
	
	public Set<ParameterKeyword> getKeywords() {
		return keywords;
	}

	/**
	 * @return the attributeUsages
	 */
	@Override
	public Set<AttributeUsage> getAttributeUsages() {
		return attributeUsages;
	}
	
	/**
	 * @param attributeUsage
	 * @return
	 */
	@Override
	public boolean addAttributeUsage(AttributeUsage attributeUsage) {
		return this.attributeUsages.add(attributeUsage);
	}

	/**
	 * adapts the member name in case the member name is a reserved word in Delphi
	 * 
	 * @return the member name prepended with ampersand in case the member name is a reserved word in Delphi
	 */
	public String getNonReservedWordName() {
		return Keyword.adaptIdentifierIfNeeded(getName());
	}
				
	/**
     * This is a helper class that allows for the creation of method objects by calling createMethod()
     * and passing type/name pairs. Otherwise the method parameter names are called arg0, arg1 ...
     * which is not so good for the readability of source code.
     *
     * @author mmt
     *
     */
    public static final class ParamInfo {

    	private final String name;
    	private final Type type;
    	private final String doc;
    	private final String defaultValue;
    	private final Set<ParameterKeyword> keywords;

    	/**
    	 * @param type
    	 * @param name
    	 */
    	public ParamInfo(Type type, String name) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = null;
    		this.defaultValue = null;
    		this.keywords = Collections.emptySet();
    	}
    	
    	/**
    	 * @param type
    	 * @param name
    	 * @param defaultValue
    	 */
    	public ParamInfo(Type type, String name, String defaultValue) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = null;
    		this.defaultValue = defaultValue;
    		this.keywords = Collections.emptySet();
    	}

    	/**
    	 * @param type
    	 * @param name
    	 * @param keywords
    	 */
    	public ParamInfo(Type type, String name, Set<ParameterKeyword> keywords) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = null;
    		this.defaultValue = null;
    		this.keywords = keywords;
    	}

    	/**
    	 * @param type
    	 * @param name
    	 * @param doc
    	 * @param defaultValue
    	 */
    	public ParamInfo(Type type, String name, String doc, String defaultValue) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = doc;
    		this.defaultValue = defaultValue;
    		this.keywords = Collections.emptySet();
    	}

    	/**
    	 * @param type
    	 * @param name
    	 * @param doc
    	 * @param keywords
    	 */
    	public ParamInfo(Type type, String name, String doc, Set<ParameterKeyword> keywords) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = doc;
    		this.defaultValue = null;
    		this.keywords = keywords;
    	}
    	
    	/**
    	 * @param type
    	 * @param name
    	 * @param doc
    	 * @param defaultValue
    	 * @param keywords
    	 */
    	public ParamInfo(Type type, String name, String doc, String defaultValue, Set<ParameterKeyword> keywords) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = doc;
    		this.defaultValue = defaultValue;
    		this.keywords = keywords;
    	}

		/**
		 * @return the name
		 */
		public final String getName() {
			return name;
		}

		/**
		 * @return the type
		 */
		public final Type getType() {
			return type;
		}



		/**
		 * @return the modifiers
		 */
		public Set<ParameterKeyword> getKeywords() {
			return keywords;
		}

		/**
		 * @return the doc
		 */
		public String getDoc() {
			return doc;
		}

		public String getDefaultValue() {
			return defaultValue;
		}
    }
}
