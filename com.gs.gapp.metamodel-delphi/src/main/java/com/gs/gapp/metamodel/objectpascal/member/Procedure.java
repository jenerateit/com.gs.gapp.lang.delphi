/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 *
 */
public class Procedure extends Method {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6338704080533042824L;

	/**
	 * This is a convenience constructor to create a procedure with no parameters
	 * 
	 * @param name
	 * @param owner
	 */
	public Procedure(String name, Type owner) {
		super(name, owner);
	}
	
	/**
	 * This is a convenience constructor to create a global procedure with no parameters
	 * 
	 * @param name
	 * @param declaringUnit
	 */
	public Procedure(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param owner
	 * @param parameters
	 */
	public Procedure(String name, Type owner, Parameter... parameters) {
		super(name, owner, parameters);
	}
	
	/**
	 * Creates a global procedure with the given parameters
	 * 
	 * @param name
	 * @param declaringUnit
	 * @param parameters
	 */
	public Procedure(String name, Unit declaringUnit, Parameter... parameters) {
		super(name, declaringUnit, parameters);
	}
}