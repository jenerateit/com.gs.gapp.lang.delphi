/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.member;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.objectpascal.Directive;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalNamingRulesEnforcer;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.VisibilityDirective;
import com.gs.gapp.metamodel.objectpascal.member.Parameter.ParamInfo;
import com.gs.gapp.metamodel.objectpascal.type.structured.Member;
import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Properties">Delphi XE7 - Properties</a>
 */
public class Property extends Member {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5809047274517896947L;
	
	
	private final Type type;
	private Member fieldOrMethodForRead;
	private Member fieldOrMethodForWrite;
	private Member enclosingFieldOrMethod;
	
	private Integer index;
	private final Set<Parameter> indexParameters = new LinkedHashSet<>();
	
	/**
	 * TODO do we need this? which specifiers do we need?
	 */
	private Set<Directive> specifiers;
	
	/**
	 * @param name
	 * @param owner
	 */
	public Property(String name, Type type, Type owner) {
		this(name, type, owner, new Parameter[0]);
	}
	
	/**
	 * @param name
	 * @param type
	 * @param owner
	 * @param indexParameters
	 */
	public Property(String name, Type type, Type owner, Parameter... indexParameters) {
		super(name, owner);
		
        if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		this.type = type;
		
		if (indexParameters != null) this.indexParameters.addAll(Arrays.asList(indexParameters));
	}

	public Type getType() {
		return type;
	}

	public Set<Directive> getSpecifiers() {
		return specifiers;
	}

	public void setSpecifiers(Set<Directive> specifiers) {
		if (specifiers == null) throw new NullPointerException("parameter 'speicifiers' must not be null");
		if (Directive.getPropertyspecifiers().containsAll(specifiers) == false) throw new IllegalArgumentException("parameter 'speicifiers' contains directives that are not allowed for properties");
		
		this.specifiers = specifiers;
	}

	public Member getFieldOrMethodForRead() {
		return fieldOrMethodForRead;
	}

	public void setFieldOrMethodForRead(Member fieldOrMethodForRead) {
		if (fieldOrMethodForWrite != null && !(fieldOrMethodForWrite instanceof Field || fieldOrMethodForWrite instanceof Method))
			throw new IllegalArgumentException("a property needs to use a Field or a Method for read access, provided was:" + fieldOrMethodForWrite.getClass());
		
		this.fieldOrMethodForRead = fieldOrMethodForRead;
	}

	public Member getFieldOrMethodForWrite() {
		return fieldOrMethodForWrite;
	}

	public void setFieldOrMethodForWrite(Member fieldOrMethodForWrite) {
		if (fieldOrMethodForWrite != null && !(fieldOrMethodForWrite instanceof Field || fieldOrMethodForWrite instanceof Method))
			throw new IllegalArgumentException("a property needs to use a Field or a Method for write access, provided was:" + fieldOrMethodForWrite.getClass());
			
		this.fieldOrMethodForWrite = fieldOrMethodForWrite;
	}
	
	public Member getEnclosingFieldOrMethod() {
		return enclosingFieldOrMethod;
	}

	public void setEnclosingFieldOrMethod(Member enclosingFieldOrMethod) {
		if (enclosingFieldOrMethod != null && !(enclosingFieldOrMethod instanceof Field || enclosingFieldOrMethod instanceof Method))
			throw new IllegalArgumentException("a property needs to use a Field or a Method for write access, provided was:" + enclosingFieldOrMethod.getClass());
		this.enclosingFieldOrMethod = enclosingFieldOrMethod;
	}

	public Set<Parameter> getIndexParameters() {
		return indexParameters;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}
	
	/**
	 * @return this
	 */
	public Procedure createSetterProcedure() {
		ParamInfo[] paramInfos = null;
    	if (indexParameters.size() > 0) {
    		paramInfos = new ParamInfo[indexParameters.size()+1];
    		int idx = 0;
    		for (Parameter parameter : indexParameters) {
    			paramInfos[idx] = new ParamInfo(parameter.getType(), parameter.getName());
    			idx++;
    		}
    	} else {
    		paramInfos = new ParamInfo[1];
    	}
    	paramInfos[paramInfos.length-1] = new ParamInfo(this.getType(), this.getName());
    	
    	
		Procedure procedure = Method.createProcedure(null, "Set" + StringTools.firstUpperCase(this.getName()), this.getOwner(), paramInfos);
		procedure.setVisibility(VisibilityDirective.PRIVATE);
		procedure.setOriginatingElement(this);
		this.setFieldOrMethodForWrite(procedure);
		return procedure;
	}
	
	/**
	 * @return this
	 */
	public Field createFieldForWrite() {
		if (indexParameters.size() > 0) throw new RuntimeException("cannot create a field for an indexed property - must use a method instead");
		if (this.getFieldOrMethodForWrite() != null && this.getFieldOrMethodForWrite() instanceof Method) throw new RuntimeException("cannot create field for write since there is already a method for write:" + this.getFieldOrMethodForWrite());
		
		if (this.getFieldOrMethodForWrite() != null && this.getFieldOrMethodForWrite() instanceof Field) {
			// do not create the same field twice
			return (Field) this.getFieldOrMethodForWrite();
		}
		
		String fieldName = "F"+this.getName();
		Field field = null;
		String normalizedFieldName = ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(fieldName);
		
		if (this.getFieldOrMethodForRead() != null && this.getFieldOrMethodForRead() instanceof Field &&
				this.getFieldOrMethodForRead().getName().equalsIgnoreCase(normalizedFieldName)) {
			field = (Field) this.getFieldOrMethodForRead();
		} else {
			field = new Field(fieldName, this.getType(), this.getOwner());
			field.setVisibility(VisibilityDirective.PRIVATE);
			field.setOriginatingElement(this);
		}
		
		this.setFieldOrMethodForWrite(field);
		field.setProperty(this);
		return field;
	}
	
    /**
     * @return this
     */
    public Function createGetterFunction() {
    	ParamInfo[] paramInfos = null;
    	if (indexParameters.size() > 0) {
    		paramInfos = new ParamInfo[indexParameters.size()];
    		int idx = 0;
    		for (Parameter parameter : indexParameters) {
    			paramInfos[idx] = new ParamInfo(parameter.getType(), parameter.getName());
    			idx++;
    		}
    	}
    	
    	Function function = Method.createFunction(this.getType(), null, "Get" + StringTools.firstUpperCase(this.getName()), this.getOwner(), paramInfos);
		function.setVisibility(VisibilityDirective.PRIVATE);
		function.setOriginatingElement(this);
		this.setFieldOrMethodForRead(function);
		return function;
	}
    
    /**
	 * @return this
	 */
	public Field createFieldForRead() {
		if (indexParameters.size() > 0) throw new RuntimeException("cannot create a field for an indexed property - must use a method instead");
		if (this.getFieldOrMethodForRead() != null && this.getFieldOrMethodForRead() instanceof Method) throw new RuntimeException("cannot create field for read since there is already a method for read:" + this.getFieldOrMethodForRead());
		
		if (this.getFieldOrMethodForRead() != null && this.getFieldOrMethodForRead() instanceof Field) {
			// do not create the same field twice
			return (Field) this.getFieldOrMethodForRead();
		}
		
		String fieldName = "F"+this.getName();
		Field field = null;
				
		String normalizedFieldName = ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(fieldName);
		if (this.getFieldOrMethodForWrite() != null && this.getFieldOrMethodForWrite() instanceof Field &&
				this.getFieldOrMethodForWrite().getName().equalsIgnoreCase(normalizedFieldName)) {
			field = (Field) this.getFieldOrMethodForWrite();
		} else {
			field = new Field(fieldName, this.getType(), this.getOwner());
			field.setVisibility(VisibilityDirective.PRIVATE);
			field.setOriginatingElement(this);
		}
		
		this.setFieldOrMethodForRead(field);
		field.setProperty(this);
		return field;
	}
	
	/**
	 * @return
	 */
    @Override
	public java.util.Set<StructuredType> getStructuredTypes() {
    	LinkedHashSet<StructuredType> result = new LinkedHashSet<>();
    	if (type instanceof StructuredType) {
    		result.add((StructuredType) type);
    	}
    	return result;
	}
    
    /**
     * @return
     */
    @Override
    public String getDeveloperAreaKey() {
        StringBuilder sb = new StringBuilder(getOwner() != null ? getOwner().getQualifiedName() : getDeclaringUnit().getName()).append(".").append(getName().toLowerCase());
        return sb.toString();
    }
}
