/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

/**
 * "Attribute" for Delphi is what "Annotation" is for Java
 * 
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Declaring_Custom_Attributes">Delphi XE7 - Custom Attributes</a>
 */
public class Attribute extends Clazz {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1880385022621380543L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Attribute(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Attribute(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerClazz
	 */
	public Attribute(String name, Clazz outerClazz) {
		super(name, outerClazz);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.structured.Clazz#setParent(com.gs.gapp.metamodel.objectpascal.type.ClassTypeI)
	 */
	@Override
	public void setParent(ClassTypeI parent) {
		if (parent != null) {
			ClassTypeI invalidParent = null;
			if (!"TCustomAttribute".equals(parent.getName())) {
				invalidParent = parent;
			}
			
			if (invalidParent != null) {
				if (parent instanceof Clazz) {
					Clazz clazz = (Clazz) parent;
					boolean foundCorrectParent = false;
					for (ClassTypeI aParentType : clazz.getAllParents()) {
						if ("TCustomAttribute".equals(aParentType.getName())) {
							foundCorrectParent = true;
							invalidParent = null;
							break;
						}
					}
					if (!foundCorrectParent) {
						invalidParent = parent;	
					}
				} else {
					invalidParent = parent;	
				}
			}
			
			if (invalidParent != null) {
				throw new IllegalArgumentException("illegal parent type given (" + parent + "), a custom attribute must inherit from type TCustomAttribute");
			}
		}
		super.setParent(parent);
	}
}
