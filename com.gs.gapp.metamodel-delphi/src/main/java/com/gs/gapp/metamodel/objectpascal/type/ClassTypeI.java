package com.gs.gapp.metamodel.objectpascal.type;

/**
 * This is a marker interface to denote a type that can be used as a parent
 * class for a given class.
 * 
 * @author mmt
 *
 */
public interface ClassTypeI extends InheritableTypeI {

	
}
