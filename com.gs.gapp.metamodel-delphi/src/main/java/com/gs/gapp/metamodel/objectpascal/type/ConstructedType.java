package com.gs.gapp.metamodel.objectpascal.type;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;

public class ConstructedType extends Type implements ClassTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4217152345844071123L;

	private final Type genericType;
	private final Set<Type> genericArguments = new LinkedHashSet<>();

	public ConstructedType(Type genericType, Type ...genericArguments) {
		super(genericType != null ? genericType.getName() : "_UNDEFINED_");
		
		if (genericType == null) throw new NullPointerException("parameter 'genericType' must not be null"); 
		if (genericArguments == null) throw new NullPointerException("parameter 'genericArguments' must not be null");
		if (genericArguments.length == 0) throw new IllegalArgumentException("parameter 'genericArguments' must have a length > 0");
		
		this.genericType = genericType;
		this.genericArguments.addAll(Arrays.asList(genericArguments));
	}

	public Set<Type> getGenericArguments() {
		return genericArguments;
	}

	public Type getGenericType() {
		return genericType;
	}
	
	@Override
	public Unit getDeclaringUnit() {
		return genericType.getDeclaringUnit();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.Type#getTypeAsString()
	 */
	@Override
	public String getTypeAsString() {
		// TODO The following implementation only works for simple generic types. If for instance does not work for nested generic types.
		StringBuilder genericTypesAsCSL = new StringBuilder();
		if (genericArguments.size() > 0) {
			String comma = "";
			for (Type type : genericArguments) {
				if (type instanceof ConstructedType) {
					throw new RuntimeException("nested constructed type identified, which is not yet supported by the Object Pascal code generation");
				}
				genericTypesAsCSL.append(comma).append(type.getName());
				comma = ", ";
			}
		}
		return new StringBuilder(genericType.getName()).append("<").append(genericTypesAsCSL).append(">").toString();
	}
}
