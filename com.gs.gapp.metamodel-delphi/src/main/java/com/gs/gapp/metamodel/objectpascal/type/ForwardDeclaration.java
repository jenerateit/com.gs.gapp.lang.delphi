/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;
import com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Classes_and_Objects#Forward_Declarations_and_Mutually_Dependent_Classes">Delphi XE7 - Forward Declarations</a>
 */
/**
 * @author marcu
 *
 */
public class ForwardDeclaration extends Type {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8826044210581982623L;
	
	private final Clazz declaredClazz;
	
	private final Interface declaredInterface;
	
	/**
	 * @param name
	 * @param declaredClazz
	 * @param declaringUnit
	 * 
	 */
	public ForwardDeclaration(String name, Clazz declaredClazz, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (declaredClazz == null) throw new NullPointerException("parameter 'declaredClazz' must not be null");
		
		this.declaredClazz = declaredClazz;
		this.declaredInterface = null;
	}
	
	/**
	 * @param name
	 * @param declaredClazz
	 * @param outerType
	 */
	public ForwardDeclaration(String name, Clazz declaredClazz, OuterTypeI outerType) {
		super(name, outerType);
		
		if (declaredClazz == null) throw new NullPointerException("parameter 'declaredClazz' must not be null");
		
		this.declaredClazz = declaredClazz;
		this.declaredInterface = null;
	}
	
	/**
	 * @param name
	 * @param declaredInterface
	 * @param declaringUnit
	 */
	public ForwardDeclaration(String name, Interface declaredInterface, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (declaredInterface == null) throw new NullPointerException("parameter 'declaredInterface' must not be null");
		
		this.declaredClazz = null;
		this.declaredInterface = declaredInterface;
	}
	
	/**
	 * @param name
	 * @param declaredInterface
	 * @param outerType
	 */
	public ForwardDeclaration(String name, Interface declaredInterface, OuterTypeI outerType) {
		super(name, outerType);
		
		if (declaredInterface == null) throw new NullPointerException("parameter 'declaredInterface' must not be null");
		
		this.declaredClazz = null;
		this.declaredInterface = declaredInterface;
	}
	
	{ // setting default values
		this.setDeveloperDocumentationGenerated(false);
	}

	public Clazz getDeclaredClazz() {
		return declaredClazz;
	}

	public Interface getDeclaredInterface() {
		return declaredInterface;
	}
	
	public StructuredType getDeclaredStructuredType() {
		if (declaredClazz != null) {
			return declaredClazz;
		}
		return declaredInterface;
	}
	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.Type#isOnlyVisibleInImplementationSection()
	 */
	@Override
	public boolean isOnlyVisibleInImplementationSection() {
		return declaredClazz != null ? declaredClazz.isOnlyVisibleInImplementationSection() : declaredInterface.isOnlyVisibleInImplementationSection();
	}
}
