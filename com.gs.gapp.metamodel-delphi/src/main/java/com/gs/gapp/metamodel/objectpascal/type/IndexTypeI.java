package com.gs.gapp.metamodel.objectpascal.type;

import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalTypeValue;

/**
 * Every type that implements this interface can serve as an index type for an array declaration.
 * 
 * @author mmt
 *
 */
public interface IndexTypeI {

	String getName();
	
	Set<OrdinalTypeValue> getValues();
}
