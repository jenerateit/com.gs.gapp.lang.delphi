package com.gs.gapp.metamodel.objectpascal.type;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;

public interface InheritableTypeI {

    String getName();
	
	String getTypeAsString();
	
	String getTypeParametersAsString();
	
	OuterTypeI getOuterType();
	
	Unit getDeclaringUnit();
}
