package com.gs.gapp.metamodel.objectpascal.type;

/**
 * This is a marker interface to denote a type that can be used as a parent
 * type for an interface
 * 
 * @author mmt
 *
 */
public interface InterfaceTypeI extends InheritableTypeI {

}
