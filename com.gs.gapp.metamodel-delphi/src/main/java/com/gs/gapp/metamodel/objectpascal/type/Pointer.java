/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type;

import com.gs.gapp.metamodel.objectpascal.Keyword;
import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Pointers_and_Pointer_Types_%28Delphi%29#Pointer_Types">Delphi XE7 - Pointers</a>
 */
public class Pointer extends Type {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8707408316181475448L;
	
	private final Type referencedType;

	/**
	 * @param name
	 * @param referencedType
	 * @param declaringUnit
	 */
	public Pointer(String name, Type referencedType, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (referencedType == null) throw new NullPointerException("parameter 'referencedType' must not be null");
		this.referencedType = referencedType;
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Pointer(String name, Type referencedType, Unit declaringUnit,
			boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (referencedType == null) throw new NullPointerException("parameter 'referencedType' must not be null");
		this.referencedType = referencedType;
	}

	/**
	 * @param name
	 * @param referencedType
	 * @param outerType
	 */
	public Pointer(String name, Type referencedType, OuterTypeI outerType) {
		super(name, outerType);
		
		if (referencedType == null) throw new NullPointerException("parameter 'referencedType' must not be null");
		this.referencedType = referencedType;
	}

	public Type getReferencedType() {
		return referencedType;
	}

	/**
	 * @return
	 */
	public String getNonReservedWordName() {
		return Keyword.adaptIdentifierIfNeeded(getName());
	}
}
