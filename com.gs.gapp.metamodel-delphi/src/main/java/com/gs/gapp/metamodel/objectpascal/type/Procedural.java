/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.member.Parameter;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Procedural_Types#About_Procedural_Types">Delphi XE7 - Procedural Types</a>
 */
public class Procedural extends Type {

	/**
	 * 
	 */
	private static final long serialVersionUID = -762007927685488410L;
	
	private final Set<Parameter> parameters = new LinkedHashSet<>();
	
	private final Type returnType;
	
	private boolean ofObject = false;

	/**
	 * @param name
	 * @param declaringUnit
	 * @param parameters
	 */
	public Procedural(String name, Unit declaringUnit, Parameter... parameters) {
		super(name, declaringUnit);
		
        if (parameters == null) throw new NullPointerException("parameter 'parameters' must not be null");
		this.getParameters().addAll(Arrays.asList(parameters));
		this.returnType = null;
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 * @param parameters
	 */
	public Procedural(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection, Parameter... parameters) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (parameters == null) throw new NullPointerException("parameter 'parameters' must not be null");
		this.getParameters().addAll(Arrays.asList(parameters));
		this.returnType = null;
	}

	/**
	 * @param name
	 * @param outerType
	 * @param parameters
	 */
	public Procedural(String name, OuterTypeI outerType, Parameter... parameters) {
		super(name, outerType);
		
		if (parameters == null) throw new NullPointerException("parameter 'parameters' must not be null");
		this.getParameters().addAll(Arrays.asList(parameters));
		this.returnType = null;
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param parameters
	 */
	public Procedural(String name, Unit declaringUnit, Type returnType, Parameter... parameters) {
		super(name, declaringUnit);
		
        if (parameters == null) throw new NullPointerException("parameter 'parameters' must not be null");
		this.getParameters().addAll(Arrays.asList(parameters));
		this.returnType = returnType;
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 * @param parameters
	 */
	public Procedural(String name, Unit declaringUnit, Type returnType, boolean onlyVisibleInImplementationSection, Parameter... parameters) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (parameters == null) throw new NullPointerException("parameter 'parameters' must not be null");
		this.getParameters().addAll(Arrays.asList(parameters));
		this.returnType = returnType;
	}

	/**
	 * @param name
	 * @param outerType
	 * @param parameters
	 */
	public Procedural(String name, OuterTypeI outerType, Type returnType, Parameter... parameters) {
		super(name, outerType);
		
		if (parameters == null) throw new NullPointerException("parameter 'parameters' must not be null");
		this.getParameters().addAll(Arrays.asList(parameters));
		this.returnType = returnType;
	}

	public Type getReturnType() {
		return returnType;
	}

	public Set<Parameter> getParameters() {
		return parameters;
	}

	public boolean isOfObject() {
		return ofObject;
	}

	public void setOfObject(boolean ofObject) {
		this.ofObject = ofObject;
	}
}
