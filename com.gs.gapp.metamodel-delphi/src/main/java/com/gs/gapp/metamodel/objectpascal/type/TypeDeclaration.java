/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalType;
import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalTypeValue;

/**
 * Note that TypeDeclaration does not represent the same type as the type it denotes.
 * Instead it creates a distinct new type.
 * 
 * Example: type TMyString = type string;
 * 
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Type_Compatibility_and_Identity">Delphi XE7 - Type Compatibility and Identity</a>
 */
public class TypeDeclaration extends Type implements IndexTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7210565922995179584L;
	
	private final Type denotedType;

	/**
	 * @param name
	 * @param denotedType
	 * @param declaringUnit
	 */
	public TypeDeclaration(String name, Type denotedType, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (denotedType == null) throw new NullPointerException("parameter 'denotedType' must not be null");
		this.denotedType = denotedType;
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public TypeDeclaration(String name, Type denotedType, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (denotedType == null) throw new NullPointerException("parameter 'denotedType' must not be null");
		this.denotedType = denotedType;
	}

	/**
	 * @param name
	 * @param denotedType
	 * @param outerType
	 */
	public TypeDeclaration(String name, Type denotedType, OuterTypeI outerType) {
		super(name, outerType);
		
		if (denotedType == null) throw new NullPointerException("parameter 'denotedType' must not be null");
		this.denotedType = denotedType;
	}

	/**
	 * @return
	 */
	public Type getDenotedType() {
		return denotedType;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.IndexTypeI#getValues()
	 */
	@Override
	public Set<OrdinalTypeValue> getValues() {
        Type type = this;
        Set<OrdinalTypeValue> result = new LinkedHashSet<>();
        
        while (type != null) {
        	if (type instanceof OrdinalType) {
        		OrdinalType ordinalType = (OrdinalType) type;
				result.addAll(ordinalType.getValues());
				break;
        	}
        	if (type instanceof TypeDeclaration) {
				TypeDeclaration typeDeclaration = (TypeDeclaration) type;
        		type = typeDeclaration.getDenotedType();
        	}
        }

        return result;
	}

	public OrdinalTypeValue getMinValue() {
		Set<OrdinalTypeValue> values = getValues();
		return values.size() > 0 ? values.iterator().next() : null;
	}
	
	public OrdinalTypeValue getMaxValue() {
		Set<OrdinalTypeValue> values = getValues();
		return values.size() > 0 ? values.toArray(new OrdinalTypeValue[0])[values.size()-1] : null;
	}
}
