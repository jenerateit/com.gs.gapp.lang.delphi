/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * Note that TypeIdentifier represents exactly the same type as the type it denotes.
 * A TypeIdentifier can be used as the parent of a class or an interface.
 * There are no checks to find out whether the denoted type actually is a class
 * or an interface in such an inheritance scenario.
 * 
 * Example: type TIntegerAlias = Integer;
 * 
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Type_Compatibility_and_Identity">Delphi XE7 - Type Compatibility and Identity</a>
 */
public class TypeIdentifier extends Type implements ClassTypeI, InterfaceTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3217317017298056345L;

	private final Type denotedType;
	
	/**
	 * @param name
	 * @param denotedType
	 * @param declaringUnit
	 */
	public TypeIdentifier(String name, Type denotedType, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (denotedType == null) throw new NullPointerException("parameter 'denotedType' must not be null");
		this.denotedType = denotedType;
	}
	
	
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public TypeIdentifier(String name, Type denotedType, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (denotedType == null) throw new NullPointerException("parameter 'denotedType' must not be null");
		this.denotedType = denotedType;
	}

	/**
	 * @param name
	 * @param denotedType
	 * @param outerType
	 */
	public TypeIdentifier(String name, Type denotedType, OuterTypeI outerType) {
		super(name, outerType);
		
		if (denotedType == null) throw new NullPointerException("parameter 'denotedType' must not be null");
		this.denotedType = denotedType;
	}
	
	/**
	 * @return
	 */
	public Type getDenotedType() {
		return denotedType;
	}
}
