/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;

/**
 * @author mmt
 * 
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Generics_Index">Delphi XE7 - Generics</a>
 *
 */
public class TypeParameter extends Type {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3240746456049587908L;
	
	private final Set<Interface> interfaces = new LinkedHashSet<>();
	private final Clazz clazz;
	private final boolean constructorConstraint;
	private final boolean classConstraint;
	private final boolean recordConstraint;

	/**
	 * @param name
	 */
	public TypeParameter(String name, Set<Interface> setOfInterfaces, Clazz clazz, boolean constructorConstraint, boolean classConstraint, boolean recordConstraint) {
		super(name);
		if (setOfInterfaces != null) interfaces.addAll(setOfInterfaces);
		this.clazz = clazz;
		this.constructorConstraint = constructorConstraint;
		this.classConstraint = classConstraint;
		this.recordConstraint = recordConstraint;
	}

	public Set<Interface> getInterfaces() {
		return interfaces;
	}

	public Clazz getClazz() {
		return clazz;
	}

	public boolean isConstructorConstraint() {
		return constructorConstraint;
	}

	public boolean isClassConstraint() {
		return classConstraint;
	}

	public boolean isRecordConstraint() {
		return recordConstraint;
	}
}
