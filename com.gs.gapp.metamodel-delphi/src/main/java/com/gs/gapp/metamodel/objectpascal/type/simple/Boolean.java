/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.simple;

import java.util.Collection;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;


/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Simple_Types#Boolean_Types">http://docwiki.embarcadero.com/...</a>
 */
public class Boolean extends OrdinalType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5408980502142331208L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Boolean(String name, Unit declaringUnit) {
		super(name, null, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param ordinalTypeValues
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Boolean(String name, Collection<OrdinalTypeValue> ordinalTypeValues,
			Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, ordinalTypeValues, declaringUnit,
				onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerType
	 */
	public Boolean(String name, OuterTypeI outerType) {
		super(name, null, outerType);
	}
}
