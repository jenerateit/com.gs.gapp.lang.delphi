/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.simple;

import java.util.Collection;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;


/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Simple_Types#Character_Types">http://docwiki.embarcadero.com/...</a>
 */
public class Character extends OrdinalType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8538191496322608482L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Character(String name, Unit declaringUnit) {
		super(name, null, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param ordinalTypeValues
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Character(String name,
			Collection<OrdinalTypeValue> ordinalTypeValues, Unit declaringUnit,
			boolean onlyVisibleInImplementationSection) {
		super(name, ordinalTypeValues, declaringUnit,
				onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerType
	 */
	public Character(String name, OuterTypeI outerType) {
		super(name, null, outerType);
	}
	
}
