/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.simple;

import java.util.Arrays;
import java.util.Collection;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;



/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Simple_Types#Enumerated_Types">Delphi XE7 - Enumerated Types</a>
 */
public class Enumeration extends OrdinalType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8512303702903616698L;

	/**
	 * @param name
	 * @param values
	 * @param declaringUnit
	 */
	public Enumeration(String name, Unit declaringUnit, OrdinalTypeValue... values) {
		super(name, Arrays.asList(values), declaringUnit);
	}
	
	/**
	 * @param name
	 * @param values
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Enumeration(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection, OrdinalTypeValue... values) {
		super(name, Arrays.asList(values), declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param values
	 * @param outerType
	 */
	public Enumeration(String name, OuterTypeI outerType, OrdinalTypeValue... values) {
		super(name, Arrays.asList(values), outerType);
	}
	
	/**
	 * @param name
	 * @param ordinalTypeValues
	 * @param declaringUnit
	 */
	@Deprecated
	public Enumeration(String name, Collection<OrdinalTypeValue> ordinalTypeValues, Unit declaringUnit) {
		super(name, ordinalTypeValues, declaringUnit);
	}

	/**
	 * @param name
	 * @param ordinalTypeValues
	 * @param outerClazz
	 */
	@Deprecated
	public Enumeration(String name, Collection<OrdinalTypeValue> ordinalTypeValues, Clazz outerClazz) {
		super(name, ordinalTypeValues, outerClazz);
	}
}
