/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.simple;

import java.util.Collection;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;



/**
 * Note that you have to use SubRange type when you want to define a type like in the following
 * example: "type MyIntegerRange : 1..10;". Amongst others, this is going to be used when you set index types for arrays. 
 * 
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Simple_Types#Integer_Types">Delphi XE7 - Simple Type Integer</a>
 */
public class Integer extends OrdinalType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5982815983937561797L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Integer(String name, Unit declaringUnit) {
		super(name, null, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param ordinalTypeValues
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Integer(String name, Collection<OrdinalTypeValue> ordinalTypeValues, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, ordinalTypeValues, declaringUnit, onlyVisibleInImplementationSection);
	}
	
	/**
	 * @param name
	 * @param outerType
	 */
	public Integer(String name, OuterTypeI outerType) {
		super(name, null, outerType);
	}
}
