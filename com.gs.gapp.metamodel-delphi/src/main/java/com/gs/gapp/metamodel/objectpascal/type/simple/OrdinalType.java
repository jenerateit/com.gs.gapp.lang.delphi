/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.simple;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.Keyword;
import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.IndexTypeI;


/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Simple_Types#Ordinal_Types">http://docwiki.embarcadero.com/...</a>
 */
public abstract class OrdinalType extends SimpleType implements IndexTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1098158457061222466L;
	
	private final Set<OrdinalTypeValue> values = new LinkedHashSet<>();

	/**
	 * @param name
	 * @param ordinalTypeValues
	 * @param declaringUnit
	 */
	public OrdinalType(String name, Collection<OrdinalTypeValue> ordinalTypeValues, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (ordinalTypeValues != null) this.values.addAll(ordinalTypeValues);
	}
	
	
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public OrdinalType(String name, Collection<OrdinalTypeValue> ordinalTypeValues, Unit declaringUnit,
			boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (ordinalTypeValues != null) this.values.addAll(ordinalTypeValues);
	}



	/**
	 * @param name
	 * @param ordinalTypeValues
	 * @param outerType
	 */
	public OrdinalType(String name, Collection<OrdinalTypeValue> ordinalTypeValues, OuterTypeI outerType) {
		super(name, outerType);
		
		if (ordinalTypeValues != null) this.values.addAll(ordinalTypeValues);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.IndexTypeI#getValues()
	 */
	@Override
	public Set<OrdinalTypeValue> getValues() {
		return values;
	}
	
	public OrdinalTypeValue getMinValue() {
		return values.size() > 0 ? values.iterator().next() : null;
	}
	
	public OrdinalTypeValue getMaxValue() {
		return values.size() > 0 ? values.toArray(new OrdinalTypeValue[0])[values.size()-1] : null;
	}
	
	/**
	 * @return
	 */
	public String getNonReservedWordName() {
		return Keyword.adaptIdentifierIfNeeded(getName());
	}
}
