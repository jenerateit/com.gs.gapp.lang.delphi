/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.simple;

import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalNamingRulesEnforcer;

/**
 * @author mmt
 *
 */
public class OrdinalTypeValue extends ObjectPascalModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3374631088686513865L;

	private final Integer explicitOrdinality;

	/**
	 * @param name
	 */
	public OrdinalTypeValue(String name) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		this.explicitOrdinality = null;
	}
	
	public OrdinalTypeValue(String name, Integer explicitOrdinality) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		this.explicitOrdinality = explicitOrdinality;
	}

	public Integer getExplicitOrdinality() {
		return explicitOrdinality;
	}

}
