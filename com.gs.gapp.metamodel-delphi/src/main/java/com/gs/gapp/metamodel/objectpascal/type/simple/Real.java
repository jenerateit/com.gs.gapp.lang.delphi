package com.gs.gapp.metamodel.objectpascal.type.simple;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Simple_Types#Real_Types">http://docwiki.embarcadero.com/...</a>
 */
public class Real extends SimpleType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3020000396978882108L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Real(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Real(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerType
	 */
	public Real(String name, OuterTypeI outerType) {
		super(name, outerType);
	}
}
