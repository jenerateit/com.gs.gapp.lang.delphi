/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.simple;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Simple_Types">Delphi XE7 - Simple Types</a>
 */
public abstract class SimpleType extends Type {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8512303702903616698L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public SimpleType(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public SimpleType(String name, Unit declaringUnit,
			boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerType
	 */
	public SimpleType(String name, OuterTypeI outerType) {
		super(name, outerType);
	}
}
