/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.simple;

import java.util.Arrays;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;


/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Simple_Types#Subrange_Types">Delphi XE7 - Subrange Types</a>
 */
public class SubRange extends OrdinalType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7154699802075101131L;

	private final OrdinalType baseType;
	
	/**
	 * @param name
	 * @param baseType
	 * @param minValue
	 * @param maxValue
	 * @param declaringUnit
	 */
	public SubRange(String name, OrdinalType baseType, OrdinalTypeValue minValue, OrdinalTypeValue maxValue, Unit declaringUnit) {
		super(name, Arrays.asList(new OrdinalTypeValue[] {minValue, maxValue}), declaringUnit);
		this.baseType = baseType;
	}
	
	/**
	 * @param name
	 * @param baseType
	 * @param minValue
	 * @param maxValue
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public SubRange(String name, OrdinalType baseType, OrdinalTypeValue minValue, OrdinalTypeValue maxValue, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, Arrays.asList(new OrdinalTypeValue[] {minValue, maxValue}), declaringUnit, onlyVisibleInImplementationSection);
		this.baseType = baseType;
	}

	/**
	 * @param name
	 * @param baseType
	 * @param minValue
	 * @param maxValue
	 * @param outerType
	 */
	public SubRange(String name, OrdinalType baseType, OrdinalTypeValue minValue, OrdinalTypeValue maxValue, OuterTypeI outerType) {
		super(name, Arrays.asList(new OrdinalTypeValue[] {minValue, maxValue}), outerType);
		this.baseType = baseType;
	}

	public OrdinalType getBaseType() {
		return baseType;
	}
}
