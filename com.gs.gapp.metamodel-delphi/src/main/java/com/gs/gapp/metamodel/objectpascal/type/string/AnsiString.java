/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.string;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;


/**
 * @author mmt
 *
 */
public class AnsiString extends StringType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8222620516824428135L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public AnsiString(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public AnsiString(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerClazz
	 */
	public AnsiString(String name, Clazz outerClazz) {
		super(name, outerClazz);
	}
}
