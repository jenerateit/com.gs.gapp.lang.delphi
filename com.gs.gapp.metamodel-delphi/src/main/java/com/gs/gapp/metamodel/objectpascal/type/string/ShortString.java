/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.string;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;


/**
 * @author mmt
 *
 */
public class ShortString extends StringType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2560527595478688237L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public ShortString(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public ShortString(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerClazz
	 */
	public ShortString(String name, Clazz outerClazz) {
		super(name, outerClazz);
	}
}
