/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.string;

import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

/**
 * @author mmt
 *
 */
public abstract class StringType extends Type {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5000549552815075566L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public StringType(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public StringType(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerClazz
	 */
	public StringType(String name, Clazz outerClazz) {
		super(name, outerClazz);
	}
}
