/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.string;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;


/**
 * @author mmt
 *
 */
public class UnicodeString extends StringType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4076570744480069757L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public UnicodeString(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public UnicodeString(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerClazz
	 */
	public UnicodeString(String name, Clazz outerClazz) {
		super(name, outerClazz);
	}
}
