/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.string;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;


/**
 * @author mmt
 *
 */
public class WideString extends StringType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1989676137319934697L;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public WideString(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public WideString(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerClazz
	 */
	public WideString(String name, Clazz outerClazz) {
		super(name, outerClazz);
	}
}
