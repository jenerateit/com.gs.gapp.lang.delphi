/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.IndexTypeI;
import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalType;


/**
 * Note that an array with no index types is a dynamic array
 * 
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Structured_Types#Arrays">Delphi XE7 - Arrays</a>
 */
public class Array extends StructuredType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5411993686990872639L;
	
	private final Type baseType;
	
	private final java.util.List<IndexTypeI> indexTypes = new ArrayList<>();
	
	private Boolean packed;

	/**
	 * This constructor creates a dynamic array
	 * 
	 * @param name
	 * @param baseType
	 * @param declaringUnit
	 */
	public Array(String name, Type baseType, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null");
		this.baseType = baseType;
	}
	
	
	
	/**
	 * @param name
	 * @param baseType
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Array(String name, Type baseType, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null");
		this.baseType = baseType;
	}



	/**
	 * This constructor creates a static array
	 * 
	 * @param name
	 * @param baseType
	 * @param indexTypes
	 * @param declaringUnit
	 */
	public Array(String name, Type baseType, Unit declaringUnit, OrdinalType... indexTypes) {
		super(name, declaringUnit);
		
		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null");
		this.baseType = baseType;
		if (indexTypes != null) {
			for (OrdinalType indexType : indexTypes) {
				this.indexTypes.add(indexType);
			}
		}
	}
	
	
	
	/**
	 * @param name
	 * @param baseType
	 * @param indexTypes
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Array(String name, Type baseType, Unit declaringUnit, boolean onlyVisibleInImplementationSection, OrdinalType... indexTypes) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);

		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null");
		this.baseType = baseType;
		if (indexTypes != null) {
			for (OrdinalType indexType : indexTypes) {
				this.indexTypes.add(indexType);
			}
		}
	}



	/**
	 * This constructor creates a dynamic array
	 * 
	 * @param name
	 * @param baseType
	 * @param outerType
	 */
	public Array(String name, Type baseType, OuterTypeI outerType) {
		super(name, outerType);
		
		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null");
		this.baseType = baseType;
	}
	
	/**
	 * @param name
	 * @param baseType
	 */
	public Array(String name, Type baseType) {
		this(name, baseType, (OuterTypeI)null);
		setGenerated(false);  // such an array type is never generated inside a 'type' declaration section
	}
	
	/**
	 * This constructor creates a static array
	 * 
	 * @param name
	 * @param baseType
	 * @param indexTypes
	 * @param outerType
	 */
	public Array(String name, Type baseType, OuterTypeI outerType, OrdinalType... indexTypes) {
		super(name, outerType);
		
		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null");
		this.baseType = baseType;
		if (indexTypes != null) {
			for (OrdinalType indexType : indexTypes) {
				this.indexTypes.add(indexType);
			}
		}
	}

	/**
	 * @return
	 */
	public Type getBaseType() {
		return baseType;
	}

	public java.util.List<IndexTypeI> getIndexTypes() {
		return indexTypes;
	}
	
	public boolean isDanymic() {
		return this.indexTypes.size() == 0;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.Type#getTypeAsString()
	 */
	@Override
	public String getTypeAsString() {
		if (isGenerated()) {
		    return super.getTypeAsString();
		} else {
			return getArrayDef();
		}
	}
	
	/**
	 * @return
	 */
	public String getArrayDef() {
		StringBuilder sb = new StringBuilder("array");

		if (!isDanymic()) {
			sb.append("[");
			String comma = "";
			for (IndexTypeI indexType : indexTypes) {
				sb.append(comma);
				
				if (indexType instanceof com.gs.gapp.metamodel.objectpascal.type.simple.Boolean) {
					sb.append(indexType.getName());
				} else if (indexType instanceof com.gs.gapp.metamodel.objectpascal.type.simple.Character) {
					sb.append(indexType.getName());
				} else if (indexType instanceof com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration) {
					sb.append(indexType.getName());
				} else if (indexType instanceof com.gs.gapp.metamodel.objectpascal.type.simple.Integer) {
					sb.append(indexType.getName());
				} else if (indexType instanceof com.gs.gapp.metamodel.objectpascal.type.simple.SubRange) {
					com.gs.gapp.metamodel.objectpascal.type.simple.SubRange subRange = (com.gs.gapp.metamodel.objectpascal.type.simple.SubRange) indexType;
					if (subRange.isGenerated() == false) {
					    sb.append(subRange.getMinValue().getName()).append("..").append(subRange.getMaxValue().getName());
					} else {
						sb.append(subRange.getName());
					}
				}
				
				comma = ", ";
			}
			
			sb.append("]");
		}
		
		if (baseType != null) {
			sb.append(" of ").append(baseType.getName());
		}
		
		return sb.toString();
	}

    public Boolean getPacked() {
        return packed;
    }

    public void setPacked(Boolean packed) {
        this.packed = packed;
    }

    /* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType#getStructuredTypes()
	 */
	@Override
	public Set<StructuredType> getStructuredTypes() {
		if (baseType instanceof StructuredType) {
		    return Collections.singleton((StructuredType)baseType);
		}
		
		return Collections.emptySet();
	}
}
