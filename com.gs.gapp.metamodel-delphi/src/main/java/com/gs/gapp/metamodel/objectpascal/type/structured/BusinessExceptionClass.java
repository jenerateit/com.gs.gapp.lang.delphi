/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 *
 */
public class BusinessExceptionClass extends Clazz {

	private static final long serialVersionUID = 4109512095737100170L;
	
	public BusinessExceptionClass(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}

	public BusinessExceptionClass(String name, OuterTypeI outerType) {
		super(name, outerType);
	}
}
