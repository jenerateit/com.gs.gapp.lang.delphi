/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.Collections;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.Unit;


/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Class_References">Delphi XE7 - Class References</a>
 */
public class ClassReference extends StructuredType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8078674839708101886L;
	
	private final Clazz referencedClass;

	/**
	 * @param name
	 * @param referencedClass
	 * @param declaringUnit
	 */
	public ClassReference(String name, Clazz referencedClass, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (referencedClass == null) throw new NullPointerException("parameter 'referencedClass' must not be null");
		this.referencedClass = referencedClass;
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public ClassReference(String name, Clazz referencedClass, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (referencedClass == null) throw new NullPointerException("parameter 'referencedClass' must not be null");
		this.referencedClass = referencedClass;
	}

	/**
	 * @param name
	 * @param referencedClass
	 * @param outerClazz
	 */
	public ClassReference(String name, Clazz referencedClass, Clazz outerClazz) {
		super(name, outerClazz);
		
		if (referencedClass == null) throw new NullPointerException("parameter 'referencedClass' must not be null");
		this.referencedClass = referencedClass;
	}
	
	public Clazz getReferencedClass() {
		return referencedClass;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType#getStructuredTypes()
	 */
	@Override
	public Set<StructuredType> getStructuredTypes() {
		return Collections.singleton(referencedClass);
	}
}
