/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.objectpascal.ClazzDirective;
import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.VisibilityDirective;
import com.gs.gapp.metamodel.objectpascal.member.ClazzConstructor;
import com.gs.gapp.metamodel.objectpascal.member.ClazzDestructor;
import com.gs.gapp.metamodel.objectpascal.member.ClazzFunction;
import com.gs.gapp.metamodel.objectpascal.member.ClazzProcedure;
import com.gs.gapp.metamodel.objectpascal.member.Constructor;
import com.gs.gapp.metamodel.objectpascal.member.Destructor;
import com.gs.gapp.metamodel.objectpascal.member.Field;
import com.gs.gapp.metamodel.objectpascal.member.Function;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Procedure;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.ClassTypeI;
import com.gs.gapp.metamodel.objectpascal.type.InterfaceTypeI;


/**
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Classes_and_Objects_Index">Delphi XE7 - Classes and Objects</a>
 * @author mmt
 *
 */
public class Clazz extends StructuredType implements OuterTypeI, ClassTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8360906733453072057L;
	
	private final Set<Member> members = new LinkedHashSet<>();
	private ClassTypeI parent;
	private final Set<InterfaceTypeI> interfaces = new LinkedHashSet<>();
	private ClazzDirective directive;
	
	private final Set<Type> nestedTypes = new LinkedHashSet<>();
	
	private Boolean packed;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Clazz(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Clazz(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerType
	 */
	public Clazz(String name, OuterTypeI outerType) {
		super(name, outerType);
	}

	/**
	 * @return
	 */
	public ClassTypeI getParent() {
		return parent;
	}

	/**
	 * @param parent
	 */
	public void setParent(ClassTypeI parent) {
		this.parent = parent;
	}

	public Set<Member> getMembers() {
		return members;
	}
	
	/**
	 * @return
	 */
	@Override
	public Set<Method> getMethods() {
		Set<Method> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member instanceof Method) {
				result.add((Method) member);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<Function> getFunctions() {
		Set<Function> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member.getClass() == Function.class) {
				result.add((Function) member);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<ClazzFunction> getClassFunctions() {
		Set<ClazzFunction> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member.getClass() == ClazzFunction.class) {
				result.add((ClazzFunction) member);
			}
		}
		return result;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public Function getFunction(String name) {
		for (Function function : getFunctions()) {
			if (function.getName().equals(name)) return function;
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public Set<Procedure> getProcedures() {
		Set<Procedure> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member.getClass() == Procedure.class) {
				result.add((Procedure) member);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<ClazzProcedure> getClazzProcedures() {
		Set<ClazzProcedure> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member.getClass() == ClazzProcedure.class) {
				result.add((ClazzProcedure) member);
			}
		}
		return result;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public Procedure getProcedure(String name) {
		for (Procedure procedure : getProcedures()) {
			if (procedure.getName().equals(name)) return procedure;
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public Set<Constructor> getConstructors() {
		Set<Constructor> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member.getClass() ==  Constructor.class) {
				result.add((Constructor) member);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<ClazzConstructor> getClassConstructors() {
		Set<ClazzConstructor> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member.getClass() ==  ClazzConstructor.class) {
				result.add((ClazzConstructor) member);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<Destructor> getDestructors() {
		Set<Destructor> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member.getClass() == Destructor.class) {
				result.add((Destructor) member);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<ClazzDestructor> getClassDestructors() {
		Set<ClazzDestructor> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member.getClass() == ClazzDestructor.class) {
				result.add((ClazzDestructor) member);
			}
		}
		return result;
	}
	
	// ----------------------------------------------------
	// -------------- methods to get public members
	
	public Set<Member> getPublicMembers() {
		return getMembers(VisibilityDirective.PUBLIC);
	}
	
	public Set<Field> getPublicFields() {
	    return getMembers(VisibilityDirective.PUBLIC, Field.class); 
    }
	
	public Set<Method> getPublicMethods() {
        return getMembers(VisibilityDirective.PUBLIC, Method.class); 
    }
	
	public Set<Property> getPublicProperties() {
        return getMembers(VisibilityDirective.PUBLIC, Property.class); 
    }
	
    // ----------------------------------------------------
    // -------------- methods to get protected members
	
	public Set<Member> getProtectedMembers() {
		return getMembers(VisibilityDirective.PROTECTED);
	}
	
	public Set<Field> getProtectedFields() {
        return getMembers(VisibilityDirective.PROTECTED, Field.class);
    }
	
	public Set<Method> getProtectedMethods() {
        return getMembers(VisibilityDirective.PROTECTED, Method.class);
    }
	
	public Set<Property> getProtectedProperties() {
        return getMembers(VisibilityDirective.PROTECTED, Property.class);
    }
	
    // ----------------------------------------------------------
    // -------------- methods to get strict protected members
	
	public Set<Member> getStrictProtectedMembers() {
		return getMembers(VisibilityDirective.PROTECTED, true);
	}
	
	public Set<Field> getStrictProtectedFields() {
        return getMembers(VisibilityDirective.PROTECTED, true, Field.class);
    }
	
	public Set<Method> getStrictProtectedMethods() {
        return getMembers(VisibilityDirective.PROTECTED, true, Method.class);
    }
	
	public Set<Property> getStrictProtectedProperties() {
        return getMembers(VisibilityDirective.PROTECTED, true, Property.class);
    }
	
    // ----------------------------------------------------------
    // -------------- methods to get private members
	
	public Set<Member> getPrivateMembers() {
		return getMembers(VisibilityDirective.PRIVATE);
	}
	
	public Set<Field> getPrivateFields() {
        return getMembers(VisibilityDirective.PRIVATE, Field.class);
    }
	
	public Set<Method> getPrivateMethods() {
        return getMembers(VisibilityDirective.PRIVATE, Method.class);
    }
	
	public Set<Property> getPrivateProperties() {
        return getMembers(VisibilityDirective.PRIVATE, Property.class);
    }
	
    // ----------------------------------------------------------
    // -------------- methods to get strict private members
	
	public Set<Member> getStrictPrivateMembers() {
		return getMembers(VisibilityDirective.PRIVATE, true);
	}
	
	public Set<Field> getStrictPrivateFields() {
        return getMembers(VisibilityDirective.PRIVATE, true, Field.class);
    }
	
	public Set<Method> getStrictPrivateMethods() {
        return getMembers(VisibilityDirective.PRIVATE, true, Method.class);
    }
	
	public Set<Property> getStrictPrivateProperties() {
        return getMembers(VisibilityDirective.PRIVATE, true, Property.class);
    }
	
    // ----------------------------------------------------------
    // -------------- methods to get published members
	
	public Set<Member> getPublishedMembers() {
		return getMembers(VisibilityDirective.PUBLISHED);
	}
	
	public Set<Field> getPublishedFields() {
	    return getMembers(VisibilityDirective.PUBLISHED, Field.class);
	}
	
	public Set<Method> getPublishedMethods() {
        return getMembers(VisibilityDirective.PUBLISHED, Method.class);
    }
	
	public Set<Property> getPublishedProperties() {
        return getMembers(VisibilityDirective.PUBLISHED, Property.class);
    }
	
	/**
	 * @param visibility
	 * @return
	 */
	private Set<Member> getMembers(VisibilityDirective visibility) {
		return getMembers(visibility, false);
	}
	
	/**
	 * @param visibility
	 * @param strict
	 * @return
	 */
	private Set<Member> getMembers(VisibilityDirective visibility, boolean strict) {
	    return getMembers(visibility, strict, Member.class);  // get the members unfiltered
	}
	
	/**
	 * @param visibility
	 * @param filter
	 * @return
	 */
	private <T extends Member> Set<T> getMembers(VisibilityDirective visibility, Class<T> filter) {
        return getMembers(visibility, false, filter);
    }
	
	/**
	 * Generalized helper method that amongst others ensures that properties are at the end.
	 * 
	 * @param visibility
	 * @param strict
	 * @param filter used to let the method only return members of that type
	 * @return
	 */
	@SuppressWarnings("unchecked")
    private <T extends Member> Set<T> getMembers(VisibilityDirective visibility, boolean strict, Class<T> filter) {
	    if (filter == null) throw new ModelConverterException("parameter 'filter' must not be null");
	    
        Set<T> result = new LinkedHashSet<>();
        
        Set<Property> properties = new LinkedHashSet<>();
        Set<Field> fields = new LinkedHashSet<>();
        Set<Constructor> constructors = new LinkedHashSet<>();
        Set<Destructor> destructors = new LinkedHashSet<>();
        Set<Method> methods = new LinkedHashSet<>();
        for (Member member : members) {
            if ((visibility == null || member.getVisibility() == visibility) && (strict == member.isSctrict())) {
                
                if (filter != null && !filter.isAssignableFrom(member.getClass())) {
                    continue;
                }
                
                if (member instanceof Field) {
                    fields.add((Field) member);
                } else if (member instanceof Property) {
                    properties.add((Property) member);
                } else if (member instanceof Constructor) {
                    constructors.add((Constructor) member);
                } else if (member instanceof Destructor) {
                    destructors.add((Destructor) member);
                } else if (member instanceof Method) {
                    methods.add((Method) member);
                }
            }
        }

        // --- adding the members partitioned/ordered by the member type: FIELDS -> CONSTRUCTORs -> DESTRUCTORS -> METHODS -> PROPERTIES
        for (Field field : fields) { result.add((T) field); }
        for (Constructor constructor : constructors) { result.add((T) constructor); }
        for (Destructor destructor : destructors) { result.add((T) destructor); }
        for (Method method : methods) { result.add((T) method); }
        for (Property property : properties) { result.add((T) property); }
        
        return result;
    }

	/**
	 * @param member the member to be added
	 * @return true if the set of members did not already contain the specified element
	 * @throws NullPointerException when the passed member is null
	 * @throws IllegalArgumentException when the passed member is a destructor
	 */
	boolean addMember(Member member) {
		if (member == null) throw new NullPointerException("parameter 'member' must not be null");
		
	    return this.members.add(member);
	}

	public Set<InterfaceTypeI> getInterfaces() {
		return interfaces;
	}
	
	/**
	 * @param implementedInterface
	 * @return true if the set of interfaces did not already contain the specified element
	 */
	public boolean addInterface(InterfaceTypeI implementedInterface) {
		if (implementedInterface == null) throw new NullPointerException("parameter 'implementedInterface' must not be null");
		
		return this.interfaces.add(implementedInterface);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
		if (getDeclaringUnit() != null) {
			return getDeclaringUnit().getQualifiedName() + "." + super.getQualifiedTypeName();
		}
		
		return getName();
	}

	public ClazzDirective getDirective() {
		return directive;
	}

	public void setDirective(ClazzDirective directive) {
		this.directive = directive;
	}
	
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.OuterTypeI#getNestedTypes()
	 */
	@Override
	public Set<Type> getNestedTypes() {
		return nestedTypes;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.OuterTypeI#addNestedType(com.gs.gapp.metamodel.objectpascal.Type)
	 */
	@Override
	public boolean addNestedType(Type nestedType) {
		return this.nestedTypes.add(nestedType);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.OuterTypeI#getNestedTypes(java.lang.Class)
	 */
	@Override
	public <T extends Type> LinkedHashSet<T> getNestedTypes(Class<T> clazz) {
		return getNestedTypes(clazz, null);
	}
	
	/**
	 * @param clazz
	 * @param visibility
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T extends Type> LinkedHashSet<T> getNestedTypes(Class<T> clazz, VisibilityDirective visibility) {
        LinkedHashSet<T> result = new LinkedHashSet<>();
        
        for (Type type : nestedTypes) {
        	if (clazz.isAssignableFrom(type.getClass()) && (visibility == null || type.getVisibilityAsNestedType() == visibility)) {
        		result.add((T) type);
        	}
        }
		
		return result;
	}
	
	/**
	 * @return
	 */
	public LinkedHashSet<Type> getPublicNestedTypes() {
		return getNestedTypes(Type.class, VisibilityDirective.PUBLIC);
	}
	
	/**
	 * @return
	 */
	public LinkedHashSet<Type> getProtectedNestedTypes() {
		return getNestedTypes(Type.class, VisibilityDirective.PROTECTED);
	}
	
	/**
	 * @return
	 */
	public LinkedHashSet<Type> getPrivateNestedTypes() {
		return getNestedTypes(Type.class, VisibilityDirective.PRIVATE);
	}
	
	/**
	 * @param visibility
	 * @return
	 */
	public Set<Field> getFields(VisibilityDirective... visibility) {
		Set<Field> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member instanceof Field) {
				if (visibility != null && visibility.length > 0) {
					for (VisibilityDirective directive : visibility) {
						if (member.getVisibility() == directive) {
							result.add((Field) member);
							break;
						}
					}
				} else {
				    result.add((Field) member);
				}
			}
		}
		return result;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public Field getField(String name) {
		for (Field field: getFields(new VisibilityDirective[0])) {
			if (field.getName().equals(name)) return field;
		}
		
		return null;
	}
	
	/**
	 * @param visibility
	 * @return
	 */
	public Set<Property> getProperties(VisibilityDirective... visibility) {
		Set<Property> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member instanceof Property) {
				if (visibility != null && visibility.length > 0) {
					for (VisibilityDirective directive : visibility) {
						if (member.getVisibility() == directive) {
							result.add((Property) member);
							break;
						}
					}
				} else {
				    result.add((Property) member);
				}
			}
		}
		return result;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public Property getProperty(String name) {
		for (Property property: getProperties(new VisibilityDirective[0])) {
			if (property.getName().equals(name)) return property;
		}
		
		return null;
	}

    public Boolean getPacked() {
        return packed;
    }

    public void setPacked(Boolean packed) {
        this.packed = packed;
    }
    
    public Collection<ClassTypeI> getAllParents() {
    	LinkedHashSet<ClassTypeI> result = new LinkedHashSet<>();
    	ClassTypeI parent = this.getParent();
    	while (parent != null) {
    		result.add(parent); 
    		if (parent instanceof Clazz) {
				Clazz clazz = (Clazz) parent;
				parent = clazz.getParent();
    		} else if (parent instanceof Interface) {
				Interface anInterface = (Interface) parent;
    			InterfaceTypeI interfaceParent = anInterface.getParent();
    			if (interfaceParent instanceof ClassTypeI) {
    				parent = (ClassTypeI) interfaceParent;
    			}
    		} else {
    			parent = null;
    		}
    	}
    	
    	return result;
    }
    
    /* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType#getStructuredTypes()
	 */
	@Override
	public Set<StructuredType> getStructuredTypes() {
        final LinkedHashSet<StructuredType> result = new LinkedHashSet<>();
        if (parent != null && parent instanceof StructuredType) {
        	result.add((StructuredType) parent);
        }
        this.interfaces.stream().filter(interf -> interf instanceof StructuredType).forEach(interf -> result.add((StructuredType) interf));
        this.nestedTypes.stream().filter(nestedType -> nestedType instanceof StructuredType).forEach(nestedType -> result.add((StructuredType) nestedType));
        this.members.stream().forEach(member -> result.addAll(member.getStructuredTypes()));
        return result;
	}
}