/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.Collections;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;


/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Structured_Types#File_Types_.28Win32.29">Delphi XE7 - File Types</a>
 */
public class File extends StructuredType {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4760102256244959957L;
	
	private final Type typeStoredInFile;

	/**
	 * @param name
	 * @param typeStoredInFile
	 * @param declaringUnit
	 */
	public File(String name, Type typeStoredInFile, Unit declaringUnit) {
		super(name, declaringUnit);
		
		if (typeStoredInFile == null) throw new NullPointerException("parameter 'typeStoredInFile' must not be null");
		this.typeStoredInFile = typeStoredInFile;
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public File(String name, Type typeStoredInFile, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
		
		if (typeStoredInFile == null) throw new NullPointerException("parameter 'typeStoredInFile' must not be null");
		this.typeStoredInFile = typeStoredInFile;
	}

	/**
	 * @param name
	 * @param typeStoredInFile
	 * @param outerClazz
	 */
	public File(String name, Type typeStoredInFile, Clazz outerClazz) {
		super(name, outerClazz);
		
		if (typeStoredInFile == null) throw new NullPointerException("parameter 'typeStoredInFile' must not be null");
		this.typeStoredInFile = typeStoredInFile;
	}

	public Type getTypeStoredInFile() {
		return typeStoredInFile;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType#getStructuredTypes()
	 */
	@Override
	public Set<StructuredType> getStructuredTypes() {
		if (typeStoredInFile instanceof StructuredType) {
			return Collections.singleton((StructuredType)typeStoredInFile);
		}
		
		return Collections.emptySet();
	}
}
