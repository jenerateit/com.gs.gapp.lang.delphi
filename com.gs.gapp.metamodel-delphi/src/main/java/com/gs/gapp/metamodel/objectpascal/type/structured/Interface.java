/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.member.Function;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Procedure;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.InterfaceTypeI;

/**
 * @see <a href="docwiki.embarcadero.com/RADStudio/XE7/en/Object_Interfaces_Index">Delphi XE7 - Interfaces</a>
 * 
 * @author mmt
 *
 */
public class Interface extends StructuredType implements InterfaceTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7362375925701438899L;
	
	private InterfaceTypeI parent;
	
	private final Set<Method> methods = new LinkedHashSet<>();
	private final Set<Property> properties = new LinkedHashSet<>();
	
	private String guid;

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Interface(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Interface(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerClazz
	 */
	public Interface(String name, Clazz outerClazz) {
		super(name, outerClazz);
	}

	public InterfaceTypeI getParent() {
		return parent;
	}

	public void setParent(InterfaceTypeI parent) {
		this.parent = parent;
	}
	
	/**
	 * @return
	 */
	public Collection<InterfaceTypeI> getAllParents() {
    	LinkedHashSet<InterfaceTypeI> result = new LinkedHashSet<>();
    	InterfaceTypeI parent = this.getParent();
    	while (parent != null) {
    		result.add(parent); 
    		if (parent instanceof Interface) {
				Interface interf = (Interface) parent;
				parent = interf.getParent();
    		} else {
    			parent = null;
    		}
    	}
    	
    	return result;
    }

	public Set<Method> getMethods() {
		return methods;
	}
	
	/**
	 * @return
	 */
	public Set<Function> getFunctions() {
		Set<Function> result = new LinkedHashSet<>();
		for (Method method : methods) {
			if (method instanceof Function) {
				result.add((Function) method);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<Procedure> getProcedures() {
		Set<Procedure> result = new LinkedHashSet<>();
		for (Method method : methods) {
			if (method instanceof Procedure) {
				result.add((Procedure) method);
			}
		}
		return result;
	}
	
	/**
	 * @param method
	 * @return true if the set of methods did not already contain the specified element
	 */
	boolean addMethod(Method method) {
		if (method == null) throw new NullPointerException("parameter 'method' must not be null");
		
		return this.methods.add(method);
	}

	public Set<Property> getProperties() {
		return properties;
	}
	
	/**
	 * @param property
	 * @return true if the set of properties did not already contain the specified element
	 */
	boolean addProperty(Property property) {
		if (property == null) throw new NullPointerException("parameter 'property' must not be null");
		
		return this.properties.add(property);
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType#getStructuredTypes()
	 */
	@Override
	public Set<StructuredType> getStructuredTypes() {
        final LinkedHashSet<StructuredType> result = new LinkedHashSet<>();
        if (parent != null && parent instanceof StructuredType) {
        	result.add((StructuredType) parent);
        }
        this.methods.stream().forEach(method -> result.addAll(method.getStructuredTypes()));
        this.properties.stream().forEach(property -> result.addAll(property.getStructuredTypes()));
        return result;
	}
}
