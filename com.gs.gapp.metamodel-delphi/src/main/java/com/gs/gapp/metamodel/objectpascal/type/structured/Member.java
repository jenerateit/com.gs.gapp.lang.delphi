/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.AnnotatableI;
import com.gs.gapp.metamodel.objectpascal.AttributeUsage;
import com.gs.gapp.metamodel.objectpascal.Keyword;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalNamingRulesEnforcer;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.VisibilityDirective;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Property;


/**
 * @author mmt
 *
 */
public abstract class Member extends ObjectPascalModelElement implements AnnotatableI {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5295993139944395179L;
	
	private final Type owner;
	
	private final Unit declaringUnit;  // this is for global/local functions and procedures
	
	private VisibilityDirective visibility = VisibilityDirective.PRIVATE;
	
	private boolean sctrict = false;
	
	private final Set<AttributeUsage> attributeUsages = new LinkedHashSet<>();
	
	/**
	 * @param name
	 * @param owner
	 */
	protected Member(String name, Type owner) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		
		if (owner == null) throw new NullPointerException("parameter 'owner' must not be null");
		
		this.owner = owner;
		this.declaringUnit = null;
		
		init();
	}
	
	
	/**
	 * @param name
	 * @param declaringUnit
	 */
	protected Member(String name, Unit declaringUnit) {
		super(ObjectPascalNamingRulesEnforcer.INSTANCE.normalizeIdentifier(name));
		
		if (declaringUnit == null) throw new NullPointerException("parameter 'declaringUnit' must not be null");
		
		this.owner = null;
		this.declaringUnit = declaringUnit;
		
		init();
	}
	
	/**
	 * this method holds common initialization code that should be executed at the very end
	 * of each constructor
	 */
	private void init() {
		// --- ensure that the owner also has a reference to the member itself
		if (this.owner instanceof Clazz) {
			Clazz clazz = (Clazz) this.owner;
			clazz.addMember(this);
		} else if (owner instanceof Record) {
			Record record = (Record) this.owner;
			record.addMember(this);
		} else if (owner instanceof Interface) {
			Interface anInterface = (Interface) owner;
			// --- according to Delphi XE7 language spec only methods and properties can be part of an interface
			if (this instanceof Method) {
				anInterface.addMethod((Method) this);
			} else if (this instanceof Property) {
				anInterface.addProperty((Property) this);
			}
		}
	}

	/**
	 * @return
	 */
	public Type getOwner() {
		return owner;
	}

	public Unit getDeclaringUnit() {
		return declaringUnit;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
		if (this.owner != null) {
			return this.owner.getQualifiedName() + "." + super.getQualifiedName();
		}
        return super.getQualifiedName();
	}

	public boolean isOnlyVisibleInImplementationSection() {
		return visibility != VisibilityDirective.PUBLIC;
	}

	public VisibilityDirective getVisibility() {
		return visibility;
	}

	public void setVisibility(VisibilityDirective visibility) {
		if (visibility == null) throw new NullPointerException("a member's visibility cannot be set to null since it is mandatory");
		
		this.visibility = visibility;
	}

	public boolean isSctrict() {
		return sctrict;
	}

	public void setSctrict(boolean sctrict) {
		this.sctrict = sctrict;
	}
	
	/**
	 * @return
	 */
	public Property getRelatedProperty() {
		return this.getOriginatingElement(Property.class);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.AnnotatableI#getAttributeUsages()
	 */
	@Override
	public Set<AttributeUsage> getAttributeUsages() {
		return attributeUsages;
	}
	
	/**
	 * @param attributeUsage
	 * @return
	 */
	@Override
	public boolean addAttributeUsage(AttributeUsage attributeUsage) {
		return this.attributeUsages.add(attributeUsage);
	}
	
	/**
	 * adapts the member name in case the member name is a reserved word in Delphi
	 * 
	 * @return the member name prepended with ampersand in case the member name is a reserved word in Delphi
	 */
	public String getNonReservedWordName() {
		return Keyword.adaptIdentifierIfNeeded(getName());
	}
	
	/**
	 * @return
	 */
	public abstract java.util.Set<StructuredType> getStructuredTypes();
	
	/**
     * @return
     */
    public abstract String getDeveloperAreaKey();
}
