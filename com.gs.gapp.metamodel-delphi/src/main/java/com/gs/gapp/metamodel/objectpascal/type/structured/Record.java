/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElement;
import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.VisibilityDirective;
import com.gs.gapp.metamodel.objectpascal.member.Destructor;
import com.gs.gapp.metamodel.objectpascal.member.Field;
import com.gs.gapp.metamodel.objectpascal.member.Method;
import com.gs.gapp.metamodel.objectpascal.member.Property;
import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalType;


/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Structured_Types#Records_.28traditional.29">Delphi XE7 - Records</a>
 */
public class Record extends StructuredType implements OuterTypeI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4991375491703783091L;
	
	private final Set<Member> members = new LinkedHashSet<>();
	
	private VariantPart variantPart;
	
	private final Set<Type> nestedTypes = new LinkedHashSet<>();

	private Boolean packed;
	
	/**
	 * @param name
	 * @param declaringUnit
	 */
	public Record(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Record(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerType
	 */
	public Record(String name, OuterTypeI outerType) {
		super(name, outerType);
	}

	/**
	 * @return
	 */
	public Set<Member> getMembers() {
		LinkedHashSet<Member> result = new LinkedHashSet<>(members);
		if (variantPart != null) result.removeAll(variantPart.getFields());
		return result;
	}
	
	/**
	 * @param visibility
	 * @return
	 */
	@SuppressWarnings("unused")
	private Set<Member> getMembers(VisibilityDirective visibility) {
		Set<Member> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (visibility == null || member.getVisibility() == visibility) {
				result.add(member);
			}
		}
		
		if (variantPart != null) result.removeAll(variantPart.getFields());
		return result;
	}
	
	
	
	/**
	 * @return
	 */
	@Override
	public Set<Method> getMethods() {
		Set<Method> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member instanceof Method) {
				result.add((Method) member);
			}
		}
		return result;
	}
	
	/**
	 * @param member the member to be added, must not be a destructor
	 * @return true if the set of members did not already contain the specified element
	 * @throws NullPointerException when the passed member is null
	 * @throws IllegalArgumentException when the passed member is a destructor
	 */
	boolean addMember(Member member) {
		if (member == null) throw new NullPointerException("parameter 'member' must not be null");
		if (member instanceof Destructor) throw new IllegalArgumentException("tried to add a destructor to a record type, which is not allowed");
		
		return this.members.add(member);
	}

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.OuterTypeI#getNestedTypes()
	 */
	@Override
	public Set<Type> getNestedTypes() {
		return nestedTypes;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.OuterTypeI#addNestedType(com.gs.gapp.metamodel.objectpascal.Type)
	 */
	@Override
	public boolean addNestedType(Type nestedType) {
		return this.nestedTypes.add(nestedType);
	}
	
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.OuterTypeI#getNestedTypes(java.lang.Class)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T extends Type> LinkedHashSet<T> getNestedTypes(Class<T> clazz) {
        LinkedHashSet<T> result = new LinkedHashSet<>();
        
        for (Type type : nestedTypes) {
        	if (clazz.isAssignableFrom(type.getClass())) {
        		result.add((T) type);
        	}
        }
		
		return result;
	}
	
	public VariantPart getVariantPart() {
		return variantPart;
	}

	public void setVariantPart(VariantPart variantPart) {
		this.variantPart = variantPart;
	}
	
	/**
	 * @param visibility
	 * @return
	 */
	public Set<Field> getFields(VisibilityDirective... visibility) {
		Set<Field> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member instanceof Field) {
				if (visibility != null && visibility.length > 0) {
					for (VisibilityDirective directive : visibility) {
						if (member.getVisibility() == directive) {
							result.add((Field) member);
							break;
						}
					}
				} else {
				    result.add((Field) member);
				}
			}
		}
		return result;
	}
	
	/**
	 * @param visibility
	 * @return
	 */
	public Set<Property> getProperties(VisibilityDirective... visibility) {
		Set<Property> result = new LinkedHashSet<>();
		for (Member member : members) {
			if (member instanceof Property) {
				if (visibility != null && visibility.length > 0) {
					for (VisibilityDirective directive : visibility) {
						if (member.getVisibility() == directive) {
							result.add((Property) member);
							break;
						}
					}
				} else {
				    result.add((Property) member);
				}
			}
		}
		return result;
	}

	public Boolean getPacked() {
        return packed;
    }

    public void setPacked(Boolean packed) {
        this.packed = packed;
    }

    /**
	 * @author mmt
	 *
	 */
	public static class VariantPart extends ObjectPascalModelElement {

		/**
		 * 
		 */
		private static final long serialVersionUID = 7747368812338213424L;
		
		private final Field field;
		
		private final Record owner;
		
		private final Map<String, LinkedHashSet<Field>> fieldMap = new LinkedHashMap<>();

		/**
		 * @param field
		 */
		public VariantPart(Field field) {
			super(field != null ? field.getName() : "not-provided");
			
			if (field == null) throw new NullPointerException("parameter 'field' must not be null"); 
				
			if (!(field.getType() instanceof OrdinalType)) {
				throw new IllegalArgumentException("A record's variant part's case clause must use an ordinal type, but given is the type '" + field.getType().getName() + "'");
			}
			
			if (!(field.getOwner() instanceof Record)) {
				throw new IllegalArgumentException("A record's variant part's field must be owned by a record");
			}
			
			this.field = field;
			this.owner = (Record) field.getOwner();
		}

		public Field getField() {
			return field;
		}

		public Map<String, LinkedHashSet<Field>> getFieldMap() {
			return fieldMap;
		}
		
		/**
		 * @param constantList
		 * @param field
		 * @return
		 */
		public boolean addField(String constantList, Field field) {
			if (field.getOwner() != owner) {
				throw new IllegalArgumentException("Any record's variant part's field must be owned by the same record");
			}
			
			LinkedHashSet<Field> setOfFields = this.fieldMap.get(constantList);
			if (setOfFields == null) {
				setOfFields = new LinkedHashSet<>();
				this.fieldMap.put(constantList, setOfFields);
			}
			
			return setOfFields.add(field);
		}

		public Record getOwner() {
			return owner;
		}
		
		/**
		 * @return
		 */
		public Collection<Field> getFields() {
			LinkedHashSet<Field> fields = new LinkedHashSet<>();
			
			fields.add(this.field);
			for (LinkedHashSet<Field> mappedFields : fieldMap.values()) {
				fields.addAll(mappedFields);
			}
			
			return fields;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType#getStructuredTypes()
	 */
	@Override
	public Set<StructuredType> getStructuredTypes() {
        final LinkedHashSet<StructuredType> result = new LinkedHashSet<>();
        this.members.stream().forEach(member -> result.addAll(member.getStructuredTypes()));
        this.nestedTypes.stream().filter(nestedType -> nestedType instanceof StructuredType).forEach(nestedType -> result.add((StructuredType) nestedType));
        return result;
	}
}
