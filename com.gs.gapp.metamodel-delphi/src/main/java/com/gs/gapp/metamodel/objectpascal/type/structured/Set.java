/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.Collections;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.simple.OrdinalType;


/**
 * @author mmt
 * @see <a href="http://docwiki.embarcadero.com/RADStudio/XE7/en/Structured_Types#Sets">Delphi XE7 - Sets</a>
 */
public class Set extends StructuredType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5242999867788682357L;
	
	private final OrdinalType baseType;
	

	/**
	 * @param name
	 * @param baseType
	 * @param declaringUnit
	 */
	public Set(String name, OrdinalType baseType, Unit declaringUnit) {
		super(name, declaringUnit);
		
		// the following check has been relaxed and moved to the SetWriter in order to make predefined types easier to handle (see for instance TShiftState) (mmt 2015-Nov-23)
//		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null"); 
		this.baseType = baseType;
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public Set(String name, OrdinalType baseType, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);

		// the following check has been relaxed and moved to the SetWriter in order to make predefined types easier to handle (see for instance TShiftState) (mmt 2015-Nov-23)
//		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null"); 
		this.baseType = baseType;
	}

	/**
	 * @param name
	 * @param baseType
	 * @param outerType
	 */
	public Set(String name, OrdinalType baseType, OuterTypeI outerType) {
		super(name, outerType);

		// the following check has been relaxed and moved to the SetWriter in order to make predefined types easier to handle (see for instance TShiftState) (mmt 2015-Nov-23)
//		if (baseType == null) throw new NullPointerException("parameter 'baseType' must not be null"); 
		this.baseType = baseType;
	}

	/**
	 * @return
	 */
	public OrdinalType getBaseType() {
		return baseType;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.objectpascal.type.structured.StructuredType#getStructuredTypes()
	 */
	@Override
	public java.util.Set<StructuredType> getStructuredTypes() {
        return Collections.emptySet();
	}
}
