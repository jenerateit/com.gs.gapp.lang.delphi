/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.objectpascal.Keyword;
import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Type;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 *
 */
public abstract class StructuredType extends Type {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5170286482139046519L;
	
	

	/**
	 * @param name
	 * @param declaringUnit
	 */
	public StructuredType(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}
	
	/**
	 * @param name
	 * @param declaringUnit
	 * @param onlyVisibleInImplementationSection
	 */
	public StructuredType(String name, Unit declaringUnit, boolean onlyVisibleInImplementationSection) {
		super(name, declaringUnit, onlyVisibleInImplementationSection);
	}

	/**
	 * @param name
	 * @param outerType
	 */
	public StructuredType(String name, OuterTypeI outerType) {
		super(name, outerType);
	}
	
	/**
	 * @return
	 */
	public String getNonReservedWordName() {
		return Keyword.adaptIdentifierIfNeeded(getName());
	}
	
	/**
	 * Collects and returns all structured types that are used
	 * by this structured type.
	 * 
	 * @return
	 */
	public final java.util.Set<StructuredType> getUsedStructuredTypes() {
		return collectUsedStructuredTypes(this, new LinkedHashSet<>());
	}
	
	/**
	 * @return
	 */
	public abstract Set<StructuredType> getStructuredTypes();
	
	/**
	 * @param structuredTypes
	 * @return
	 */
	final java.util.Set<StructuredType> collectUsedStructuredTypes(StructuredType rootStructuredType, Set<StructuredType> structuredTypes) {
		if (!structuredTypes.contains(this)) {
			if (this != rootStructuredType) {
				structuredTypes.add(this);
			}
			Set<StructuredType> thisTypesStructuredTypes = getStructuredTypes();
			if (thisTypesStructuredTypes != null) {
				thisTypesStructuredTypes.stream().filter(structuredType -> structuredType != this)
				    .forEach(structuredType -> structuredType.collectUsedStructuredTypes(rootStructuredType, structuredTypes));
			}
		}
		
		return structuredTypes;
	}
}
