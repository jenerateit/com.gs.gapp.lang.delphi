/**
 * 
 */
package com.gs.gapp.metamodel.objectpascal.type.structured;

import com.gs.gapp.metamodel.objectpascal.OuterTypeI;
import com.gs.gapp.metamodel.objectpascal.Unit;

/**
 * @author mmt
 *
 */
public class TechnicalExceptionClass extends Clazz {

	private static final long serialVersionUID = 7509766910114586470L;

	public TechnicalExceptionClass(String name, Unit declaringUnit) {
		super(name, declaringUnit);
	}

	public TechnicalExceptionClass(String name, OuterTypeI outerType) {
		super(name, outerType);
	}
}
