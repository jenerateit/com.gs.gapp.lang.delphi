package com.gs.vd.converter.groovy.delphi;


import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.objectpascal.ObjectPascalModelElementCache;
import com.gs.vd.converter.groovy.any.GroovyToAnyConverter;

public class GroovyToDelphiConverter extends GroovyToAnyConverter {

	/**
	 * @param modelElementCache
	 */
	public GroovyToDelphiConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}
	
	/**
	 * 
	 */
	public GroovyToDelphiConverter() {
		super(new ObjectPascalModelElementCache());
	}
}
