/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.vd.converter.groovy.delphi;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;
import org.osgi.service.component.ComponentContext;

/**
 * @author hrr
 *
 */
public class GroovyToDelphiConverterProvider implements ModelConverterProviderI {

	private ComponentContext context;
	
	/**
	 * 
	 */
	public GroovyToDelphiConverterProvider() {
		super();
	}
	
	@Override
	public ModelConverterI getModelConverter() {
		return new GroovyToDelphiConverter();
	}
	
	public void activate(final ComponentContext context) {
		this.context = context;
	}
	
	public void deactivate() {
		this.context = null;
	}

	public ComponentContext getContext() {
		return context;
	}

	public void setContext(ComponentContext context) {
		this.context = context;
	}
}
