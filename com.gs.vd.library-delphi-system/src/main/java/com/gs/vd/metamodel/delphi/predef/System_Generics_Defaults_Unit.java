package com.gs.vd.metamodel.delphi.predef;

public class System_Generics_Defaults_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("Defaults", com.gs.vd.metamodel.delphi.predef.system.generics.defaults_Namespace.NAMESPACE);
        return result;
    }

}
