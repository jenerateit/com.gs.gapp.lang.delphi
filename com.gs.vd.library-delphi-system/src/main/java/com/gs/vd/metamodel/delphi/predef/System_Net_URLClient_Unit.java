package com.gs.vd.metamodel.delphi.predef;

import com.gs.vd.metamodel.delphi.predef.system.net_Namespace;

public class System_Net_URLClient_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("URLClient", net_Namespace.NAMESPACE);
        return result;
    }

}
