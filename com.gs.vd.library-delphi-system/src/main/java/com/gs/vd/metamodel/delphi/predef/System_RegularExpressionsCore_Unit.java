package com.gs.vd.metamodel.delphi.predef;

public class System_RegularExpressionsCore_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("RegularExpressionsCore", com.gs.vd.metamodel.delphi.predef.system.regularexpressionscore_Namespace.NAMESPACE);
        return result;
    }

}
