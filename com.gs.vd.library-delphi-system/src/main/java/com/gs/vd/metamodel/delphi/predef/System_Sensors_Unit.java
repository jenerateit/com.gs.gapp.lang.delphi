package com.gs.vd.metamodel.delphi.predef;

public class System_Sensors_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("Sensors", com.gs.vd.metamodel.delphi.predef.system.sensors_Namespace.NAMESPACE);
        return result;
    }

}
