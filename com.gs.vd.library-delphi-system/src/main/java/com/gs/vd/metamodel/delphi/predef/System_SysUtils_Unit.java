package com.gs.vd.metamodel.delphi.predef;

public class System_SysUtils_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("SysUtils", com.gs.vd.metamodel.delphi.predef.system.sysutils_Namespace.NAMESPACE);
        return result;
    }

}
