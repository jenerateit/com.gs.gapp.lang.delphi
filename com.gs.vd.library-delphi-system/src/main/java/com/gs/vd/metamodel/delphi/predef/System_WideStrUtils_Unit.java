package com.gs.vd.metamodel.delphi.predef;

public class System_WideStrUtils_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("WideStrUtils", com.gs.vd.metamodel.delphi.predef.system.widestrutils_Namespace.NAMESPACE);
        return result;
    }

}
