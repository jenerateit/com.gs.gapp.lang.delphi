package com.gs.vd.metamodel.delphi.predef;

public class System_Win_Bluetooth_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("Bluetooth", com.gs.vd.metamodel.delphi.predef.system.win.bluetooth_Namespace.NAMESPACE);
        return result;
    }

}
