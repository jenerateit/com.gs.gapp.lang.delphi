package com.gs.vd.metamodel.delphi.predef;

public class Winapi_D2D1_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("D2D1", com.gs.vd.metamodel.delphi.predef.winapi.d2d1_Namespace.NAMESPACE);
        return result;
    }

}
