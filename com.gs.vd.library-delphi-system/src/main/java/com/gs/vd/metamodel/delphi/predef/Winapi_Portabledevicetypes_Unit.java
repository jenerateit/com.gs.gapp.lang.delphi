package com.gs.vd.metamodel.delphi.predef;

public class Winapi_Portabledevicetypes_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("Portabledevicetypes", com.gs.vd.metamodel.delphi.predef.winapi.portabledevicetypes_Namespace.NAMESPACE);
        return result;
    }

}
