package com.gs.vd.metamodel.delphi.predef;

public class Winapi_Winsock2_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("Winsock2", com.gs.vd.metamodel.delphi.predef.winapi.winsock2_Namespace.NAMESPACE);
        return result;
    }

}
