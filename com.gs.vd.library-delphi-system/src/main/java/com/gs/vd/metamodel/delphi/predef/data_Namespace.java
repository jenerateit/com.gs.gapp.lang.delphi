package com.gs.vd.metamodel.delphi.predef;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class data_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("Data");
        return result;
    }

}
