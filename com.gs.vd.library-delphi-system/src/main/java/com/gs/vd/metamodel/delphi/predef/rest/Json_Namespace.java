package com.gs.vd.metamodel.delphi.predef.rest;

import com.gs.gapp.metamodel.objectpascal.Namespace;
import com.gs.vd.metamodel.delphi.predef.Rest_Namespace;

public class Json_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("Json", Rest_Namespace.NAMESPACE);
        return result;
    }
}
