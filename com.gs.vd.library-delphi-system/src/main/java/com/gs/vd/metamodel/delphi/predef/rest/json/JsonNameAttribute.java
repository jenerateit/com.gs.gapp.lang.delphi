package com.gs.vd.metamodel.delphi.predef.rest.json;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.Attribute;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.vd.metamodel.delphi.predef.system.TCustomAttribute;

public class JsonNameAttribute {

    public static final Attribute TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return Rest_Json_Types_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Attribute getType() {
    	Attribute result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Attribute("JSONNameAttribute", getUnit());
            }
        }
        result.setParent(TCustomAttribute.TYPE);
        return result;
    }
}
