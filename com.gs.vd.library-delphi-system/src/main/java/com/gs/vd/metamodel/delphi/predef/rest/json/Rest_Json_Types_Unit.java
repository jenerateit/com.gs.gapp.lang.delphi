package com.gs.vd.metamodel.delphi.predef.rest.json;

public class Rest_Json_Types_Unit {

    public static final com.gs.gapp.metamodel.objectpascal.Unit UNIT;

    static {
        UNIT = getUnit();
        UNIT.setGenerated(false);  // predefined elements must never be generated
    }

    private static final com.gs.gapp.metamodel.objectpascal.Unit getUnit() {
        com.gs.gapp.metamodel.objectpascal.Unit result = new com.gs.gapp.metamodel.objectpascal.Unit("Types", com.gs.vd.metamodel.delphi.predef.rest.Json_Namespace.NAMESPACE);
        return result;
    }
}
