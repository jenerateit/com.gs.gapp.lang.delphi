package com.gs.vd.metamodel.delphi.predef.system;

import com.gs.gapp.metamodel.objectpascal.Unit;

public class Double {

    public static final com.gs.gapp.metamodel.objectpascal.type.simple.Real TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_Unit.UNIT;
    }


    private static final com.gs.gapp.metamodel.objectpascal.type.simple.Real getType() {
    	com.gs.gapp.metamodel.objectpascal.type.simple.Real result = null;
        result = new com.gs.gapp.metamodel.objectpascal.type.simple.Real("Double", getUnit());

        return result;
    }

}
