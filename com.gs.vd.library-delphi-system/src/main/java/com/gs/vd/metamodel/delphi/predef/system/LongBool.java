package com.gs.vd.metamodel.delphi.predef.system;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class LongBool {

    public static final com.gs.gapp.metamodel.objectpascal.type.simple.Boolean TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final com.gs.gapp.metamodel.objectpascal.type.simple.Boolean getType() {
        com.gs.gapp.metamodel.objectpascal.type.simple.Boolean result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new com.gs.gapp.metamodel.objectpascal.type.simple.Boolean("LongBool", getUnit());
            }
        }
        return result;
    }

}
