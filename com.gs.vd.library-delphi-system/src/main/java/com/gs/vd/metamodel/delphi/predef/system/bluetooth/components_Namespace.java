package com.gs.vd.metamodel.delphi.predef.system.bluetooth;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class components_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("Components", com.gs.vd.metamodel.delphi.predef.system.bluetooth_Namespace.NAMESPACE);
        return result;
    }

}
