package com.gs.vd.metamodel.delphi.predef.system.generics.collections;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.TypeParameter;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class TList {

    public static final Clazz TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_Generics_Collections_Unit.UNIT;
    }

    private static final Clazz getType() {
    	Clazz result = null;

        result = new Clazz("TList", getUnit());  // in reality generics TList inherits from TEnumerable<T>
        TypeParameter typeParameter = new TypeParameter("T", null, com.gs.vd.metamodel.delphi.predef.system.TObject.TYPE, false, false, false);
        result.addTypeParameter(typeParameter);
        result.setParent(com.gs.vd.metamodel.delphi.predef.system.TObject.TYPE);
        return result;
    }

}
