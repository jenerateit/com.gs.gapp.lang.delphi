package com.gs.vd.metamodel.delphi.predef.system.generics;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class collections_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("Collections", com.gs.vd.metamodel.delphi.predef.system.generics_Namespace.NAMESPACE);
        return result;
    }

}
