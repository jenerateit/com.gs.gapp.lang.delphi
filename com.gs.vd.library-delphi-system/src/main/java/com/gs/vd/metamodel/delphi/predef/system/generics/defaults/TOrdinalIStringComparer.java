package com.gs.vd.metamodel.delphi.predef.system.generics.defaults;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class TOrdinalIStringComparer {

    public static final Clazz TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_Generics_Defaults_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Clazz getType() {
        Clazz result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Clazz("TOrdinalIStringComparer", getUnit());
            }
        }
        result.setParent(com.gs.vd.metamodel.delphi.predef.system.generics.defaults.TStringComparer.TYPE);
        result.addInterface(com.gs.vd.metamodel.delphi.predef.system.IInterface.TYPE);
        return result;
    }

}
