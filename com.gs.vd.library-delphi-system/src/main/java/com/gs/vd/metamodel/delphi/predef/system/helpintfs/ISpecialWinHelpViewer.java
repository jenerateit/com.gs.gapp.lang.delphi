package com.gs.vd.metamodel.delphi.predef.system.helpintfs;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;

public class ISpecialWinHelpViewer {

    public static final Interface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_HelpIntfs_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Interface getType() {
        Interface result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Interface("ISpecialWinHelpViewer", getUnit());
            }
        }
        result.setParent(com.gs.vd.metamodel.delphi.predef.system.helpintfs.IExtendedHelpViewer.TYPE);
        return result;
    }

}
