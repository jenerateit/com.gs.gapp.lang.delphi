package com.gs.vd.metamodel.delphi.predef.system.internal;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class debugutils_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("DebugUtils", com.gs.vd.metamodel.delphi.predef.system.internal_Namespace.NAMESPACE);
        return result;
    }

}
