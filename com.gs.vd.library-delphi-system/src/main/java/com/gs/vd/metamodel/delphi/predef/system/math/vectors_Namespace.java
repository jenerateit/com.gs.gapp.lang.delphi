package com.gs.vd.metamodel.delphi.predef.system.math;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class vectors_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("Vectors", com.gs.vd.metamodel.delphi.predef.system.math_Namespace.NAMESPACE);
        return result;
    }

}
