package com.gs.vd.metamodel.delphi.predef.system.messaging;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;

public class TMessageListener {

    public static final Interface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_Messaging_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Interface getType() {
        Interface result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Interface("TMessageListener", getUnit());
            }
        }
        result.setParent(com.gs.vd.metamodel.delphi.predef.system.IInterface.TYPE);
        return result;
    }

}
