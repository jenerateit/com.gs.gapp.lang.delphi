package com.gs.vd.metamodel.delphi.predef.system;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class regularexpressionscore_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("RegularExpressionsCore", com.gs.vd.metamodel.delphi.predef.system_Namespace.NAMESPACE);
        return result;
    }

}
