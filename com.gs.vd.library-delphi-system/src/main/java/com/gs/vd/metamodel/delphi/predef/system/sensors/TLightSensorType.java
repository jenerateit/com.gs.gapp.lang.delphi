package com.gs.vd.metamodel.delphi.predef.system.sensors;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class TLightSensorType {

    public static final com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_Sensors_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration getType() {
        com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new com.gs.gapp.metamodel.objectpascal.type.simple.Enumeration("TLightSensorType", getUnit());
            }
        }
        return result;
    }

}
