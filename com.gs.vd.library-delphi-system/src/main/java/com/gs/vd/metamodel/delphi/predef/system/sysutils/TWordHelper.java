package com.gs.vd.metamodel.delphi.predef.system.sysutils;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class TWordHelper {

    public static final Clazz TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_SysUtils_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Clazz getType() {
        Clazz result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Clazz("TWordHelper", getUnit());
            }
        }
        result.setParent(com.gs.vd.metamodel.delphi.predef.system.TClassHelperBase.TYPE);
        result.addInterface(com.gs.vd.metamodel.delphi.predef.system.IInterface.TYPE);
        result.addInterface(com.gs.vd.metamodel.delphi.predef.system.IInterface.TYPE);
        return result;
    }

}
