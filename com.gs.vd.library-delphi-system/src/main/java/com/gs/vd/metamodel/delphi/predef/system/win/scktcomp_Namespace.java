package com.gs.vd.metamodel.delphi.predef.system.win;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class scktcomp_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("ScktComp", com.gs.vd.metamodel.delphi.predef.system.win_Namespace.NAMESPACE);
        return result;
    }

}
