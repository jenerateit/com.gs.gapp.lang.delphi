package com.gs.vd.metamodel.delphi.predef.system.win;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class vclcom_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("VCLCom", com.gs.vd.metamodel.delphi.predef.system.win_Namespace.NAMESPACE);
        return result;
    }

}
