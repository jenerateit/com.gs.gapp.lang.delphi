package com.gs.vd.metamodel.delphi.predef.system.zlib;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class Pinternal_state {

    public static final com.gs.gapp.metamodel.objectpascal.type.Pointer TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.System_ZLib_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final com.gs.gapp.metamodel.objectpascal.type.Pointer getType() {
        com.gs.gapp.metamodel.objectpascal.type.Pointer result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new com.gs.gapp.metamodel.objectpascal.type.Pointer("Pinternal_state", null, getUnit());
            }
        }
        return result;
    }

}
