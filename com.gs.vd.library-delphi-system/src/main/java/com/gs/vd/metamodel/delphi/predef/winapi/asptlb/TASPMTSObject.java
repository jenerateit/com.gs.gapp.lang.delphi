package com.gs.vd.metamodel.delphi.predef.winapi.asptlb;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class TASPMTSObject {

    public static final Clazz TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.Winapi_AspTlb_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Clazz getType() {
        Clazz result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Clazz("TASPMTSObject", getUnit());
            }
        }
        result.setParent(com.gs.vd.metamodel.delphi.predef.system.win.comobj.TAutoObject.TYPE);
        result.addInterface(com.gs.vd.metamodel.delphi.predef.system.IDispatch.TYPE);
        result.addInterface(com.gs.vd.metamodel.delphi.predef.winapi.activex.IProvideClassInfo.TYPE);
        result.addInterface(com.gs.vd.metamodel.delphi.predef.winapi.activex.ISupportErrorInfo.TYPE);
        result.addInterface(com.gs.vd.metamodel.delphi.predef.system.IInterface.TYPE);
        return result;
    }

}
