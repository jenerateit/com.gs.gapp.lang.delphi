package com.gs.vd.metamodel.delphi.predef.winapi;

import com.gs.gapp.metamodel.objectpascal.Namespace;

public class comadmin_Namespace {

    public static final Namespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
        NAMESPACE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Namespace getNamespace() {
        Namespace result = new Namespace("COMAdmin", com.gs.vd.metamodel.delphi.predef.winapi_Namespace.NAMESPACE);
        return result;
    }

}
