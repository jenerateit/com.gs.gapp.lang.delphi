package com.gs.vd.metamodel.delphi.predef.winapi.d2d1;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class D3D10_FEATURE_LEVEL1 {

    public static final com.gs.gapp.metamodel.objectpascal.type.simple.Integer TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.Winapi_D2D1_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final com.gs.gapp.metamodel.objectpascal.type.simple.Integer getType() {
        com.gs.gapp.metamodel.objectpascal.type.simple.Integer result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new com.gs.gapp.metamodel.objectpascal.type.simple.Integer("D3D10_FEATURE_LEVEL1", getUnit());
            }
        }
        return result;
    }

}
