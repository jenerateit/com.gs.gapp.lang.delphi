package com.gs.vd.metamodel.delphi.predef.winapi.d2d1;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;
import com.gs.gapp.metamodel.objectpascal.type.structured.Interface;

public class ID2D1EllipseGeometry {

    public static final Interface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.Winapi_D2D1_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Interface getType() {
        Interface result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Interface("ID2D1EllipseGeometry", getUnit());
            }
        }
        result.setParent(com.gs.vd.metamodel.delphi.predef.winapi.d2d1.ID2D1Geometry.TYPE);
        return result;
    }

}
