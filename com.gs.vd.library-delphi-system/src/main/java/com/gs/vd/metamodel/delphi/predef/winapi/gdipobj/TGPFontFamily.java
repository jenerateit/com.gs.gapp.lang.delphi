package com.gs.vd.metamodel.delphi.predef.winapi.gdipobj;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class TGPFontFamily {

    public static final Clazz TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.Winapi_GDIPOBJ_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Clazz getType() {
        Clazz result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Clazz("TGPFontFamily", getUnit());
            }
        }
        result.setParent(com.gs.vd.metamodel.delphi.predef.winapi.gdipapi.TGdiplusBase.TYPE);
        return result;
    }

}
