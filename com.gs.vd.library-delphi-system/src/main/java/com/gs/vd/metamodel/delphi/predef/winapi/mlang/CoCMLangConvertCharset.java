package com.gs.vd.metamodel.delphi.predef.winapi.mlang;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class CoCMLangConvertCharset {

    public static final Clazz TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.Winapi_MLang_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Clazz getType() {
        Clazz result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Clazz("CoCMLangConvertCharset", getUnit());
            }
        }
        result.setParent(com.gs.vd.metamodel.delphi.predef.system.TObject.TYPE);
        return result;
    }

}
