package com.gs.vd.metamodel.delphi.predef.winapi.oledb;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.type.structured.Array;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class TSecObjectElementArray {

    public static final Array TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.Winapi_OleDB_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Array getType() {
        Array result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Array("TSecObjectElementArray", null, getUnit());
            }
        }
        return result;
    }

}
