package com.gs.vd.metamodel.delphi.predef.winapi.openglext;

import com.gs.gapp.metamodel.objectpascal.Unit;
import com.gs.gapp.metamodel.objectpascal.member.Procedure;
import com.gs.gapp.metamodel.objectpascal.type.structured.Clazz;

public class GLDEBUGPROCARB {

    public static final Procedure TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }

    private static final Unit getUnit() {
        return com.gs.vd.metamodel.delphi.predef.Winapi_OpenGLext_Unit.UNIT;
    }

    private static final Clazz getOuterClazz() {
        // TODO implement code generation to get real code here
        return null;
    }

    private static final Procedure getType() {
        Procedure result = null;
        Clazz outerClazz = getOuterClazz();
        if (outerClazz != null) {
            // TODO implement code generation to get real code here (Outer Class)
        } else {
            if (getUnit() != null) {
                result = new Procedure("GLDEBUGPROCARB", getUnit());
            }
        }
        return result;
    }

}
